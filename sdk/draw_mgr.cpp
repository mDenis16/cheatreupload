@@ - 0, 0 + 1, 284 @@
#include "draw_mgr.h"
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
#include "zgui.hh"
std::unique_ptr< c_renderer > renderer = std::make_unique< c_renderer >( );

void c_renderer::initialize( IDirect3DDevice9* device ) {
	this->device = device;

	D3DXCreateFont( this->device, 24, 0, FW_NORMAL, 1, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH, "Verdana", &this->fonts.normal_selected );
	D3DXCreateFont( this->device, 20, 0, FW_NORMAL, 1, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH, "Unispace", &this->fonts.normal );
	D3DXCreateFont( this->device, 12, 0, FW_REGULAR, 1, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH, "Choktoff", &this->fonts.esp );
	D3DXCreateFont( this->device, 12, 0, FW_REGULAR, 1, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH, "Choktoff", &this->fonts.esp );
	D3DXCreateFont( this->device, 18, 0, FW_REGULAR, 1, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH, "Verdana", &this->fonts.tabs );
	D3DXCreateFont( this->device, 24, 0, FW_REGULAR, 1, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH, "Verdana", &this->fonts.tabs_out );
	D3DXCreateFont( this->device, 25, 0, FW_REGULAR, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH, "Counter-Strike", &this->fonts.weapon_icon );
}

void c_renderer::reset( void ) {
	D3DVIEWPORT9 scrn;
	this->device->GetViewport( &scrn );

	this->device->SetVertexShader( nullptr );
	this->device->SetPixelShader( nullptr );
	this->device->SetFVF( D3DFVF_XYZRHW | D3DFVF_DIFFUSE );
	this->device->SetRenderState( D3DRS_LIGHTING, false );
	this->device->SetRenderState( D3DRS_FOGENABLE, false );
	this->device->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
	this->device->SetRenderState( D3DRS_FILLMODE, D3DFILL_SOLID );

	this->device->SetRenderState( D3DRS_ZENABLE, D3DZB_FALSE );
	this->device->SetRenderState( D3DRS_SCISSORTESTENABLE, true );
	this->device->SetRenderState( D3DRS_ZWRITEENABLE, false );
	this->device->SetRenderState( D3DRS_STENCILENABLE, false );

	this->device->SetRenderState( D3DRS_MULTISAMPLEANTIALIAS, false );
	this->device->SetRenderState( D3DRS_ANTIALIASEDLINEENABLE, false );

	this->device->SetRenderState( D3DRS_ALPHABLENDENABLE, true );
	this->device->SetRenderState( D3DRS_ALPHATESTENABLE, false );
	this->device->SetRenderState( D3DRS_SEPARATEALPHABLENDENABLE, true );
	this->device->SetRenderState( D3DRS_SRCBLEND, D3DBLEND_SRCALPHA );
	this->device->SetRenderState( D3DRS_SRCBLENDALPHA, D3DBLEND_INVDESTALPHA );
	this->device->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
	this->device->SetRenderState( D3DRS_DESTBLENDALPHA, D3DBLEND_ONE );

	this->device->SetRenderState( D3DRS_SRGBWRITEENABLE, false );
	this->device->SetRenderState( D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA );
}

int c_renderer::string_width( ID3DXFont* font, const char* string ) {
	RECT rect = RECT( );
	font->DrawTextA( nullptr, string, strlen( string ), &rect, DT_CALCRECT, D3DCOLOR_RGBA( 0, 0, 0, 0 ) );
	return rect.right - rect.left;
}
float c_renderer::get_frametime( ) {

	return 60.f;
}
int c_renderer::string_height( ID3DXFont* font, const char* string ) {
	RECT rect = RECT( );
	font->DrawTextA( nullptr, string, strlen( string ), &rect, DT_CALCRECT, D3DCOLOR_RGBA( 0, 0, 0, 0 ) );
	return rect.bottom - rect.top;
}
RECT c_renderer::string_rect( ID3DXFont* font, const char* string ) {
	RECT rect = RECT( );
	font->DrawTextA( nullptr, string, strlen( string ), &rect, DT_CALCRECT, D3DCOLOR_RGBA( 0, 0, 0, 0 ) );
	return rect;
}
void c_renderer::line( float x, float y, float x2, float y2, color _color ) {
	vertex vtx [ 2 ] = { { x, y, 0.0f, 1.0f, _color.to_d3d( ) }, { x2, y2, 0.0f, 1.0f, _color.to_d3d( ) } };

	this->device->SetTexture( 0, nullptr );
	this->device->DrawPrimitiveUP( D3DPT_LINELIST, 1, &vtx, sizeof( vertex ) );
}

void c_renderer::filled_box_zgui( float x, float y, float width, float height, color  _color ) {
	color c( _color.r, _color.g, _color.b, _color.a );
	vertex pVertex [ 4 ] = { { x, y + height, 0.0f, 1.0f, c.to_d3d( ) }, { x, y, 0.0f, 1.0f, c.to_d3d( ) }, { x + width, y + height, 0.0f, 1.0f, c.to_d3d( ) }, { x + width, y, 0.0f, 1.0f, c.to_d3d( ) } };
	this->device->SetTexture( 0, nullptr );
	this->device->SetRenderState( D3DRS_ANTIALIASEDLINEENABLE, false );
	this->device->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, pVertex, sizeof( vertex ) );
	this->device->SetRenderState( D3DRS_ANTIALIASEDLINEENABLE, true );
}
void c_renderer::filled_box_outlined_zgui( float x, float y, float width, float height, color  _color, color  outline_color, float thickness ) {
	this->filled_box_zgui( x, y, width, height, _color );
	this->bordered_box_zgui( x, y, width, height, outline_color, 0.f );
}

void c_renderer::filled_box( float x, float y, float width, float height, color _color )
{
	color c = _color;
	vertex pVertex [ 4 ] = { { x, y + height, 0.0f, 1.0f, c.to_d3d( ) }, { x, y, 0.0f, 1.0f, c.to_d3d( ) }, { x + width, y + height, 0.0f, 1.0f, c.to_d3d( ) }, { x + width, y, 0.0f, 1.0f, c.to_d3d( ) } };
	this->device->SetTexture( 0, nullptr );
	this->device->SetRenderState( D3DRS_ANTIALIASEDLINEENABLE, false );
	this->device->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, pVertex, sizeof( vertex ) );
	this->device->SetRenderState( D3DRS_ANTIALIASEDLINEENABLE, true );
}

void c_renderer::filled_box_outlined( float x, float y, float width, float height, color _color, color outline_color, float thickness ) {
	this->filled_box( x, y, width, height, _color );
	this->bordered_box( x, y, width, height, outline_color );
}
void c_renderer::bordered_box_zgui( float x, float y, float width, float height, color  _color, float thickness ) {
	this->filled_box_zgui( x, y, width, thickness, _color );
	this->filled_box_zgui( x, y, thickness, height, _color );
	this->filled_box_zgui( x + width - thickness, y, thickness, height, _color );
	this->filled_box_zgui( x, y + height - thickness, width, thickness, _color );
}
void c_renderer::bordered_box( float x, float y, float width, float height, color _color, float thickness ) {
	this->filled_box( x, y, width, thickness, _color );
	this->filled_box( x, y, thickness, height, _color );
	this->filled_box( x + width - thickness, y, thickness, height, _color );
	this->filled_box( x, y + height - thickness, width, thickness, _color );
}

void c_renderer::bordered_box_outlined( float x, float y, float width, float height, color _color, color outline_color, float thickness ) {
	this->bordered_box( x, y, width, height, outline_color, thickness );
	this->bordered_box( x + thickness, y + thickness, width - ( thickness * 2 ), height - ( thickness * 2 ), _color, thickness );
	this->bordered_box( x + ( thickness * 2 ), y + ( thickness * 2 ), width - ( thickness * 4 ), height - ( thickness * 4 ), outline_color, thickness );
}

void c_renderer::gradient_box( float x, float y, float width, float height, color _color, color _color2, bool vertical ) {
	vertex vtx [ 4 ] = { { x, y, 0.0f, 1.0f, _color.to_d3d( ) }, { x + width, y, 0.0f, 1.0f, vertical?_color.to_d3d( ):_color2.to_d3d( ) }, { x, y + height, 0.0f, 1.0f, vertical?_color2.to_d3d( ):_color.to_d3d( ) }, { x + width, y + height, 0.0f, 1.0f, _color2.to_d3d( ) } };
	this->device->SetTexture( 0, nullptr );
	this->device->SetRenderState( D3DRS_ANTIALIASEDLINEENABLE, false );
	this->device->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, vtx, sizeof( vertex ) );
	this->device->SetRenderState( D3DRS_ANTIALIASEDLINEENABLE, true );
}

void c_renderer::gradient_box_zgui( float x, float y, float width, float height, color  _color, color  _color2, bool vertical ) {
	color c( _color.r, _color.g, _color.b, _color.a );
	color c1( _color.r, _color.g, _color.b, _color.a );
	vertex vtx [ 4 ] = { { x, y, 0.0f, 1.0f,  c.to_d3d( ) }, { x + width, y, 0.0f, 1.0f, vertical?c.to_d3d( ):c1.to_d3d( ) }, { x, y + height, 0.0f, 1.0f, vertical?c1.to_d3d( ):c.to_d3d( ) }, { x + width, y + height, 0.0f, 1.0f, c1.to_d3d( ) } };
	this->device->SetTexture( 0, nullptr );
	this->device->SetRenderState( D3DRS_ANTIALIASEDLINEENABLE, false );
	this->device->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, vtx, sizeof( vertex ) );
	this->device->SetRenderState( D3DRS_ANTIALIASEDLINEENABLE, true );
}
void c_renderer::gradient_box_outlined( float x, float y, float width, float height, float thickness, color _color, color _color2, color outline_color, bool vertical ) {
	this->gradient_box( x, y, width, height, _color, _color2, vertical );
	this->bordered_box( x, y, width, height, outline_color, thickness );
}

void c_renderer::circle( float x, float y, float radius, float pofloats, color _color ) {
	vertex* vtx = new vertex [ pofloats + 1 ];
	for ( int i = 0; i <= pofloats; i++ ) vtx [ i ] = { x + radius * cos( D3DX_PI * ( i / ( pofloats / 2.0f ) ) ), y - radius * sin( D3DX_PI * ( i / ( pofloats / 2.0f ) ) ), 0.0f, 1.0f, _color.to_d3d( ) };
	this->device->SetFVF( D3DFVF_XYZRHW | D3DFVF_DIFFUSE );
	this->device->DrawPrimitiveUP( D3DPT_LINESTRIP, pofloats, vtx, sizeof( vertex ) );
	delete [ ] vtx;
}

void c_renderer::filled_circle( float x, float y, float radius, float pofloats, color _color ) {
	vertex* vtx = new vertex [ pofloats + 1 ];

	for ( int i = 0; i <= pofloats; i++ )
		vtx [ i ] = { x + radius * std::cosf( D3DX_PI * ( i / ( pofloats / 2.0f ) ) ), y + radius * std::sinf( D3DX_PI * ( i / ( pofloats / 2.0f ) ) ), 0.0f, 1.0f, _color.to_d3d( ) };

	this->device->SetFVF( D3DFVF_XYZRHW | D3DFVF_DIFFUSE );
	this->device->DrawPrimitiveUP( D3DPT_TRIANGLEFAN, pofloats, vtx, sizeof( vertex ) );
	delete [ ] vtx;
}

void c_renderer::filled_circle_outlined( float x, float y, float radius, float pofloats, color _color, color outline_color ) {
	filled_circle( x, y, radius, pofloats, _color );
	circle( x, y, radius, pofloats, outline_color );
}

void c_renderer::string( bool borderedl, ID3DXFont* font, float x, float y, color _color, int orientation, const char *input, ... ) {
	char szBuffer [ MAX_PATH ];

	if ( !input )
		return;

	auto b_color = D3DCOLOR_RGBA( 0, 0, 0, _color.a );

	vsprintf_s( szBuffer, input, ( char* ) &input + _INTSIZEOF( input ) );

	RECT rect;

	switch ( orientation )
	{
	case font_left: {
		if ( borderedl )
		{
			SetRect( &rect, x - 1, y, x - 1, y );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_LEFT | DT_NOCLIP, b_color );
			SetRect( &rect, x + 1, y, x + 1, y );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_LEFT | DT_NOCLIP, b_color );
			SetRect( &rect, x, y - 1, x, y - 1 );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_LEFT | DT_NOCLIP, b_color );
			SetRect( &rect, x, y + 1, x, y + 1 );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_LEFT | DT_NOCLIP, b_color );
		}
		SetRect( &rect, x, y, x, y );
		font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_LEFT | DT_NOCLIP, _color.to_d3d( ) );
	} break;
	case font_center: {
		y -= string_height( font, szBuffer ) / 2;

		if ( borderedl )
		{
			SetRect( &rect, x - 1, y, x - 1, y );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_CENTER | DT_NOCLIP, b_color );
			SetRect( &rect, x + 1, y, x + 1, y );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_CENTER | DT_NOCLIP, b_color );
			SetRect( &rect, x, y - 1, x, y - 1 );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_CENTER | DT_NOCLIP, b_color );
			SetRect( &rect, x, y + 1, x, y + 1 );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_CENTER | DT_NOCLIP, b_color );
		}
		SetRect( &rect, x, y, x, y );
		font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_CENTER | DT_NOCLIP, _color.to_d3d( ) );
	} break;
	case font_right: {
		if ( borderedl )
		{
			SetRect( &rect, x - 1, y, x - 1, y );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_RIGHT | DT_NOCLIP, b_color );
			SetRect( &rect, x + 1, y, x + 1, y );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_RIGHT | DT_NOCLIP, b_color );
			SetRect( &rect, x, y - 1, x, y - 1 );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_RIGHT | DT_NOCLIP, b_color );
			SetRect( &rect, x, y + 1, x, y + 1 );
			font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_RIGHT | DT_NOCLIP, b_color );
		}
		SetRect( &rect, x, y, x, y );
		font->DrawTextA( nullptr, szBuffer, -1, &rect, DT_RIGHT | DT_NOCLIP, _color.to_d3d( ) );
	} break;
	}
}

void c_renderer::filled_circle_gradient( float x, float y, float rad, float angle, float fraction, float resolution, color _color, color _color2 ) {
	IDirect3DVertexBuffer9* vb2 = nullptr;

	std::vector< vertex > circle( resolution + 2 );

	float pi = D3DX_PI;

	pi = D3DX_PI / fraction;

	circle [ 0 ].x = x;
	circle [ 0 ].y = y;
	circle [ 0 ].z = 0;
	circle [ 0 ].rhw = 1;
	circle [ 0 ]._color = _color2.to_d3d( );

	for ( float i = 1; i < resolution + 2; i++ ) {
		circle [ i ].x = ( float ) ( x - rad * std::cosf( pi * ( ( i - 1 ) / ( resolution / 2.0f ) ) ) );
		circle [ i ].y = ( float ) ( y - rad * std::sinf( pi * ( ( i - 1 ) / ( resolution / 2.0f ) ) ) );
		circle [ i ].z = 0;
		circle [ i ].rhw = 1;
		circle [ i ]._color = _color.to_d3d( );
	}

	device->CreateVertexBuffer( ( resolution + 2 ) * sizeof( vertex ), D3DUSAGE_WRITEONLY, D3DFVF_XYZRHW | D3DFVF_DIFFUSE, D3DPOOL_DEFAULT, &vb2, nullptr );

	void* verticies = nullptr;
	vb2->Lock( 0, ( resolution + 2 ) * sizeof( vertex ), ( void** ) &verticies, 0 );
	memcpy( verticies, &circle [ 0 ], ( resolution + 2 ) * sizeof( vertex ) );
	vb2->Unlock( );

	device->SetTexture( 0, nullptr );
	device->SetPixelShader( nullptr );
	device->SetRenderState( D3DRS_ALPHABLENDENABLE, true );
	device->SetRenderState( D3DRS_SRCBLEND, D3DBLEND_SRCALPHA );
	device->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );

	device->SetStreamSource( 0, vb2, 0, sizeof( vertex ) );
	device->SetFVF( D3DFVF_XYZRHW | D3DFVF_DIFFUSE );
	device->DrawPrimitive( D3DPT_TRIANGLEFAN, 0, resolution );

	if ( vb2 )
		vb2->Release( );
}


void c_renderer::release( void ) {
	if ( this->fonts.normal )
		this->fonts.normal->Release( );

	if ( this->fonts.esp )
		this->fonts.esp->Release( );
}