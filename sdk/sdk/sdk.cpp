#include "sdk.h"
#include "utils.h"
#include "mathematics.h"
#include "..\hacks\animfix.h"
std::unique_ptr< c_netvar_manager > netvars;

bool in_setup_bones = false;
using msg_t = void( __cdecl* )( const char*, ... );
msg_t		  msg = reinterpret_cast< msg_t >( GetProcAddress( GetModuleHandleA( "tier0.dll" ), "Msg" ) );

/*
bool c_trace_filter::should_hit_entity( c_baseentity* entity, int contents_mask ) {
auto clazz = entity->get_client_class( );

if ( clazz && strcmp( ignore_class.c_str( ), "" ) ) {
if ( strstr( clazz->network_name, ignore_class.c_str( ) ) )
return false;
}

return entity != skip;
}
*/

inline int time_to_ticks( float time ) {
	return static_cast< int >( time / globals->interval_per_tick + 0.5f );
}

inline float ticks_to_time( int ticks ) {
	return static_cast< float >( ticks * globals->interval_per_tick );
}

/*
*	XREF: "Player.Swim" used by CGameMovement::FullWalkMove
*	IDA-Style Sig: 55 8B EC 83 E4 F8 56 8B F1 57 8B 06 8B 80
*	Sig: \x55\x8B\xEC\x83\xE4\xF8\x56\x8B\xF1\x57\x8B\x06\x8B\x80
*	Mask: xxxxxxxxxxxxxx
*/

void c_game_movement::full_walk_move( c_baseentity* entity ) {
	static auto full_walk_move_fn = ( void( __thiscall* )( void*, c_baseentity* ) ) utils->find_pattern( "client_panorama.dll", "55 8B EC 83 E4 F8 56 8B F1 57 8B 06 8B 80" );
	full_walk_move_fn( this, entity );
}

int c_baseentity::lookup_pose_parameter( const char* pose_name ) {
	if ( !this )
		return -1;

	auto mdl = get_model( );

	if ( !mdl || !model_info )
		return -1;

	auto hdr = model_info->get_studio_model( mdl );

	if ( !hdr )
		return -1;

	static auto lookup_pose_parameter_fn = ( int( __stdcall* )( studiohdr_t*, const char* ) ) utils->find_pattern( "client_panorama.dll", "55 8B EC 57 8B 7D 08 85 FF 75 ? 83" );

	lookup_pose_parameter_fn( hdr, pose_name );
}

void c_baseentity::reset_animation_state( c_csgoplayeranimstate* state ) {
	if ( !state )
		return;

	static auto _reset_animation_state = utils->find_pattern( "client_panorama.dll", "56 6A 01 68 ? ? ? ? 8B F1" );

	( ( void( __thiscall* )( c_csgoplayeranimstate* ) ) _reset_animation_state )( state );
}
void c_baseentity::set_current_command( c_usercmd *cmd )
{
	constexpr fnv_t hash = fnv_hash( "CBasePlayer->m_hConstraintEntity" );

	static uint16_t offset = 0;

	if ( !offset )
		offset = netvars->get_offset( hash );


	*reinterpret_cast< c_usercmd** >( uintptr_t( this ) + offset - 0xC ) = cmd;
}
void c_baseentity::update_animation_state( c_csgoplayeranimstate* state, vec_t angle ) {
	if ( !state )
		return;

	static auto _update_animation_state = utils->find_pattern( "client_panorama.dll", "55 8B EC 83 E4 F8 83 EC 18 56 57 8B F9 F3 0F 11 54 24" );

	__asm {
		push 0
		mov ecx, state
		movss xmm1, [ angle + 4 ]
		movss xmm2, [ angle ]
		call _update_animation_state
	}
}

void c_baseentity::create_animation_state( c_csgoplayeranimstate* state ) {
	if ( !state )
		return;
	using fn = void( __thiscall* )( c_csgoplayeranimstate*, c_baseentity* );
	static auto ret = reinterpret_cast< fn >( utils->find_pattern( "client_panorama.dll", "55 8B EC 56 8B F1 B9 ? ? ? ? C7 46" ) );

	ret( state, this );
}

void c_baseentity::update_clientside_animation( void ) {
	if ( !this )
		return;

	static auto update_clientside_animation_fn = utils->find_pattern( "client_panorama.dll", "55 8B EC 51 56 8B F1 80 BE ? ? ? ? 00 74 ? 8B 06 FF" );

	( ( void( __thiscall* )( void* ) ) update_clientside_animation_fn )( this );
}

vec_t c_baseentity::get_abs_angles( void ) {
	return ( ( vec_t&( __thiscall* )( void* ) ) get_vfunc( this, 11 ) )( this );
}

void c_baseentity::set_abs_origin( const vec_t& value ) {
	if ( !this )
		return;

	static auto _set_abs_origin = ( void( __thiscall* )( void*, const vec_t& ) )( utils->find_pattern( "client_panorama.dll", "55 8B EC 83 E4 F8 51 53 56 57 8B F1" ) );

	_set_abs_origin( this, value );
}

void c_baseentity::set_abs_angles( const vec_t& angles ) {
	if ( !this )
		return;

	static auto _set_abs_angles = ( void( __thiscall* )( void*, const vec_t& ) ) utils->find_pattern( "client_panorama.dll", "55 8B EC 83 E4 F8 83 EC 64 53 56 57 8B F1 E8" );

	_set_abs_angles( this, angles );
}

bool c_baseentity::has_c4( void ) {
	if ( !this )
		return false;

	static auto _has_c4 = ( bool( __thiscall* )( void* ) ) utils->find_pattern( "client_panorama.dll", "56 8B F1 85 F6 74 31" );

	return _has_c4( this );
}

vec_t c_baseentity::get_eye_pos( void ) {
	return get_origin( ) + get_view_offset( );
}

void c_baseentity::invalidate_bone_cache( void ) {
	static auto invalidate_bone_bache_fn = utils->find_pattern( "client_panorama.dll", "80 3D ?? ?? ?? ?? ?? 74 16 A1 ?? ?? ?? ?? 48 C7 81" );

	*( uintptr_t* ) ( ( uintptr_t ) this + 0x2924 ) = 0xFF7FFFFF;
	*( uintptr_t* ) ( ( uintptr_t ) this + 0x2690 ) = **( uintptr_t** ) ( ( uintptr_t ) invalidate_bone_bache_fn + 10 ) - 1;
}

c_csgoplayeranimstate* c_baseentity::get_anim_state( void ) {
	static auto animstate_offset = *( uintptr_t* ) ( ( uintptr_t ) utils->find_pattern( "client_panorama.dll", "8B 8E ? ? ? ? F3 0F 10 48 04 E8 ? ? ? ? E9" ) + 0x2 );
	return *( c_csgoplayeranimstate** ) ( ( uintptr_t ) this + animstate_offset );
}

int c_baseentity::get_sequence_activity( int sequence ) {
	if ( !this )
		return -1;

	auto mdl = get_model( );

	if ( !mdl || !model_info )
		return -1;

	auto hdr = model_info->get_studio_model( mdl );

	if ( !hdr )
		return -1;

	static auto get_sequence_activity_fn = ( int( __fastcall* )( void*, const studiohdr_t*, int ) ) utils->find_pattern( "client_panorama.dll", "55 8B EC 83 7D 08 FF 56 8B F1 74" );

	if ( !get_sequence_activity_fn || !sequence )
		return -1;

	return get_sequence_activity_fn( this, hdr, sequence );
}

c_bone_accessor *c_baseentity::get_bone_accessor( )
{
	return ( c_bone_accessor* ) ( ( uintptr_t ) this + 0x26A8 );
}

c_studio_hdr *c_baseentity::get_model_ptr( )
{
	return *( c_studio_hdr** ) ( ( uintptr_t ) this + 0x294C );
}
template<typename T>
__forceinline static T vfunc( void *base, int index )
{
	DWORD *vTabella = *( DWORD** ) base;
	return ( T ) vTabella [ index ];
}
void c_baseentity::standard_bleding_rules( c_studio_hdr *hdr, vec_t *pos, quaternion *q, float_t curtime, int32_t boneMask )
{
	typedef void( __thiscall *o_StandardBlendingRules )( void*, c_studio_hdr*, vec_t*, quaternion*, float_t, int32_t );
	vfunc<o_StandardBlendingRules>( this, 201 )( this, hdr, pos, q, curtime, boneMask );
}

void c_baseentity::build_transformations( c_studio_hdr *hdr, vec_t *pos, quaternion *q, const matrix3x4_t &cameraTransform, int32_t boneMask, byte *computed )
{
	typedef void( __thiscall *o_BuildTransformations )( void*, c_studio_hdr *, vec_t*, quaternion*, const matrix3x4_t&, int32_t, byte* );
	vfunc<o_BuildTransformations>( this, 185 )( this, hdr, pos, q, cameraTransform, boneMask, computed );
}
void c_baseentity::build_bones( matrix3x4_t *boneOut )
{
	auto state = this->get_anim_state( ); if ( !state ) return;

	int backup_mne = this->get_mneffects( );
	vec_t backup_origin = this->get_origin( );
	vec_t backup_absorigin = this->get_abs_origin( );
	vec_t backup_velocity = this->get_velocity( );
	int backup_flags = this->get_flags( );
	int backup_eflags = this->get_eflags( );

	state->on_ground ? this->get_flags( ) |= ( int ) flags_t::fl_onground : this->get_flags( ) &= ~( int ) flags_t::fl_onground;

	this->get_eflags( ) &= ~0x1000;
	this->get_abs_velocity( ) = this->get_velocity( );


	this->get_mneffects( ) |= 0x008; //no interp flag

	this->setup_bones( boneOut, 128, 256, globals->curtime );

	this->get_origin( ) = backup_origin;
	this->set_abs_origin( backup_absorigin );
	this->get_velocity( ) = backup_velocity;
	this->get_flags( ) = backup_flags;
	this->get_eflags( ) = backup_eflags;

	//math->angle_matrix( this->get_abs_angles( ), this->get_abs_origin( ), *boneOut );
}
bool c_baseentity::handle_bone_setup( int32_t boneMask, matrix3x4_t *boneOut, float_t curtime )
{
	auto state = this->get_anim_state( );
	auto e = this;

	if ( !state )return false;



	c_studio_hdr *hdr = this->get_model_ptr( );
	if ( !hdr )
		return false;

	c_bone_accessor *accessor = this->get_bone_accessor( );
	if ( !accessor )
		return false;

	matrix3x4_t *backup_matrix = accessor->get_bone_array_for_write( );
	if ( !backup_matrix )
		return false;

	vec_t origin = this->get_origin( );
	vec_t angles = this->get_eyeangles( );

	vec_t backup_origin = this->get_abs_origin( );
	vec_t backup_angles = this->get_abs_angles( );

	std::array<float_t, 24> backup_poses;
	backup_poses = this->get_poseparameter( );

	animationlayer backup_layers [ 15 ];
	std::memcpy( backup_layers, this->get_anim_overlays( ), ( sizeof( animationlayer ) * 15 ) );

	alignas( 16 ) matrix3x4_t parentTransform;
	math->angle_matrix( angles, origin, parentTransform );

	auto &anim_data = animfix->player_data [ this->get_index( ) ];


	this->set_abs_origin( origin );
	this->set_abs_angles( angles );
	this->get_poseparameter( ) = anim_data.pose_params;
	std::memcpy( this->get_anim_overlays( ), anim_data.m_layers, ( sizeof( animationlayer ) * 15 ) );

	vec_t *pos = ( vec_t* ) ( mem_alloc->alloc( sizeof( vec_t [ 128 ] ) ) );
	quaternion *q = ( quaternion* ) ( mem_alloc->alloc( sizeof( quaternion [ 128 ] ) ) );
	std::memset( pos, 0xFF, sizeof( pos ) );
	std::memset( q, 0xFF, sizeof( q ) );

	this->standard_bleding_rules( hdr, pos, q, curtime, boneMask );

	accessor->set_bone_array_for_write( boneOut );

	byte *computed = ( byte* ) ( mem_alloc->alloc( sizeof( byte [ 0x20 ] ) ) );
	std::memset( computed, 0, sizeof( byte [ 0x20 ] ) );

	this->build_transformations( hdr, pos, q, parentTransform, boneMask, &computed [ 0 ] );

	accessor->set_bone_array_for_write( backup_matrix );

	this->set_abs_origin( backup_origin );
	this->set_abs_angles( backup_angles );
	this->get_poseparameter( ) = backup_poses;
	std::memcpy( this->get_anim_overlays( ), backup_layers, ( sizeof( animationlayer ) * 15 ) );

	return true;
	/*

	c_studio_hdr *hdr = this->get_model_ptr( );
	if ( !hdr )
		return false;

	c_bone_accessor *accessor = this->get_bone_accessor( );
	if ( !accessor )
		return false;

	matrix3x4_t *backup_matrix = accessor->get_bone_array_for_write( );
	if ( !backup_matrix )
		return false;

	vec_t origin = this->get_origin( );
	vec_t angles = this->get_eyeangles( );

	vec_t backup_origin = this->get_abs_origin( );
	vec_t backup_angles = this->get_abs_angles( );

	std::array<float_t, 24> backup_poses;
	backup_poses = this->get_poseparameter( );

	animationlayer backup_layers [ 15 ];
	std::memcpy( backup_layers, this->get_anim_overlays( ), ( sizeof( animationlayer ) * 15) );

	alignas( 16 ) matrix3x4_t parentTransform;
	math->angle_matrix( angles, origin, parentTransform );

	auto &anim_data = animfix->player_data [ this->get_index( ) ];


	this->set_abs_origin( origin );
	this->set_abs_angles( angles );
	this->get_poseparameter( ) = anim_data.pose_params;
	std::memcpy( this->get_anim_overlays( ), anim_data.m_layers, ( sizeof( animationlayer ) * 15 ) );

	vec_t *pos = ( vec_t* )( mem_alloc->alloc( sizeof( vec_t [ 128 ] ) ) );
	quaternion *q = ( quaternion* )( mem_alloc->alloc( sizeof( quaternion [ 128 ] ) ) );
	std::memset( pos, 0xFF, sizeof( pos ) );
	std::memset( q, 0xFF, sizeof( q ) );

	this->standard_bleding_rules( hdr, pos, q, curtime, boneMask );

	accessor->set_bone_array_for_write( boneOut );

	byte *computed = ( byte* )( mem_alloc->alloc( sizeof( byte [ 0x20 ] ) ) );
	std::memset( computed, 0, sizeof( byte [ 0x20 ] ) );

	this->build_transformations( hdr, pos, q, parentTransform, boneMask, &computed [ 0 ] );

	accessor->set_bone_array_for_write( backup_matrix );

	this->set_abs_origin( backup_origin );
	this->set_abs_angles( backup_angles );
	this->get_poseparameter( ) = backup_poses;
	/std::memcpy( this->get_anim_overlays( ), backup_layers, ( sizeof( animationlayer ) * 15 ) );

	return true;
	*/
}
bool c_baseentity::can_shoot( ) {

	auto weapon = get_active_weapon( );

	if ( !weapon || weapon->is_grenade( ) || weapon->is_knife( ) || ( weapon->is_reloading( ) || !weapon->get_clip_1( ) ) )
		return false;


	if ( weapon->is_revolver( ) ) {
		auto nci = engine->get_net_channel_info( );

		if ( !nci )
			return false;

		auto postpone_fire_ready_time = weapon->get_postpone_fire_ready_time( );

		if ( !postpone_fire_ready_time || postpone_fire_ready_time >= globals->curtime + globals->interval_per_tick * engine->get_net_channel( )->choked_packets )
			return false;

		return true;
	}
	else
		return weapon->get_next_primary_attack( ) <= get_tickbase( ) * globals->interval_per_tick;

	return false;
}

void c_baseweapon::draw_crosshair( void ) {
	static auto draw_crosshair_fn = ( void( __thiscall* )( void* ) ) utils->find_pattern( "client_panorama.dll", "55 8B EC 83 E4 F8 83 EC 70 56 8B F1 8B 0D" );
	draw_crosshair_fn( this );
}

c_weapon_system* c_baseweapon::get_weapon_system( void ) {
	if ( !this )
		return nullptr;

	static auto weapon_system = *( c_weapon_system** ) ( ( uintptr_t ) utils->find_pattern( "client_panorama.dll", "FF 50 04 83 BE ? ? ? ? ? 74 0A" ) - 0x4 );
	return weapon_system;
}

c_csweapon_info* c_baseweapon::get_cs_weapon_data( void ) {
	if ( !this ) return nullptr;

	static auto get_cs_weapon_data_fn = ( c_csweapon_info*( __thiscall* )( void* ) )( utils->find_pattern( "client_panorama.dll", "55 8B EC 81 EC ? ? ? ? 53 8B D9 56 57 8D 8B" ) );
	return get_cs_weapon_data_fn( this );
}

bool c_baseweapon::is_reloading( void ) {
	if ( !this )
		return false;

	static auto in_reload = *( uintptr_t* ) ( ( uintptr_t ) utils->find_pattern( "client_panorama.dll", "C6 87 ? ? ? ? ? 8B 06 8B CE FF 90" ) + 0x2 );
	return *( bool* ) ( ( uintptr_t ) this + in_reload );
}

bool trace_t::did_hit_world( ) const {
	return hit_entity == entity_list->get_client_entity( 0 );
}

c_mem_alloc* mem_alloc = nullptr;
c_base_client_dll* client = nullptr;
c_engine_client* engine = nullptr;
c_base_entity_list* entity_list = nullptr;
c_engine_trace* engine_trace = nullptr;
c_model_info* model_info = nullptr;
c_localize* localize = nullptr;
void* model_cache = nullptr;
c_debug_overlay* debug_overlay = nullptr;
c_cvar* cvar = nullptr;
c_surface* vgui_surface = nullptr;
 CGlowObjectManager* glow_object_manager = nullptr;
c_physics_api* physics = nullptr;
c_input_system* input_system = nullptr;
c_prediction* prediction = nullptr;
c_game_movement* game_movement = nullptr;
c_material_system* material_system = nullptr;
c_game_event_manager_2* game_event_manager = nullptr;
c_model_render* modelrender = nullptr;
c_render_view* renderview = nullptr;
c_panel* vgui_panel = nullptr;
void* clientmode = nullptr;
c_global_vars_base* globals = nullptr;
c_input* input = nullptr;
c_client_state* clientstate = nullptr;
c_move_helper* move_helper = nullptr;
void* studio_render = nullptr;
void* d3d_device = nullptr;
c_baseentity* local_player = nullptr;
c_baseweapon* local_weapon = nullptr;
c_usercmd*    global_cmd = nullptr;
std::uintptr_t u_random_seed = NULL;

float local_goal_feeet = 0.f;
float local_max_desync_delta = 0.f;
 vec_t my_angle = vec_t( );
 vec_t local_eye_position = vec_t( );
 vec_t real_angle = vec_t( );
 int choked_cmd = 0;
namespace sdk {
	void initialize( void ) {
		mem_alloc = *( c_mem_alloc** ) ( GetProcAddress( GetModuleHandleA( "tier0.dll" ), "g_pMemAlloc" ) );

		client = ( c_base_client_dll* ) utils->grab_interface( "client_panorama.dll", "VClient" );
		engine = ( c_engine_client* ) utils->grab_interface( "engine.dll", "VEngineClient" );
		entity_list = ( c_base_entity_list* ) utils->grab_interface( "client_panorama.dll", "VClientEntityList" );
		engine_trace = ( c_engine_trace* ) utils->grab_interface( "engine.dll", "EngineTraceClient" );
		model_info = ( c_model_info* ) utils->grab_interface( "engine.dll", "VModelInfoClient" );
		localize = ( c_localize* ) utils->grab_interface( "localize.dll", "Localize_" );
		model_cache = utils->grab_interface( "datacache.dll", "MDLCache" );
		debug_overlay = ( c_debug_overlay * ) utils->grab_interface( "engine.dll", "VDebugOverlay" );
		cvar = ( c_cvar* ) utils->grab_interface( "vstdlib.dll", "VEngineCvar" );
		vgui_panel = ( c_panel* ) utils->grab_interface( "vgui2.dll", "VGUI_Panel" );
		vgui_surface = ( c_surface* ) utils->grab_interface( "vguimatsurface.dll", "VGUI_Surface" );
		physics = ( c_physics_api* ) utils->grab_interface( "vphysics.dll", "VPhysicsSurfaceProps" );
		input_system = ( c_input_system * ) utils->grab_interface( "inputsystem.dll", "InputSystemVersion" );
		prediction = ( c_prediction* ) utils->grab_interface( "client_panorama.dll", "VClientPrediction" );
		game_movement = ( c_game_movement* ) utils->grab_interface( "client_panorama.dll", "GameMovement" );
		game_event_manager = ( c_game_event_manager_2* ) utils->grab_interface( "engine.dll", "GAMEEVENTSMANAGER002", false );
		modelrender = ( c_model_render* ) utils->grab_interface( "engine.dll", "VEngineModel" );
		renderview = ( c_render_view* ) utils->grab_interface( "engine.dll", "VEngineRenderView" );
		material_system = ( c_material_system* ) utils->grab_interface( "materialsystem.dll", "VMaterialSystem" );
		studio_render = utils->grab_interface( "studiorender.dll", "VStudioRender" );

		glow_object_manager = *reinterpret_cast< CGlowObjectManager** >( ( uintptr_t ) utils->find_pattern( "client_panorama.dll", "0F 11 05 ? ? ? ? 83 C8 01 C7 05 ? ? ? ? 00 00 00 00" ) + 3 );
		d3d_device = **( void*** ) ( ( uintptr_t ) utils->find_pattern( "shaderapidx9.dll", "A1 ? ? ? ? 50 8B 08 FF 51 0C" ) + 0x1 );
		input = *( c_input** ) ( ( uintptr_t ) utils->find_pattern( "client_panorama.dll", "B9 ? ? ? ? F3 0F 11 04 24 FF 50 10" ) + 0x1 );
		move_helper = **( c_move_helper*** ) ( ( uintptr_t ) utils->find_pattern( "client_panorama.dll", "8B 0D ? ? ? ? 8B 45 ? 51 8B D4 89 02 8B 01" ) + 0x2 );

		clientmode = **( void*** ) ( get_vfunc( client, 10 ) + 0x5 );
		globals = **( c_global_vars_base*** ) ( get_vfunc( client, 0 ) + 0x1B );
		clientstate = **( c_client_state*** ) ( ( uintptr_t ) utils->find_pattern( "engine.dll", "A1 ? ? ? ? 8B 80 ? ? ? ? C3" ) + 1 );

		netvars = std::make_unique< c_netvar_manager >( );
	}
}