#include "vmt.h"


inline unsigned int get_vfunc( void* pp_class, unsigned int index ) {
	return ( unsigned int ) ( *( int** ) pp_class )[ index ];
}