#pragma once
#include "sdk.h"

#define in_range( x, a, b ) ( x >= a && x <= b ) 
#define get_bits( x ) ( in_range( ( x & ( ~0x20 ) ), 'A', 'F' ) ? ( ( x & ( ~0x20 ) ) - 'A' + 0xA ) : ( in_range( x, '0', '9' ) ? x - '0' : 0 ) )
#define get_byte( x ) ( get_bits( x[ 0 ] ) << 4 | get_bits( x[ 1 ] ) )

class c_utils {
public:
	void* grab_interface( const char* module_name, const char* interf, bool bruteforce = true );
	void* find_pattern( const char* module_name, const char* pattern );
	float clamp( float x, float min, float max );
	void random_seed(int seed);
	float random_float( float min, float max );
	int random_int( int min, int max );
	void force_update( void );
	c_baseentity * util_player_by_index(int index);
	float fov_player( vec_t ViewOffSet, vec_t View, c_baseentity * entity, int hitbox );
	float get_lerp_time( void );
	bool hovered(int x, int y, int w, int h);
	bool is_valid_tick(const float & simtime, float time = 0.2f );
	color rainbow( float ct, int a );
	int ticks_choked( c_baseentity * pEntity );
	std::string get_gun_image(item_definition_index index);
	int closest_to_crosshair( );
	float get_hitchance(void);
	bool is_visible(vec_t pos_notangles, bool smokecheck);
};

extern std::unique_ptr< c_utils > utils;