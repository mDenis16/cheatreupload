#pragma once

/*
*	standard includes
*/

#include <Windows.h>
#include <TlHelp32.h>
#include <Psapi.h>
#include <cstdio>
#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <map>
#include <math.h>
#include <array>
#include <cstdint>
#include <deque>
#include <playsoundapi.h>
#include <ShlObj.h>
#include <fstream>
#include <algorithm>
#include <d3d9.h>
#include <d3dx9.h>


/*
*	any includes we need for the sdk
*/

#include "vmt.h"

/*
*	declarations
*/
typedef float quaternion [ 4 ];
typedef float radian_euler [ 3 ];

class bf_read;
class bf_write;
enum class client_frame_stage_t : int;
enum class send_prop_t : int;
enum class flags_t : int;

enum class csgoplayeractivity_t : int;
enum class buttons_t : int;
enum class life_state_t : byte;
enum class movetype_t : byte;
enum class team_t : int;
enum class cs_weapon_t : int;
enum class class_id : int;
enum class item_definition_index : short;
enum class hitgroup_t : int;
enum class material_character_identifiers_t : int;
enum class trace_type_t : int;
enum class hitbox_t : int;
enum class observermode_t : int;
enum class material_var_flags_t : int;

class color;
class c_collideable;
class c_csgoplayeranimstate;
class animationlayer;

class dvariant_t;
class recv_prop_hook;
class c_recv_proxy_data;
class recv_table;
class recv_prop;

using array_length_recv_proxy_fn = void( *)( void*, int, int );
using recv_var_proxy_fn = void( *)( const c_recv_proxy_data*, void*, void* );
using data_table_recv_var_proxy_fn = void( *)( const recv_prop*, void**, void*, int );
using create_client_class_fn = void( *)( int, int );
using create_event_fn = void*( *)( );

class vec_t;
class __declspec( align( 16 ) ) vec_aligned_t;
class c_csweapon_info;
class c_economy_item_view;
class c_weapon_system;

struct player_info_t;

struct mstudiobone_t;
struct mstudiobbox_t;
struct mstudiohitboxset_t;
struct model_t;
struct studiohdr_t;

struct vcollide_t;
struct cmodel_t;
struct csurface_t;
struct surfacephysicsparams_t;
struct surfaceaudioparams_t;
struct surfacesoundnames_t;
struct surfacegameprops_t;
struct surfacedata_t;
class c_physics_api;
struct ray_t;
class trace_t;
class c_trace_filter;
class c_physics_collide;
struct cplane_t;

class client_class;
class c_usercmd;
class c_debug_overlay;
class c_verified_usercmd;
class c_input;
class c_base_client_dll;
class c_panel;
class c_surface;
class CGlowObjectManager;
class GlowObjectDefinition_t;
class c_global_vars_base;
class c_net_channel_info;
class c_net_channel_handler;
class c_net_channel;
class c_convar;
class c_cvar;

class c_input_system;
class c_mem_alloc;
class c_engine_client;
class c_clock_drift_mgr;
class c_client_state;
class c_base_entity_list;
class c_model_info;
class c_localize;
class c_engine_trace;
class c_move_data;
class c_game_movement;
class c_move_helper;
class c_prediction;
class c_material_system;
class c_model_render;
class c_render_view;
class c_game_event_manager_2;

class c_client_entity;
class c_baseviewmodel;
class c_baseentity;
class c_baseweapon;

class color {
public:
	int r, g, b, a;

	color( ) {
		r = g = b = a = 0;
	}

	color( int _r, int _g, int _b, int _a ) {
		r = _r;
		g = _g;
		b = _b;
		a = _a;
	}

	color( float _r, float _g, float _b, float _a ) {
		r = static_cast< int >( _r * 255.0f );
		g = static_cast< int >( _g * 255.0f );
		b = static_cast< int >( _b * 255.0f );
		a = static_cast< int >( _a * 255.0f );
	}

	std::uint32_t to_d3d( void ) {
		return D3DCOLOR_RGBA( this->r, this->g, this->b, this->a );
	}
};

using fnv_t = unsigned;

class fnv_hash {
private:
	template < unsigned int len >
	static constexpr fnv_t fnv_hash_const( const char( &str ) [ len ], unsigned int I = len ) {
		return ( fnv_t ) ( 1ULL * ( I == 1 ? ( 2166136261 ^ str [ 0 ] ) : ( fnv_hash_const( str, I - 1 ) ^ str [ I - 1 ] ) ) * 16777619 );
	}

	static fnv_t get_fnv_hash( const char* str ) {
		const auto length = strlen( str ) + 1;
		uintptr_t hash = 2166136261;

		for ( size_t i = 0; i < length; ++i ) {
			hash ^= *str++;
			hash *= 16777619;
		}

		return hash;
	}

	struct wrapper {
		wrapper( const char* str ) : str( str ) { }
		const char* str = nullptr;
	};

	fnv_t hash_value;
public:
	fnv_hash( wrapper wrapper ) : hash_value( get_fnv_hash( wrapper.str ) ) {

	}

	template < unsigned int len >
	constexpr fnv_hash( const char( &str ) [ len ] ) : hash_value( fnv_hash_const( str ) ) {

	}

	constexpr operator fnv_t( ) const {
		return this->hash_value;
	}
};

class c_netvar_manager {
private:
	struct stored_prop_data {
		recv_prop* p_prop;
		uint16_t class_relative_offset;
	};

	void dump_recursive( const char* base_class, recv_table* table, uint16_t offset );

public:
	c_netvar_manager( );

	std::map< fnv_t, stored_prop_data > props;

	uint16_t get_offset( fnv_t hash ) {
		return props [ hash ].class_relative_offset;
	}

	recv_prop* get_prop( fnv_t hash ) {
		return props [ hash ].p_prop;
	}
};

extern std::unique_ptr< c_netvar_manager > netvars;

/*
*	interfaces
*/
extern     c_usercmd*    global_cmd;
extern c_mem_alloc* mem_alloc;
extern c_base_client_dll* client;
extern c_engine_client* engine;
extern c_base_entity_list* entity_list;
extern c_engine_trace* engine_trace;
extern c_model_info* model_info;
extern c_localize* localize;
extern void* model_cache;
extern c_debug_overlay* debug_overlay;
extern c_cvar* cvar;
extern c_surface* vgui_surface;
extern CGlowObjectManager* glow_object_manager;
extern c_physics_api* physics;
extern c_input_system* input_system;
extern c_prediction* prediction;
extern c_game_movement* game_movement;
extern c_material_system* material_system;
extern c_game_event_manager_2* game_event_manager;
extern c_model_render* modelrender;
extern c_render_view* renderview;
extern c_panel* vgui_panel;
extern void* clientmode;
extern c_global_vars_base* globals;
extern c_input* input;
extern c_client_state* clientstate;
extern c_move_helper* move_helper;
extern void* studio_render;
extern void* d3d_device;
extern c_baseentity* local_player;

extern c_baseweapon* local_weapon;
extern float local_goal_feeet;
extern vec_t my_angle;
extern vec_t real_angle;
extern vec_t local_eye_position;
extern  int choked_cmd;
extern float local_max_desync_delta;
using msg_t = void( __cdecl* )( const char*, ... );
extern msg_t		msg;
extern std::uintptr_t u_random_seed;
constexpr auto minimum_moving_speed = 0.1f;

/*
*	heart of sdk; classes, structs, definitions, and anything else
*/

/* vmt class*/
#include <map>

typedef DWORD** PPDWORD;
class table_hook {
	table_hook( const table_hook& ) = delete;
public:
	table_hook( PPDWORD ppClass, bool bReplace ) {
		m_ppClassBase = ppClass;
		m_bReplace = bReplace;
		if ( bReplace ) {
			m_pOriginalVMTable = *ppClass;
			uint32_t dwLength = calc_lenght( );

			m_pNewVMTable = new DWORD [ dwLength ];
			memcpy( m_pNewVMTable, m_pOriginalVMTable, dwLength * sizeof( DWORD ) );

			DWORD old;
			VirtualProtect( m_ppClassBase, sizeof( DWORD ), PAGE_EXECUTE_READWRITE, &old );
			*m_ppClassBase = m_pNewVMTable;
			VirtualProtect( m_ppClassBase, sizeof( DWORD ), old, &old );

		}
		else {
			m_pOriginalVMTable = *ppClass;
			m_pNewVMTable = *ppClass;
		}
	}
	~table_hook( ) {
		RestoreTable( );
		if ( m_bReplace && m_pNewVMTable ) delete[ ] m_pNewVMTable;
	}

	void RestoreTable( ) {
		for ( auto& pair : m_vecHookedIndexes ) {
			un_hook(  pair.first );
		}
	}

	template<class Type>
	static Type hook_manual( uintptr_t* vftable, uint32_t index, Type fnNew ) {
		DWORD Dummy;
		Type fnOld = ( Type ) vftable [ index ];
		VirtualProtect( ( void* ) ( vftable + index * 0x4 ), 0x4, PAGE_EXECUTE_READWRITE, &Dummy );
		vftable [ index ] = ( uintptr_t ) fnNew;
		VirtualProtect( ( void* ) ( vftable + index * 0x4 ), 0x4, Dummy, &Dummy );
		return fnOld;
	}

	template<class Type>
	static void un_hook_manual( uintptr_t* vftable, uint32_t index, Type fnNew ) {
		DWORD Dummy;
		VirtualProtect( ( void* ) ( vftable + index * 0x4 ), 0x4, PAGE_EXECUTE_READWRITE, &Dummy );
		vftable [ index ] = ( uintptr_t ) fnNew;
		VirtualProtect( ( void* ) ( vftable + index * 0x4 ), 0x4, Dummy, &Dummy );
	}

	template<class Type>
	Type hook( uint32_t index, Type fnNew ) {
		DWORD dwOld = ( DWORD ) m_pOriginalVMTable [ index ];
		m_pNewVMTable [ index ] = ( DWORD ) fnNew;
		m_vecHookedIndexes.insert( std::make_pair( index, ( DWORD ) dwOld ) );
		return ( Type ) dwOld;
	}

	void un_hook( uint32_t index ) {
		auto it = m_vecHookedIndexes.find( index );
		if ( it != m_vecHookedIndexes.end( ) ) {
			m_pNewVMTable [ index ] = ( DWORD ) it->second;
			m_vecHookedIndexes.erase( it );
		}
	}

	template<class Type>
	Type get_original( uint32_t index ) {
		return ( Type ) m_pOriginalVMTable [ index ];
	}

private:
	uint32_t calc_lenght( ) {
		uint32_t dwIndex = 0;
		if ( !m_pOriginalVMTable ) return 0;
		for ( dwIndex = 0; m_pOriginalVMTable [ dwIndex ]; dwIndex++ ) {
			if ( IsBadCodePtr( ( FARPROC ) m_pOriginalVMTable [ dwIndex ] ) ) {
				break;
			}
		}
		return dwIndex;
	}

private:
	std::map<uint32_t, DWORD> m_vecHookedIndexes;

	PPDWORD m_ppClassBase;
	PDWORD m_pOriginalVMTable;
	PDWORD m_pNewVMTable;
	bool m_bReplace;
};

class vmt_hook
{
private:
	uintptr_t* vmt;
public:
	// New virtual method table
	uintptr_t** object = nullptr;

	uintptr_t* original_vmt = nullptr;

	unsigned int methodCount = 0;

	vmt_hook( void* object )
	{
		this->object = reinterpret_cast< uintptr_t** >( object );

		size_t method_count = 0;

		while ( reinterpret_cast< uintptr_t* >( *this->object ) [ method_count ] )
			method_count++;

		original_vmt = *this->object;

		vmt = new uintptr_t [ sizeof( uintptr_t ) * method_count ];

		memcpy( vmt, original_vmt, sizeof( uintptr_t ) * method_count );
	}

	// Hook virtual method
	void hook_vm( void* method, size_t methodIndex )
	{
		vmt [ methodIndex ] = reinterpret_cast< uintptr_t >( method );
	}

	template<typename Fn>
	Fn original_method( size_t methodIndex )
	{
		return reinterpret_cast< Fn >( original_vmt [ methodIndex ] );
	}

	void apply_vmt( )
	{
		*this->object = vmt;
	}

	void realease_vmt( )
	{
		*this->object = original_vmt;
	}
};
/**/
#define   dispsurf_flag_surface			1
#define   dispsurf_flag_walkable        2
#define   dispsurf_flag_buildable       4
#define   dispsurf_flag_surfprop1       8
#define   dispsurf_flag_surfprop2       16

#define   contents_empty                0x0
#define   contents_solid                0x1       
#define   contents_window               0x2
#define   contents_aux                  0x4
#define   contents_grate                0x8
#define   contents_slime                0x10
#define   contents_water                0x20
#define   contents_blockos              0x40 
#define   contents_opaque               0x80 
#define   last_visible_contents         contents_opaque

#define   all_visible_contents			( last_visible_contents | ( last_visible_contents - 1 ) )

#define   contents_testfogvolume		0x100
#define   contents_unused               0x200     
#define   contents_blocklight           0x400
#define   contents_team1                0x800 
#define   contents_team2                0x1000 
#define   contents_ignore_nodraw_opaque 0x2000
#define   contents_moveable             0x4000
#define   contents_areaportal           0x8000
#define   contents_playerclip           0x10000
#define   contents_monsterclip          0x20000
#define   contents_current_0            0x40000
#define   contents_current_90           0x80000
#define   contents_current_180          0x100000
#define   contents_current_270          0x200000
#define   contents_current_up           0x400000
#define   contents_current_down         0x800000

#define   contents_origin               0x1000000 

#define   contents_monster              0x2000000 
#define   contents_debris               0x4000000
#define   contents_detail               0x8000000 
#define   contents_translucent          0x10000000
#define   contents_ladder               0x20000000
#define   contents_hitbox               0x40000000

#define   surf_light                    0x0001
#define   surf_sky2d                    0x0002
#define   surf_sky                      0x0004
#define   surf_warp                     0x0008
#define   surf_trans                    0x0010
#define   surf_noportal                 0x0020
#define   surf_trigger                  0x0040
#define   surf_nodraw                   0x0080

#define   surf_hint                     0x0100

#define   surf_skip                     0x0200
#define   surf_nolight                  0x0400
#define   surf_bumplight                0x0800
#define   surf_noshadows                0x1000
#define   surf_nodecals                 0x2000
#define   surf_nochop                   0x4000
#define   surf_hitbox                   0x8000
#define   surf_nopaint                  surf_nodecals

#define   mask_all                      ( 0xFFFFFFFF )
#define   mask_solid                    ( contents_solid | contents_moveable | contents_window | contents_monster | contents_grate )
#define   mask_playersolid              ( contents_solid | contents_moveable | contents_playerclip | contents_window | contents_monster | contents_grate )
#define   mask_npcsolid                 ( contents_solid | contents_moveable | contents_monsterclip | contents_window | contents_monster | contents_grate )
#define   mask_npcfluid                 ( contents_solid | contents_moveable | contents_monsterclip | contents_window | contents_monster )
#define   mask_water                    ( contents_water | contents_moveable | contents_slime )
#define   mask_opaque                   ( contents_solid | contents_moveable | contents_opaque )
#define   mask_opaque_and_npcs          ( mask_opaque | contents_monster )
#define   mask_blockos                  ( contents_solid | contents_moveable | mask_blockos )
#define   mask_blockos_and_npcs         ( mask_blockos | contents_monster )
#define   mask_visible                  ( mask_opaque | contents_ignore_nodraw_opaque )
#define   mask_visible_and_npcs         ( mask_opaque_and_npcs | contents_ignore_nodraw_opaque )
#define   mask_shot                     ( contents_solid | contents_moveable | contents_monster | contents_window | contents_debris | contents_hitbox )
#define   mask_shot_brushonly           ( contents_solid | contents_moveable | contents_window | contents_debris )
#define   mask_shot_hull                ( contents_solid | contents_moveable | contents_monster | contents_window | contents_debris | contents_grate )
#define   mask_shot_portal              ( contents_solid | contents_moveable | contents_window | contents_monster )
#define   mask_solid_brushonly          ( contents_solid | contents_moveable | contents_window | contents_grate )
#define   mask_playersolid_brushonly	( contents_solid | contents_moveable | contents_window | contents_playerclip | contents_grate )
#define   mask_npcsolid_brushonly		( contents_solid | contents_moveable | contents_window | contents_monsterclip | contents_grate )
#define   mask_npcworldstatic           ( contents_solid | contents_window | contents_monsterclip | contents_grate )
#define   mask_npcworldstatic_fluid     ( contents_solid | contents_window | contents_monsterclip )
#define   mask_splitareaportal          ( contents_water | contents_slime )
#define   mask_current                  ( contents_current_0 | contents_current_90 | contents_current_180 | contents_current_270 | contents_current_up | contents_current_down )
#define   mask_deadsolid                ( contents_solid | contents_playerclip | contents_window | contents_grate )

enum class hitgroup_t : int {
	hitgroup_generic = 0,
	hitgroup_head,
	hitgroup_chest,
	hitgroup_stomach,
	hitgroup_leftarm,
	hitgroup_rightarm,
	hitgroup_leftleg,
	hitgroup_rightleg,
	hitgroup_gear
};

enum class material_character_identifiers_t : int {
	char_tex_antlion = 'A',
	char_tex_bloodyflesh = 'B',
	char_tex_concrete = 'C',
	char_tex_dirt = 'D',
	char_tex_eggshell = 'E',
	char_tex_flesh = 'F',
	char_tex_grate = 'G',
	char_tex_alienflesh = 'H',
	char_tex_clip = 'I',
	char_tex_plastic = 'L',
	char_tex_metal = 'M',
	char_tex_sand = 'N',
	char_tex_foliage = 'O',
	char_tex_computer = 'P',
	char_tex_slosh = 'S',
	char_tex_title = 'T',
	char_tex_cardboard = 'U',
	char_tex_vent = 'V',
	char_tex_wood = 'W',
	char_tex_glass = 'Y',
	char_tex_warpshield = 'Z',
};

enum class trace_type_t : int {
	trace_everything,
	trace_world_only,
	trace_entities_only,
	trace_everything_filter_props,
};

enum class client_frame_stage_t : int {
	frame_undefined = -1,
	frame_start,
	frame_net_update_start,
	frame_net_update_postdataupdate_start,
	frame_net_update_postdataupdate_end,
	frame_net_update_end,
	frame_render_start,
	frame_render_end
};

enum class send_prop_t : int {
	dpt_int,
	dpt_float,
	dpt_vec_t,
	dpt_vec_txy,
	dpt_string,
	dpt_array,
	dpt_datatable,
	dpt_int64,
	dpt_num_sendproptypes
};

enum class flags_t : int {
	fl_onground = 1,
	fl_ducking = 2
};

enum class buttons_t : int {
	in_attack = 1,
	in_jump = 2,
	in_duck = 4,
	in_forward = 8,
	in_back = 16,
	in_use = 32,
	in_cancel = 64,
	in_left = 128,
	in_right = 256,
	in_moveleft = 512,
	in_moveright = 1024,
	in_attack2 = 2048,
	in_reload = 8192,
	in_zoom = 524288,
	in_walk = 262144,
	in_bullrush = 4194304
};

enum class life_state_t : byte {
	life_alive,
	life_dying,
	life_dead
};

enum class movetype_t : byte {
	movetype_none,
	movetype_isometric,
	movetype_walk,
	movetype_step,
	movetype_fly,
	movetype_flygravity,
	movetype_vphysics,
	movetype_push,
	movetype_noclip,
	movetype_ladder,
	movetype_observer,
	movetype_custom,
	movetype_last = movetype_custom,
	movetype_max_bits = movetype_fly,
	max_movetype
};

enum class team_t : int {
	team_none,
	team_spectator,
	team_terrorist,
	team_counter_terrorist,
	max_team
};

enum class cs_weapon_t : int {
	weapontype_c4 = 0,
	weapontype_grenade = 0,
	weapontype_knife,
	weapontype_pistol,
	weapontype_submachinegun,
	weapontype_rifle,
	weapontype_sniper_rifle = 4,
	weapontype_shotgun,
	weapontype_machinegun = 5,
	weapontype_placeholder,
	weapontype_unknown
};

enum class class_id : int {
	cai_basenpc,
	cak47,
	cbaseanimating,
	cbaseanimatingoverlay,
	cbaseattributableitem,
	cbasebutton,
	cbasecombatcharacter,
	cbasecombatweapon,
	cbasecsgrenade,
	cbasecsgrenadeprojectile,
	cbasedoor,
	cbaseentity,
	cbaseflex,
	cbasegrenade,
	cbaseparticleentity,
	cbaseplayer,
	cbasepropdoor,
	cbaseteamobjectiveresource,
	cbasetempentity,
	cbasetoggle,
	cbasetrigger,
	cbaseviewmodel,
	cbasevphysicstrigger,
	cbaseweaponworldmodel,
	cbeam,
	cbeamspotlight,
	cbonefollower,
	cbreakableprop,
	cbreakablesurface,
	cc4,
	ccascadelight,
	cchicken,
	ccolorcorrection,
	ccolorcorrectionvolume,
	ccsgamerulesproxy,
	ccsplayer,
	ccsplayerresource,
	ccsragdoll,
	ccsteam,
	cdeagle,
	cdecoygrenade,
	cdecoyprojectile,
	cdynamiclight,
	cdynamicprop,
	ceconentity,
	ceconwearable,
	cembers,
	centitydissolve,
	centityflame,
	centityfreezing,
	centityparticletrail,
	cenvambientlight,
	cenvdetailcontroller,
	cenvdofcontroller,
	cenvparticlescript,
	cenvprojectedtexture,
	cenvquadraticbeam,
	cenvscreeneffect,
	cenvscreenoverlay,
	cenvtonemapcontroller,
	cenvwind,
	cfeplayerdecal,
	cfirecrackerblast,
	cfiresmoke,
	cfiretrail,
	cfish,
	cflashbang,
	cfogcontroller,
	cfootstepcontrol,
	cfunc_dust,
	cfunc_lod,
	cfuncareaportalwindow,
	cfuncbrush,
	cfuncconveyor,
	cfuncladder,
	cfuncmonitor,
	cfuncmovelinear,
	cfuncoccluder,
	cfuncreflectiveglass,
	cfuncrotating,
	cfuncsmokevolume,
	cfunctracktrain,
	cgamerulesproxy,
	chandletest,
	chegrenade,
	chostage,
	chostagecarriableprop,
	cincendiarygrenade,
	cinferno,
	cinfoladderdismount,
	cinfooverlayaccessor,
	citem_healthshot,
	citemdogtags,
	cknife,
	cknifegg,
	clightglow,
	cmaterialmodifycontrol,
	cmolotovgrenade,
	cmolotovprojectile,
	cmoviedisplay,
	cparticlefire,
	cparticleperformancemonitor,
	cparticlesystem,
	cphysbox,
	cphysboxmultiplayer,
	cphysicsprop,
	cphysicspropmultiplayer,
	cphysmagnet,
	cplantedc4,
	cplasma,
	cplayerresource,
	cpointcamera,
	cpointcommentarynode,
	cpointworldtext,
	cposecontroller,
	cpostprocesscontroller,
	cprecipitation,
	cprecipitationblocker,
	cpredictedviewmodel,
	cprop_hallucination,
	cpropdoorrotating,
	cpropjeep,
	cpropvehicledriveable,
	cragdollmanager,
	cragdollprop,
	cragdollpropattached,
	cropekeyframe,
	cscar17,
	csceneentity,
	csensorgrenade,
	csensorgrenadeprojectile,
	cshadowcontrol,
	cslideshowdisplay,
	csmokegrenade,
	csmokegrenadeprojectile,
	csmokestack,
	cspatialentity,
	cspotlightend,
	csprite,
	cspriteoriented,
	cspritetrail,
	cstatueprop,
	csteamjet,
	csun,
	csunlightshadowcontrol,
	cteam,
	cteamplayroundbasedrulesproxy,
	ctearmorricochet,
	ctebasebeam,
	ctebeamentpoint,
	ctebeaments,
	ctebeamfollow,
	ctebeamlaser,
	ctebeampoints,
	ctebeamring,
	ctebeamringpoint,
	ctebeamspline,
	ctebloodsprite,
	ctebloodstream,
	ctebreakmodel,
	ctebspdecal,
	ctebubbles,
	ctebubbletrail,
	cteclientprojectile,
	ctedecal,
	ctedust,
	ctedynamiclight,
	cteeffectdispatch,
	cteenergysplash,
	cteexplosion,
	ctefirebullets,
	ctefizz,
	ctefootprintdecal,
	ctefoundryhelpers,
	ctegaussexplosion,
	cteglowsprite,
	cteimpact,
	ctekillplayerattachments,
	ctelargefunnel,
	ctemetalsparks,
	ctemuzzleflash,
	cteparticlesystem,
	ctephysicsprop,
	cteplantbomb,
	cteplayeranimevent,
	cteplayerdecal,
	cteprojecteddecal,
	cteradioicon,
	cteshattersurface,
	cteshowline,
	ctesla,
	ctesmoke,
	ctesparks,
	ctesprite,
	ctespritespray,
	ctest_proxytoggle_networkable,
	ctesttraceline,
	cteworlddecal,
	ctriggerplayermovement,
	ctriggersoundoperator,
	cvguiscreen,
	cvotecontroller,
	cwaterbullet,
	cwaterlodcontrol,
	cweaponaug,
	cweaponawp,
	cweaponbaseitem,
	cweaponbizon,
	cweaponcsbase,
	cweaponcsbasegun,
	cweaponcycler,
	cweaponelite,
	cweaponfamas,
	cweaponfiveseven,
	cweapong3sg1,
	cweapongalil,
	cweapongalilar,
	cweaponglock,
	cweaponhkp2000,
	cweaponm249,
	cweaponm3,
	cweaponm4a1,
	cweaponmac10,
	cweaponmag7,
	cweaponmp5navy,
	cweaponmp7,
	cweaponmp9,
	cweaponnegev,
	cweaponnova,
	cweaponp228,
	cweaponp250,
	cweaponp90,
	cweaponsawedoff,
	cweaponscar20,
	cweaponscout,
	cweaponsg550,
	cweaponsg552,
	cweaponsg556,
	cweaponssg08,
	cweapontaser,
	cweapontec9,
	cweapontmp,
	cweaponump45,
	cweaponusp,
	cweaponxm1014,
	cworld,
	cworldvguitext,
	dusttrail,
	movieexplosion,
	particlesmokegrenade,
	rockettrail,
	smoketrail,
	sporeexplosion,
	sporetrail,
};

enum class item_definition_index : short {
	weapon_deagle = 1,
	weapon_elite = 2,
	weapon_fiveseven = 3,
	weapon_glock = 4,
	weapon_ak47 = 7,
	weapon_aug = 8,
	weapon_awp = 9,
	weapon_famas = 10,
	weapon_g3sg1 = 11,
	weapon_galilar = 13,
	weapon_m249 = 14,
	weapon_m4a1 = 16,
	weapon_mac10 = 17,
	weapon_p90 = 19,
	weapon_mp5sd = 23,
	weapon_ump45 = 24,
	weapon_xm1014 = 25,
	weapon_bizon = 26,
	weapon_mag7 = 27,
	weapon_negev = 28,
	weapon_sawedoff = 29,
	weapon_tec9 = 30,
	weapon_taser = 31,
	weapon_hkp2000 = 32,
	weapon_mp7 = 33,
	weapon_mp9 = 34,
	weapon_nova = 35,
	weapon_p250 = 36,
	weapon_scar20 = 38,
	weapon_sg556 = 39,
	weapon_ssg08 = 40,
	weapon_knifegg = 41,
	weapon_knife = 42,
	weapon_flashbang = 43,
	weapon_hegrenade = 44,
	weapon_smokegrenade = 45,
	weapon_molotov = 46,
	weapon_decoy = 47,
	weapon_incgrenade = 48,
	weapon_c4 = 49,
	weapon_knife_t = 59,
	weapon_m4a1_silencer = 60,
	weapon_usp_silencer = 61,
	weapon_cz75a = 63,
	weapon_revolver = 64,
	weapon_knife_bayonet = 500,
	weapon_knife_flip = 505,
	weapon_knife_gut = 506,
	weapon_knife_karambit = 507,
	weapon_knife_m9_bayonet = 508,
	weapon_knife_tactical = 509,
	weapon_knife_falchion = 512,
	weapon_knife_bowie = 514,
	weapon_knife_butterfly = 515,
	weapon_knife_shadowdaggers = 516,
	weapon_knife_navaja = 520,
	weapon_knife_stiletto = 522,
	weapon_knife_ursus = 519,
	weapon_knife_widowmaker = 523
};

enum class observermode_t : int
{
	obs_mode_none = 0,
	obs_mode_deathcam = 1,
	obs_mode_freezecam = 2,
	obs_mode_fixed = 3,
	obs_mode_in_eye = 4,
	obs_mode_chase = 5,
	obs_mode_roaming = 6
};

enum class hitbox_t : int {
	head,
	neck,
	pelvis,
	body,
	thorax,
	chest,
	upper_chest,
	right_thigh,
	left_thigh,
	right_calf,
	left_calf,
	right_foot,
	left_foot,
	right_hand,
	left_hand,
	right_upper_arm,
	right_forearm,
	left_upper_arm,
	left_forearm,
	max
};

enum class material_var_flags_t : int {
	material_var_no_draw = 4,
	material_var_vertexcolor = 16,
	material_var_vertexalpha = 32,
	material_var_selfillum = 64,
	material_var_additive = 128,
	material_var_alphatest = 256,
	material_var_znearer = 1024,
	material_var_model = 2048,
	material_var_flat = 4096,
	material_var_nocull = 8192,
	material_var_nofog = 16384,
	material_var_ignorez = 32768,
	material_var_decal = 65536,
	material_var_envmapsphere = 131072,
	material_var_translucent = 2097152,
	material_var_halflambert = 134217728,
	material_var_wireframe = 268435456
};
enum class csgoplayeractivity_t : int {
	ACT_RESET,
	ACT_IDLE,
	ACT_TRANSITION,
	ACT_COVER,
	ACT_COVER_MED,
	ACT_COVER_LOW,
	ACT_WALK,
	ACT_WALK_AIM,
	ACT_WALK_CROUCH,
	ACT_WALK_CROUCH_AIM,
	ACT_RUN,
	ACT_RUN_AIM,
	ACT_RUN_CROUCH,
	ACT_RUN_CROUCH_AIM,
	ACT_RUN_PROTECTED,
	ACT_SCRIPT_CUSTOM_MOVE,
	ACT_RANGE_ATTACK1,
	ACT_RANGE_ATTACK2,
	ACT_RANGE_ATTACK1_LOW,
	ACT_RANGE_ATTACK2_LOW,
	ACT_DIESIMPLE,
	ACT_DIEBACKWARD,
	ACT_DIEFORWARD,
	ACT_DIEVIOLENT,
	ACT_DIERAGDOLL,
	ACT_FLY,
	ACT_HOVER,
	ACT_GLIDE,
	ACT_SWIM,
	ACT_JUMP,
	ACT_HOP,
	ACT_LEAP,
	ACT_LAND,
	ACT_CLIMB_UP,
	ACT_CLIMB_DOWN,
	ACT_CLIMB_DISMOUNT,
	ACT_SHIPLADDER_UP,
	ACT_SHIPLADDER_DOWN,
	ACT_STRAFE_LEFT,
	ACT_STRAFE_RIGHT,
	ACT_ROLL_LEFT,
	ACT_ROLL_RIGHT,
	ACT_TURN_LEFT,
	ACT_TURN_RIGHT,
	ACT_CROUCH,
	ACT_CROUCHIDLE,
	ACT_STAND,
	ACT_USE,
	ACT_ALIEN_BURROW_IDLE,
	ACT_ALIEN_BURROW_OUT,
	ACT_SIGNAL1,
	ACT_SIGNAL2,
	ACT_SIGNAL3,
	ACT_SIGNAL_ADVANCE,
	ACT_SIGNAL_FORWARD,
	ACT_SIGNAL_GROUP,
	ACT_SIGNAL_HALT,
	ACT_SIGNAL_LEFT,
	ACT_SIGNAL_RIGHT,
	ACT_SIGNAL_TAKECOVER,
	ACT_LOOKBACK_RIGHT,
	ACT_LOOKBACK_LEFT,
	ACT_COWER,
	ACT_SMALL_FLINCH,
	ACT_BIG_FLINCH,
	ACT_MELEE_ATTACK1,
	ACT_MELEE_ATTACK2,
	ACT_RELOAD,
	ACT_RELOAD_START,
	ACT_RELOAD_FINISH,
	ACT_RELOAD_LOW,
	ACT_ARM,
	ACT_DISARM,
	ACT_DROP_WEAPON,
	ACT_DROP_WEAPON_SHOTGUN,
	ACT_PICKUP_GROUND,
	ACT_PICKUP_RACK,
	ACT_IDLE_ANGRY,
	ACT_IDLE_RELAXED,
	ACT_IDLE_STIMULATED,
	ACT_IDLE_AGITATED,
	ACT_IDLE_STEALTH,
	ACT_IDLE_HURT,
	ACT_WALK_RELAXED,
	ACT_WALK_STIMULATED,
	ACT_WALK_AGITATED,
	ACT_WALK_STEALTH,
	ACT_RUN_RELAXED,
	ACT_RUN_STIMULATED,
	ACT_RUN_AGITATED,
	ACT_RUN_STEALTH,
	ACT_IDLE_AIM_RELAXED,
	ACT_IDLE_AIM_STIMULATED,
	ACT_IDLE_AIM_AGITATED,
	ACT_IDLE_AIM_STEALTH,
	ACT_WALK_AIM_RELAXED,
	ACT_WALK_AIM_STIMULATED,
	ACT_WALK_AIM_AGITATED,
	ACT_WALK_AIM_STEALTH,
	ACT_RUN_AIM_RELAXED,
	ACT_RUN_AIM_STIMULATED,
	ACT_RUN_AIM_AGITATED,
	ACT_RUN_AIM_STEALTH,
	ACT_CROUCHIDLE_STIMULATED,
	ACT_CROUCHIDLE_AIM_STIMULATED,
	ACT_CROUCHIDLE_AGITATED,
	ACT_WALK_HURT,
	ACT_RUN_HURT,
	ACT_SPECIAL_ATTACK1,
	ACT_SPECIAL_ATTACK2,
	ACT_COMBAT_IDLE,
	ACT_WALK_SCARED,
	ACT_RUN_SCARED,
	ACT_VICTORY_DANCE,
	ACT_DIE_HEADSHOT,
	ACT_DIE_CHESTSHOT,
	ACT_DIE_GUTSHOT,
	ACT_DIE_BACKSHOT,
	ACT_FLINCH_HEAD,
	ACT_FLINCH_CHEST,
	ACT_FLINCH_STOMACH,
	ACT_FLINCH_LEFTARM,
	ACT_FLINCH_RIGHTARM,
	ACT_FLINCH_LEFTLEG,
	ACT_FLINCH_RIGHTLEG,
	ACT_FLINCH_PHYSICS,
	ACT_FLINCH_HEAD_BACK,
	ACT_FLINCH_HEAD_LEFT,
	ACT_FLINCH_HEAD_RIGHT,
	ACT_FLINCH_CHEST_BACK,
	ACT_FLINCH_STOMACH_BACK,
	ACT_FLINCH_CROUCH_FRONT,
	ACT_FLINCH_CROUCH_BACK,
	ACT_FLINCH_CROUCH_LEFT,
	ACT_FLINCH_CROUCH_RIGHT,
	ACT_IDLE_ON_FIRE,
	ACT_WALK_ON_FIRE,
	ACT_RUN_ON_FIRE,
	ACT_RAPPEL_LOOP,
	ACT_180_LEFT,
	ACT_180_RIGHT,
	ACT_90_LEFT,
	ACT_90_RIGHT,
	ACT_STEP_LEFT,
	ACT_STEP_RIGHT,
	ACT_STEP_BACK,
	ACT_STEP_FORE,
	ACT_GESTURE_RANGE_ATTACK1,
	ACT_GESTURE_RANGE_ATTACK2,
	ACT_GESTURE_MELEE_ATTACK1,
	ACT_GESTURE_MELEE_ATTACK2,
	ACT_GESTURE_RANGE_ATTACK1_LOW,
	ACT_GESTURE_RANGE_ATTACK2_LOW,
	ACT_MELEE_ATTACK_SWING_GESTURE,
	ACT_GESTURE_SMALL_FLINCH,
	ACT_GESTURE_BIG_FLINCH,
	ACT_GESTURE_FLINCH_BLAST,
	ACT_GESTURE_FLINCH_BLAST_SHOTGUN,
	ACT_GESTURE_FLINCH_BLAST_DAMAGED,
	ACT_GESTURE_FLINCH_BLAST_DAMAGED_SHOTGUN,
	ACT_GESTURE_FLINCH_HEAD,
	ACT_GESTURE_FLINCH_CHEST,
	ACT_GESTURE_FLINCH_STOMACH,
	ACT_GESTURE_FLINCH_LEFTARM,
	ACT_GESTURE_FLINCH_RIGHTARM,
	ACT_GESTURE_FLINCH_LEFTLEG,
	ACT_GESTURE_FLINCH_RIGHTLEG,
	ACT_GESTURE_TURN_LEFT,
	ACT_GESTURE_TURN_RIGHT,
	ACT_GESTURE_TURN_LEFT45,
	ACT_GESTURE_TURN_RIGHT45,
	ACT_GESTURE_TURN_LEFT90,
	ACT_GESTURE_TURN_RIGHT90,
	ACT_GESTURE_TURN_LEFT45_FLAT,
	ACT_GESTURE_TURN_RIGHT45_FLAT,
	ACT_GESTURE_TURN_LEFT90_FLAT,
	ACT_GESTURE_TURN_RIGHT90_FLAT,
	ACT_BARNACLE_HIT,
	ACT_BARNACLE_PULL,
	ACT_BARNACLE_CHOMP,
	ACT_BARNACLE_CHEW,
	ACT_DO_NOT_DISTURB,
	ACT_SPECIFIC_SEQUENCE,
	ACT_VM_DRAW,
	ACT_VM_HOLSTER,
	ACT_VM_IDLE,
	ACT_VM_FIDGET,
	ACT_VM_PULLBACK,
	ACT_VM_PULLBACK_HIGH,
	ACT_VM_PULLBACK_LOW,
	ACT_VM_THROW,
	ACT_VM_PULLPIN,
	ACT_VM_PRIMARYATTACK,
	ACT_VM_SECONDARYATTACK,
	ACT_VM_RELOAD,
	ACT_VM_DRYFIRE,
	ACT_VM_HITLEFT,
	ACT_VM_HITLEFT2,
	ACT_VM_HITRIGHT,
	ACT_VM_HITRIGHT2,
	ACT_VM_HITCENTER,
	ACT_VM_HITCENTER2,
	ACT_VM_MISSLEFT,
	ACT_VM_MISSLEFT2,
	ACT_VM_MISSRIGHT,
	ACT_VM_MISSRIGHT2,
	ACT_VM_MISSCENTER,
	ACT_VM_MISSCENTER2,
	ACT_VM_HAULBACK,
	ACT_VM_SWINGHARD,
	ACT_VM_SWINGMISS,
	ACT_VM_SWINGHIT,
	ACT_VM_IDLE_TO_LOWERED,
	ACT_VM_IDLE_LOWERED,
	ACT_VM_LOWERED_TO_IDLE,
	ACT_VM_RECOIL1,
	ACT_VM_RECOIL2,
	ACT_VM_RECOIL3,
	ACT_VM_PICKUP,
	ACT_VM_RELEASE,
	ACT_VM_ATTACH_SILENCER,
	ACT_VM_DETACH_SILENCER,
	ACT_VM_EMPTY_FIRE,
	ACT_VM_EMPTY_RELOAD,
	ACT_VM_EMPTY_DRAW,
	ACT_VM_EMPTY_IDLE,
	ACT_SLAM_STICKWALL_IDLE,
	ACT_SLAM_STICKWALL_ND_IDLE,
	ACT_SLAM_STICKWALL_ATTACH,
	ACT_SLAM_STICKWALL_ATTACH2,
	ACT_SLAM_STICKWALL_ND_ATTACH,
	ACT_SLAM_STICKWALL_ND_ATTACH2,
	ACT_SLAM_STICKWALL_DETONATE,
	ACT_SLAM_STICKWALL_DETONATOR_HOLSTER,
	ACT_SLAM_STICKWALL_DRAW,
	ACT_SLAM_STICKWALL_ND_DRAW,
	ACT_SLAM_STICKWALL_TO_THROW,
	ACT_SLAM_STICKWALL_TO_THROW_ND,
	ACT_SLAM_STICKWALL_TO_TRIPMINE_ND,
	ACT_SLAM_THROW_IDLE,
	ACT_SLAM_THROW_ND_IDLE,
	ACT_SLAM_THROW_THROW,
	ACT_SLAM_THROW_THROW2,
	ACT_SLAM_THROW_THROW_ND,
	ACT_SLAM_THROW_THROW_ND2,
	ACT_SLAM_THROW_DRAW,
	ACT_SLAM_THROW_ND_DRAW,
	ACT_SLAM_THROW_TO_STICKWALL,
	ACT_SLAM_THROW_TO_STICKWALL_ND,
	ACT_SLAM_THROW_DETONATE,
	ACT_SLAM_THROW_DETONATOR_HOLSTER,
	ACT_SLAM_THROW_TO_TRIPMINE_ND,
	ACT_SLAM_TRIPMINE_IDLE,
	ACT_SLAM_TRIPMINE_DRAW,
	ACT_SLAM_TRIPMINE_ATTACH,
	ACT_SLAM_TRIPMINE_ATTACH2,
	ACT_SLAM_TRIPMINE_TO_STICKWALL_ND,
	ACT_SLAM_TRIPMINE_TO_THROW_ND,
	ACT_SLAM_DETONATOR_IDLE,
	ACT_SLAM_DETONATOR_DRAW,
	ACT_SLAM_DETONATOR_DETONATE,
	ACT_SLAM_DETONATOR_HOLSTER,
	ACT_SLAM_DETONATOR_STICKWALL_DRAW,
	ACT_SLAM_DETONATOR_THROW_DRAW,
	ACT_SHOTGUN_RELOAD_START,
	ACT_SHOTGUN_RELOAD_FINISH,
	ACT_SHOTGUN_PUMP,
	ACT_SMG2_IDLE2,
	ACT_SMG2_FIRE2,
	ACT_SMG2_DRAW2,
	ACT_SMG2_RELOAD2,
	ACT_SMG2_DRYFIRE2,
	ACT_SMG2_TOAUTO,
	ACT_SMG2_TOBURST,
	ACT_PHYSCANNON_UPGRADE,
	ACT_RANGE_ATTACK_AR1,
	ACT_RANGE_ATTACK_AR2,
	ACT_RANGE_ATTACK_AR2_LOW,
	ACT_RANGE_ATTACK_AR2_GRENADE,
	ACT_RANGE_ATTACK_HMG1,
	ACT_RANGE_ATTACK_ML,
	ACT_RANGE_ATTACK_SMG1,
	ACT_RANGE_ATTACK_SMG1_LOW,
	ACT_RANGE_ATTACK_SMG2,
	ACT_RANGE_ATTACK_SHOTGUN,
	ACT_RANGE_ATTACK_SHOTGUN_LOW,
	ACT_RANGE_ATTACK_PISTOL,
	ACT_RANGE_ATTACK_PISTOL_LOW,
	ACT_RANGE_ATTACK_SLAM,
	ACT_RANGE_ATTACK_TRIPWIRE,
	ACT_RANGE_ATTACK_THROW,
	ACT_RANGE_ATTACK_SNIPER_RIFLE,
	ACT_RANGE_ATTACK_RPG,
	ACT_MELEE_ATTACK_SWING,
	ACT_RANGE_AIM_LOW,
	ACT_RANGE_AIM_SMG1_LOW,
	ACT_RANGE_AIM_PISTOL_LOW,
	ACT_RANGE_AIM_AR2_LOW,
	ACT_COVER_PISTOL_LOW,
	ACT_COVER_SMG1_LOW,
	ACT_GESTURE_RANGE_ATTACK_AR1,
	ACT_GESTURE_RANGE_ATTACK_AR2,
	ACT_GESTURE_RANGE_ATTACK_AR2_GRENADE,
	ACT_GESTURE_RANGE_ATTACK_HMG1,
	ACT_GESTURE_RANGE_ATTACK_ML,
	ACT_GESTURE_RANGE_ATTACK_SMG1,
	ACT_GESTURE_RANGE_ATTACK_SMG1_LOW,
	ACT_GESTURE_RANGE_ATTACK_SMG2,
	ACT_GESTURE_RANGE_ATTACK_SHOTGUN,
	ACT_GESTURE_RANGE_ATTACK_PISTOL,
	ACT_GESTURE_RANGE_ATTACK_PISTOL_LOW,
	ACT_GESTURE_RANGE_ATTACK_SLAM,
	ACT_GESTURE_RANGE_ATTACK_TRIPWIRE,
	ACT_GESTURE_RANGE_ATTACK_THROW,
	ACT_GESTURE_RANGE_ATTACK_SNIPER_RIFLE,
	ACT_GESTURE_MELEE_ATTACK_SWING,
	ACT_IDLE_RIFLE,
	ACT_IDLE_SMG1,
	ACT_IDLE_ANGRY_SMG1,
	ACT_IDLE_PISTOL,
	ACT_IDLE_ANGRY_PISTOL,
	ACT_IDLE_ANGRY_SHOTGUN,
	ACT_IDLE_STEALTH_PISTOL,
	ACT_IDLE_PACKAGE,
	ACT_WALK_PACKAGE,
	ACT_IDLE_SUITCASE,
	ACT_WALK_SUITCASE,
	ACT_IDLE_SMG1_RELAXED,
	ACT_IDLE_SMG1_STIMULATED,
	ACT_WALK_RIFLE_RELAXED,
	ACT_RUN_RIFLE_RELAXED,
	ACT_WALK_RIFLE_STIMULATED,
	ACT_RUN_RIFLE_STIMULATED,
	ACT_IDLE_AIM_RIFLE_STIMULATED,
	ACT_WALK_AIM_RIFLE_STIMULATED,
	ACT_RUN_AIM_RIFLE_STIMULATED,
	ACT_IDLE_SHOTGUN_RELAXED,
	ACT_IDLE_SHOTGUN_STIMULATED,
	ACT_IDLE_SHOTGUN_AGITATED,
	ACT_WALK_ANGRY,
	ACT_POLICE_HARASS1,
	ACT_POLICE_HARASS2,
	ACT_IDLE_MANNEDGUN,
	ACT_IDLE_MELEE,
	ACT_IDLE_ANGRY_MELEE,
	ACT_IDLE_RPG_RELAXED,
	ACT_IDLE_RPG,
	ACT_IDLE_ANGRY_RPG,
	ACT_COVER_LOW_RPG,
	ACT_WALK_RPG,
	ACT_RUN_RPG,
	ACT_WALK_CROUCH_RPG,
	ACT_RUN_CROUCH_RPG,
	ACT_WALK_RPG_RELAXED,
	ACT_RUN_RPG_RELAXED,
	ACT_WALK_RIFLE,
	ACT_WALK_AIM_RIFLE,
	ACT_WALK_CROUCH_RIFLE,
	ACT_WALK_CROUCH_AIM_RIFLE,
	ACT_RUN_RIFLE,
	ACT_RUN_AIM_RIFLE,
	ACT_RUN_CROUCH_RIFLE,
	ACT_RUN_CROUCH_AIM_RIFLE,
	ACT_RUN_STEALTH_PISTOL,
	ACT_WALK_AIM_SHOTGUN,
	ACT_RUN_AIM_SHOTGUN,
	ACT_WALK_PISTOL,
	ACT_RUN_PISTOL,
	ACT_WALK_AIM_PISTOL,
	ACT_RUN_AIM_PISTOL,
	ACT_WALK_STEALTH_PISTOL,
	ACT_WALK_AIM_STEALTH_PISTOL,
	ACT_RUN_AIM_STEALTH_PISTOL,
	ACT_RELOAD_PISTOL,
	ACT_RELOAD_PISTOL_LOW,
	ACT_RELOAD_SMG1,
	ACT_RELOAD_SMG1_LOW,
	ACT_RELOAD_SHOTGUN,
	ACT_RELOAD_SHOTGUN_LOW,
	ACT_GESTURE_RELOAD,
	ACT_GESTURE_RELOAD_PISTOL,
	ACT_GESTURE_RELOAD_SMG1,
	ACT_GESTURE_RELOAD_SHOTGUN,
	ACT_BUSY_LEAN_LEFT,
	ACT_BUSY_LEAN_LEFT_ENTRY,
	ACT_BUSY_LEAN_LEFT_EXIT,
	ACT_BUSY_LEAN_BACK,
	ACT_BUSY_LEAN_BACK_ENTRY,
	ACT_BUSY_LEAN_BACK_EXIT,
	ACT_BUSY_SIT_GROUND,
	ACT_BUSY_SIT_GROUND_ENTRY,
	ACT_BUSY_SIT_GROUND_EXIT,
	ACT_BUSY_SIT_CHAIR,
	ACT_BUSY_SIT_CHAIR_ENTRY,
	ACT_BUSY_SIT_CHAIR_EXIT,
	ACT_BUSY_STAND,
	ACT_BUSY_QUEUE,
	ACT_DUCK_DODGE,
	ACT_DIE_BARNACLE_SWALLOW,
	ACT_GESTURE_BARNACLE_STRANGLE,
	ACT_PHYSCANNON_DETACH,
	ACT_PHYSCANNON_ANIMATE,
	ACT_PHYSCANNON_ANIMATE_PRE,
	ACT_PHYSCANNON_ANIMATE_POST,
	ACT_DIE_FRONTSIDE,
	ACT_DIE_RIGHTSIDE,
	ACT_DIE_BACKSIDE,
	ACT_DIE_LEFTSIDE,
	ACT_DIE_CROUCH_FRONTSIDE,
	ACT_DIE_CROUCH_RIGHTSIDE,
	ACT_DIE_CROUCH_BACKSIDE,
	ACT_DIE_CROUCH_LEFTSIDE,
	ACT_OPEN_DOOR,
	ACT_DI_ALYX_ZOMBIE_MELEE,
	ACT_DI_ALYX_ZOMBIE_TORSO_MELEE,
	ACT_DI_ALYX_HEADCRAB_MELEE,
	ACT_DI_ALYX_ANTLION,
	ACT_DI_ALYX_ZOMBIE_SHOTGUN64,
	ACT_DI_ALYX_ZOMBIE_SHOTGUN26,
	ACT_READINESS_RELAXED_TO_STIMULATED,
	ACT_READINESS_RELAXED_TO_STIMULATED_WALK,
	ACT_READINESS_AGITATED_TO_STIMULATED,
	ACT_READINESS_STIMULATED_TO_RELAXED,
	ACT_READINESS_PISTOL_RELAXED_TO_STIMULATED,
	ACT_READINESS_PISTOL_RELAXED_TO_STIMULATED_WALK,
	ACT_READINESS_PISTOL_AGITATED_TO_STIMULATED,
	ACT_READINESS_PISTOL_STIMULATED_TO_RELAXED,
	ACT_IDLE_CARRY,
	ACT_WALK_CARRY,
	ACT_STARTDYING,
	ACT_DYINGLOOP,
	ACT_DYINGTODEAD,
	ACT_RIDE_MANNED_GUN,
	ACT_VM_SPRINT_ENTER,
	ACT_VM_SPRINT_IDLE,
	ACT_VM_SPRINT_LEAVE,
	ACT_FIRE_START,
	ACT_FIRE_LOOP,
	ACT_FIRE_END,
	ACT_CROUCHING_GRENADEIDLE,
	ACT_CROUCHING_GRENADEREADY,
	ACT_CROUCHING_PRIMARYATTACK,
	ACT_OVERLAY_GRENADEIDLE,
	ACT_OVERLAY_GRENADEREADY,
	ACT_OVERLAY_PRIMARYATTACK,
	ACT_OVERLAY_SHIELD_UP,
	ACT_OVERLAY_SHIELD_DOWN,
	ACT_OVERLAY_SHIELD_UP_IDLE,
	ACT_OVERLAY_SHIELD_ATTACK,
	ACT_OVERLAY_SHIELD_KNOCKBACK,
	ACT_SHIELD_UP,
	ACT_SHIELD_DOWN,
	ACT_SHIELD_UP_IDLE,
	ACT_SHIELD_ATTACK,
	ACT_SHIELD_KNOCKBACK,
	ACT_CROUCHING_SHIELD_UP,
	ACT_CROUCHING_SHIELD_DOWN,
	ACT_CROUCHING_SHIELD_UP_IDLE,
	ACT_CROUCHING_SHIELD_ATTACK,
	ACT_CROUCHING_SHIELD_KNOCKBACK,
	ACT_TURNRIGHT45,
	ACT_TURNLEFT45,
	ACT_TURN,
	ACT_OBJ_ASSEMBLING,
	ACT_OBJ_DISMANTLING,
	ACT_OBJ_STARTUP,
	ACT_OBJ_RUNNING,
	ACT_OBJ_IDLE,
	ACT_OBJ_PLACING,
	ACT_OBJ_DETERIORATING,
	ACT_OBJ_UPGRADING,
	ACT_DEPLOY,
	ACT_DEPLOY_IDLE,
	ACT_UNDEPLOY,
	ACT_CROSSBOW_DRAW_UNLOADED,
	ACT_GAUSS_SPINUP,
	ACT_GAUSS_SPINCYCLE,
	ACT_VM_PRIMARYATTACK_SILENCED,
	ACT_VM_RELOAD_SILENCED,
	ACT_VM_DRYFIRE_SILENCED,
	ACT_VM_IDLE_SILENCED,
	ACT_VM_DRAW_SILENCED,
	ACT_VM_IDLE_EMPTY_LEFT,
	ACT_VM_DRYFIRE_LEFT,
	ACT_VM_IS_DRAW,
	ACT_VM_IS_HOLSTER,
	ACT_VM_IS_IDLE,
	ACT_VM_IS_PRIMARYATTACK,
	ACT_PLAYER_IDLE_FIRE,
	ACT_PLAYER_CROUCH_FIRE,
	ACT_PLAYER_CROUCH_WALK_FIRE,
	ACT_PLAYER_WALK_FIRE,
	ACT_PLAYER_RUN_FIRE,
	ACT_IDLETORUN,
	ACT_RUNTOIDLE,
	ACT_VM_DRAW_DEPLOYED,
	ACT_HL2MP_IDLE_MELEE,
	ACT_HL2MP_RUN_MELEE,
	ACT_HL2MP_IDLE_CROUCH_MELEE,
	ACT_HL2MP_WALK_CROUCH_MELEE,
	ACT_HL2MP_GESTURE_RANGE_ATTACK_MELEE,
	ACT_HL2MP_GESTURE_RELOAD_MELEE,
	ACT_HL2MP_JUMP_MELEE,
	ACT_VM_FIZZLE,
	ACT_MP_STAND_IDLE,
	ACT_MP_CROUCH_IDLE,
	ACT_MP_CROUCH_DEPLOYED_IDLE,
	ACT_MP_CROUCH_DEPLOYED,
	ACT_MP_DEPLOYED_IDLE,
	ACT_MP_RUN,
	ACT_MP_WALK,
	ACT_MP_AIRWALK,
	ACT_MP_CROUCHWALK,
	ACT_MP_SPRINT,
	ACT_MP_JUMP,
	ACT_MP_JUMP_START,
	ACT_MP_JUMP_FLOAT,
	ACT_MP_JUMP_LAND,
	ACT_MP_JUMP_IMPACT_N,
	ACT_MP_JUMP_IMPACT_E,
	ACT_MP_JUMP_IMPACT_W,
	ACT_MP_JUMP_IMPACT_S,
	ACT_MP_JUMP_IMPACT_TOP,
	ACT_MP_DOUBLEJUMP,
	ACT_MP_SWIM,
	ACT_MP_DEPLOYED,
	ACT_MP_SWIM_DEPLOYED,
	ACT_MP_VCD,
	ACT_MP_ATTACK_STAND_PRIMARYFIRE,
	ACT_MP_ATTACK_STAND_PRIMARYFIRE_DEPLOYED,
	ACT_MP_ATTACK_STAND_SECONDARYFIRE,
	ACT_MP_ATTACK_STAND_GRENADE,
	ACT_MP_ATTACK_CROUCH_PRIMARYFIRE,
	ACT_MP_ATTACK_CROUCH_PRIMARYFIRE_DEPLOYED,
	ACT_MP_ATTACK_CROUCH_SECONDARYFIRE,
	ACT_MP_ATTACK_CROUCH_GRENADE,
	ACT_MP_ATTACK_SWIM_PRIMARYFIRE,
	ACT_MP_ATTACK_SWIM_SECONDARYFIRE,
	ACT_MP_ATTACK_SWIM_GRENADE,
	ACT_MP_ATTACK_AIRWALK_PRIMARYFIRE,
	ACT_MP_ATTACK_AIRWALK_SECONDARYFIRE,
	ACT_MP_ATTACK_AIRWALK_GRENADE,
	ACT_MP_RELOAD_STAND,
	ACT_MP_RELOAD_STAND_LOOP,
	ACT_MP_RELOAD_STAND_END,
	ACT_MP_RELOAD_CROUCH,
	ACT_MP_RELOAD_CROUCH_LOOP,
	ACT_MP_RELOAD_CROUCH_END,
	ACT_MP_RELOAD_SWIM,
	ACT_MP_RELOAD_SWIM_LOOP,
	ACT_MP_RELOAD_SWIM_END,
	ACT_MP_RELOAD_AIRWALK,
	ACT_MP_RELOAD_AIRWALK_LOOP,
	ACT_MP_RELOAD_AIRWALK_END,
	ACT_MP_ATTACK_STAND_PREFIRE,
	ACT_MP_ATTACK_STAND_POSTFIRE,
	ACT_MP_ATTACK_STAND_STARTFIRE,
	ACT_MP_ATTACK_CROUCH_PREFIRE,
	ACT_MP_ATTACK_CROUCH_POSTFIRE,
	ACT_MP_ATTACK_SWIM_PREFIRE,
	ACT_MP_ATTACK_SWIM_POSTFIRE,
	ACT_MP_STAND_PRIMARY,
	ACT_MP_CROUCH_PRIMARY,
	ACT_MP_RUN_PRIMARY,
	ACT_MP_WALK_PRIMARY,
	ACT_MP_AIRWALK_PRIMARY,
	ACT_MP_CROUCHWALK_PRIMARY,
	ACT_MP_JUMP_PRIMARY,
	ACT_MP_JUMP_START_PRIMARY,
	ACT_MP_JUMP_FLOAT_PRIMARY,
	ACT_MP_JUMP_LAND_PRIMARY,
	ACT_MP_SWIM_PRIMARY,
	ACT_MP_DEPLOYED_PRIMARY,
	ACT_MP_SWIM_DEPLOYED_PRIMARY,
	ACT_MP_ATTACK_STAND_PRIMARY,
	ACT_MP_ATTACK_STAND_PRIMARY_DEPLOYED,
	ACT_MP_ATTACK_CROUCH_PRIMARY,
	ACT_MP_ATTACK_CROUCH_PRIMARY_DEPLOYED,
	ACT_MP_ATTACK_SWIM_PRIMARY,
	ACT_MP_ATTACK_AIRWALK_PRIMARY,
	ACT_MP_RELOAD_STAND_PRIMARY,
	ACT_MP_RELOAD_STAND_PRIMARY_LOOP,
	ACT_MP_RELOAD_STAND_PRIMARY_END,
	ACT_MP_RELOAD_CROUCH_PRIMARY,
	ACT_MP_RELOAD_CROUCH_PRIMARY_LOOP,
	ACT_MP_RELOAD_CROUCH_PRIMARY_END,
	ACT_MP_RELOAD_SWIM_PRIMARY,
	ACT_MP_RELOAD_SWIM_PRIMARY_LOOP,
	ACT_MP_RELOAD_SWIM_PRIMARY_END,
	ACT_MP_RELOAD_AIRWALK_PRIMARY,
	ACT_MP_RELOAD_AIRWALK_PRIMARY_LOOP,
	ACT_MP_RELOAD_AIRWALK_PRIMARY_END,
	ACT_MP_ATTACK_STAND_GRENADE_PRIMARY,
	ACT_MP_ATTACK_CROUCH_GRENADE_PRIMARY,
	ACT_MP_ATTACK_SWIM_GRENADE_PRIMARY,
	ACT_MP_ATTACK_AIRWALK_GRENADE_PRIMARY,
	ACT_MP_STAND_SECONDARY,
	ACT_MP_CROUCH_SECONDARY,
	ACT_MP_RUN_SECONDARY,
	ACT_MP_WALK_SECONDARY,
	ACT_MP_AIRWALK_SECONDARY,
	ACT_MP_CROUCHWALK_SECONDARY,
	ACT_MP_JUMP_SECONDARY,
	ACT_MP_JUMP_START_SECONDARY,
	ACT_MP_JUMP_FLOAT_SECONDARY,
	ACT_MP_JUMP_LAND_SECONDARY,
	ACT_MP_SWIM_SECONDARY,
	ACT_MP_ATTACK_STAND_SECONDARY,
	ACT_MP_ATTACK_CROUCH_SECONDARY,
	ACT_MP_ATTACK_SWIM_SECONDARY,
	ACT_MP_ATTACK_AIRWALK_SECONDARY,
	ACT_MP_RELOAD_STAND_SECONDARY,
	ACT_MP_RELOAD_STAND_SECONDARY_LOOP,
	ACT_MP_RELOAD_STAND_SECONDARY_END,
	ACT_MP_RELOAD_CROUCH_SECONDARY,
	ACT_MP_RELOAD_CROUCH_SECONDARY_LOOP,
	ACT_MP_RELOAD_CROUCH_SECONDARY_END,
	ACT_MP_RELOAD_SWIM_SECONDARY,
	ACT_MP_RELOAD_SWIM_SECONDARY_LOOP,
	ACT_MP_RELOAD_SWIM_SECONDARY_END,
	ACT_MP_RELOAD_AIRWALK_SECONDARY,
	ACT_MP_RELOAD_AIRWALK_SECONDARY_LOOP,
	ACT_MP_RELOAD_AIRWALK_SECONDARY_END,
	ACT_MP_ATTACK_STAND_GRENADE_SECONDARY,
	ACT_MP_ATTACK_CROUCH_GRENADE_SECONDARY,
	ACT_MP_ATTACK_SWIM_GRENADE_SECONDARY,
	ACT_MP_ATTACK_AIRWALK_GRENADE_SECONDARY,
	ACT_MP_STAND_MELEE,
	ACT_MP_CROUCH_MELEE,
	ACT_MP_RUN_MELEE,
	ACT_MP_WALK_MELEE,
	ACT_MP_AIRWALK_MELEE,
	ACT_MP_CROUCHWALK_MELEE,
	ACT_MP_JUMP_MELEE,
	ACT_MP_JUMP_START_MELEE,
	ACT_MP_JUMP_FLOAT_MELEE,
	ACT_MP_JUMP_LAND_MELEE,
	ACT_MP_SWIM_MELEE,
	ACT_MP_ATTACK_STAND_MELEE,
	ACT_MP_ATTACK_STAND_MELEE_SECONDARY,
	ACT_MP_ATTACK_CROUCH_MELEE,
	ACT_MP_ATTACK_CROUCH_MELEE_SECONDARY,
	ACT_MP_ATTACK_SWIM_MELEE,
	ACT_MP_ATTACK_AIRWALK_MELEE,
	ACT_MP_ATTACK_STAND_GRENADE_MELEE,
	ACT_MP_ATTACK_CROUCH_GRENADE_MELEE,
	ACT_MP_ATTACK_SWIM_GRENADE_MELEE,
	ACT_MP_ATTACK_AIRWALK_GRENADE_MELEE,
	ACT_MP_STAND_ITEM1,
	ACT_MP_CROUCH_ITEM1,
	ACT_MP_RUN_ITEM1,
	ACT_MP_WALK_ITEM1,
	ACT_MP_AIRWALK_ITEM1,
	ACT_MP_CROUCHWALK_ITEM1,
	ACT_MP_JUMP_ITEM1,
	ACT_MP_JUMP_START_ITEM1,
	ACT_MP_JUMP_FLOAT_ITEM1,
	ACT_MP_JUMP_LAND_ITEM1,
	ACT_MP_SWIM_ITEM1,
	ACT_MP_ATTACK_STAND_ITEM1,
	ACT_MP_ATTACK_STAND_ITEM1_SECONDARY,
	ACT_MP_ATTACK_CROUCH_ITEM1,
	ACT_MP_ATTACK_CROUCH_ITEM1_SECONDARY,
	ACT_MP_ATTACK_SWIM_ITEM1,
	ACT_MP_ATTACK_AIRWALK_ITEM1,
	ACT_MP_STAND_ITEM2,
	ACT_MP_CROUCH_ITEM2,
	ACT_MP_RUN_ITEM2,
	ACT_MP_WALK_ITEM2,
	ACT_MP_AIRWALK_ITEM2,
	ACT_MP_CROUCHWALK_ITEM2,
	ACT_MP_JUMP_ITEM2,
	ACT_MP_JUMP_START_ITEM2,
	ACT_MP_JUMP_FLOAT_ITEM2,
	ACT_MP_JUMP_LAND_ITEM2,
	ACT_MP_SWIM_ITEM2,
	ACT_MP_ATTACK_STAND_ITEM2,
	ACT_MP_ATTACK_STAND_ITEM2_SECONDARY,
	ACT_MP_ATTACK_CROUCH_ITEM2,
	ACT_MP_ATTACK_CROUCH_ITEM2_SECONDARY,
	ACT_MP_ATTACK_SWIM_ITEM2,
	ACT_MP_ATTACK_AIRWALK_ITEM2,
	ACT_MP_GESTURE_FLINCH,
	ACT_MP_GESTURE_FLINCH_PRIMARY,
	ACT_MP_GESTURE_FLINCH_SECONDARY,
	ACT_MP_GESTURE_FLINCH_MELEE,
	ACT_MP_GESTURE_FLINCH_ITEM1,
	ACT_MP_GESTURE_FLINCH_ITEM2,
	ACT_MP_GESTURE_FLINCH_HEAD,
	ACT_MP_GESTURE_FLINCH_CHEST,
	ACT_MP_GESTURE_FLINCH_STOMACH,
	ACT_MP_GESTURE_FLINCH_LEFTARM,
	ACT_MP_GESTURE_FLINCH_RIGHTARM,
	ACT_MP_GESTURE_FLINCH_LEFTLEG,
	ACT_MP_GESTURE_FLINCH_RIGHTLEG,
	ACT_MP_GRENADE1_DRAW,
	ACT_MP_GRENADE1_IDLE,
	ACT_MP_GRENADE1_ATTACK,
	ACT_MP_GRENADE2_DRAW,
	ACT_MP_GRENADE2_IDLE,
	ACT_MP_GRENADE2_ATTACK,
	ACT_MP_PRIMARY_GRENADE1_DRAW,
	ACT_MP_PRIMARY_GRENADE1_IDLE,
	ACT_MP_PRIMARY_GRENADE1_ATTACK,
	ACT_MP_PRIMARY_GRENADE2_DRAW,
	ACT_MP_PRIMARY_GRENADE2_IDLE,
	ACT_MP_PRIMARY_GRENADE2_ATTACK,
	ACT_MP_SECONDARY_GRENADE1_DRAW,
	ACT_MP_SECONDARY_GRENADE1_IDLE,
	ACT_MP_SECONDARY_GRENADE1_ATTACK,
	ACT_MP_SECONDARY_GRENADE2_DRAW,
	ACT_MP_SECONDARY_GRENADE2_IDLE,
	ACT_MP_SECONDARY_GRENADE2_ATTACK,
	ACT_MP_MELEE_GRENADE1_DRAW,
	ACT_MP_MELEE_GRENADE1_IDLE,
	ACT_MP_MELEE_GRENADE1_ATTACK,
	ACT_MP_MELEE_GRENADE2_DRAW,
	ACT_MP_MELEE_GRENADE2_IDLE,
	ACT_MP_MELEE_GRENADE2_ATTACK,
	ACT_MP_ITEM1_GRENADE1_DRAW,
	ACT_MP_ITEM1_GRENADE1_IDLE,
	ACT_MP_ITEM1_GRENADE1_ATTACK,
	ACT_MP_ITEM1_GRENADE2_DRAW,
	ACT_MP_ITEM1_GRENADE2_IDLE,
	ACT_MP_ITEM1_GRENADE2_ATTACK,
	ACT_MP_ITEM2_GRENADE1_DRAW,
	ACT_MP_ITEM2_GRENADE1_IDLE,
	ACT_MP_ITEM2_GRENADE1_ATTACK,
	ACT_MP_ITEM2_GRENADE2_DRAW,
	ACT_MP_ITEM2_GRENADE2_IDLE,
	ACT_MP_ITEM2_GRENADE2_ATTACK,
	ACT_MP_STAND_BUILDING,
	ACT_MP_CROUCH_BUILDING,
	ACT_MP_RUN_BUILDING,
	ACT_MP_WALK_BUILDING,
	ACT_MP_AIRWALK_BUILDING,
	ACT_MP_CROUCHWALK_BUILDING,
	ACT_MP_JUMP_BUILDING,
	ACT_MP_JUMP_START_BUILDING,
	ACT_MP_JUMP_FLOAT_BUILDING,
	ACT_MP_JUMP_LAND_BUILDING,
	ACT_MP_SWIM_BUILDING,
	ACT_MP_ATTACK_STAND_BUILDING,
	ACT_MP_ATTACK_CROUCH_BUILDING,
	ACT_MP_ATTACK_SWIM_BUILDING,
	ACT_MP_ATTACK_AIRWALK_BUILDING,
	ACT_MP_ATTACK_STAND_GRENADE_BUILDING,
	ACT_MP_ATTACK_CROUCH_GRENADE_BUILDING,
	ACT_MP_ATTACK_SWIM_GRENADE_BUILDING,
	ACT_MP_ATTACK_AIRWALK_GRENADE_BUILDING,
	ACT_MP_STAND_PDA,
	ACT_MP_CROUCH_PDA,
	ACT_MP_RUN_PDA,
	ACT_MP_WALK_PDA,
	ACT_MP_AIRWALK_PDA,
	ACT_MP_CROUCHWALK_PDA,
	ACT_MP_JUMP_PDA,
	ACT_MP_JUMP_START_PDA,
	ACT_MP_JUMP_FLOAT_PDA,
	ACT_MP_JUMP_LAND_PDA,
	ACT_MP_SWIM_PDA,
	ACT_MP_ATTACK_STAND_PDA,
	ACT_MP_ATTACK_SWIM_PDA,
	ACT_MP_GESTURE_VC_HANDMOUTH,
	ACT_MP_GESTURE_VC_FINGERPOINT,
	ACT_MP_GESTURE_VC_FISTPUMP,
	ACT_MP_GESTURE_VC_THUMBSUP,
	ACT_MP_GESTURE_VC_NODYES,
	ACT_MP_GESTURE_VC_NODNO,
	ACT_MP_GESTURE_VC_HANDMOUTH_PRIMARY,
	ACT_MP_GESTURE_VC_FINGERPOINT_PRIMARY,
	ACT_MP_GESTURE_VC_FISTPUMP_PRIMARY,
	ACT_MP_GESTURE_VC_THUMBSUP_PRIMARY,
	ACT_MP_GESTURE_VC_NODYES_PRIMARY,
	ACT_MP_GESTURE_VC_NODNO_PRIMARY,
	ACT_MP_GESTURE_VC_HANDMOUTH_SECONDARY,
	ACT_MP_GESTURE_VC_FINGERPOINT_SECONDARY,
	ACT_MP_GESTURE_VC_FISTPUMP_SECONDARY,
	ACT_MP_GESTURE_VC_THUMBSUP_SECONDARY,
	ACT_MP_GESTURE_VC_NODYES_SECONDARY,
	ACT_MP_GESTURE_VC_NODNO_SECONDARY,
	ACT_MP_GESTURE_VC_HANDMOUTH_MELEE,
	ACT_MP_GESTURE_VC_FINGERPOINT_MELEE,
	ACT_MP_GESTURE_VC_FISTPUMP_MELEE,
	ACT_MP_GESTURE_VC_THUMBSUP_MELEE,
	ACT_MP_GESTURE_VC_NODYES_MELEE,
	ACT_MP_GESTURE_VC_NODNO_MELEE,
	ACT_MP_GESTURE_VC_HANDMOUTH_ITEM1,
	ACT_MP_GESTURE_VC_FINGERPOINT_ITEM1,
	ACT_MP_GESTURE_VC_FISTPUMP_ITEM1,
	ACT_MP_GESTURE_VC_THUMBSUP_ITEM1,
	ACT_MP_GESTURE_VC_NODYES_ITEM1,
	ACT_MP_GESTURE_VC_NODNO_ITEM1,
	ACT_MP_GESTURE_VC_HANDMOUTH_ITEM2,
	ACT_MP_GESTURE_VC_FINGERPOINT_ITEM2,
	ACT_MP_GESTURE_VC_FISTPUMP_ITEM2,
	ACT_MP_GESTURE_VC_THUMBSUP_ITEM2,
	ACT_MP_GESTURE_VC_NODYES_ITEM2,
	ACT_MP_GESTURE_VC_NODNO_ITEM2,
	ACT_MP_GESTURE_VC_HANDMOUTH_BUILDING,
	ACT_MP_GESTURE_VC_FINGERPOINT_BUILDING,
	ACT_MP_GESTURE_VC_FISTPUMP_BUILDING,
	ACT_MP_GESTURE_VC_THUMBSUP_BUILDING,
	ACT_MP_GESTURE_VC_NODYES_BUILDING,
	ACT_MP_GESTURE_VC_NODNO_BUILDING,
	ACT_MP_GESTURE_VC_HANDMOUTH_PDA,
	ACT_MP_GESTURE_VC_FINGERPOINT_PDA,
	ACT_MP_GESTURE_VC_FISTPUMP_PDA,
	ACT_MP_GESTURE_VC_THUMBSUP_PDA,
	ACT_MP_GESTURE_VC_NODYES_PDA,
	ACT_MP_GESTURE_VC_NODNO_PDA,
	ACT_VM_UNUSABLE,
	ACT_VM_UNUSABLE_TO_USABLE,
	ACT_VM_USABLE_TO_UNUSABLE,
	ACT_PRIMARY_VM_DRAW,
	ACT_PRIMARY_VM_HOLSTER,
	ACT_PRIMARY_VM_IDLE,
	ACT_PRIMARY_VM_PULLBACK,
	ACT_PRIMARY_VM_PRIMARYATTACK,
	ACT_PRIMARY_VM_SECONDARYATTACK,
	ACT_PRIMARY_VM_RELOAD,
	ACT_PRIMARY_VM_DRYFIRE,
	ACT_PRIMARY_VM_IDLE_TO_LOWERED,
	ACT_PRIMARY_VM_IDLE_LOWERED,
	ACT_PRIMARY_VM_LOWERED_TO_IDLE,
	ACT_SECONDARY_VM_DRAW,
	ACT_SECONDARY_VM_HOLSTER,
	ACT_SECONDARY_VM_IDLE,
	ACT_SECONDARY_VM_PULLBACK,
	ACT_SECONDARY_VM_PRIMARYATTACK,
	ACT_SECONDARY_VM_SECONDARYATTACK,
	ACT_SECONDARY_VM_RELOAD,
	ACT_SECONDARY_VM_DRYFIRE,
	ACT_SECONDARY_VM_IDLE_TO_LOWERED,
	ACT_SECONDARY_VM_IDLE_LOWERED,
	ACT_SECONDARY_VM_LOWERED_TO_IDLE,
	ACT_MELEE_VM_DRAW,
	ACT_MELEE_VM_HOLSTER,
	ACT_MELEE_VM_IDLE,
	ACT_MELEE_VM_PULLBACK,
	ACT_MELEE_VM_PRIMARYATTACK,
	ACT_MELEE_VM_SECONDARYATTACK,
	ACT_MELEE_VM_RELOAD,
	ACT_MELEE_VM_DRYFIRE,
	ACT_MELEE_VM_IDLE_TO_LOWERED,
	ACT_MELEE_VM_IDLE_LOWERED,
	ACT_MELEE_VM_LOWERED_TO_IDLE,
	ACT_PDA_VM_DRAW,
	ACT_PDA_VM_HOLSTER,
	ACT_PDA_VM_IDLE,
	ACT_PDA_VM_PULLBACK,
	ACT_PDA_VM_PRIMARYATTACK,
	ACT_PDA_VM_SECONDARYATTACK,
	ACT_PDA_VM_RELOAD,
	ACT_PDA_VM_DRYFIRE,
	ACT_PDA_VM_IDLE_TO_LOWERED,
	ACT_PDA_VM_IDLE_LOWERED,
	ACT_PDA_VM_LOWERED_TO_IDLE,
	ACT_ITEM1_VM_DRAW,
	ACT_ITEM1_VM_HOLSTER,
	ACT_ITEM1_VM_IDLE,
	ACT_ITEM1_VM_PULLBACK,
	ACT_ITEM1_VM_PRIMARYATTACK,
	ACT_ITEM1_VM_SECONDARYATTACK,
	ACT_ITEM1_VM_RELOAD,
	ACT_ITEM1_VM_DRYFIRE,
	ACT_ITEM1_VM_IDLE_TO_LOWERED,
	ACT_ITEM1_VM_IDLE_LOWERED,
	ACT_ITEM1_VM_LOWERED_TO_IDLE,
	ACT_ITEM2_VM_DRAW,
	ACT_ITEM2_VM_HOLSTER,
	ACT_ITEM2_VM_IDLE,
	ACT_ITEM2_VM_PULLBACK,
	ACT_ITEM2_VM_PRIMARYATTACK,
	ACT_ITEM2_VM_SECONDARYATTACK,
	ACT_ITEM2_VM_RELOAD,
	ACT_ITEM2_VM_DRYFIRE,
	ACT_ITEM2_VM_IDLE_TO_LOWERED,
	ACT_ITEM2_VM_IDLE_LOWERED,
	ACT_ITEM2_VM_LOWERED_TO_IDLE,
	ACT_RELOAD_SUCCEED,
	ACT_RELOAD_FAIL,
	ACT_WALK_AIM_AUTOGUN,
	ACT_RUN_AIM_AUTOGUN,
	ACT_IDLE_AUTOGUN,
	ACT_IDLE_AIM_AUTOGUN,
	ACT_RELOAD_AUTOGUN,
	ACT_CROUCH_IDLE_AUTOGUN,
	ACT_RANGE_ATTACK_AUTOGUN,
	ACT_JUMP_AUTOGUN,
	ACT_IDLE_AIM_PISTOL,
	ACT_WALK_AIM_DUAL,
	ACT_RUN_AIM_DUAL,
	ACT_IDLE_DUAL,
	ACT_IDLE_AIM_DUAL,
	ACT_RELOAD_DUAL,
	ACT_CROUCH_IDLE_DUAL,
	ACT_RANGE_ATTACK_DUAL,
	ACT_JUMP_DUAL,
	ACT_IDLE_SHOTGUN,
	ACT_IDLE_AIM_SHOTGUN,
	ACT_CROUCH_IDLE_SHOTGUN,
	ACT_JUMP_SHOTGUN,
	ACT_IDLE_AIM_RIFLE,
	ACT_RELOAD_RIFLE,
	ACT_CROUCH_IDLE_RIFLE,
	ACT_RANGE_ATTACK_RIFLE,
	ACT_JUMP_RIFLE,
	ACT_SLEEP,
	ACT_WAKE,
	ACT_FLICK_LEFT,
	ACT_FLICK_LEFT_MIDDLE,
	ACT_FLICK_RIGHT_MIDDLE,
	ACT_FLICK_RIGHT,
	ACT_SPINAROUND,
	ACT_PREP_TO_FIRE,
	ACT_FIRE,
	ACT_FIRE_RECOVER,
	ACT_SPRAY,
	ACT_PREP_EXPLODE,
	ACT_EXPLODE,
	ACT_DOTA_IDLE,
	ACT_DOTA_RUN,
	ACT_DOTA_ATTACK,
	ACT_DOTA_ATTACK_EVENT,
	ACT_DOTA_DIE,
	ACT_DOTA_FLINCH,
	ACT_DOTA_DISABLED,
	ACT_DOTA_CAST_ABILITY_1,
	ACT_DOTA_CAST_ABILITY_2,
	ACT_DOTA_CAST_ABILITY_3,
	ACT_DOTA_CAST_ABILITY_4,
	ACT_DOTA_OVERRIDE_ABILITY_1,
	ACT_DOTA_OVERRIDE_ABILITY_2,
	ACT_DOTA_OVERRIDE_ABILITY_3,
	ACT_DOTA_OVERRIDE_ABILITY_4,
	ACT_DOTA_CHANNEL_ABILITY_1,
	ACT_DOTA_CHANNEL_ABILITY_2,
	ACT_DOTA_CHANNEL_ABILITY_3,
	ACT_DOTA_CHANNEL_ABILITY_4,
	ACT_DOTA_CHANNEL_END_ABILITY_1,
	ACT_DOTA_CHANNEL_END_ABILITY_2,
	ACT_DOTA_CHANNEL_END_ABILITY_3,
	ACT_DOTA_CHANNEL_END_ABILITY_4,
	ACT_MP_RUN_SPEEDPAINT,
	ACT_MP_LONG_FALL,
	ACT_MP_TRACTORBEAM_FLOAT,
	ACT_MP_DEATH_CRUSH,
	ACT_MP_RUN_SPEEDPAINT_PRIMARY,
	ACT_MP_DROWNING_PRIMARY,
	ACT_MP_LONG_FALL_PRIMARY,
	ACT_MP_TRACTORBEAM_FLOAT_PRIMARY,
	ACT_MP_DEATH_CRUSH_PRIMARY,
	ACT_DIE_STAND,
	ACT_DIE_STAND_HEADSHOT,
	ACT_DIE_CROUCH,
	ACT_DIE_CROUCH_HEADSHOT,
	ACT_CSGO_NULL,
	ACT_CSGO_DEFUSE,
	ACT_CSGO_DEFUSE_WITH_KIT,
	ACT_CSGO_FLASHBANG_REACTION,
	ACT_CSGO_FIRE_PRIMARY,
	ACT_CSGO_FIRE_PRIMARY_OPT_1,
	ACT_CSGO_FIRE_PRIMARY_OPT_2,
	ACT_CSGO_FIRE_SECONDARY,
	ACT_CSGO_FIRE_SECONDARY_OPT_1,
	ACT_CSGO_FIRE_SECONDARY_OPT_2,
	ACT_CSGO_RELOAD,
	ACT_CSGO_RELOAD_START,
	ACT_CSGO_RELOAD_LOOP,
	ACT_CSGO_RELOAD_END,
	ACT_CSGO_OPERATE,
	ACT_CSGO_DEPLOY,
	ACT_CSGO_CATCH,
	ACT_CSGO_SILENCER_DETACH,
	ACT_CSGO_SILENCER_ATTACH,
	ACT_CSGO_TWITCH,
	ACT_CSGO_TWITCH_BUYZONE,
	ACT_CSGO_PLANT_BOMB,
	ACT_CSGO_IDLE_TURN_BALANCEADJUST,
	ACT_CSGO_IDLE_ADJUST_STOPPEDMOVING,
	ACT_CSGO_ALIVE_LOOP,
	ACT_CSGO_FLINCH,
	ACT_CSGO_FLINCH_HEAD,
	ACT_CSGO_FLINCH_MOLOTOV,
	ACT_CSGO_JUMP,
	ACT_CSGO_FALL,
	ACT_CSGO_CLIMB_LADDER,
	ACT_CSGO_LAND_LIGHT,
	ACT_CSGO_LAND_HEAVY,
	ACT_CSGO_EXIT_LADDER_TOP,
	ACT_CSGO_EXIT_LADDER_BOTTOM
};
class vec2_t {
public:
	float x, y;

	vec2_t( float _x, float _y ) {
		x = _x;
		y = _y;
	}

	vec2_t( void ) {
		x = 0.0f;
		y = 0.0f;
	}

	~vec2_t( void ) {

	}

	vec2_t& operator=( const vec2_t& vec ) {
		x = vec.x; y = vec.y;
		return *this;
	}
};

class vec_t {
public:
	float x, y, z;

	vec_t( void ) {
		init( );
	}

	vec_t( float _x, float _y, float _z = 0.0f ) {
		x = _x;
		y = _y;
		z = _z;
	}

	void init( void ) {
		x = y = z = 0.0f;
	}

	void init( float _x, float _y, float _z ) {
		x = _x;
		y = _y;
		z = _z;
	}

	bool is_valid( void ) {
		return std::isfinite( x ) && std::isfinite( y ) && std::isfinite( z );
	}
	bool is_zero( void )
	{
		return vec_t( x, y, z ) == vec_t( 0, 0, 0 );
	}
	void invalidate( void ) {
		x = y = z = std::numeric_limits< float >::infinity( );
	}

	void clear( void ) {
		x = y = z = 0;
	}

	float& operator[]( int i )
	{
		return ( ( float* ) this ) [ i ];
	}

	float operator[]( int i ) const
	{
		return ( ( float* ) this ) [ i ];
	}

	void zero( void ) {
		x = y = z = 0.0f;
	}

	bool operator==( const vec_t& src ) const {
		return ( src.x == x ) && ( src.y == y ) && ( src.z == z );
	}

	bool operator!=( const vec_t& src ) const {
		return ( src.x != x ) || ( src.y != y ) || ( src.z != z );
	}

	vec_t& operator+=( const vec_t& v ) {
		x += v.x; y += v.y; z += v.z;
		return *this;
	}

	vec_t& operator-=( const vec_t& v ) {
		x -= v.x; y -= v.y; z -= v.z;
		return *this;
	}

	vec_t& operator*=( float fl ) {
		x *= fl;
		y *= fl;
		z *= fl;
		return *this;
	}

	vec_t& operator*=( const vec_t& v ) {
		x *= v.x;
		y *= v.y;
		z *= v.z;
		return *this;
	}

	vec_t& operator/=( const vec_t& v ) {
		x /= v.x;
		y /= v.y;
		z /= v.z;
		return *this;
	}

	vec_t& operator+=( float fl ) {
		x += fl;
		y += fl;
		z += fl;
		return *this;
	}

	vec_t& operator/=( float fl ) {
		x /= fl;
		y /= fl;
		z /= fl;
		return *this;
	}

	vec_t& operator-=( float fl ) {
		x -= fl;
		y -= fl;
		z -= fl;
		return *this;
	}

	void normalize( ) {

		*this = normalized( );
	}

	vec_t normalized( ) const {
		vec_t res = *this;
		float l = res.length( );

		if ( l != 0.0f )
			res /= l;
		else
			res.x = res.y = res.z = 0.0f;

		return res;
	}
	void normalize_place( ) {
		vec_t res = *this;
		float radius = ( sqrt ) ( x * x + y * y + z * z );

		// FLT_EPSILON is added to the radius to eliminate the possibility of divide by zero.
		float iradius = 1.f / ( radius + FLT_EPSILON );

		res.x *= iradius;
		res.y *= iradius;
		res.z *= iradius;


	}


	float dist_to( const vec_t& vec ) const {
		vec_t delta;

		delta.x = x - vec.x;
		delta.y = y - vec.y;
		delta.z = z - vec.z;

		return delta.length( );
	}

	float dist_to_sqr( const vec_t& vec ) const {
		vec_t delta;

		delta.x = x - vec.x;
		delta.y = y - vec.y;
		delta.z = z - vec.z;

		return delta.length_sqr( );
	}

	float dot_product( const vec_t& vec ) const {
		return ( x * vec.x + y * vec.y + z * vec.z );
	}

	vec_t cross_product( const vec_t& vec ) const {
		return vec_t( y * vec.z - z * vec.y, z * vec.x - x * vec.z, x * vec.y - y * vec.x );
	}

	float length( ) const {
		return sqrt( x*x + y * y + z * z );
	}

	float length_sqr( void ) const {
		return ( x*x + y * y + z * z );
	}

	float length_2d( ) const {
		return sqrt( x * x + y * y );
	}

	vec_t& operator=( const vec_t& vec ) {
		x = vec.x; y = vec.y; z = vec.z;
		return *this;
	}

	vec_t operator-( void ) const {
		return vec_t( -x, -y, -z );
	}

	vec_t operator+( const vec_t& v ) const {
		return vec_t( x + v.x, y + v.y, z + v.z );
	}

	vec_t operator-( const vec_t& v ) const {
		return vec_t( x - v.x, y - v.y, z - v.z );
	}

	vec_t operator*( float fl ) const {
		return vec_t( x * fl, y * fl, z * fl );
	}

	vec_t operator*( const vec_t& v ) const {
		return vec_t( x * v.x, y * v.y, z * v.z );
	}

	vec_t operator/( float fl ) const {
		return vec_t( x / fl, y / fl, z / fl );
	}

	vec_t operator/( const vec_t& v ) const {
		return vec_t( x / v.x, y / v.y, z / v.z );
	}
};

__forceinline vec_t operator*( float lhs, const vec_t& rhs ) {
	return rhs * lhs;
}

__forceinline vec_t operator/( float lhs, const vec_t& rhs ) {
	return rhs / lhs;
}

class __declspec( align( 16 ) ) vec_aligned_t : public vec_t {
public:
	float w;

	vec_aligned_t( void ) {

	}

	vec_aligned_t( float X, float Y, float Z ) {
		init( X, Y, Z );
	}

	explicit vec_aligned_t( const vec_t &otr ) {
		x = otr.x;
		y = otr.y;
		z = otr.z;
	}

	vec_aligned_t& operator=( const vec_t &otr ) {
		x = otr.x;
		y = otr.y;
		z = otr.z;

		return *this;
	}

	vec_aligned_t& operator=( const vec_aligned_t &otr ) {
		x = otr.x;
		y = otr.y;
		z = otr.z;

		return *this;
	}
};

enum preview_image_ret_val_t {
	material_preview_image_bad = 0,
	material_preview_image_ok,
	material_no_preview_image,
};

enum image_format_t {
	image_format_unknown = -1,
	image_format_rgba8888 = 0,
	image_format_abgr8888,
	image_format_rgb888,
	image_format_bgr888,
	image_format_rgb565,
	image_format_i8,
	image_format_ia88,
	image_format_p8,
	image_format_a8,
	image_format_rgb888_bluescreen,
	image_format_bgr888_bluescreen,
	image_format_argb8888,
	image_format_bgra8888,
	image_format_dxt1,
	image_format_dxt3,
	image_format_dxt5,
	image_format_bgrx8888,
	image_format_bgr565,
	image_format_bgrx5551,
	image_format_bgra4444,
	image_format_dxt1_onebitalpha,
	image_format_bgra5551,
	image_format_uv88,
	image_format_uvwq8888,
	image_format_rgba16161616f,
	image_format_rgba16161616,
	image_format_uvlx8888,
	image_format_r32f,
	image_format_rgb323232f,
	image_format_rgba32323232f,
	image_format_rg1616f,
	image_format_rg3232f,
	image_format_rgbx8888,

	image_format_null,

	image_format_ati2n,
	image_format_ati1n,

	image_format_rgba1010102,
	image_format_bgra1010102,
	image_format_r16f,

	image_format_d16,
	image_format_d15s1,
	image_format_d32,
	image_format_d24s8,
	image_format_linear_d24s8,
	image_format_d24x8,
	image_format_d24x4s4,
	image_format_d24fs8,
	image_format_d16_shadow,
	image_format_d24x8_shadow,

	image_format_linear_bgrx8888,
	image_format_linear_rgba8888,
	image_format_linear_abgr8888,
	image_format_linear_argb8888,
	image_format_linear_bgra8888,
	image_format_linear_rgb888,
	image_format_linear_bgr888,
	image_format_linear_bgrx5551,
	image_format_linear_i8,
	image_format_linear_rgba16161616,

	image_format_le_bgrx8888,
	image_format_le_bgra8888,

	num_image_formats
};

enum material_property_types_t
{
	material_property_needs_lightmap = 0,
	material_property_opacity,
	material_property_reflectivity,
	material_property_needs_bumped_lightmaps
};

class c_mat_unknown;

class c_material {
public:
	const char* get_name( void ) {
		return ( ( const char*( __thiscall* )( void* ) ) get_vfunc( this, 0 ) )( this );
	}

	const char* get_texture_group_name( void ) {
		return ( ( const char*( __thiscall* )( void* ) ) get_vfunc( this, 1 ) )( this );
	}

	void increment_reference_count( void ) {
		( ( void( __thiscall* )( void* ) ) get_vfunc( this, 12 ) )( this );
	}

	void alpha_modulate( float alpha ) {
		( ( void( __thiscall* )( void*, float ) ) get_vfunc( this, 27 ) )( this, alpha );
	}

	void color_modulate( float r, float g, float b ) {
		( ( void( __thiscall* )( void*, float, float, float ) ) get_vfunc( this, 28 ) )( this, r, g, b );
	}

	void set_material_var_flag( material_var_flags_t flag, bool state ) {
		( ( void( __thiscall* )( void*, material_var_flags_t, bool ) ) get_vfunc( this, 29 ) )( this, flag, state );
	}

	bool get_material_var_flag( material_var_flags_t flag ) {
		return ( ( bool( __thiscall* )( void*, material_var_flags_t ) ) get_vfunc( this, 30 ) )( this, flag );
	}

	float get_alpha_modulation( void ) {
		return ( ( float( __thiscall* )( void* ) ) get_vfunc( this, 44 ) )( this );
	}

	void get_color_modulation( float* r, float* g, float* b ) {
		( ( void( __thiscall* )( void*, float*, float*, float* ) ) get_vfunc( this, 45 ) )( this, r, g, b );
	}
};

class c_collideable {
private:
	virtual void vtpad_0( void ) = 0;

public:
	virtual vec_t& vec_mins( void ) = 0;
	virtual vec_t& vec_maxs( void ) = 0;
};

class c_csgoplayeranimstate {
public:
	char pad_0x0000 [ 0x18 ]; //0x0000
	float anim_update_timer; //0x0018 
	char pad_0x001C [ 0xC ]; //0x001C
	float started_moving_time; //0x0028 
	float last_move_time; //0x002C 
	char pad_0x0030 [ 0x10 ]; //0x0030
	float last_lby_time; //0x0040 
	char pad_0x0044 [ 0x8 ]; //0x0044
	float run_amount; //0x004C 
	char pad_0x0050 [ 0x10 ]; //0x0050
	void *entity; //0x0060 
	__int32 active_weapon; //0x0064 
	__int32 last_active_weapon; //0x0068 
	float last_client_side_animation_update_time; //0x006C 
	__int32 last_client_side_animation_update_framecount; //0x0070 
	float eye_timer; //0x0074 
	float eye_angles_y; //0x0078 
	float eye_angles_x; //0x007C 
	float goal_feet_yaw; //0x0080 
	float current_feet_yaw; //0x0084 
	float torso_yaw; //0x0088 
	float last_move_yaw; //0x008C 
	float lean_amount; //0x0090 
	char pad_0x0094 [ 0x4 ]; //0x0094
	float feet_cycle; //0x0098 
	float feet_yaw_rate; //0x009C 
	char pad_0x00A0 [ 0x4 ]; //0x00A0
	float duck_amount; //0x00A4 
	float landing_duck_amount; //0x00A8 
	char pad_0x00AC [ 0x4 ]; //0x00AC
	vec_t current_origin;
	vec_t last_origin;
	float velocity_x; //0x00C8 
	float velocity_y; //0x00CC 
	char pad_0x00D0 [ 0x10 ]; //0x00D0
	float move_direction_1; //0x00E0 
	float move_direction_2; //0x00E4 
	char pad_0x00E8 [ 0x4 ]; //0x00E8
	float m_velocity; //0x00EC 
	float jump_fall_velocity; //0x00F0 
	float clamped_velocity; //0x00F4 
	float feet_speed_forwards_or_sideways; //0x00F8 
	float feet_speed_unknown_forwards_or_sideways; //0x00FC 
	float last_time_started_moving; //0x0100 
	float last_time_stopped_moving; //0x0104 
	bool on_ground; //0x0108 
	bool hit_in_ground_animation; //0x010C 
	char pad_0x0110 [ 0x4 ]; //0x0110
	float last_origin_z; //0x0114 
	float head_from_ground_distance_standing; //0x0118 
	float stop_to_full_running_fraction; //0x011C 
	char pad_0x0120 [ 0x14 ]; //0x0120
	__int32 is_not_moving; //0x0134 
	char pad_0x0138 [ 0x20 ]; //0x0138
	float last_anim_update_time; //0x0158 
	float moving_direction_x; //0x015C 
	float moving_direction_y; //0x0160 
	float moving_direction_z; //0x0164 
	char pad_0x0168 [ 0x44 ]; //0x0168
	__int32 started_moving; //0x01AC 
	char pad_0x01B0 [ 0x8 ]; //0x01B0
	float lean_yaw; //0x01B8 
	char pad_0x01BC [ 0x8 ]; //0x01BC
	float poses_speed; //0x01C4 
	char pad_0x01C8 [ 0x8 ]; //0x01C8
	float ladder_speed; //0x01D0 
	char pad_0x01D4 [ 0x8 ]; //0x01D4
	float ladder_yaw; //0x01DC 
	char pad_0x01E0 [ 0x8 ]; //0x01E0
	float some_pose; //0x01E8 
	char pad_0x01EC [ 0x14 ]; //0x01EC
	float body_yaw; //0x0200 
	char pad_0x0204 [ 0x8 ]; //0x0204
	float body_pitch; //0x020C 
	char pad_0x0210 [ 0x8 ]; //0x0210
	float death_yaw; //0x0218 
	char pad_0x021C [ 0x8 ]; //0x021C
	float stand; //0x0224 
	char pad_0x0228 [ 0x8 ]; //0x0228
	float jump_fall; //0x0230 
	char pad_0x0234 [ 0x8 ]; //0x0234
	float aim_blend_stand_idle; //0x023C 
	char pad_0x0240 [ 0x8 ]; //0x0240
	float aim_blend_crouch_idle; //0x0248 
	char pad_0x024C [ 0x8 ]; //0x024C
	float strafe_yaw; //0x0254 
	char pad_0x0258 [ 0x8 ]; //0x0258
	float aim_blend_stand_walk; //0x0260 
	char pad_0x0264 [ 0x8 ]; //0x0264
	float aim_blend_stand_run; //0x026C 
	char pad_0x0270 [ 0x8 ]; //0x0270
	float aim_blend_crouch_walk; //0x0278 
	char pad_0x027C [ 0x8 ]; //0x027C
	float move_blend_walk; //0x0284 
	char pad_0x0288 [ 0x8 ]; //0x0288
	float move_blend_run; //0x0290 
	char pad_0x0294 [ 0x8 ]; //0x0294
	float move_blend_crouch; //0x029C 
	char pad_0x02A0 [ 0x4 ]; //0x02A0
	float speed; //0x02A4 
	__int32 moving_in_any_direction; //0x02A8 
	float acceleration; //0x02AC 
	char pad_0x02B0 [ 0x74 ]; //0x02B0
	float crouch_height; //0x0324 
	__int32 is_full_crouched; //0x0328 
	char pad_0x032C [ 0x4 ]; //0x032C
	float velocity_subtract_x; //0x0330 
	float velocity_subtract_y; //0x0334 
	float velocity_subtract_z; //0x0338 
	float standing_head_height; //0x033C 
	char pad_0x0340 [ 0x4 ]; //0x0340


	/*void update( float y, float x ) {
		static auto update = utils->find_pattern( "client_panorama.dll", "55 8B EC 83 E4 F8 83 EC 18 56 57 8B F9 F3 0F 11 54" );

		_asm {
			mov ecx, this
			movss xmm1, y
			movss xmm2, x
			call update
		}
	}*/
};

class animationlayer {
public:
	byte	pad_0x0 [ 0x14 ];
	int		order;
	int		sequence;
	float	previous_cycle;
	float	weight;
	float	weight_delta_rate;
	float	playback_rate;
	float	cycle;
	void*	owner;
	byte	pad_0x38 [ 0x4 ];
};
inline int UtlMemory_CalcNewAllocationCount( int nAllocationCount, int nGrowSize, int nNewSize, int nBytesItem )
{
	if ( nGrowSize )
		nAllocationCount = ( ( 1 + ( ( nNewSize - 1 ) / nGrowSize ) ) * nGrowSize );
	else
	{
		if ( !nAllocationCount )
			nAllocationCount = ( 31 + nBytesItem ) / nBytesItem;

		while ( nAllocationCount < nNewSize )
			nAllocationCount *= 2;
	}

	return nAllocationCount;
}

template< class T, class I = int >
class c_utl_memory
{
public:
	T& operator[]( I i )
	{
		return m_pMemory [ i ];
	}

	T* Base( )
	{
		return m_pMemory;
	}

	int NumAllocated( ) const
	{
		return m_nAllocationCount;
	}

	void Grow( int num = 1 )
	{
		if ( IsExternallyAllocated( ) )
			return;

		int nAllocationRequested = m_nAllocationCount + num;
		int nNewAllocationCount = UtlMemory_CalcNewAllocationCount( m_nAllocationCount, m_nGrowSize, nAllocationRequested, sizeof( T ) );

		if ( ( int ) ( I ) nNewAllocationCount < nAllocationRequested )
		{
			if ( ( int ) ( I ) nNewAllocationCount == 0 && ( int ) ( I ) ( nNewAllocationCount - 1 ) >= nAllocationRequested )
				--nNewAllocationCount;
			else
			{
				if ( ( int ) ( I ) nAllocationRequested != nAllocationRequested )
					return;

				while ( ( int ) ( I ) nNewAllocationCount < nAllocationRequested )
					nNewAllocationCount = ( nNewAllocationCount + nAllocationRequested ) / 2;
			}
		}

		m_nAllocationCount = nNewAllocationCount;

		if ( m_pMemory )
			m_pMemory = ( T* ) realloc( m_pMemory, m_nAllocationCount * sizeof( T ) );
		else
			m_pMemory = ( T* ) malloc( m_nAllocationCount * sizeof( T ) );
	}

	bool IsExternallyAllocated( ) const
	{
		return m_nGrowSize < 0;
	}

protected:
	T* m_pMemory;
	int m_nAllocationCount;
	int m_nGrowSize;
};

template <class T>
inline T* Construct( T* pMemory )
{
	return ::new( pMemory ) T;
}

template <class T>
inline void Destruct( T* pMemory )
{
	pMemory->~T( );
}
template< class T, class A = c_utl_memory<T> >
class c_utl_vector
{
	typedef A CAllocator;
public:
	T& operator[]( int i )
	{
		return m_Memory [ i ];
	}

	T& Element( int i )
	{
		return m_Memory [ i ];
	}

	T* Base( )
	{
		return m_Memory.Base( );
	}

	int Count( ) const
	{
		return m_Size;
	}

	void RemoveAll( )
	{
		for ( int i = m_Size; --i >= 0; )
			Destruct( &Element( i ) );

		m_Size = 0;
	}

	int AddToTail( )
	{
		return InsertBefore( m_Size );
	}

	int InsertBefore( int elem )
	{
		GrowVector( );
		ShiftElementsRight( elem );
		Construct( &Element( elem ) );

		return elem;
	}

protected:
	void GrowVector( int num = 1 )
	{
		if ( m_Size + num > m_Memory.NumAllocated( ) )
			m_Memory.Grow( m_Size + num - m_Memory.NumAllocated( ) );

		m_Size += num;
		ResetDbgInfo( );
	}

	void ShiftElementsRight( int elem, int num = 1 )
	{
		int numToMove = m_Size - elem - num;
		if ( ( numToMove > 0 ) && ( num > 0 ) )
			memmove( &Element( elem + num ), &Element( elem ), numToMove * sizeof( T ) );
	}

	CAllocator m_Memory;
	int m_Size;

	T* m_pElements;

	inline void ResetDbgInfo( )
	{
		m_pElements = Base( );
	}
};
enum c_csgoplayeractivity
{
	ACT_RESET,
	ACT_IDLE,
	ACT_TRANSITION,
	ACT_COVER,
	ACT_COVER_MED,
	ACT_COVER_LOW,
	ACT_WALK,
	ACT_WALK_AIM,
	ACT_WALK_CROUCH,
	ACT_WALK_CROUCH_AIM,
	ACT_RUN,
	ACT_RUN_AIM,
	ACT_RUN_CROUCH,
	ACT_RUN_CROUCH_AIM,
	ACT_RUN_PROTECTED,
	ACT_SCRIPT_CUSTOM_MOVE,
	ACT_RANGE_ATTACK1,
	ACT_RANGE_ATTACK2,
	ACT_RANGE_ATTACK1_LOW,
	ACT_RANGE_ATTACK2_LOW,
	ACT_DIESIMPLE,
	ACT_DIEBACKWARD,
	ACT_DIEFORWARD,
	ACT_DIEVIOLENT,
	ACT_DIERAGDOLL,
	ACT_FLY,
	ACT_HOVER,
	ACT_GLIDE,
	ACT_SWIM,
	ACT_JUMP,
	ACT_HOP,
	ACT_LEAP,
	ACT_LAND,
	ACT_CLIMB_UP,
	ACT_CLIMB_DOWN,
	ACT_CLIMB_DISMOUNT,
	ACT_SHIPLADDER_UP,
	ACT_SHIPLADDER_DOWN,
	ACT_STRAFE_LEFT,
	ACT_STRAFE_RIGHT,
	ACT_ROLL_LEFT,
	ACT_ROLL_RIGHT,
	ACT_TURN_LEFT,
	ACT_TURN_RIGHT,
	ACT_CROUCH,
	ACT_CROUCHIDLE,
	ACT_STAND,
	ACT_USE,
	ACT_ALIEN_BURROW_IDLE,
	ACT_ALIEN_BURROW_OUT,
	ACT_SIGNAL1,
	ACT_SIGNAL2,
	ACT_SIGNAL3,
	ACT_SIGNAL_ADVANCE,
	ACT_SIGNAL_FORWARD,
	ACT_SIGNAL_GROUP,
	ACT_SIGNAL_HALT,
	ACT_SIGNAL_LEFT,
	ACT_SIGNAL_RIGHT,
	ACT_SIGNAL_TAKECOVER,
	ACT_LOOKBACK_RIGHT,
	ACT_LOOKBACK_LEFT,
	ACT_COWER,
	ACT_SMALL_FLINCH,
	ACT_BIG_FLINCH,
	ACT_MELEE_ATTACK1,
	ACT_MELEE_ATTACK2,
	ACT_RELOAD,
	ACT_RELOAD_START,
	ACT_RELOAD_FINISH,
	ACT_RELOAD_LOW,
	ACT_ARM,
	ACT_DISARM,
	ACT_DROP_WEAPON,
	ACT_DROP_WEAPON_SHOTGUN,
	ACT_PICKUP_GROUND,
	ACT_PICKUP_RACK,
	ACT_IDLE_ANGRY,
	ACT_IDLE_RELAXED,
	ACT_IDLE_STIMULATED,
	ACT_IDLE_AGITATED,
	ACT_IDLE_STEALTH,
	ACT_IDLE_HURT,
	ACT_WALK_RELAXED,
	ACT_WALK_STIMULATED,
	ACT_WALK_AGITATED,
	ACT_WALK_STEALTH,
	ACT_RUN_RELAXED,
	ACT_RUN_STIMULATED,
	ACT_RUN_AGITATED,
	ACT_RUN_STEALTH,
	ACT_IDLE_AIM_RELAXED,
	ACT_IDLE_AIM_STIMULATED,
	ACT_IDLE_AIM_AGITATED,
	ACT_IDLE_AIM_STEALTH,
	ACT_WALK_AIM_RELAXED,
	ACT_WALK_AIM_STIMULATED,
	ACT_WALK_AIM_AGITATED,
	ACT_WALK_AIM_STEALTH,
	ACT_RUN_AIM_RELAXED,
	ACT_RUN_AIM_STIMULATED,
	ACT_RUN_AIM_AGITATED,
	ACT_RUN_AIM_STEALTH,
	ACT_CROUCHIDLE_STIMULATED,
	ACT_CROUCHIDLE_AIM_STIMULATED,
	ACT_CROUCHIDLE_AGITATED,
	ACT_WALK_HURT,
	ACT_RUN_HURT,
	ACT_SPECIAL_ATTACK1,
	ACT_SPECIAL_ATTACK2,
	ACT_COMBAT_IDLE,
	ACT_WALK_SCARED,
	ACT_RUN_SCARED,
	ACT_VICTORY_DANCE,
	ACT_DIE_HEADSHOT,
	ACT_DIE_CHESTSHOT,
	ACT_DIE_GUTSHOT,
	ACT_DIE_BACKSHOT,
	ACT_FLINCH_HEAD,
	ACT_FLINCH_CHEST,
	ACT_FLINCH_STOMACH,
	ACT_FLINCH_LEFTARM,
	ACT_FLINCH_RIGHTARM,
	ACT_FLINCH_LEFTLEG,
	ACT_FLINCH_RIGHTLEG,
	ACT_FLINCH_PHYSICS,
	ACT_FLINCH_HEAD_BACK,
	ACT_FLINCH_HEAD_LEFT,
	ACT_FLINCH_HEAD_RIGHT,
	ACT_FLINCH_CHEST_BACK,
	ACT_FLINCH_STOMACH_BACK,
	ACT_FLINCH_CROUCH_FRONT,
	ACT_FLINCH_CROUCH_BACK,
	ACT_FLINCH_CROUCH_LEFT,
	ACT_FLINCH_CROUCH_RIGHT,
	ACT_IDLE_ON_FIRE,
	ACT_WALK_ON_FIRE,
	ACT_RUN_ON_FIRE,
	ACT_RAPPEL_LOOP,
	ACT_180_LEFT,
	ACT_180_RIGHT,
	ACT_90_LEFT,
	ACT_90_RIGHT,
	ACT_STEP_LEFT,
	ACT_STEP_RIGHT,
	ACT_STEP_BACK,
	ACT_STEP_FORE,
	ACT_GESTURE_RANGE_ATTACK1,
	ACT_GESTURE_RANGE_ATTACK2,
	ACT_GESTURE_MELEE_ATTACK1,
	ACT_GESTURE_MELEE_ATTACK2,
	ACT_GESTURE_RANGE_ATTACK1_LOW,
	ACT_GESTURE_RANGE_ATTACK2_LOW,
	ACT_MELEE_ATTACK_SWING_GESTURE,
	ACT_GESTURE_SMALL_FLINCH,
	ACT_GESTURE_BIG_FLINCH,
	ACT_GESTURE_FLINCH_BLAST,
	ACT_GESTURE_FLINCH_BLAST_SHOTGUN,
	ACT_GESTURE_FLINCH_BLAST_DAMAGED,
	ACT_GESTURE_FLINCH_BLAST_DAMAGED_SHOTGUN,
	ACT_GESTURE_FLINCH_HEAD,
	ACT_GESTURE_FLINCH_CHEST,
	ACT_GESTURE_FLINCH_STOMACH,
	ACT_GESTURE_FLINCH_LEFTARM,
	ACT_GESTURE_FLINCH_RIGHTARM,
	ACT_GESTURE_FLINCH_LEFTLEG,
	ACT_GESTURE_FLINCH_RIGHTLEG,
	ACT_GESTURE_TURN_LEFT,
	ACT_GESTURE_TURN_RIGHT,
	ACT_GESTURE_TURN_LEFT45,
	ACT_GESTURE_TURN_RIGHT45,
	ACT_GESTURE_TURN_LEFT90,
	ACT_GESTURE_TURN_RIGHT90,
	ACT_GESTURE_TURN_LEFT45_FLAT,
	ACT_GESTURE_TURN_RIGHT45_FLAT,
	ACT_GESTURE_TURN_LEFT90_FLAT,
	ACT_GESTURE_TURN_RIGHT90_FLAT,
	ACT_BARNACLE_HIT,
	ACT_BARNACLE_PULL,
	ACT_BARNACLE_CHOMP,
	ACT_BARNACLE_CHEW,
	ACT_DO_NOT_DISTURB,
	ACT_SPECIFIC_SEQUENCE,
	ACT_VM_DRAW,
	ACT_VM_HOLSTER,
	ACT_VM_IDLE,
	ACT_VM_FIDGET,
	ACT_VM_PULLBACK,
	ACT_VM_PULLBACK_HIGH,
	ACT_VM_PULLBACK_LOW,
	ACT_VM_THROW,
	ACT_VM_PULLPIN,
	ACT_VM_PRIMARYATTACK,
	ACT_VM_SECONDARYATTACK,
	ACT_VM_RELOAD,
	ACT_VM_DRYFIRE,
	ACT_VM_HITLEFT,
	ACT_VM_HITLEFT2,
	ACT_VM_HITRIGHT,
	ACT_VM_HITRIGHT2,
	ACT_VM_HITCENTER,
	ACT_VM_HITCENTER2,
	ACT_VM_MISSLEFT,
	ACT_VM_MISSLEFT2,
	ACT_VM_MISSRIGHT,
	ACT_VM_MISSRIGHT2,
	ACT_VM_MISSCENTER,
	ACT_VM_MISSCENTER2,
	ACT_VM_HAULBACK,
	ACT_VM_SWINGHARD,
	ACT_VM_SWINGMISS,
	ACT_VM_SWINGHIT,
	ACT_VM_IDLE_TO_LOWERED,
	ACT_VM_IDLE_LOWERED,
	ACT_VM_LOWERED_TO_IDLE,
	ACT_VM_RECOIL1,
	ACT_VM_RECOIL2,
	ACT_VM_RECOIL3,
	ACT_VM_PICKUP,
	ACT_VM_RELEASE,
	ACT_VM_ATTACH_SILENCER,
	ACT_VM_DETACH_SILENCER,
	ACT_VM_EMPTY_FIRE,
	ACT_VM_EMPTY_RELOAD,
	ACT_VM_EMPTY_DRAW,
	ACT_VM_EMPTY_IDLE,
	ACT_SLAM_STICKWALL_IDLE,
	ACT_SLAM_STICKWALL_ND_IDLE,
	ACT_SLAM_STICKWALL_ATTACH,
	ACT_SLAM_STICKWALL_ATTACH2,
	ACT_SLAM_STICKWALL_ND_ATTACH,
	ACT_SLAM_STICKWALL_ND_ATTACH2,
	ACT_SLAM_STICKWALL_DETONATE,
	ACT_SLAM_STICKWALL_DETONATOR_HOLSTER,
	ACT_SLAM_STICKWALL_DRAW,
	ACT_SLAM_STICKWALL_ND_DRAW,
	ACT_SLAM_STICKWALL_TO_THROW,
	ACT_SLAM_STICKWALL_TO_THROW_ND,
	ACT_SLAM_STICKWALL_TO_TRIPMINE_ND,
	ACT_SLAM_THROW_IDLE,
	ACT_SLAM_THROW_ND_IDLE,
	ACT_SLAM_THROW_THROW,
	ACT_SLAM_THROW_THROW2,
	ACT_SLAM_THROW_THROW_ND,
	ACT_SLAM_THROW_THROW_ND2,
	ACT_SLAM_THROW_DRAW,
	ACT_SLAM_THROW_ND_DRAW,
	ACT_SLAM_THROW_TO_STICKWALL,
	ACT_SLAM_THROW_TO_STICKWALL_ND,
	ACT_SLAM_THROW_DETONATE,
	ACT_SLAM_THROW_DETONATOR_HOLSTER,
	ACT_SLAM_THROW_TO_TRIPMINE_ND,
	ACT_SLAM_TRIPMINE_IDLE,
	ACT_SLAM_TRIPMINE_DRAW,
	ACT_SLAM_TRIPMINE_ATTACH,
	ACT_SLAM_TRIPMINE_ATTACH2,
	ACT_SLAM_TRIPMINE_TO_STICKWALL_ND,
	ACT_SLAM_TRIPMINE_TO_THROW_ND,
	ACT_SLAM_DETONATOR_IDLE,
	ACT_SLAM_DETONATOR_DRAW,
	ACT_SLAM_DETONATOR_DETONATE,
	ACT_SLAM_DETONATOR_HOLSTER,
	ACT_SLAM_DETONATOR_STICKWALL_DRAW,
	ACT_SLAM_DETONATOR_THROW_DRAW,
	ACT_SHOTGUN_RELOAD_START,
	ACT_SHOTGUN_RELOAD_FINISH,
	ACT_SHOTGUN_PUMP,
	ACT_SMG2_IDLE2,
	ACT_SMG2_FIRE2,
	ACT_SMG2_DRAW2,
	ACT_SMG2_RELOAD2,
	ACT_SMG2_DRYFIRE2,
	ACT_SMG2_TOAUTO,
	ACT_SMG2_TOBURST,
	ACT_PHYSCANNON_UPGRADE,
	ACT_RANGE_ATTACK_AR1,
	ACT_RANGE_ATTACK_AR2,
	ACT_RANGE_ATTACK_AR2_LOW,
	ACT_RANGE_ATTACK_AR2_GRENADE,
	ACT_RANGE_ATTACK_HMG1,
	ACT_RANGE_ATTACK_ML,
	ACT_RANGE_ATTACK_SMG1,
	ACT_RANGE_ATTACK_SMG1_LOW,
	ACT_RANGE_ATTACK_SMG2,
	ACT_RANGE_ATTACK_SHOTGUN,
	ACT_RANGE_ATTACK_SHOTGUN_LOW,
	ACT_RANGE_ATTACK_PISTOL,
	ACT_RANGE_ATTACK_PISTOL_LOW,
	ACT_RANGE_ATTACK_SLAM,
	ACT_RANGE_ATTACK_TRIPWIRE,
	ACT_RANGE_ATTACK_THROW,
	ACT_RANGE_ATTACK_SNIPER_RIFLE,
	ACT_RANGE_ATTACK_RPG,
	ACT_MELEE_ATTACK_SWING,
	ACT_RANGE_AIM_LOW,
	ACT_RANGE_AIM_SMG1_LOW,
	ACT_RANGE_AIM_PISTOL_LOW,
	ACT_RANGE_AIM_AR2_LOW,
	ACT_COVER_PISTOL_LOW,
	ACT_COVER_SMG1_LOW,
	ACT_GESTURE_RANGE_ATTACK_AR1,
	ACT_GESTURE_RANGE_ATTACK_AR2,
	ACT_GESTURE_RANGE_ATTACK_AR2_GRENADE,
	ACT_GESTURE_RANGE_ATTACK_HMG1,
	ACT_GESTURE_RANGE_ATTACK_ML,
	ACT_GESTURE_RANGE_ATTACK_SMG1,
	ACT_GESTURE_RANGE_ATTACK_SMG1_LOW,
	ACT_GESTURE_RANGE_ATTACK_SMG2,
	ACT_GESTURE_RANGE_ATTACK_SHOTGUN,
	ACT_GESTURE_RANGE_ATTACK_PISTOL,
	ACT_GESTURE_RANGE_ATTACK_PISTOL_LOW,
	ACT_GESTURE_RANGE_ATTACK_SLAM,
	ACT_GESTURE_RANGE_ATTACK_TRIPWIRE,
	ACT_GESTURE_RANGE_ATTACK_THROW,
	ACT_GESTURE_RANGE_ATTACK_SNIPER_RIFLE,
	ACT_GESTURE_MELEE_ATTACK_SWING,
	ACT_IDLE_RIFLE,
	ACT_IDLE_SMG1,
	ACT_IDLE_ANGRY_SMG1,
	ACT_IDLE_PISTOL,
	ACT_IDLE_ANGRY_PISTOL,
	ACT_IDLE_ANGRY_SHOTGUN,
	ACT_IDLE_STEALTH_PISTOL,
	ACT_IDLE_PACKAGE,
	ACT_WALK_PACKAGE,
	ACT_IDLE_SUITCASE,
	ACT_WALK_SUITCASE,
	ACT_IDLE_SMG1_RELAXED,
	ACT_IDLE_SMG1_STIMULATED,
	ACT_WALK_RIFLE_RELAXED,
	ACT_RUN_RIFLE_RELAXED,
	ACT_WALK_RIFLE_STIMULATED,
	ACT_RUN_RIFLE_STIMULATED,
	ACT_IDLE_AIM_RIFLE_STIMULATED,
	ACT_WALK_AIM_RIFLE_STIMULATED,
	ACT_RUN_AIM_RIFLE_STIMULATED,
	ACT_IDLE_SHOTGUN_RELAXED,
	ACT_IDLE_SHOTGUN_STIMULATED,
	ACT_IDLE_SHOTGUN_AGITATED,
	ACT_WALK_ANGRY,
	ACT_POLICE_HARASS1,
	ACT_POLICE_HARASS2,
	ACT_IDLE_MANNEDGUN,
	ACT_IDLE_MELEE,
	ACT_IDLE_ANGRY_MELEE,
	ACT_IDLE_RPG_RELAXED,
	ACT_IDLE_RPG,
	ACT_IDLE_ANGRY_RPG,
	ACT_COVER_LOW_RPG,
	ACT_WALK_RPG,
	ACT_RUN_RPG,
	ACT_WALK_CROUCH_RPG,
	ACT_RUN_CROUCH_RPG,
	ACT_WALK_RPG_RELAXED,
	ACT_RUN_RPG_RELAXED,
	ACT_WALK_RIFLE,
	ACT_WALK_AIM_RIFLE,
	ACT_WALK_CROUCH_RIFLE,
	ACT_WALK_CROUCH_AIM_RIFLE,
	ACT_RUN_RIFLE,
	ACT_RUN_AIM_RIFLE,
	ACT_RUN_CROUCH_RIFLE,
	ACT_RUN_CROUCH_AIM_RIFLE,
	ACT_RUN_STEALTH_PISTOL,
	ACT_WALK_AIM_SHOTGUN,
	ACT_RUN_AIM_SHOTGUN,
	ACT_WALK_PISTOL,
	ACT_RUN_PISTOL,
	ACT_WALK_AIM_PISTOL,
	ACT_RUN_AIM_PISTOL,
	ACT_WALK_STEALTH_PISTOL,
	ACT_WALK_AIM_STEALTH_PISTOL,
	ACT_RUN_AIM_STEALTH_PISTOL,
	ACT_RELOAD_PISTOL,
	ACT_RELOAD_PISTOL_LOW,
	ACT_RELOAD_SMG1,
	ACT_RELOAD_SMG1_LOW,
	ACT_RELOAD_SHOTGUN,
	ACT_RELOAD_SHOTGUN_LOW,
	ACT_GESTURE_RELOAD,
	ACT_GESTURE_RELOAD_PISTOL,
	ACT_GESTURE_RELOAD_SMG1,
	ACT_GESTURE_RELOAD_SHOTGUN,
	ACT_BUSY_LEAN_LEFT,
	ACT_BUSY_LEAN_LEFT_ENTRY,
	ACT_BUSY_LEAN_LEFT_EXIT,
	ACT_BUSY_LEAN_BACK,
	ACT_BUSY_LEAN_BACK_ENTRY,
	ACT_BUSY_LEAN_BACK_EXIT,
	ACT_BUSY_SIT_GROUND,
	ACT_BUSY_SIT_GROUND_ENTRY,
	ACT_BUSY_SIT_GROUND_EXIT,
	ACT_BUSY_SIT_CHAIR,
	ACT_BUSY_SIT_CHAIR_ENTRY,
	ACT_BUSY_SIT_CHAIR_EXIT,
	ACT_BUSY_STAND,
	ACT_BUSY_QUEUE,
	ACT_DUCK_DODGE,
	ACT_DIE_BARNACLE_SWALLOW,
	ACT_GESTURE_BARNACLE_STRANGLE,
	ACT_PHYSCANNON_DETACH,
	ACT_PHYSCANNON_ANIMATE,
	ACT_PHYSCANNON_ANIMATE_PRE,
	ACT_PHYSCANNON_ANIMATE_POST,
	ACT_DIE_FRONTSIDE,
	ACT_DIE_RIGHTSIDE,
	ACT_DIE_BACKSIDE,
	ACT_DIE_LEFTSIDE,
	ACT_DIE_CROUCH_FRONTSIDE,
	ACT_DIE_CROUCH_RIGHTSIDE,
	ACT_DIE_CROUCH_BACKSIDE,
	ACT_DIE_CROUCH_LEFTSIDE,
	ACT_OPEN_DOOR,
	ACT_DI_ALYX_ZOMBIE_MELEE,
	ACT_DI_ALYX_ZOMBIE_TORSO_MELEE,
	ACT_DI_ALYX_HEADCRAB_MELEE,
	ACT_DI_ALYX_ANTLION,
	ACT_DI_ALYX_ZOMBIE_SHOTGUN64,
	ACT_DI_ALYX_ZOMBIE_SHOTGUN26,
	ACT_READINESS_RELAXED_TO_STIMULATED,
	ACT_READINESS_RELAXED_TO_STIMULATED_WALK,
	ACT_READINESS_AGITATED_TO_STIMULATED,
	ACT_READINESS_STIMULATED_TO_RELAXED,
	ACT_READINESS_PISTOL_RELAXED_TO_STIMULATED,
	ACT_READINESS_PISTOL_RELAXED_TO_STIMULATED_WALK,
	ACT_READINESS_PISTOL_AGITATED_TO_STIMULATED,
	ACT_READINESS_PISTOL_STIMULATED_TO_RELAXED,
	ACT_IDLE_CARRY,
	ACT_WALK_CARRY,
	ACT_STARTDYING,
	ACT_DYINGLOOP,
	ACT_DYINGTODEAD,
	ACT_RIDE_MANNED_GUN,
	ACT_VM_SPRINT_ENTER,
	ACT_VM_SPRINT_IDLE,
	ACT_VM_SPRINT_LEAVE,
	ACT_FIRE_START,
	ACT_FIRE_LOOP,
	ACT_FIRE_END,
	ACT_CROUCHING_GRENADEIDLE,
	ACT_CROUCHING_GRENADEREADY,
	ACT_CROUCHING_PRIMARYATTACK,
	ACT_OVERLAY_GRENADEIDLE,
	ACT_OVERLAY_GRENADEREADY,
	ACT_OVERLAY_PRIMARYATTACK,
	ACT_OVERLAY_SHIELD_UP,
	ACT_OVERLAY_SHIELD_DOWN,
	ACT_OVERLAY_SHIELD_UP_IDLE,
	ACT_OVERLAY_SHIELD_ATTACK,
	ACT_OVERLAY_SHIELD_KNOCKBACK,
	ACT_SHIELD_UP,
	ACT_SHIELD_DOWN,
	ACT_SHIELD_UP_IDLE,
	ACT_SHIELD_ATTACK,
	ACT_SHIELD_KNOCKBACK,
	ACT_CROUCHING_SHIELD_UP,
	ACT_CROUCHING_SHIELD_DOWN,
	ACT_CROUCHING_SHIELD_UP_IDLE,
	ACT_CROUCHING_SHIELD_ATTACK,
	ACT_CROUCHING_SHIELD_KNOCKBACK,
	ACT_TURNRIGHT45,
	ACT_TURNLEFT45,
	ACT_TURN,
	ACT_OBJ_ASSEMBLING,
	ACT_OBJ_DISMANTLING,
	ACT_OBJ_STARTUP,
	ACT_OBJ_RUNNING,
	ACT_OBJ_IDLE,
	ACT_OBJ_PLACING,
	ACT_OBJ_DETERIORATING,
	ACT_OBJ_UPGRADING,
	ACT_DEPLOY,
	ACT_DEPLOY_IDLE,
	ACT_UNDEPLOY,
	ACT_CROSSBOW_DRAW_UNLOADED,
	ACT_GAUSS_SPINUP,
	ACT_GAUSS_SPINCYCLE,
	ACT_VM_PRIMARYATTACK_SILENCED,
	ACT_VM_RELOAD_SILENCED,
	ACT_VM_DRYFIRE_SILENCED,
	ACT_VM_IDLE_SILENCED,
	ACT_VM_DRAW_SILENCED,
	ACT_VM_IDLE_EMPTY_LEFT,
	ACT_VM_DRYFIRE_LEFT,
	ACT_VM_IS_DRAW,
	ACT_VM_IS_HOLSTER,
	ACT_VM_IS_IDLE,
	ACT_VM_IS_PRIMARYATTACK,
	ACT_PLAYER_IDLE_FIRE,
	ACT_PLAYER_CROUCH_FIRE,
	ACT_PLAYER_CROUCH_WALK_FIRE,
	ACT_PLAYER_WALK_FIRE,
	ACT_PLAYER_RUN_FIRE,
	ACT_IDLETORUN,
	ACT_RUNTOIDLE,
	ACT_VM_DRAW_DEPLOYED,
	ACT_HL2MP_IDLE_MELEE,
	ACT_HL2MP_RUN_MELEE,
	ACT_HL2MP_IDLE_CROUCH_MELEE,
	ACT_HL2MP_WALK_CROUCH_MELEE,
	ACT_HL2MP_GESTURE_RANGE_ATTACK_MELEE,
	ACT_HL2MP_GESTURE_RELOAD_MELEE,
	ACT_HL2MP_JUMP_MELEE,
	ACT_VM_FIZZLE,
	ACT_MP_STAND_IDLE,
	ACT_MP_CROUCH_IDLE,
	ACT_MP_CROUCH_DEPLOYED_IDLE,
	ACT_MP_CROUCH_DEPLOYED,
	ACT_MP_DEPLOYED_IDLE,
	ACT_MP_RUN,
	ACT_MP_WALK,
	ACT_MP_AIRWALK,
	ACT_MP_CROUCHWALK,
	ACT_MP_SPRINT,
	ACT_MP_JUMP,
	ACT_MP_JUMP_START,
	ACT_MP_JUMP_FLOAT,
	ACT_MP_JUMP_LAND,
	ACT_MP_JUMP_IMPACT_N,
	ACT_MP_JUMP_IMPACT_E,
	ACT_MP_JUMP_IMPACT_W,
	ACT_MP_JUMP_IMPACT_S,
	ACT_MP_JUMP_IMPACT_TOP,
	ACT_MP_DOUBLEJUMP,
	ACT_MP_SWIM,
	ACT_MP_DEPLOYED,
	ACT_MP_SWIM_DEPLOYED,
	ACT_MP_VCD,
	ACT_MP_ATTACK_STAND_PRIMARYFIRE,
	ACT_MP_ATTACK_STAND_PRIMARYFIRE_DEPLOYED,
	ACT_MP_ATTACK_STAND_SECONDARYFIRE,
	ACT_MP_ATTACK_STAND_GRENADE,
	ACT_MP_ATTACK_CROUCH_PRIMARYFIRE,
	ACT_MP_ATTACK_CROUCH_PRIMARYFIRE_DEPLOYED,
	ACT_MP_ATTACK_CROUCH_SECONDARYFIRE,
	ACT_MP_ATTACK_CROUCH_GRENADE,
	ACT_MP_ATTACK_SWIM_PRIMARYFIRE,
	ACT_MP_ATTACK_SWIM_SECONDARYFIRE,
	ACT_MP_ATTACK_SWIM_GRENADE,
	ACT_MP_ATTACK_AIRWALK_PRIMARYFIRE,
	ACT_MP_ATTACK_AIRWALK_SECONDARYFIRE,
	ACT_MP_ATTACK_AIRWALK_GRENADE,
	ACT_MP_RELOAD_STAND,
	ACT_MP_RELOAD_STAND_LOOP,
	ACT_MP_RELOAD_STAND_END,
	ACT_MP_RELOAD_CROUCH,
	ACT_MP_RELOAD_CROUCH_LOOP,
	ACT_MP_RELOAD_CROUCH_END,
	ACT_MP_RELOAD_SWIM,
	ACT_MP_RELOAD_SWIM_LOOP,
	ACT_MP_RELOAD_SWIM_END,
	ACT_MP_RELOAD_AIRWALK,
	ACT_MP_RELOAD_AIRWALK_LOOP,
	ACT_MP_RELOAD_AIRWALK_END,
	ACT_MP_ATTACK_STAND_PREFIRE,
	ACT_MP_ATTACK_STAND_POSTFIRE,
	ACT_MP_ATTACK_STAND_STARTFIRE,
	ACT_MP_ATTACK_CROUCH_PREFIRE,
	ACT_MP_ATTACK_CROUCH_POSTFIRE,
	ACT_MP_ATTACK_SWIM_PREFIRE,
	ACT_MP_ATTACK_SWIM_POSTFIRE,
	ACT_MP_STAND_PRIMARY,
	ACT_MP_CROUCH_PRIMARY,
	ACT_MP_RUN_PRIMARY,
	ACT_MP_WALK_PRIMARY,
	ACT_MP_AIRWALK_PRIMARY,
	ACT_MP_CROUCHWALK_PRIMARY,
	ACT_MP_JUMP_PRIMARY,
	ACT_MP_JUMP_START_PRIMARY,
	ACT_MP_JUMP_FLOAT_PRIMARY,
	ACT_MP_JUMP_LAND_PRIMARY,
	ACT_MP_SWIM_PRIMARY,
	ACT_MP_DEPLOYED_PRIMARY,
	ACT_MP_SWIM_DEPLOYED_PRIMARY,
	ACT_MP_ATTACK_STAND_PRIMARY,
	ACT_MP_ATTACK_STAND_PRIMARY_DEPLOYED,
	ACT_MP_ATTACK_CROUCH_PRIMARY,
	ACT_MP_ATTACK_CROUCH_PRIMARY_DEPLOYED,
	ACT_MP_ATTACK_SWIM_PRIMARY,
	ACT_MP_ATTACK_AIRWALK_PRIMARY,
	ACT_MP_RELOAD_STAND_PRIMARY,
	ACT_MP_RELOAD_STAND_PRIMARY_LOOP,
	ACT_MP_RELOAD_STAND_PRIMARY_END,
	ACT_MP_RELOAD_CROUCH_PRIMARY,
	ACT_MP_RELOAD_CROUCH_PRIMARY_LOOP,
	ACT_MP_RELOAD_CROUCH_PRIMARY_END,
	ACT_MP_RELOAD_SWIM_PRIMARY,
	ACT_MP_RELOAD_SWIM_PRIMARY_LOOP,
	ACT_MP_RELOAD_SWIM_PRIMARY_END,
	ACT_MP_RELOAD_AIRWALK_PRIMARY,
	ACT_MP_RELOAD_AIRWALK_PRIMARY_LOOP,
	ACT_MP_RELOAD_AIRWALK_PRIMARY_END,
	ACT_MP_ATTACK_STAND_GRENADE_PRIMARY,
	ACT_MP_ATTACK_CROUCH_GRENADE_PRIMARY,
	ACT_MP_ATTACK_SWIM_GRENADE_PRIMARY,
	ACT_MP_ATTACK_AIRWALK_GRENADE_PRIMARY,
	ACT_MP_STAND_SECONDARY,
	ACT_MP_CROUCH_SECONDARY,
	ACT_MP_RUN_SECONDARY,
	ACT_MP_WALK_SECONDARY,
	ACT_MP_AIRWALK_SECONDARY,
	ACT_MP_CROUCHWALK_SECONDARY,
	ACT_MP_JUMP_SECONDARY,
	ACT_MP_JUMP_START_SECONDARY,
	ACT_MP_JUMP_FLOAT_SECONDARY,
	ACT_MP_JUMP_LAND_SECONDARY,
	ACT_MP_SWIM_SECONDARY,
	ACT_MP_ATTACK_STAND_SECONDARY,
	ACT_MP_ATTACK_CROUCH_SECONDARY,
	ACT_MP_ATTACK_SWIM_SECONDARY,
	ACT_MP_ATTACK_AIRWALK_SECONDARY,
	ACT_MP_RELOAD_STAND_SECONDARY,
	ACT_MP_RELOAD_STAND_SECONDARY_LOOP,
	ACT_MP_RELOAD_STAND_SECONDARY_END,
	ACT_MP_RELOAD_CROUCH_SECONDARY,
	ACT_MP_RELOAD_CROUCH_SECONDARY_LOOP,
	ACT_MP_RELOAD_CROUCH_SECONDARY_END,
	ACT_MP_RELOAD_SWIM_SECONDARY,
	ACT_MP_RELOAD_SWIM_SECONDARY_LOOP,
	ACT_MP_RELOAD_SWIM_SECONDARY_END,
	ACT_MP_RELOAD_AIRWALK_SECONDARY,
	ACT_MP_RELOAD_AIRWALK_SECONDARY_LOOP,
	ACT_MP_RELOAD_AIRWALK_SECONDARY_END,
	ACT_MP_ATTACK_STAND_GRENADE_SECONDARY,
	ACT_MP_ATTACK_CROUCH_GRENADE_SECONDARY,
	ACT_MP_ATTACK_SWIM_GRENADE_SECONDARY,
	ACT_MP_ATTACK_AIRWALK_GRENADE_SECONDARY,
	ACT_MP_STAND_MELEE,
	ACT_MP_CROUCH_MELEE,
	ACT_MP_RUN_MELEE,
	ACT_MP_WALK_MELEE,
	ACT_MP_AIRWALK_MELEE,
	ACT_MP_CROUCHWALK_MELEE,
	ACT_MP_JUMP_MELEE,
	ACT_MP_JUMP_START_MELEE,
	ACT_MP_JUMP_FLOAT_MELEE,
	ACT_MP_JUMP_LAND_MELEE,
	ACT_MP_SWIM_MELEE,
	ACT_MP_ATTACK_STAND_MELEE,
	ACT_MP_ATTACK_STAND_MELEE_SECONDARY,
	ACT_MP_ATTACK_CROUCH_MELEE,
	ACT_MP_ATTACK_CROUCH_MELEE_SECONDARY,
	ACT_MP_ATTACK_SWIM_MELEE,
	ACT_MP_ATTACK_AIRWALK_MELEE,
	ACT_MP_ATTACK_STAND_GRENADE_MELEE,
	ACT_MP_ATTACK_CROUCH_GRENADE_MELEE,
	ACT_MP_ATTACK_SWIM_GRENADE_MELEE,
	ACT_MP_ATTACK_AIRWALK_GRENADE_MELEE,
	ACT_MP_STAND_ITEM1,
	ACT_MP_CROUCH_ITEM1,
	ACT_MP_RUN_ITEM1,
	ACT_MP_WALK_ITEM1,
	ACT_MP_AIRWALK_ITEM1,
	ACT_MP_CROUCHWALK_ITEM1,
	ACT_MP_JUMP_ITEM1,
	ACT_MP_JUMP_START_ITEM1,
	ACT_MP_JUMP_FLOAT_ITEM1,
	ACT_MP_JUMP_LAND_ITEM1,
	ACT_MP_SWIM_ITEM1,
	ACT_MP_ATTACK_STAND_ITEM1,
	ACT_MP_ATTACK_STAND_ITEM1_SECONDARY,
	ACT_MP_ATTACK_CROUCH_ITEM1,
	ACT_MP_ATTACK_CROUCH_ITEM1_SECONDARY,
	ACT_MP_ATTACK_SWIM_ITEM1,
	ACT_MP_ATTACK_AIRWALK_ITEM1,
	ACT_MP_STAND_ITEM2,
	ACT_MP_CROUCH_ITEM2,
	ACT_MP_RUN_ITEM2,
	ACT_MP_WALK_ITEM2,
	ACT_MP_AIRWALK_ITEM2,
	ACT_MP_CROUCHWALK_ITEM2,
	ACT_MP_JUMP_ITEM2,
	ACT_MP_JUMP_START_ITEM2,
	ACT_MP_JUMP_FLOAT_ITEM2,
	ACT_MP_JUMP_LAND_ITEM2,
	ACT_MP_SWIM_ITEM2,
	ACT_MP_ATTACK_STAND_ITEM2,
	ACT_MP_ATTACK_STAND_ITEM2_SECONDARY,
	ACT_MP_ATTACK_CROUCH_ITEM2,
	ACT_MP_ATTACK_CROUCH_ITEM2_SECONDARY,
	ACT_MP_ATTACK_SWIM_ITEM2,
	ACT_MP_ATTACK_AIRWALK_ITEM2,
	ACT_MP_GESTURE_FLINCH,
	ACT_MP_GESTURE_FLINCH_PRIMARY,
	ACT_MP_GESTURE_FLINCH_SECONDARY,
	ACT_MP_GESTURE_FLINCH_MELEE,
	ACT_MP_GESTURE_FLINCH_ITEM1,
	ACT_MP_GESTURE_FLINCH_ITEM2,
	ACT_MP_GESTURE_FLINCH_HEAD,
	ACT_MP_GESTURE_FLINCH_CHEST,
	ACT_MP_GESTURE_FLINCH_STOMACH,
	ACT_MP_GESTURE_FLINCH_LEFTARM,
	ACT_MP_GESTURE_FLINCH_RIGHTARM,
	ACT_MP_GESTURE_FLINCH_LEFTLEG,
	ACT_MP_GESTURE_FLINCH_RIGHTLEG,
	ACT_MP_GRENADE1_DRAW,
	ACT_MP_GRENADE1_IDLE,
	ACT_MP_GRENADE1_ATTACK,
	ACT_MP_GRENADE2_DRAW,
	ACT_MP_GRENADE2_IDLE,
	ACT_MP_GRENADE2_ATTACK,
	ACT_MP_PRIMARY_GRENADE1_DRAW,
	ACT_MP_PRIMARY_GRENADE1_IDLE,
	ACT_MP_PRIMARY_GRENADE1_ATTACK,
	ACT_MP_PRIMARY_GRENADE2_DRAW,
	ACT_MP_PRIMARY_GRENADE2_IDLE,
	ACT_MP_PRIMARY_GRENADE2_ATTACK,
	ACT_MP_SECONDARY_GRENADE1_DRAW,
	ACT_MP_SECONDARY_GRENADE1_IDLE,
	ACT_MP_SECONDARY_GRENADE1_ATTACK,
	ACT_MP_SECONDARY_GRENADE2_DRAW,
	ACT_MP_SECONDARY_GRENADE2_IDLE,
	ACT_MP_SECONDARY_GRENADE2_ATTACK,
	ACT_MP_MELEE_GRENADE1_DRAW,
	ACT_MP_MELEE_GRENADE1_IDLE,
	ACT_MP_MELEE_GRENADE1_ATTACK,
	ACT_MP_MELEE_GRENADE2_DRAW,
	ACT_MP_MELEE_GRENADE2_IDLE,
	ACT_MP_MELEE_GRENADE2_ATTACK,
	ACT_MP_ITEM1_GRENADE1_DRAW,
	ACT_MP_ITEM1_GRENADE1_IDLE,
	ACT_MP_ITEM1_GRENADE1_ATTACK,
	ACT_MP_ITEM1_GRENADE2_DRAW,
	ACT_MP_ITEM1_GRENADE2_IDLE,
	ACT_MP_ITEM1_GRENADE2_ATTACK,
	ACT_MP_ITEM2_GRENADE1_DRAW,
	ACT_MP_ITEM2_GRENADE1_IDLE,
	ACT_MP_ITEM2_GRENADE1_ATTACK,
	ACT_MP_ITEM2_GRENADE2_DRAW,
	ACT_MP_ITEM2_GRENADE2_IDLE,
	ACT_MP_ITEM2_GRENADE2_ATTACK,
	ACT_MP_STAND_BUILDING,
	ACT_MP_CROUCH_BUILDING,
	ACT_MP_RUN_BUILDING,
	ACT_MP_WALK_BUILDING,
	ACT_MP_AIRWALK_BUILDING,
	ACT_MP_CROUCHWALK_BUILDING,
	ACT_MP_JUMP_BUILDING,
	ACT_MP_JUMP_START_BUILDING,
	ACT_MP_JUMP_FLOAT_BUILDING,
	ACT_MP_JUMP_LAND_BUILDING,
	ACT_MP_SWIM_BUILDING,
	ACT_MP_ATTACK_STAND_BUILDING,
	ACT_MP_ATTACK_CROUCH_BUILDING,
	ACT_MP_ATTACK_SWIM_BUILDING,
	ACT_MP_ATTACK_AIRWALK_BUILDING,
	ACT_MP_ATTACK_STAND_GRENADE_BUILDING,
	ACT_MP_ATTACK_CROUCH_GRENADE_BUILDING,
	ACT_MP_ATTACK_SWIM_GRENADE_BUILDING,
	ACT_MP_ATTACK_AIRWALK_GRENADE_BUILDING,
	ACT_MP_STAND_PDA,
	ACT_MP_CROUCH_PDA,
	ACT_MP_RUN_PDA,
	ACT_MP_WALK_PDA,
	ACT_MP_AIRWALK_PDA,
	ACT_MP_CROUCHWALK_PDA,
	ACT_MP_JUMP_PDA,
	ACT_MP_JUMP_START_PDA,
	ACT_MP_JUMP_FLOAT_PDA,
	ACT_MP_JUMP_LAND_PDA,
	ACT_MP_SWIM_PDA,
	ACT_MP_ATTACK_STAND_PDA,
	ACT_MP_ATTACK_SWIM_PDA,
	ACT_MP_GESTURE_VC_HANDMOUTH,
	ACT_MP_GESTURE_VC_FINGERPOINT,
	ACT_MP_GESTURE_VC_FISTPUMP,
	ACT_MP_GESTURE_VC_THUMBSUP,
	ACT_MP_GESTURE_VC_NODYES,
	ACT_MP_GESTURE_VC_NODNO,
	ACT_MP_GESTURE_VC_HANDMOUTH_PRIMARY,
	ACT_MP_GESTURE_VC_FINGERPOINT_PRIMARY,
	ACT_MP_GESTURE_VC_FISTPUMP_PRIMARY,
	ACT_MP_GESTURE_VC_THUMBSUP_PRIMARY,
	ACT_MP_GESTURE_VC_NODYES_PRIMARY,
	ACT_MP_GESTURE_VC_NODNO_PRIMARY,
	ACT_MP_GESTURE_VC_HANDMOUTH_SECONDARY,
	ACT_MP_GESTURE_VC_FINGERPOINT_SECONDARY,
	ACT_MP_GESTURE_VC_FISTPUMP_SECONDARY,
	ACT_MP_GESTURE_VC_THUMBSUP_SECONDARY,
	ACT_MP_GESTURE_VC_NODYES_SECONDARY,
	ACT_MP_GESTURE_VC_NODNO_SECONDARY,
	ACT_MP_GESTURE_VC_HANDMOUTH_MELEE,
	ACT_MP_GESTURE_VC_FINGERPOINT_MELEE,
	ACT_MP_GESTURE_VC_FISTPUMP_MELEE,
	ACT_MP_GESTURE_VC_THUMBSUP_MELEE,
	ACT_MP_GESTURE_VC_NODYES_MELEE,
	ACT_MP_GESTURE_VC_NODNO_MELEE,
	ACT_MP_GESTURE_VC_HANDMOUTH_ITEM1,
	ACT_MP_GESTURE_VC_FINGERPOINT_ITEM1,
	ACT_MP_GESTURE_VC_FISTPUMP_ITEM1,
	ACT_MP_GESTURE_VC_THUMBSUP_ITEM1,
	ACT_MP_GESTURE_VC_NODYES_ITEM1,
	ACT_MP_GESTURE_VC_NODNO_ITEM1,
	ACT_MP_GESTURE_VC_HANDMOUTH_ITEM2,
	ACT_MP_GESTURE_VC_FINGERPOINT_ITEM2,
	ACT_MP_GESTURE_VC_FISTPUMP_ITEM2,
	ACT_MP_GESTURE_VC_THUMBSUP_ITEM2,
	ACT_MP_GESTURE_VC_NODYES_ITEM2,
	ACT_MP_GESTURE_VC_NODNO_ITEM2,
	ACT_MP_GESTURE_VC_HANDMOUTH_BUILDING,
	ACT_MP_GESTURE_VC_FINGERPOINT_BUILDING,
	ACT_MP_GESTURE_VC_FISTPUMP_BUILDING,
	ACT_MP_GESTURE_VC_THUMBSUP_BUILDING,
	ACT_MP_GESTURE_VC_NODYES_BUILDING,
	ACT_MP_GESTURE_VC_NODNO_BUILDING,
	ACT_MP_GESTURE_VC_HANDMOUTH_PDA,
	ACT_MP_GESTURE_VC_FINGERPOINT_PDA,
	ACT_MP_GESTURE_VC_FISTPUMP_PDA,
	ACT_MP_GESTURE_VC_THUMBSUP_PDA,
	ACT_MP_GESTURE_VC_NODYES_PDA,
	ACT_MP_GESTURE_VC_NODNO_PDA,
	ACT_VM_UNUSABLE,
	ACT_VM_UNUSABLE_TO_USABLE,
	ACT_VM_USABLE_TO_UNUSABLE,
	ACT_PRIMARY_VM_DRAW,
	ACT_PRIMARY_VM_HOLSTER,
	ACT_PRIMARY_VM_IDLE,
	ACT_PRIMARY_VM_PULLBACK,
	ACT_PRIMARY_VM_PRIMARYATTACK,
	ACT_PRIMARY_VM_SECONDARYATTACK,
	ACT_PRIMARY_VM_RELOAD,
	ACT_PRIMARY_VM_DRYFIRE,
	ACT_PRIMARY_VM_IDLE_TO_LOWERED,
	ACT_PRIMARY_VM_IDLE_LOWERED,
	ACT_PRIMARY_VM_LOWERED_TO_IDLE,
	ACT_SECONDARY_VM_DRAW,
	ACT_SECONDARY_VM_HOLSTER,
	ACT_SECONDARY_VM_IDLE,
	ACT_SECONDARY_VM_PULLBACK,
	ACT_SECONDARY_VM_PRIMARYATTACK,
	ACT_SECONDARY_VM_SECONDARYATTACK,
	ACT_SECONDARY_VM_RELOAD,
	ACT_SECONDARY_VM_DRYFIRE,
	ACT_SECONDARY_VM_IDLE_TO_LOWERED,
	ACT_SECONDARY_VM_IDLE_LOWERED,
	ACT_SECONDARY_VM_LOWERED_TO_IDLE,
	ACT_MELEE_VM_DRAW,
	ACT_MELEE_VM_HOLSTER,
	ACT_MELEE_VM_IDLE,
	ACT_MELEE_VM_PULLBACK,
	ACT_MELEE_VM_PRIMARYATTACK,
	ACT_MELEE_VM_SECONDARYATTACK,
	ACT_MELEE_VM_RELOAD,
	ACT_MELEE_VM_DRYFIRE,
	ACT_MELEE_VM_IDLE_TO_LOWERED,
	ACT_MELEE_VM_IDLE_LOWERED,
	ACT_MELEE_VM_LOWERED_TO_IDLE,
	ACT_PDA_VM_DRAW,
	ACT_PDA_VM_HOLSTER,
	ACT_PDA_VM_IDLE,
	ACT_PDA_VM_PULLBACK,
	ACT_PDA_VM_PRIMARYATTACK,
	ACT_PDA_VM_SECONDARYATTACK,
	ACT_PDA_VM_RELOAD,
	ACT_PDA_VM_DRYFIRE,
	ACT_PDA_VM_IDLE_TO_LOWERED,
	ACT_PDA_VM_IDLE_LOWERED,
	ACT_PDA_VM_LOWERED_TO_IDLE,
	ACT_ITEM1_VM_DRAW,
	ACT_ITEM1_VM_HOLSTER,
	ACT_ITEM1_VM_IDLE,
	ACT_ITEM1_VM_PULLBACK,
	ACT_ITEM1_VM_PRIMARYATTACK,
	ACT_ITEM1_VM_SECONDARYATTACK,
	ACT_ITEM1_VM_RELOAD,
	ACT_ITEM1_VM_DRYFIRE,
	ACT_ITEM1_VM_IDLE_TO_LOWERED,
	ACT_ITEM1_VM_IDLE_LOWERED,
	ACT_ITEM1_VM_LOWERED_TO_IDLE,
	ACT_ITEM2_VM_DRAW,
	ACT_ITEM2_VM_HOLSTER,
	ACT_ITEM2_VM_IDLE,
	ACT_ITEM2_VM_PULLBACK,
	ACT_ITEM2_VM_PRIMARYATTACK,
	ACT_ITEM2_VM_SECONDARYATTACK,
	ACT_ITEM2_VM_RELOAD,
	ACT_ITEM2_VM_DRYFIRE,
	ACT_ITEM2_VM_IDLE_TO_LOWERED,
	ACT_ITEM2_VM_IDLE_LOWERED,
	ACT_ITEM2_VM_LOWERED_TO_IDLE,
	ACT_RELOAD_SUCCEED,
	ACT_RELOAD_FAIL,
	ACT_WALK_AIM_AUTOGUN,
	ACT_RUN_AIM_AUTOGUN,
	ACT_IDLE_AUTOGUN,
	ACT_IDLE_AIM_AUTOGUN,
	ACT_RELOAD_AUTOGUN,
	ACT_CROUCH_IDLE_AUTOGUN,
	ACT_RANGE_ATTACK_AUTOGUN,
	ACT_JUMP_AUTOGUN,
	ACT_IDLE_AIM_PISTOL,
	ACT_WALK_AIM_DUAL,
	ACT_RUN_AIM_DUAL,
	ACT_IDLE_DUAL,
	ACT_IDLE_AIM_DUAL,
	ACT_RELOAD_DUAL,
	ACT_CROUCH_IDLE_DUAL,
	ACT_RANGE_ATTACK_DUAL,
	ACT_JUMP_DUAL,
	ACT_IDLE_SHOTGUN,
	ACT_IDLE_AIM_SHOTGUN,
	ACT_CROUCH_IDLE_SHOTGUN,
	ACT_JUMP_SHOTGUN,
	ACT_IDLE_AIM_RIFLE,
	ACT_RELOAD_RIFLE,
	ACT_CROUCH_IDLE_RIFLE,
	ACT_RANGE_ATTACK_RIFLE,
	ACT_JUMP_RIFLE,
	ACT_SLEEP,
	ACT_WAKE,
	ACT_FLICK_LEFT,
	ACT_FLICK_LEFT_MIDDLE,
	ACT_FLICK_RIGHT_MIDDLE,
	ACT_FLICK_RIGHT,
	ACT_SPINAROUND,
	ACT_PREP_TO_FIRE,
	ACT_FIRE,
	ACT_FIRE_RECOVER,
	ACT_SPRAY,
	ACT_PREP_EXPLODE,
	ACT_EXPLODE,
	ACT_DOTA_IDLE,
	ACT_DOTA_RUN,
	ACT_DOTA_ATTACK,
	ACT_DOTA_ATTACK_EVENT,
	ACT_DOTA_DIE,
	ACT_DOTA_FLINCH,
	ACT_DOTA_DISABLED,
	ACT_DOTA_CAST_ABILITY_1,
	ACT_DOTA_CAST_ABILITY_2,
	ACT_DOTA_CAST_ABILITY_3,
	ACT_DOTA_CAST_ABILITY_4,
	ACT_DOTA_OVERRIDE_ABILITY_1,
	ACT_DOTA_OVERRIDE_ABILITY_2,
	ACT_DOTA_OVERRIDE_ABILITY_3,
	ACT_DOTA_OVERRIDE_ABILITY_4,
	ACT_DOTA_CHANNEL_ABILITY_1,
	ACT_DOTA_CHANNEL_ABILITY_2,
	ACT_DOTA_CHANNEL_ABILITY_3,
	ACT_DOTA_CHANNEL_ABILITY_4,
	ACT_DOTA_CHANNEL_END_ABILITY_1,
	ACT_DOTA_CHANNEL_END_ABILITY_2,
	ACT_DOTA_CHANNEL_END_ABILITY_3,
	ACT_DOTA_CHANNEL_END_ABILITY_4,
	ACT_MP_RUN_SPEEDPAINT,
	ACT_MP_LONG_FALL,
	ACT_MP_TRACTORBEAM_FLOAT,
	ACT_MP_DEATH_CRUSH,
	ACT_MP_RUN_SPEEDPAINT_PRIMARY,
	ACT_MP_DROWNING_PRIMARY,
	ACT_MP_LONG_FALL_PRIMARY,
	ACT_MP_TRACTORBEAM_FLOAT_PRIMARY,
	ACT_MP_DEATH_CRUSH_PRIMARY,
	ACT_DIE_STAND,
	ACT_DIE_STAND_HEADSHOT,
	ACT_DIE_CROUCH,
	ACT_DIE_CROUCH_HEADSHOT,
	ACT_CSGO_NULL,
	ACT_CSGO_DEFUSE,
	ACT_CSGO_DEFUSE_WITH_KIT,
	ACT_CSGO_FLASHBANG_REACTION,
	ACT_CSGO_FIRE_PRIMARY,
	ACT_CSGO_FIRE_PRIMARY_OPT_1,
	ACT_CSGO_FIRE_PRIMARY_OPT_2,
	ACT_CSGO_FIRE_SECONDARY,
	ACT_CSGO_FIRE_SECONDARY_OPT_1,
	ACT_CSGO_FIRE_SECONDARY_OPT_2,
	ACT_CSGO_RELOAD,
	ACT_CSGO_RELOAD_START,
	ACT_CSGO_RELOAD_LOOP,
	ACT_CSGO_RELOAD_END,
	ACT_CSGO_OPERATE,
	ACT_CSGO_DEPLOY,
	ACT_CSGO_CATCH,
	ACT_CSGO_SILENCER_DETACH,
	ACT_CSGO_SILENCER_ATTACH,
	ACT_CSGO_TWITCH,
	ACT_CSGO_TWITCH_BUYZONE,
	ACT_CSGO_PLANT_BOMB,
	ACT_CSGO_IDLE_TURN_BALANCEADJUST,
	ACT_CSGO_IDLE_ADJUST_STOPPEDMOVING,
	ACT_CSGO_ALIVE_LOOP,
	ACT_CSGO_FLINCH,
	ACT_CSGO_FLINCH_HEAD,
	ACT_CSGO_FLINCH_MOLOTOV,
	ACT_CSGO_JUMP,
	ACT_CSGO_FALL,
	ACT_CSGO_CLIMB_LADDER,
	ACT_CSGO_LAND_LIGHT,
	ACT_CSGO_LAND_HEAVY,
	ACT_CSGO_EXIT_LADDER_TOP,
	ACT_CSGO_EXIT_LADDER_BOTTOM,
};
class client_class {
public:
	create_client_class_fn create_fn;
	create_event_fn create_event_fn;
	char* network_name;
	recv_table* p_recv_table;
	client_class* next;
	int class_id;
};

class c_csweapon_info {
public:
	byte _0x0000 [ 20 ];
	int max_clip;
	byte _0x0018 [ 12 ];
	int max_reserved_ammo;
	byte _0x0028 [ 96 ];
	char* hud_name;
	char* weapon_name;
	byte _0x0090 [ 60 ];
	int weapon_type;
	int price;
	int reward;
	byte _0x00D8 [ 20 ];
	bool full_auto;
	byte _0x00ED [ 3 ];
	int damage;
	float armor_ratio;
	int bullets;
	float penetration;
	byte _0x0100 [ 8 ];
	float range;
	float range_modifier;
	byte _0x0110 [ 16 ];
	bool silencer;
	byte _0x0121 [ 15 ];
	float max_speed;
	float max_speed_alt;
	byte _0x0138 [ 76 ];
	int recoil_seed;
	byte _0x0188 [ 32 ];

};

class c_economy_item_view {
public:
	auto& get_account_id( void ) {
		return *( int* ) ( this + 0x1C );
	}

	auto& get_item_id( void ) {
		return *( unsigned long long* ) ( this + 0x8 );
	}

	auto& get_original_id( void ) {
		return *( unsigned long long* ) ( this + 0x10 );
	}

	auto& get_definition_index( void ) {
		return *( unsigned short* ) ( this + 0x24 );
	}

	auto& get_inventory( void ) {
		return *( int* ) ( this + 0x20 );
	}

	auto& get_flags( void ) {
		return *( byte* ) ( this + 0x30 );
	}

	auto& get_economy_item_data( void ) {
		return *( unsigned short* ) ( this + 0x26 );
	}
};

class c_weapon_system {
private:
	virtual void pad_vt0x0( ) = 0;
	virtual void pad_vt0x4( ) = 0;

public:
	virtual c_csweapon_info* get_weapon_data( short item_definition ) = 0;
};
#define END_OF_FREE_LIST -1
#define ENTRY_IN_USE -2

struct GlowObjectDefinition_t
{
	GlowObjectDefinition_t( ) { memset( this, 0, sizeof( *this ) ); }

	class c_baseentity* m_pEntity;    //0x0000
	union
	{
		vec_t m_vGlowColor;           //0x0004
		struct
		{
			float   m_flRed;           //0x0004
			float   m_flGreen;         //0x0008
			float   m_flBlue;          //0x000C
		};
	};
	float   m_flAlpha;                 //0x0010
	uint8_t pad_0014 [ 4 ];               //0x0014
	float   m_flSomeFloat;             //0x0018
	uint8_t pad_001C [ 4 ];               //0x001C
	float   m_flAnotherFloat;          //0x0020
	bool    m_bRenderWhenOccluded;     //0x0024
	bool    m_bRenderWhenUnoccluded;   //0x0025
	bool    m_bFullBloomRender;        //0x0026
	uint8_t pad_0027 [ 5 ];               //0x0027
	int32_t m_nGlowStyle;              //0x002C
	int32_t m_nSplitScreenSlot;        //0x0030
	int32_t m_nNextFreeSlot;           //0x0034

	bool IsUnused( ) const { return m_nNextFreeSlot != ENTRY_IN_USE; }
}; //Size: 0x0038 (56)

class CGlowObjectManager
{
public:
	int RegisterGlowObject( c_baseentity *pEntity, const vec_t &vGlowColor, float flGlowAlpha, bool bRenderWhenOccluded, bool bRenderWhenUnoccluded, int nSplitScreenSlot )
	{
		int nIndex;
		if ( m_nFirstFreeSlot == END_OF_FREE_LIST ) {
			nIndex = -1;
		}
		else {
			nIndex = m_nFirstFreeSlot;
			m_nFirstFreeSlot = m_GlowObjectDefinitions [ nIndex ].m_nNextFreeSlot;
		}

		m_GlowObjectDefinitions [ nIndex ].m_pEntity = pEntity;
		m_GlowObjectDefinitions [ nIndex ].m_vGlowColor = vGlowColor;
		m_GlowObjectDefinitions [ nIndex ].m_flAlpha = flGlowAlpha;
		m_GlowObjectDefinitions [ nIndex ].m_bRenderWhenOccluded = bRenderWhenOccluded;
		m_GlowObjectDefinitions [ nIndex ].m_bRenderWhenUnoccluded = bRenderWhenUnoccluded;
		m_GlowObjectDefinitions [ nIndex ].m_nSplitScreenSlot = nSplitScreenSlot;
		m_GlowObjectDefinitions [ nIndex ].m_nNextFreeSlot = ENTRY_IN_USE;

		return nIndex;
	}

	void UnregisterGlowObject( int nGlowObjectHandle )
	{
		m_GlowObjectDefinitions [ nGlowObjectHandle ].m_nNextFreeSlot = m_nFirstFreeSlot;
		m_GlowObjectDefinitions [ nGlowObjectHandle ].m_pEntity = NULL;
		m_nFirstFreeSlot = nGlowObjectHandle;
	}

	void SetEntity( int nGlowObjectHandle, c_baseentity *pEntity )
	{
		m_GlowObjectDefinitions [ nGlowObjectHandle ].m_pEntity = pEntity;
	}

	void SetColor( int nGlowObjectHandle, const vec_t &vGlowColor )
	{
		m_GlowObjectDefinitions [ nGlowObjectHandle ].m_vGlowColor = vGlowColor;
	}

	void SetAlpha( int nGlowObjectHandle, float flAlpha )
	{
		m_GlowObjectDefinitions [ nGlowObjectHandle ].m_flAlpha = flAlpha;
	}

	void SetRenderFlags( int nGlowObjectHandle, bool bRenderWhenOccluded, bool bRenderWhenUnoccluded )
	{
		m_GlowObjectDefinitions [ nGlowObjectHandle ].m_bRenderWhenOccluded = bRenderWhenOccluded;
		m_GlowObjectDefinitions [ nGlowObjectHandle ].m_bRenderWhenUnoccluded = bRenderWhenUnoccluded;
	}

	GlowObjectDefinition_t* m_GlowObjectDefinitions; //0x0000

	int GetSize( )
	{
		return *reinterpret_cast< int* >( uintptr_t( this ) + 0xC );
	}

	int m_nFirstFreeSlot;                              //0x000C                                      //0x000C
};
struct player_info_t {
	__int64			unknown;

	union {
		__int64		steam_id_64;

		struct {
			int		xuid_low;
			int		xuid_high;
		};
	};

	char			name [ 128 ];
	int				user_id;
	char			str_steam_id [ 20 ];
	byte			pad_0xA8 [ 0x10 ];
	uintptr_t		steam_id;
	char            friends_name [ 128 ];
	bool            fake_player;
	bool            is_hltv;
	uintptr_t		custom_files [ 4 ];
	byte			files_downloaded;
};

class matrix3x4_t {
public:
	float values [ 3 ] [ 4 ];

	matrix3x4_t( void ) {
		values [ 0 ] [ 0 ] = 0.0f; values [ 0 ] [ 1 ] = 0.0f; values [ 0 ] [ 2 ] = 0.0f; values [ 0 ] [ 3 ] = 0.0f;
		values [ 1 ] [ 0 ] = 0.0f; values [ 1 ] [ 1 ] = 0.0f; values [ 1 ] [ 2 ] = 0.0f; values [ 1 ] [ 3 ] = 0.0f;
		values [ 2 ] [ 0 ] = 0.0f; values [ 2 ] [ 1 ] = 0.0f; values [ 2 ] [ 2 ] = 0.0f; values [ 2 ] [ 3 ] = 0.0f;
	}

	matrix3x4_t(
		float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23 ) {
		values [ 0 ] [ 0 ] = m00; values [ 0 ] [ 1 ] = m01; values [ 0 ] [ 2 ] = m02; values [ 0 ] [ 3 ] = m03;
		values [ 1 ] [ 0 ] = m10; values [ 1 ] [ 1 ] = m11; values [ 1 ] [ 2 ] = m12; values [ 1 ] [ 3 ] = m13;
		values [ 2 ] [ 0 ] = m20; values [ 2 ] [ 1 ] = m21; values [ 2 ] [ 2 ] = m22; values [ 2 ] [ 3 ] = m23;
	}

	void init( const vec_t& x, const vec_t& y, const vec_t& z, const vec_t &origin ) {
		values [ 0 ] [ 0 ] = x.x; values [ 0 ] [ 1 ] = y.x; values [ 0 ] [ 2 ] = z.x; values [ 0 ] [ 3 ] = origin.x;
		values [ 1 ] [ 0 ] = x.y; values [ 1 ] [ 1 ] = y.y; values [ 1 ] [ 2 ] = z.y; values [ 1 ] [ 3 ] = origin.y;
		values [ 2 ] [ 0 ] = x.z; values [ 2 ] [ 1 ] = y.z; values [ 2 ] [ 2 ] = z.z; values [ 2 ] [ 3 ] = origin.z;
	}

	matrix3x4_t( const vec_t& x, const vec_t& y, const vec_t& z, const vec_t &origin ) {
		init( x, y, z, origin );
	}

	inline void set_origin( vec_t const& p ) {
		values [ 0 ] [ 3 ] = p.x;
		values [ 1 ] [ 3 ] = p.y;
		values [ 2 ] [ 3 ] = p.z;
	}

	inline void invalidate( void ) {
		for ( int i = 0; i < 3; i++ ) {
			for ( int j = 0; j < 4; j++ ) {
				values [ i ] [ j ] = std::numeric_limits<float>::infinity( );;
			}
		}
	}

	vec_t get_x_axis( void ) const {
		return at( 0 );
	}

	vec_t get_y_axis( void ) const {
		return at( 1 );
	}

	vec_t get_z_axis( void ) const {
		return at( 2 );
	}

	vec_t get_origin( void ) const {
		return at( 3 );
	}

	vec_t at( int i ) const {
		return vec_t { values [ 0 ] [ i ], values [ 1 ] [ i ], values [ 2 ] [ i ] };
	}

	float* operator[]( int i ) {
		return values [ i ];
	}

	const float* operator[]( int i ) const {
		return values [ i ];
	}

	float* Base( ) {
		return &values [ 0 ] [ 0 ];
	}

	const float* base( ) const {
		return &values [ 0 ] [ 0 ];
	}

	const bool operator==( matrix3x4_t matrix ) const {
		return
			values [ 0 ] [ 0 ] == matrix [ 0 ] [ 0 ] && values [ 0 ] [ 1 ] == matrix [ 0 ] [ 1 ] && values [ 0 ] [ 2 ] == matrix [ 0 ] [ 2 ] && values [ 0 ] [ 3 ] == matrix [ 0 ] [ 3 ] &&
			values [ 1 ] [ 0 ] == matrix [ 1 ] [ 0 ] && values [ 1 ] [ 1 ] == matrix [ 1 ] [ 1 ] && values [ 1 ] [ 2 ] == matrix [ 1 ] [ 2 ] && values [ 1 ] [ 3 ] == matrix [ 1 ] [ 3 ] &&
			values [ 2 ] [ 0 ] == matrix [ 2 ] [ 0 ] && values [ 2 ] [ 1 ] == matrix [ 2 ] [ 1 ] && values [ 2 ] [ 2 ] == matrix [ 2 ] [ 2 ] && values [ 2 ] [ 3 ] == matrix [ 2 ] [ 3 ];
	}
};


struct mstudiobone_t {
	int sznameindex;

	inline char* const get_name( void ) const {
		return ( ( char* ) this ) + sznameindex;
	}

	int parent;
	int bonecontroller [ 6 ];

	vec_t pos;
	float quat [ 4 ];
	vec_t rot;
	vec_t posscale;
	vec_t rotscale;

	matrix3x4_t	pose_to_bone;
	float quaternion_alignment [ 4 ];
	int flags;
	int proctype;
	int procindex;
	mutable int physicsbone;

	inline void* get_procedure( void ) const {
		if ( !procindex )
			return nullptr;
		else
			return ( void* ) ( ( ( byte * ) this ) + procindex );
	};

	int surfacepropidx;

	inline char* const get_surface_props( void ) const {
		return ( ( char* ) this ) + surfacepropidx;
	}

	int contents;
	int unused [ 8 ];
};

struct mstudiobbox_t {
	int bone;
	int group;
	vec_t bbmin;
	vec_t bbmax;
	int hitbox_name_index;
	int pad_0 [ 3 ];
	float radius;
	int pad_1 [ 4 ];

	const char* get_name( ) {
		if ( !hitbox_name_index )
			return nullptr;

		return ( const char* ) ( ( uint8_t* ) this + hitbox_name_index );
	}
};

struct mstudiohitboxset_t {
	int name_index;
	int num_hitboxes;
	int hitbox_index;

	const char* get_name( void ) {
		if ( !name_index )
			return nullptr;

		return ( const char* ) ( ( uint8_t* ) this + name_index );
	}

	inline mstudiobbox_t* hitbox( int i ) const {
		if ( i > num_hitboxes )
			return nullptr;

		return ( mstudiobbox_t* ) ( ( byte* ) this + hitbox_index ) + i;
	}
};

struct model_t {
	char name [ 255 ];
};

struct studiohdr_t {
	int id;
	int version;
	long checksum;
	char name [ 64 ];
	int length;
	vec_t vec_eye_pos;
	vec_t vec_illumination_pos;
	vec_t vec_hull_min;
	vec_t vec_hull_max;
	vec_t vec_min;
	vec_t vec_max;
	int flags;
	int numbones;
	int boneindex;
	int numbonecontrollers;
	int bonecontrollerindex;
	int numhitboxsets;
	int hitboxsetindex;
	int numlocalanim;
	int localanimindex;
	int numlocalseq;
	int localseqindex;
	int activitylistversion;
	int eventsindexed;
	int numtextures;
	int textureindex;

	inline const char* get_name( void ) const {
		return name;
	}

	mstudiohitboxset_t* hitbox_set( int i ) {
		if ( i > numhitboxsets )
			return nullptr;

		return ( mstudiohitboxset_t* ) ( ( uint8_t* ) this + hitboxsetindex ) + i;
	}

	mstudiobone_t* bone( int i ) {
		if ( i > numbones )
			return nullptr;

		return ( mstudiobone_t* ) ( ( uint8_t* ) this + boneindex ) + i;
	}
};

enum button_code_t
{
	button_code_invalid = -1,
	button_code_none = 0,
	key_first = 0,
	key_none = key_first,
	key_0,
	key_1,
	key_2,
	key_3,
	key_4,
	key_5,
	key_6,
	key_7,
	key_8,
	key_9,
	key_a,
	key_b,
	key_c,
	key_d,
	key_e,
	key_f,
	key_g,
	key_h,
	key_i,
	key_j,
	key_k,
	key_l,
	key_m,
	key_n,
	key_o,
	key_p,
	key_q,
	key_r,
	key_s,
	key_t,
	key_u,
	key_v,
	key_w,
	key_x,
	key_y,
	key_z,
	key_pad_0,
	key_pad_1,
	key_pad_2,
	key_pad_3,
	key_pad_4,
	key_pad_5,
	key_pad_6,
	key_pad_7,
	key_pad_8,
	key_pad_9,
	key_pad_divide,
	key_pad_multiply,
	key_pad_minus,
	key_pad_plus,
	key_pad_enter,
	key_pad_decimal,
	key_lbracket,
	key_rbracket,
	key_semicolon,
	key_apostrophe,
	key_backquote,
	key_comma,
	key_period,
	key_slash,
	key_backslash,
	key_minus,
	key_equal,
	key_enter,
	key_space,
	key_backspace,
	key_tab,
	key_capslock,
	key_numlock,
	key_escape,
	key_scrolllock,
	key_insert,
	key_delete,
	key_home,
	key_end,
	key_pageup,
	key_pagedown,
	key_break,
	key_lshift,
	key_rshift,
	key_lalt,
	key_ralt,
	key_lcontrol,
	key_rcontrol,
	key_lwin,
	key_rwin,
	key_app,
	key_up,
	key_left,
	key_down2,
	key_right,
	key_f1,
	key_f2,
	key_f3,
	key_f4,
	key_f5,
	key_f6,
	key_f7,
	key_f8,
	key_f9,
	key_f10,
	key_f11,
	key_f12,
	key_capslocktoggle,
	key_numlocktoggle,
	key_scrolllocktoggle,

	key_last = key_scrolllocktoggle,
	key_count = key_last - key_first + 1,

	mouse_first = key_last + 1,

	mouse_left = mouse_first,
	mouse_right,
	mouse_middle,
	mouse_4,
	mouse_5,
	mouse_wheel_up,
	mouse_wheel_down,

	mouse_last = mouse_wheel_down,
	mouse_count = mouse_last - mouse_first + 1,
};

class c_input_system
{
public:
	void enable_input( bool enabled )
	{
		return ( ( void( __thiscall* )( void*, bool ) ) get_vfunc( this, 11 ) )( this, enabled );
	}

	//void reset_input_state()
	//{
	//	typedef void(__thiscall* OriginalFn)(void*);
	//	return get_vfunc<OriginalFn>(this, 39)(this);
	//}

	//bool is_button_down(button_code_t code)
	//{
	//	typedef bool(__thiscall* OriginalFn)(void*, button_code_t);
	//	return get_vfunc<OriginalFn>(this, 15)(this, code);
	//}

	//void get_cursor_position(int* m_pX, int* m_pY)
	//{
	//	typedef void(__thiscall* OriginalFn)(void*, int*, int*);
	//	return get_vfunc<OriginalFn>(this, 56)(this, m_pX, m_pY);
	//}

	//const char* button_code_to_string(button_code_t ButtonCode)
	//{
	//	typedef const char*(__thiscall* OriginalFn)(void*, button_code_t);
	//	return get_vfunc<OriginalFn>(this, 40)(this, ButtonCode);
	//}

	//button_code_t virtual_key_to_buttoncode(int nVirtualKey)
	//{
	//	typedef button_code_t(__thiscall* OriginalFn)(void*, int);
	//	return get_vfunc<OriginalFn>(this, 44)(this, nVirtualKey);
	//}

	//int button_code_to_virtual_key(button_code_t code)
	//{
	//	typedef int(__thiscall* OriginalFn)(void*, button_code_t);
	//	return get_vfunc<OriginalFn>(this, 45)(this, code);
	//}
};

class dvariant_t {
public:
	float floating_point;
	long integer;
	char* string;
	void* data;
	vec_t vec_t;
	int64_t int64;
	send_prop_t type;
};

class c_recv_proxy_data {
public:
	const recv_prop* recvprop;
	dvariant_t value;
	int element;
	int object_id;
};

class recv_prop {
public:
	char* var_name;
	send_prop_t recv_type;
	int flags;
	int string_buffer_size;
	int inside_array;
	const void* extra_data;
	recv_prop* array_prop;
	array_length_recv_proxy_fn array_length_proxy;
	recv_var_proxy_fn proxy_fn;
	data_table_recv_var_proxy_fn data_table_proxy_fn;
	recv_table* data_table;
	int offset;
	int element_stride;
	int elements;
	const char* parent_array_prop_name;

	__forceinline recv_var_proxy_fn	get_proxy_fn( void ) {
		return proxy_fn;
	}

	__forceinline void set_proxy_fn( recv_var_proxy_fn fn ) {
		proxy_fn = fn;
	}

	__forceinline data_table_recv_var_proxy_fn get_data_table_proxy_fn( void ) {
		return data_table_proxy_fn;
	}

	__forceinline void set_data_table_proxy_fn( data_table_recv_var_proxy_fn fn ) {
		data_table_proxy_fn = fn;
	}
};

class recv_table {
public:
	recv_prop * props;
	int			i_props;
	void*		decoder;
	char*		net_table_name;
	bool		initialized;
	bool		in_main_list;
};

class recv_prop_hook {
public:
	recv_prop_hook( recv_prop* prop, const recv_var_proxy_fn proxy_fn ) :
		m_property( prop ),
		m_original_proxy_fn( prop->proxy_fn ) {
		set_proxy_function( proxy_fn );
	}

	~recv_prop_hook( void ) {
		m_property->proxy_fn = m_original_proxy_fn;
	}

	recv_var_proxy_fn get_original_function( ) const {
		return m_original_proxy_fn;
	}

	void set_proxy_function( const recv_var_proxy_fn proxy_fn ) const {
		m_property->proxy_fn = proxy_fn;
	}

private:
	recv_prop* m_property;
	recv_var_proxy_fn m_original_proxy_fn;
};

class overlay_text;

class c_debug_overlay
{
public:
	virtual void            __unkn( ) = 0;
	virtual void           add_entity_text_overlay( int ent_index, int line_offset, float duration, int r, int g, int b, int a, const char *format, ... ) = 0;
	virtual void            add_box_overlay( const vec_t& origin, const vec_t& mins, const vec_t& max, vec_t const& orientation, int r, int g, int b, int a, float duration ) = 0;
	virtual void            add_sphere_overlay( const vec_t& vOrigin, float flRadius, int nTheta, int nPhi, int r, int g, int b, int a, float flDuration ) = 0;
	virtual void            add_triangle_overlay( const vec_t& p1, const vec_t& p2, const vec_t& p3, int r, int g, int b, int a, bool noDepthTest, float duration ) = 0;
	virtual void            add_line_overlay( const vec_t& origin, const vec_t& dest, int r, int g, int b, bool noDepthTest, float duration ) = 0;
	virtual void            add_text_overlay( const vec_t& origin, float duration, const char *format, ... ) = 0;
	virtual void              add_text_overlay( const vec_t& origin, int line_offset, float duration, const char *format, ... ) = 0;
	virtual void             add_screen_text_overlay( float flXPos, float flYPos, float flDuration, int r, int g, int b, int a, const char *text ) = 0;
	virtual void           add_swept_box_overlay( const vec_t& start, const vec_t& end, const vec_t& mins, const vec_t& max, const vec_t & angles, int r, int g, int b, int a, float flDuration ) = 0;
	virtual void            add_grid_overlay( const vec_t& origin ) = 0;
	virtual void           add_coord_frame_overlay( const matrix3x4_t& frame, float flScale, int vColorTable [ 3 ] [ 3 ] = NULL ) = 0;
	virtual int             screen_position( const vec_t& point, vec_t& screen ) = 0;
	virtual int             screen_position( float flXPos, float flYPos, vec_t& screen ) = 0;
	virtual overlay_text*  get_first( void ) = 0;
	virtual overlay_text*  get_next( overlay_text *current ) = 0;
	virtual void            clear_dead_overlays( void ) = 0;
	virtual void            clear_all_overlays( ) = 0;
	virtual void           add_text_overlay_rgb( const vec_t& origin, int line_offset, float duration, float r, float g, float b, float alpha, const char *format, ... ) = 0;
	virtual void            add_text_overlay_rgb( const vec_t& origin, int line_offset, float duration, int r, int g, int b, int a, const char *format, ... ) = 0;
	virtual void            add_line_overlay_alpha( const vec_t& origin, const vec_t& dest, int r, int g, int b, int a, bool noDepthTest, float duration ) = 0;
	virtual void            add_box_overlay_orientation( const vec_t& origin, const vec_t& mins, const vec_t& max, vec_t const& orientation, const uint8_t* faceColor, const uint8_t* edgeColor, float duration ) = 0;
	virtual void            purge_text_overlays( ) = 0;
	virtual void            draw_pill( const vec_t& mins, const vec_t& max, float& diameter, int r, int g, int b, int a, float duration ) = 0;

public:
	void capsule_overlay( vec_t& mins, vec_t& maxs, float pillradius, int r, int g, int b, int a, float duration )
	{
		( ( void( __thiscall* )( void*, vec_t&, vec_t&, float&, int, int, int, int, float ) ) get_vfunc( this, 24 ) )( this, mins, maxs, pillradius, r, g, b, a, duration );
	};
};


class c_usercmd {
public:
	virtual ~c_usercmd( void ) { };

	c_usercmd( ) {
		memset( this, 0, sizeof( *this ) );
	}

	int			command_number;
	int			tick_count;
	vec_t		viewangles;
	vec_t		aimdirection;
	float		forwardmove;
	float		sidemove;
	float		upmove;
	int			buttons;
	char		impulse;
	int			weapon_select;
	int			weapon_subtype;
	int			random_seed;
	short		mousedx;
	short		mousedy;
	bool		hasbeenpredicted;
	char		pad_0x4C [ 0x18 ];
};

class c_verified_usercmd {
public:
	c_usercmd	cmd;
	uintptr_t	crc;
};

class c_input {
public:
	byte						pad_0x00 [ 0xC ];
	bool						track_ir_available;
	bool						mouse_initialized;
	bool						mouse_active;
	bool						joystick_advanced_init;
	byte						pad_0x08 [ 0x2C ];
	void*						keys;
	byte						pad_0x38 [ 0x6C ];
	bool						camera_intercepting_mouse;
	bool						camera_in_third_person;
	bool						camera_moving_with_mouse;
	vec_t						camera_offset;
};

class c_base_client_dll {
public:
	client_class * get_all_classes( void ) {
		return ( ( client_class*( __thiscall* )( void* ) ) get_vfunc( this, 8 ) )( this );
	}
};

class c_panel {
public:
	const char* get_name( uintptr_t panel ) {
		return ( ( const char*( __thiscall* )( void*, uintptr_t ) ) get_vfunc( this, 36 ) )( this, panel );
	}
};

struct vertex_t {
public:
	vertex_t( void ) {

	}

	vertex_t( const vec2_t& _pos, const vec2_t& coord = vec2_t( ) ) {
		pos = _pos;
		tc = coord;
	}

	void init( const vec2_t& _pos, const vec2_t& coord = vec2_t( ) ) {
		pos = _pos;
		tc = coord;
	}

	vec2_t pos;
	vec2_t tc;
};

struct i_rect {
	int x0;
	int y0;
	int x1;
	int y1;

	i_rect( int _x0, int _y0, int _x1, int _y1 ) {
		x0 = _x0;
		y0 = _y0;
		x1 = _x1;
		y1 = _y1;
	}
};

struct c_view_setup {
	char _0x0000 [ 16 ];
	__int32 x;
	__int32 x_old;
	__int32 y;
	__int32 y_old;
	__int32 width;
	__int32    width_old;
	__int32 height;
	__int32    height_old;
	char _0x0030 [ 128 ];
	float fov;
	float fov_viewmodel;
	vec_t origin;
	vec_t angles;
	float z_near;
	float z_far;
	float near_viewmodel;
	float dar_viewmodel;
	float aspect_ratio;
	float near_blur_depth;
	float near_focus_depth;
	float far_focus_depth;
	float far_blur_depth;
	float near_blur_radius;
	float far_blur_radius;
	float do_quality;
	__int32 motion_blur;
	char _0x0104 [ 68 ];
	__int32 edge_blur;
};

enum font_draw_type_t {
	font_default = 0,
	font_nonadditive,
	font_additive,
	font_type_count = 2,
};

enum font_flags_t {
	flag_non,
	flag_italic = 0x001,
	flag_underline = 0x002,
	flag_strikeout = 0x004,
	flag_symbol = 0x008,
	flag_aa = 0x010,
	flag_gaussianblur = 0x020,
	flag_rotary = 0x040,
	flag_dropshadow = 0x080,
	flag_additive = 0x100,
	flag_outline = 0x200,
	flag_custom = 0x400,
	flag_bitmap = 0x800,
};

class c_surface {
public:
	void unlock_cursor( void ) {
		( ( void( __thiscall* )( void* ) ) get_vfunc( this, 66 ) )( this );
	}

	void lock_cursor( void ) {
		( ( void( __thiscall* )( void* ) ) get_vfunc( this, 67 ) )( this );
	}

	void draw_set_color( int r, int g, int b, int a ) {
		( ( void( __thiscall* )( void*, int, int, int, int ) ) get_vfunc( this, 15 ) )( this, r, g, b, a );
	}

	void draw_filled_rect( int x0, int y0, int x1, int y1 ) {
		( ( void( __thiscall* )( void*, int, int, int, int ) ) get_vfunc( this, 16 ) )( this, x0, y0, x1, y1 );
	}

	void draw_set_text_font( unsigned long font ) {
		( ( void( __thiscall* )( void*, unsigned long ) ) get_vfunc( this, 23 ) )( this, font );
	}

	void draw_line( int x0, int y0, int x1, int y1 ) {
		( ( void( __thiscall* )( void*, int, int, int, int ) ) get_vfunc( this, 19 ) )( this, x0, y0, x1, y1 );
	}

	void draw_set_text_color( int r, int g, int b, int a ) {
		( ( void( __thiscall* )( void*, int, int, int, int ) ) get_vfunc( this, 25 ) )( this, r, g, b, a );
	}

	void draw_set_text_pos( int x, int y ) {
		( ( void( __thiscall* )( void*, int, int ) ) get_vfunc( this, 26 ) )( this, x, y );
	}

	void draw_print_text( const wchar_t* text, int len ) {
		( ( void( __thiscall* )( void*, const wchar_t *, int, int ) ) get_vfunc( this, 28 ) )( this, text, len, 0 );
	}

	void set_font_glyph_set( unsigned long font, const char* fname, int tall, int weight, int blur, int scanlines, int flags ) {
		( ( void( __thiscall* )( void*, unsigned long, const char*, int, int, int, int, int, int, int ) ) get_vfunc( this, 72 ) )( this, font, fname, tall, weight, blur, scanlines, flags, 0, 0 );
	}

	int get_text_size( unsigned long font, const wchar_t* text, int& wide, int& tall ) {
		return ( ( int( __thiscall* )( void*, unsigned long, const wchar_t*, int&, int& ) ) get_vfunc( this, 79 ) )( this, font, text, wide, tall );
	}

	void draw_colored_circle( int centerx, int centery, float radius, int r, int g, int b, int a ) {
		( ( void( __thiscall* )( void*, int, int, float, int, int, int, int ) ) get_vfunc( this, 162 ) )( this, centerx, centery, radius, r, g, b, a );
	}

	void draw_outlined_circle( int x, int y, int r, int seg ) {
		( ( void( __thiscall* )( void*, int, int, int, int ) ) get_vfunc( this, 103 ) )( this, x, y, r, seg );
	}

	void draw_outlined_rect( int x, int y, int w, int h ) {
		( ( void( __thiscall* )( void*, int, int, int, int ) ) get_vfunc( this, 18 ) )( this, x, y, w, h );
	}

	void draw_filled_rect_fade( int x0, int y0, int x1, int y1, int alpha0, int alpha1, bool horizontal ) {
		( ( void( __thiscall* )( void*, int, int, int, int, int, int, bool ) ) get_vfunc( this, 123 ) )( this, x0, y0, x1, y1, alpha0, alpha1, horizontal );
	}

	void draw_text( int x, int y, color clr, int font, bool center, const char* input, ... ) {
		int apple = 0;
		char buf [ 2048 ] = { '\0' };

		va_list args;

		va_start( args, input );
		vsprintf_s( buf, input, args );
		va_end( args );

		size_t sz = strlen( buf ) + 1;

		wchar_t* wbuf = new wchar_t [ sz ];

		mbstowcs_s( 0, wbuf, sz, buf, sz - 1 );

		int w = 0, h = 0;

		if ( center )
			get_text_size( font, wbuf, w, h );

		draw_set_text_color( clr.r, clr.g, clr.b, clr.a );
		draw_set_text_font( font );
		draw_set_text_pos( x - ( w / 2 ), y );
		draw_print_text( wbuf, wcslen( wbuf ) );
	}

	int create_new_texture_id( bool procedural ) {
		return ( ( int( __thiscall* )( void*, bool ) ) get_vfunc( this, 43 ) )( this, procedural );
	}

	void draw_set_texture( int texture_id ) {
		( ( void( __thiscall* )( void*, int ) ) get_vfunc( this, 38 ) )( this, texture_id );
	}

	void draw_set_texture_rgba( int texture_id, unsigned char const* colors, int w, int h ) {
		( ( void( __thiscall* )( void*, int, unsigned char  const*, int, int ) ) get_vfunc( this, 37 ) )( this, texture_id, colors, w, h );
	}

	void draw_textured_polygon( int count, vertex_t* verts, bool unk = false ) {
		( ( void( __thiscall* )( void*, int, void*, bool ) ) get_vfunc( this, 106 ) )( this, count, verts, unk );
	}

	unsigned long create_font( void ) {
		return ( ( unsigned long( __thiscall* )( void* ) ) get_vfunc( this, 71 ) )( this );
	}

	bool is_texture_id_valid( int texture_id ) {
		return ( ( bool( __thiscall* )( void*, int ) ) get_vfunc( this, 42 ) )( this, texture_id );
	}

	void draw_textured_rect( int x0, int y0, int x1, int y1 ) {
		return ( ( void( __thiscall* )( void*, int, int, int, int ) ) get_vfunc( this, 41 ) )( this, x0, y0, x1, y1 );
	}
	vec2_t get_mouse_pos( ) // bolbi ware
	{
		POINT mousePosition;
		GetCursorPos( &mousePosition );
		ScreenToClient( FindWindowA( 0, "Counter-Strike: Global Offensive" ), &mousePosition );
		return { static_cast< float >( mousePosition.x ), static_cast< float >( mousePosition.y ) };
	}

	bool mouse_in_region( int x, int y, int x2, int y2 ) {
		if ( get_mouse_pos( ).x > x && get_mouse_pos( ).y > y &&  get_mouse_pos( ).x < x2 + x && get_mouse_pos( ).y < y2 + y )
			return true;
		return false;
	}
};

class c_global_vars_base {
public:
	float     real_time;
	int       frame_count;
	float     absolute_frametime;
	float     absolute_frame_start_time;
	float     curtime;
	float     frametime;
	int       max_clients;
	int       tickcount;
	float     interval_per_tick;
	float     interpolation_amount;
	int       sim_ticks_this_frame;
	int       network_protocol;
	byte	  pad_0x30 [ 0xE ];
	c_usercmd* user_cmd;
};

class c_net_channel_info {
public:
	virtual const char*	get_name( void ) const = 0;
	virtual const char*	get_address( void ) const = 0;
	virtual float		get_time( void ) const = 0;
	virtual float		get_time_connected( void ) const = 0;
	virtual int			get_buffer_size( void ) const = 0;
	virtual int			get_data_rate( void ) const = 0;
	virtual bool		is_loopback( void ) const = 0;
	virtual bool		is_timing_out( void ) const = 0;
	virtual bool		is_playback( void ) const = 0;
	virtual float		get_latency( int flow ) const = 0;
	virtual float		get_avg_latency( int flow ) const = 0;
};

class c_net_channel {
public:
	char pad_0000 [ 20 ];
	bool is_processing_messages;
	bool should_delete;
	char pad_0016 [ 2 ];
	int32_t out_sequence_nr;
	int32_t in_sequence_nr;
	int32_t out_sequence_nr_ack;
	int32_t out_reliable_state_count;
	int32_t in_reliable_state_count;
	int32_t choked_packets;
	char pad_0030 [ 1044 ];

	bool transmit( bool only_reliable ) {
		return ( ( bool( __thiscall* )( void*, bool ) ) get_vfunc( this, 49 ) )( this, only_reliable );
	}

	void send_datagram( )
	{
		( ( void( __thiscall* )( void *, void * ) ) get_vfunc( this, 46 ) )( this, NULL );
	}
};

class c_net_channel_handler {
public:
	virtual		 ~c_net_channel_handler( void ) = 0;
	virtual void connection_start( c_net_channel *chan ) = 0;
	virtual void connection_closing( const char *reason ) = 0;
	virtual void connection_crashed( const char *reason ) = 0;
	virtual void packet_start( int incoming_sequence, int outgoing_acknowledged ) = 0;
	virtual void packet_end( void ) = 0;
	virtual void file_requested( const char* file_name, uintptr_t transfer_id ) = 0;
	virtual void file_recieved( const char* file_name, uintptr_t transfer_id ) = 0;
	virtual void file_denied( const char* file_name, uintptr_t transfer_id ) = 0;
	virtual void file_sent( const char* file_name, uintptr_t transfer_id ) = 0;
};

class c_convar {
public:
	__forceinline float get_float( void ) {
		int xord = ( int ) ( *( int* ) ( &f_value ) ^ ( uintptr_t ) this );
		return *( float* ) &xord;
	}

	__forceinline int get_int( void ) {
		int xord = ( int ) ( *( int* ) ( &n_value ) ^ ( uintptr_t ) this );
		return *( int* ) &xord;
	}

	bool get_bool( void ) {
		return !!get_int( );
	}

	void set_value( const char* value ) {
		return ( ( void( __thiscall* )( void*, const char* ) ) get_vfunc( this, 14 ) )( this, value );
	}

	void set_value( float value ) {
		return ( ( void( __thiscall* )( void*, float ) ) get_vfunc( this, 15 ) )( this, value );
	}

	void set_value( int value ) {
		return ( ( void( __thiscall* )( void*, int ) ) get_vfunc( this, 16 ) )( this, value );
	}

	void set_value( color value ) {
		return ( ( void( __thiscall* )( void*, color ) ) get_vfunc( this, 17 ) )( this, value );
	}

	void null_callback( void ) {
		*( int* ) ( ( uintptr_t ) &change_callback_fn + 0xC ) = 0;
	}

	byte		pad_0x0 [ 0x4 ];
	c_convar*	next;
	int			registered;
	char*		name;
	char*		help_string;
	int			flags;
	byte		pad_0x18 [ 0x4 ];
	c_convar*	parent;
	char*		default_value;
	char*		string;
	int			string_len;
	float		f_value;
	int			n_value;
	int			has_min;
	float		min_val;
	int			has_max;
	float		max_val;
	void*		change_callback_fn;
};

class c_cvar {
public:
	c_convar * find_var( const char* convar_name ) {
		return ( ( c_convar*( __thiscall* )( void*, const char* ) ) get_vfunc( this, 16 ) )( this, convar_name );
	}
};

class c_mem_alloc {
public:
	virtual ~c_mem_alloc( void ) = 0;
	virtual void* alloc( size_t size ) = 0;
	virtual void* realloc( void* mem, size_t size ) = 0;
	virtual void free( void* mem ) = 0;
};

class c_engine_client {
public:
	virtual int                   get_intersecting_surfaces( const model_t* model, const vec_t& center, const float radius, const bool only_visible_surfaces, void* p_infos, const int max_infos ) = 0;
	virtual vec_t				  get_light_for_point( const vec_t& pos, bool clamp ) = 0;
	virtual void*				  trace_line_material_and_lighting( const vec_t& start, const vec_t& end, vec_t& diffuse_light_color, vec_t& base_color ) = 0;
	virtual const char*           parse_file( const char* data, char* token, int maxlen ) = 0;
	virtual bool                  copy_file( const char* source, const char* destination ) = 0;
	virtual void                  get_screen_size( int& width, int& height ) = 0;
	virtual void                  server_cmd( const char* cmd, bool reliable = true ) = 0;
	virtual void                  client_cmd( const char* cmd ) = 0;
	virtual bool                  get_player_info( int ent_num, player_info_t* p_info ) = 0;
	virtual int                   get_player_for_userid( int user_id ) = 0;
	virtual void*				  text_message_get( const char* name ) = 0;
	virtual bool                  con_is_visible( void ) = 0;
	virtual int                   get_local_player( void ) = 0;
	virtual const model_t*        load_model( const char* name, bool prop = false ) = 0;
	virtual float                 get_last_time_stamp( void ) = 0;
	virtual void*				  get_sentence( void* audio_source ) = 0;
	virtual float                 get_sentence_length( void* audio_source ) = 0;
	virtual bool                  is_streaming( void* audio_source ) const = 0;
	virtual void                  get_viewangles( vec_t& angs ) = 0;
	virtual void                  set_viewangles( vec_t& angs ) = 0;
	virtual int                   get_max_clients( void ) = 0;
	virtual const char*           key_lookup_binding( const char* binding ) = 0;
	virtual const char*           key_binding_for_key( int& code ) = 0;
	virtual void                  key_set_binding( int, char const* ) = 0;
	virtual void                  start_key_trap_mode( void ) = 0;
	virtual bool                  check_done_key_trapping( int& code ) = 0;
	virtual bool                  is_in_game( void ) = 0;
	virtual bool                  is_connected( void ) = 0;
	virtual bool                  is_drawing_loading_image( void ) = 0;
	virtual void                  hide_loading_plaque( void ) = 0;
	virtual void                  con_nprintf( int pos, const char* fmt, ... ) = 0;
	virtual void                  con_nxprintf( const struct con_nprint_s* info, const char* fmt, ... ) = 0;
	virtual int                   is_box_visible( const vec_t& mins, const vec_t& maxs ) = 0;
	virtual int                   is_box_in_view_cluster( const vec_t& mins, const vec_t& maxs ) = 0;
	virtual bool                  cull_box( const vec_t& mins, const vec_t& maxs ) = 0;
	virtual void                  sound_extra_update( void ) = 0;
	virtual const char*           get_game_directory( void ) = 0;
	virtual const matrix3x4_t&	  world_to_screen_matrix( ) = 0;
	virtual const matrix3x4_t&	  world_to_view_matrix( ) = 0;
	virtual int                   game_lump_version( int lumpId ) const = 0;
	virtual int                   game_lump_size( int lumpId ) const = 0;
	virtual bool                  load_game_lump( int lumpId, void* pBuffer, int size ) = 0;
	virtual int                   level_leaf_count( ) const = 0;
	virtual void*				  get_bsp_tree_query( ) = 0;
	virtual void                  linear_to_gamma( float* linear, float* gamma ) = 0;
	virtual float                 light_style_value( int style ) = 0;
	virtual void                  compute_dynamic_lighting( const vec_t& pt, const vec_t* pNormal, vec_t& color ) = 0;
	virtual void                  get_ambient_light_color( vec_t& color ) = 0;
	virtual int                   get_dx_support_level( ) = 0;
	virtual bool                  supports_hdr( ) = 0;
	virtual void                  mat_stub( void* mat_sys ) = 0;
	virtual void                  get_chapter_name( char* buf, int max_len ) = 0;
	virtual char const*           get_level_name( void ) = 0;
	virtual char const*           get_level_name_short( void ) = 0;
	virtual char const*           get_map_group_name( void ) = 0;
	virtual struct IVoiceTweak_s* get_voice_tweak_api( void ) = 0;
	virtual void                  set_voice_caster_id( unsigned int id ) = 0;
	virtual void                  enginestats_begin_frame( void ) = 0;
	virtual void                  enginestats_end_frame( void ) = 0;
	virtual void                  fire_events( ) = 0;
	virtual int                   get_leaves_area( unsigned short* p_leaves, int leaves ) = 0;
	virtual bool                  does_box_touch_area_frustum( const vec_t& mins, const vec_t& maxs, int area ) = 0;
	virtual int                   get_frustum_list( void** p_list, int list_max ) = 0;
	virtual bool                  should_use_area_frustrum( int i ) = 0;
	virtual void                  set_audio_state( const void* state ) = 0;
	virtual int                   sentence_group_pick( int group_index, char* name, int name_buf_len ) = 0;
	virtual int                   sentence_group_pick_sequential( int group_index, char* name, int name_buf_len, int sentence_index, int reset ) = 0;
	virtual int                   sentence_index_from_name( const char* sentence_name ) = 0;
	virtual const char*           sentence_name_from_index( int sentence_index ) = 0;
	virtual int                   sentence_group_index_from_name( const char* group_name ) = 0;
	virtual const char*           sentence_group_name_from_index( int group_index ) = 0;
	virtual float                 sentence_length( int sentence_index ) = 0;
	virtual void                  compute_lighting( const vec_t& point, const vec_t* normal, bool clamp, vec_t& color, vec_t* box_colors = nullptr ) = 0;
	virtual void                  activate_occluder( int occluder_index, bool active ) = 0;
	virtual bool                  is_occluded( const vec_t& mins, const vec_t& maxs ) = 0;
	virtual int                   get_occlusion_view_id( void ) = 0;
	virtual void*                 save_alloc_memory( size_t num, size_t size ) = 0;
	virtual void                  save_free_memory( void* save_mem ) = 0;
	virtual c_net_channel_info*	  get_net_channel_info( void ) = 0;

	void client_cmd_unrestricted( const char* cmd ) {
		( ( void( __thiscall* )( void*, const char* ) ) get_vfunc( this, 108 ) )( this, cmd );
	}

	c_net_channel* get_net_channel( ) // i wanna fucking die
	{
		return ( ( c_net_channel*( __thiscall* )( void* ) ) get_vfunc( this, 78 ) )( this );
	}

};

struct vcollide_t {
	uint16_t			solid_count : 15;
	uint16_t			packed : 1;
	uint16_t			desc_size;
	c_physics_collide**	solids;
	char*				key_values;
	void*				user_data;
};

struct cmodel_t {
	vec_t		mins, maxs, origin;
	int			head_node;
	vcollide_t	collision_data;
};

struct csurface_t {
	const char*	name;
	short		surface_props;
	uint16_t	flags;
};

struct surfacephysicsparams_t {
	float friction;
	float elasticity;
	float density;
	float thickness;
	float dampening;
};

struct surfaceaudioparams_t {
	float reflectivity;
	float hardness_factor;
	float roughness_factor;
	float rough_threshold;
	float hard_threshold;
	float hard_velocity_threshold;
	float high_pitch_occlusion;
	float mid_pitch_occlusion;
	float low_pitch_occlusion;
};

struct surfacesoundnames_t {
	uint16_t walk_step_left;
	uint16_t walk_step_right;
	uint16_t run_step_left;
	uint16_t run_step_right;
	uint16_t impact_soft;
	uint16_t impact_hard;
	uint16_t scrape_smooth;
	uint16_t scrape_rough;
	uint16_t bullet_impact;
	uint16_t rolling;
	uint16_t break_sound;
	uint16_t strain_sound;
};

struct surfacegameprops_t {
public:
	float		max_speed_factor;
	float		jump_factor;
	float		penetration_modifier;
	float		damage_modifier;
	uint16_t	material;
	bool		climbable;
	byte		pad [ 0x4 ];
};

struct surfacedata_t {
	surfacephysicsparams_t	physics;
	surfaceaudioparams_t	audio;
	surfacesoundnames_t		sounds;
	surfacegameprops_t		game;
};

class c_physics_api {
public:
	virtual ~c_physics_api( void ) {

	}

	virtual int             parse_surface_data( const char* file_name, const char* text_file ) = 0;
	virtual int             surface_prop_count( void ) const = 0;
	virtual int             get_surface_index( const char* surface_prop_name ) const = 0;
	virtual void            get_physics_properties( int surface_data_index, float* density, float* thickness, float* friction, float* elasticity ) const = 0;
	virtual surfacedata_t*  get_surface_data( int surface_data_index ) = 0;
	virtual const char*     get_string( uint16_t string_table_index ) const = 0;
	virtual const char*     get_prop_name( int surface_data_index ) const = 0;
	virtual void            set_world_material_index_table( int* map_array, int map_size ) = 0;
	virtual void            get_physics_parameters( int surface_data_index, surfacephysicsparams_t* params_out ) const = 0;
};

struct ray_t
{
	vec_aligned_t m_Start;  // starting point, centered within the extents
	vec_aligned_t  m_Delta;  // direction + length of the ray
	vec_aligned_t  m_StartOffset; // Add this to m_Start to Get the actual ray start
	vec_aligned_t  m_Extents;     // Describes an axis aligned box extruded along a ray
	const matrix3x4_t *m_pWorldAxisTransform;
	bool m_IsRay;  // are the extents zero?
	bool m_IsSwept;     // is delta != 0?

	ray_t( ) : m_pWorldAxisTransform( NULL ) {}

	void init( vec_t const& start, vec_t const& end )
	{
		m_Delta = end - start;

		m_IsSwept = ( m_Delta.length_sqr( ) != 0 );

		m_Extents.init( );

		m_pWorldAxisTransform = NULL;
		m_IsRay = true;

		// Offset m_Start to be in the center of the box...
		m_StartOffset.init( );
		m_Start = start;
	}

	void init( vec_t const& start, vec_t const& end, vec_t const& mins, vec_t const& maxs )
	{
		m_Delta = end - start;

		m_pWorldAxisTransform = NULL;
		m_IsSwept = ( m_Delta.length_sqr( ) != 0 );

		m_Extents = maxs - mins;
		m_Extents *= 0.5f;
		m_IsRay = ( m_Extents.length_sqr( ) < 1e-6 );

		// Offset m_Start to be in the center of the box...
		m_StartOffset = maxs + mins;
		m_StartOffset *= 0.5f;
		m_Start = start + m_StartOffset;
		m_StartOffset *= -1.0f;
	}
	vec_t inv_delta( ) const
	{
		vec_t vecInvDelta;
		for ( int iAxis = 0; iAxis < 3; ++iAxis ) {
			if ( m_Delta [ iAxis ] != 0.0f ) {
				vecInvDelta [ iAxis ] = 1.0f / m_Delta [ iAxis ];
			}
			else {
				vecInvDelta [ iAxis ] = FLT_MAX;
			}
		}
		return vecInvDelta;
	}

private:
};

struct cplane_t {
	vec_t normal;
	float dist;
	byte type;
	byte signbits;
	byte pad [ 0x2 ];
};

struct client_animating_t {
	c_baseentity* animating;
	uintptr_t flags;

	client_animating_t( c_baseentity* _anim, uintptr_t _flags ) : animating( _anim ), flags( _flags ) { }
};

class utl_vec_simple {
public:
	unsigned memory;
	char pad [ 8 ];
	unsigned int count;

	inline void* retrieve( int index, unsigned sizeofdata ) {
		return ( void* ) ( ( *( unsigned* ) this ) + ( sizeofdata * index ) );
	}
};

class c_base_trace {
public:
	c_base_trace( void ) {

	}

	vec_t			start_pos;
	vec_t			end_pos;
	cplane_t		plane;
	float			fraction;
	int				contents;
	unsigned short	disp_flags;
	bool			all_solid;
	bool			start_solid;
};

class trace_t : public c_base_trace {
public:
	trace_t( ) {

	}

	bool did_hit_world( ) const;

	inline bool did_hit( ) const {
		return fraction < 1 || all_solid || start_solid;
	}

	inline bool is_visible( ) const {
		return fraction > 0.97f;
	}

	float               fraction_left_solid;
	csurface_t          surface;
	int                 hitgroup;
	short               physics_bone;
	unsigned short      world_surface_index;
	void*				hit_entity;
	int                 hitbox;

private:
	trace_t( const trace_t& other ) :
		fraction_left_solid( other.fraction_left_solid ),
		surface( other.surface ),
		hitgroup( other.hitgroup ),
		physics_bone( other.physics_bone ),
		world_surface_index( other.world_surface_index ),
		hit_entity( other.hit_entity ),
		hitbox( other.hitbox ) {
		start_pos = other.start_pos;
		end_pos = other.end_pos;
		plane = other.plane;
		fraction = other.fraction;
		contents = other.contents;
		disp_flags = other.disp_flags;
		all_solid = other.all_solid;
		start_solid = other.start_solid;
	}
};

class c_i_trace_filter {
public:
	virtual bool should_hit_entity( void* entity, int contents_mask ) = 0;
	virtual trace_type_t get_trace_type( ) const = 0;
};

class c_trace_filter : public c_i_trace_filter {
public:
	c_trace_filter( ) {

	}

	c_trace_filter( c_baseentity* entity ) {
		skip = ( void* ) entity;
	}

	bool should_hit_entity( void* entity_handle, int contents_mask ) {
		return entity_handle != skip;
	}

	trace_type_t get_trace_type( ) const {
		return trace_type_t::trace_everything;
	}

	inline void set_ignore_class( char* clazz ) {
		ignore_class = clazz;
	}

	void* skip;
	const char* ignore_class = "";
};

class c_trace_filter_skip_two_entities : public c_i_trace_filter {
public:
	c_trace_filter_skip_two_entities( ) {

	}

	c_trace_filter_skip_two_entities( c_baseentity* entity, c_baseentity* entity1 ) {
		skip = ( void* ) entity;
		skip1 = ( void* ) entity1;
	}

	bool should_hit_entity( void* entity_handle, int contents_mask ) {
		return !( entity_handle == skip || entity_handle == skip1 );
	}

	trace_type_t get_trace_type( ) const {
		return trace_type_t::trace_everything;
	}

	void* skip;
	void* skip1;
};

class c_engine_trace {
public:
	virtual int   get_point_contents( const vec_t& abs_position, uintptr_t mask, void** pp_entity = nullptr ) = 0;
	virtual int   get_point_contents_world_only( const vec_t& abs_position, uintptr_t mask ) = 0;
	virtual int   get_point_contents_collideable( c_collideable* collide, const vec_t& abs_position ) = 0;
	virtual void  clip_ray_to_entity( const ray_t& ray, uintptr_t mask, c_baseentity* entity, trace_t* trace ) = 0;
	virtual void  clip_ray_to_collideable( const ray_t& ray, uintptr_t mask, c_collideable* collide, trace_t* trace ) = 0;
	virtual void  trace_ray( const ray_t& ray, uintptr_t mask, c_i_trace_filter* trace_filter, trace_t* trace ) = 0;
};

class c_studio_hdr;
class c_bone_accessor
{

public:

	inline matrix3x4_t *get_bone_array_for_write( )
	{
		return m_pBones;
	}

	inline void set_bone_array_for_write( matrix3x4_t *bone_array )
	{
		m_pBones = bone_array;
	}

	alignas( 16 ) matrix3x4_t *m_pBones;
	int32_t m_readable_bones; // Which bones can be read.
	int32_t m_writable_bones; // Which bones can be written.
};
class c_move_data {
public:
	bool first_run_of_functions : 1;
	bool game_code_moved_player : 1;
	int player_handle;
	int impulse_command;
	vec_t viewangles;
	vec_t abs_viewangles;
	int buttons;
	int old_buttons;
	float forward_move;
	float side_move;
	float up_move;
	float max_speed;
	float client_max_speed;
	vec_t velocity;
	vec_t angles;
	vec_t old_angles;
	float out_step_height;
	vec_t out_wish_velocity;
	vec_t out_jump_velocity;
	vec_t constraint_center;
	float constraint_radius;
	float constraint_width;
	float constraint_speed_factor;
	float pad [ 5 ];
	vec_t abs_origin;
};

class c_game_movement {
public:
	virtual ~c_game_movement( void ) {

	}

	virtual void process_movement( c_baseentity* entity, c_move_data* move_data ) = 0;
	virtual void reset( void ) = 0;
	virtual void start_track_prediction_errors( c_baseentity* entity ) = 0;
	virtual void finish_track_prediction_errors( c_baseentity* entity ) = 0;

	void full_walk_move( c_baseentity* entity );
};

class c_move_helper {
public:
	uint8_t pad [ 0x2 ];
	int player_handle;
	int impulse_command;
	vec_t viewangles;
	vec_t abs_viewangles;
	int buttons;
	int o_buttons;
	float forward_move;
	float side_move;
	float up_move;
	float max_speed;
	float client_max_speed;
	vec_t velocity;
	vec_t angles;
	vec_t old_angles;
	float out_step_height;
	vec_t out_wish_vel;
	vec_t out_jump_vel;
	vec_t constraint_center;
	float constraint_radius;
	float constraint_width;
	float constraint_speed_factor;
	float pad1 [ 0x5 ];
	vec_t abs_origin;

	virtual void pad_0x0( ) = 0;
	virtual void set_host( void* host ) = 0;

	virtual void pad00( ) = 0;
	virtual void pad01( ) = 0;

	virtual void process_impacts( ) = 0;
};

class c_prediction {
public:
	void get_local_viewangles( vec_t &angle ) {
		( ( void( __thiscall * )( void *, vec_t & )  ) get_vfunc( this, 12 ) )( this, angle );
	}

	void set_local_viewangles( vec_t &angle ) {
		( ( void( __thiscall * )( void *, vec_t & ) ) get_vfunc( this, 13 ) )( this, angle );
	}
/*	bool in_prediction( ) {
		return ( ( bool( __thiscall* )( void* ) ) get_vfunc( this, 14 ) )( this );
	}*/

	void run_command( c_baseentity* entity, c_usercmd* cmd, c_move_helper* move_helper ) {
		( ( void( __thiscall* )( void*, c_baseentity*, c_usercmd*, c_move_helper* ) ) get_vfunc( this, 19 ) )( this, entity, cmd, move_helper );
	}
	void update( int start_frame, bool valid_frame, int inc_ack, int out_cmd )
	{
		( ( void( __thiscall* )( void *, int, bool, int, int ) ) get_vfunc( this, 3 ) )( this, start_frame, valid_frame, inc_ack, out_cmd );
	}

	void check_moving_ground( c_baseentity *entity, double frametime )
	{
		( ( void( __thiscall* )( void *, c_baseentity *, double ) ) get_vfunc( this, 18 ) )( this, entity, frametime );
	}
	void setup_move( c_baseentity* entity, c_usercmd* cmd, c_move_helper* move_helper, void* move_data ) {
		( ( void( __thiscall* )( void*, c_baseentity*, c_usercmd*, c_move_helper*, void* ) ) get_vfunc( this, 20 ) )( this, entity, cmd, move_helper, move_data );
	}

	void finish_move( c_baseentity* entity, c_usercmd* cmd, void* move_data ) {
		( ( void( __thiscall* )( void*, c_baseentity*, c_usercmd*, void* ) ) get_vfunc( this, 21 ) )( this, entity, cmd, move_data );
	}

	char pad00 [ 8 ]; 					// 0x0000
	bool in_prediction;				// 0x0008
	char pad01 [ 1 ];					// 0x0009
	bool engine_paused;				// 0x000A
	char pad02 [ 13 ];					// 0x000B
	bool is_first_time_predicted;		// 0x0018
};

class c_material_system {
public:
	c_material * create_material( const char* name, void* kv ) {
		return ( ( c_material*( __thiscall* )( void*, const char*, void* ) ) get_vfunc( this, 83 ) )( this, name, kv );
	}

	c_material * find_material( char const* name, const char* tex_group, bool complain = true, const char* complain_prefix = nullptr ) {
		return ( ( c_material*( __thiscall* )( void*, char const*, const char*, bool, const char* ) ) get_vfunc( this, 84 ) )( this, name, tex_group, complain, complain_prefix );
	}

	uint16_t first_material( void ) {
		return ( ( uint16_t( __thiscall* )( void* ) ) get_vfunc( this, 86 ) )( this );
	}

	uint16_t next_material( uint16_t mat_handle ) {
		return ( ( uint16_t( __thiscall* )( void*, uint16_t ) ) get_vfunc( this, 87 ) )( this, mat_handle );
	}

	uint16_t invalid_material( void ) {
		return ( ( uint16_t( __thiscall* )( void* ) ) get_vfunc( this, 88 ) )( this );
	}

	c_material * get_material( uint16_t material_handle ) {
		return ( ( c_material*( __thiscall* )( void*, uint16_t ) ) get_vfunc( this, 89 ) )( this, material_handle );
	}
};

class c_model_render {
public:
	void forced_material_override( c_material* mat ) {
		( ( void( __thiscall* )( void*, c_material*, void*, void* ) ) get_vfunc( this, 1 ) )( this, mat, nullptr, nullptr );
	}
};

class c_render_view {
public:
	void set_blend( float blend ) {
		( ( void( __thiscall* )( void*, float ) ) get_vfunc( this, 4 ) )( this, blend );
	}

	void set_blend( int blend ) {
		set_blend( static_cast< float >( blend ) / 255.0f );
	}

	void set_color_modulation( float* clr ) {
		( ( void( __thiscall* )( void*, float* ) ) get_vfunc( this, 6 ) )( this, clr );
	}

	void set_color_modulation( float r, float g, float b ) {
		float clr [ 3 ] { r, g, b };
		set_color_modulation( clr );
	}

	void set_color_modulation( int r, int g, int b ) {
		float clr [ 3 ] { static_cast< float >( r ) / 255.0f, static_cast< float >( g ) / 255.0f, static_cast< float >( b ) / 255.0f };
		set_color_modulation( clr );
	}

};

#pragma pack( push, 1 )

class c_clock_drift_mgr {
public:
	float		clock_offsets [ 17 ];
	uint32_t	current_clock_offset;
	uint32_t	server_tick;
	uint32_t	client_tick;
};

class c_client_state {
public:
	char pad_0000 [ 148 ]; //0x0000
	c_net_channel *net_channel; //0x0094
	char pad_0098 [ 8 ]; //0x0098
	uint32_t m_nChallengeNr; //0x00A0
	char pad_00A4 [ 100 ]; //0x00A4
	uint32_t m_nSignonState; //0x0108
	char pad_010C [ 8 ]; //0x010C
	float m_flNextCmdTime; //0x0114
	uint32_t m_nServerCount; //0x0118
	uint32_t m_nCurrentSequence; //0x011C
	char pad_0120 [ 84 ]; //0x0120
	uint32_t m_nDeltaTick; //0x0174
	bool m_bPaused; //0x0178
	char pad_0179 [ 7 ]; //0x0179
	uint32_t m_nViewEntity; //0x0180
	uint32_t m_nPlayerSlot; //0x0184
	char m_szLevelName [ 260 ]; //0x0188
	char m_szLevelNameShort [ 40 ]; //0x028C
	char m_szGroupName [ 40 ]; //0x02B4
	char pad_02DC [ 52 ]; //0x02DC
	uint32_t m_nMaxClients; //0x0310
	char pad_0314 [ 18820 ]; //0x0314
	float m_flLastServerTickTime; //0x4C98
	bool insimulation; //0x4C9C
	char pad_4C9D [ 3 ]; //0x4C9D
	uint32_t oldtickcount; //0x4CA0
	float m_tickRemainder; //0x4CA4
	float m_frameTime; //0x4CA8
	uint32_t lastoutgoingcommand; //0x4CAC
	uint32_t chokedcommands; //0x4CB0
	uint32_t last_command_ack; //0x4CB4
	uint32_t command_ack; //0x4CB8
	uint32_t m_nSoundSequence; //0x4CBC
	char pad_4CC0 [ 80 ]; //0x4CC0
	vec_t viewangles; //0x4D10
	char pad_4D1C [ 208 ]; //0x4D1C
	void *events; //0x4DEC

	void full_update( ) {
		m_nDeltaTick = -1;
	}
};

#pragma pack( pop )

class c_base_entity_list {
private:
	virtual void*	get_client_networkable( int i ) = 0;
	virtual void*	pad_0x4( void ) = 0;
	virtual void*	pad_0x8( void ) = 0;

public:
	virtual void*   get_client_entity( int i ) = 0;
	virtual void*   get_client_entity_from_handle( void* handle ) = 0;
	virtual int		number_of_entities( bool include_non_networkable ) = 0;
	virtual int		get_highest_entity_index( void ) = 0;

private:
	virtual void	set_max_entities( int max ) = 0;
	virtual int		get_max_entities( void ) = 0;
};

class c_model_info {
public:
	const model_t* get_model( int model_index ) {
		return ( ( const model_t*( __thiscall* )( void*, int ) ) get_vfunc( this, 1 ) )( this, model_index );
	}

	int get_model_index( const char* name ) {
		return ( ( int( __thiscall* )( void*, const char* ) ) get_vfunc( this, 2 ) )( this, name );
	}

	const char* get_model_name( const model_t* model ) {
		return ( ( const char*( __thiscall* )( void*, const model_t* ) ) get_vfunc( this, 3 ) )( this, model );
	}

	studiohdr_t* get_studio_model( const model_t* model ) {
		return ( ( studiohdr_t*( __thiscall* )( void*, const model_t* ) ) get_vfunc( this, 32 ) )( this, model );
	}
};

class c_localize {
public:
	virtual void vtpad_0( void ) = 0;
	virtual void vtpad_1( void ) = 0;
	virtual void vtpad_2( void ) = 0;
	virtual void vtpad_3( void ) = 0;
	virtual void vtpad_4( void ) = 0;
	virtual void vtpad_5( void ) = 0;
	virtual void vtpad_6( void ) = 0;
	virtual void vtpad_7( void ) = 0;
	virtual void vtpad_8( void ) = 0;
	virtual bool add_file( const char* file, const char* path_id = nullptr, bool include_fallback_search_paths = false ) = 0;
	virtual void remove_all( void ) = 0;
	virtual wchar_t* find( const char* token ) = 0;
	virtual const wchar_t*	 find_safe( const char* token ) = 0;
	virtual int ansi_to_unicode( const char* ansi, wchar_t* unicode, int sz ) = 0;
	virtual int unicode_to_ansi( const wchar_t* unicode, char* ansi, int sz ) = 0;
	virtual uintptr_t find_index( const char* token ) = 0;
	virtual void construct_string( wchar_t* unicode, int sz, const wchar_t* str, int num_params, ... ) = 0;
	virtual const char*	get_name_by_index( uintptr_t index ) = 0;
	virtual wchar_t*	get_value_by_index( uintptr_t index ) = 0;
};

/*
*	credits to namazo for this super fast and easy-to-use nevar manager ;3
*/

#define netvar_ptr( type, func, name ) auto func( void ) {		\
	constexpr fnv_t hash = fnv_hash( name );					\
	static uint16_t offset = 0;									\
																\
	if ( !offset )												\
		offset = netvars->get_offset( hash );	\
																\
	return ( type* ) ( ( uintptr_t ) this + offset );			\
}

/*
*	credits to namazo for this super fast and easy-to-use nevar manager ;3
*/

#define netvar( type, func, name ) auto& func( void ) {			\
	constexpr fnv_t hash = fnv_hash( name );					\
	static uint16_t offset = 0;									\
																\
	if ( !offset )												\
		offset = netvars->get_offset( hash );	\
																\
	return *( type* ) ( ( uintptr_t ) this + offset );			\
}

#define offset( t, n, o )\
t &n( )\
{\
	return *( t * )( (uintptr_t)this + o );\
}

class c_client_entity {
public:
	netvar( vec_t, get_origin, "CBaseEntity->m_vecOrigin" );

	netvar_ptr( c_collideable, get_collision, "CBaseEntity->m_Collision" );

	void* get_renderable( void ) {
		return ( void* ) ( ( uintptr_t ) this + 0x4 );
	}

	void* get_networkable( void ) {
		return ( void* ) ( ( uintptr_t ) this + 0x8 );
	}

	client_class* get_client_class( void ) {
		if ( !this )
			return nullptr;

		void* networked = get_networkable( );

		if ( !networked )
			return nullptr;

		return ( ( client_class*( __thiscall* )( void* ) ) get_vfunc( networked, 2 ) )( networked );
	}

	bool is_player( void ) {
		if ( !this )
			return false;

		return get_client_class( )->class_id == 40;
	}

	bool is_weapon( void ) {
		if ( !this )
			return false;

		return ( ( bool( __thiscall* )( void* ) ) get_vfunc( this, 160 ) )( this );
	}

	int get_index( void ) {
		if ( !this )
			return 0;

		return *( int* ) ( ( uintptr_t ) this + 0x64 );
	}

	bool& get_dormant( void ) {
		return *( bool* ) ( ( uintptr_t ) this + 0xED );
	}
};

class c_baseviewmodel {
public:
	netvar( int, get_model_index, "CBaseEntity->m_nModelIndex" );
	netvar( int, get_viewmodel_index, "CBaseViewModel->m_nViewModelIndex" );

	inline c_baseweapon* get_weapon( void ) {
		constexpr fnv_t hash = fnv_hash( "CBaseViewModel->m_hWeapon" );

		static uint16_t offset = 0;

		if ( !offset )
			offset = netvars->get_offset( hash );

		return ( c_baseweapon* ) entity_list->get_client_entity_from_handle( ( void* ) ( ( uintptr_t ) this + offset ) );
	}

	inline c_baseentity* get_owner( void ) {
		constexpr fnv_t hash = fnv_hash( "CBaseViewModel->m_hOwner" );

		static uint16_t offset = 0;

		if ( !offset )
			offset = netvars->get_offset( hash );

		return ( c_baseentity* ) entity_list->get_client_entity_from_handle( ( void* ) ( ( uintptr_t ) this + offset ) );
	}
};

extern bool in_setup_bones;

class c_baseentity : public c_client_entity {
public:
	__declspec( noinline ) static c_baseentity* get_client_entity( int ent_num ) {
		return ( c_baseentity* ) entity_list->get_client_entity( ent_num );
	}

	netvar( int, get_health, "CBasePlayer->m_iHealth" );
	netvar( int, get_flags, "CBasePlayer->m_fFlags" );
	netvar( int, get_hide_hud, "CBasePlayer->m_iHideHUD" );
	netvar( bool, get_ducked, "CBasePlayer->m_bDucked" );
	netvar( bool, get_ducking, "CBasePlayer->m_bDucking" );
	netvar( vec_t, get_view_offset, "CBasePlayer->m_vecViewOffset[0]" );
	netvar( float, get_last_duck_time, "CBasePlayer->m_flLastDuckTime" );
	netvar( float, get_fall_velocity, "CBasePlayer->m_flFallVelocity" );
	netvar( vec_t, get_viewpunch_angle, "CBasePlayer->m_viewPunchAngle" );
	netvar( vec_t, get_aimpunch_angle, "CBasePlayer->m_aimPunchAngle" );
	netvar( vec_t, get_aimpunch_angle_velocity, "CBasePlayer->m_aimPunchAngleVel" );
	netvar( int, get_tickbase, "CBasePlayer->m_nTickBase" );
	netvar( vec_t, get_velocity, "CBasePlayer->m_vecVelocity[0]" );
	netvar( float, get_lagged_movement_value, "CBasePlayer->m_flLaggedMovementValue" );
	netvar( byte, get_deadflag, "CBasePlayer->deadflag" );
	netvar( byte, get_lifestate, "CBasePlayer->m_lifeState" );
	netvar( float, get_animtime, "CBasePlayer->m_flAnimTime" );
	netvar( vec_t, get_eyeangles, "CCSPlayer->m_angEyeAngles[0]" );
	netvar( float, get_stamina, "CCSPlayer->m_flStamina" );
	netvar( float, get_max_speed, "CBasePlayer->m_flMaxspeed" );
	netvar( vec_t, get_base_velocity, "CBasePlayer->m_vecBaseVelocity" );
	netvar( bool, get_in_bomb_zone, "CCSPlayer->m_bInBombZone" );
	netvar( bool, get_in_buy_zone, "CCSPlayer->m_bInBuyZone" );
	netvar( bool, get_in_no_defuse_area, "CCSPlayer->m_bInNoDefuseArea" );
	netvar( bool, get_night_vision_on, "CCSPlayer->m_bNightVisionOn" );
	netvar( bool, get_has_night_vision, "CCSPlayer->m_bHasNightVision" );
	netvar( bool, get_is_defusing, "CCSPlayer->m_bIsDefusing" );
	netvar( bool, get_in_hostage_rescue_zone, "CCSPlayer->m_bInHostageRescueZone" );
	netvar( bool, get_is_grabbing_hostage, "CCSPlayer->m_bIsGrabbingHostage" );
	netvar( bool, get_is_scoped, "CCSPlayer->m_bIsScoped" );
	netvar( bool, get_is_walking, "CCSPlayer->m_bIsWalking" );
	netvar( float, get_immune_to_damage_time, "CCSPlayer->m_fImmuneToGunGameDamageTime" );
	netvar( bool, get_immune, "CCSPlayer->m_bGunGameImmunity" );
	netvar( bool, get_has_moved_since_spawn, "CCSPlayer->m_bHasMovedSincespawn" );
	netvar( bool, get_is_rescuing, "CCSPlayer->m_bIsRescuing" );
	netvar( int, get_kills, "CCSPlayer->m_iMatchStats_Kills" );
	netvar( int, get_damage, "CCSPlayer->m_iMatchStats_Damage" );
	netvar( int, get_killreward, "CCSPlayer->m_iMatchStats_KillReward" );
	netvar( int, get_equipment_value, "CCSPlayer->m_iMatchStats_EquipmentValue" );
	netvar( int, get_money_saved, "CCSPlayer->m_iMatchStats_MoneySaved" );
	netvar( int, get_deaths, "CCSPlayer->m_iMatchStats_Deaths" );
	netvar( int, get_assists, "CCSPlayer->m_iMatchStats_Assists" );
	netvar( int, get_headshot_kills, "CCSPlayer->m_iMatchStats_HeadShotKills" );
	netvar( int, get_rank, "CCSPlayer->m_rank" );
	netvar( int, get_armor, "CCSPlayer->m_ArmorValue" );
	netvar( bool, get_has_heavy_armor, "CCSPlayer->m_bHasHeavyArmor" );
	netvar( int, get_music_id, "CCSPlayer->m_unMusicID" );
	netvar( bool, get_has_helmet, "CCSPlayer->m_bHasHelmet" );
	netvar( float, get_flash_duration, "CCSPlayer->m_flFlashDuration" );
	netvar( float, get_flash_max_alpha, "CCSPlayer->m_flFlashMaxAlpha" );
	netvar( bool, get_radar_hidden, "CCSPlayer->m_bHud_RadarHidden" );
	netvar( int, get_deathcam_music, "CCSPlayer->m_nDeathCamMusic" );
	netvar( bool, get_is_holding_look_at_weapon, "CCSPlayer->m_bIsHoldingLookAtWeapon" );
	netvar( bool, get_is_looking_at_weapon, "CCSPlayer->m_bIsLookingAtWeapon" );
	netvar( int, get_round_headshot_kills, "CCSPlayer->m_iNumRoundKillsHeadshots" );
	netvar( float, get_lby, "CCSPlayer->m_flLowerBodyYawTarget" );
	netvar( float, get_thirdperson_recoil, "CCSPlayer->m_flThirdpersonRecoil" );
	netvar( bool, get_strafing, "CCSPlayer->m_bStrafing" );
	netvar( int, get_team, "CBaseEntity->m_iTeamNum" );
	netvar( int, get_collision_group, "CBaseEntity->m_CollisionGroup" );
	netvar( float, get_simulation_time, "CBaseEntity->m_flSimulationTime" );
	netvar( int, get_observer_mode, "CBasePlayer->m_iObserverMode" );
	netvar( int, get_hitbox_set, "CBasePlayer->m_nHitboxSet" );
	netvar( int, get_shots_fired, "CCSPlayer->m_iShotsFired" );
	netvar( int, get_ping, "CCSPlayerResource->m_iPing" );
	netvar( float, get_friction, "CBasePlayer->m_flFriction" );
	netvar_ptr( int, m_hconstraint_entity, "CBasePlayer->m_hConstraintEntity" );
	netvar( float, get_surface_friction, "CBaseEntity->m_surfaceFriction" );
	netvar( float, get_step_size, "CBasePlayer->m_flStepSize" );
	netvar( vec_t, get_rotation, "CCSPlayer->m_angRotation" );
	netvar( bool, get_clientside_animation, "CBaseAnimating->m_bClientSideAnimation" );
	netvar( float, get_next_attack, "CBaseCombatCharacter->m_flNextAttack" );
	netvar( int, get_force_bone, "CBaseAnimating->m_nForceBone" );
	netvar( float, get_c4_blow_time, "CPlantedC4->m_flC4Blow" );
	netvar( bool, get_bomb_ticking, "CPlantedC4->m_bBombTicking" );
	netvar( float, get_defuse_countdown, "CPlantedC4->m_flDefuseCountDown" );
	offset( int, get_mneffects, 0xE8 );
	offset( int, get_eflags, 0x44 );
	offset( vec_t, get_abs_velocity, 0x94 );
	offset( int, last_setupbones_frame, 0xA68 );

	const matrix3x4_t& get_coorinate_frame( void ) {
		constexpr fnv_t hash = fnv_hash( "CBaseEntity->m_CollisionGroup" );
		static uint16_t offset = 0;

		if ( !offset )
			offset = netvars->get_offset( hash );

		return *( matrix3x4_t* ) ( ( uintptr_t ) this + offset );
	}

	std::vector< c_baseweapon* > get_my_weapons( void ) {
		constexpr fnv_t hash = fnv_hash( "CBaseCombatCharacter->m_hMyWeapons" );
		static uint16_t offset = 0;

		if ( !offset )
			offset = netvars->get_offset( hash );

		uintptr_t* weapon_handles = ( uintptr_t* ) ( ( uintptr_t ) this + offset );

		std::vector< c_baseweapon* > weapon_list { };

		for ( int i = 0; i < 64; i++ ) {
			auto weapon = ( c_baseweapon* ) entity_list->get_client_entity_from_handle( ( void* ) ( weapon_handles [ i ] ) );

			if ( !weapon )
				continue;

			weapon_list.push_back( weapon );
		}

		return weapon_list;
	}

	bool is_bomb( ) {
		if ( !this )
			return false;

		return strcmp( this->get_client_class( )->network_name, "CPlantedC4" ) == 0 ? true : false;
	}


	//bool is_weapon( void ) {
	//	if ( !this )
	//		return false;

	//	return strstr( this->get_client_class( )->network_name, "CWeapon" ) != nullptr;
	//}
	const vec_t world_space_center( )
	{
		vec_t vec_origin = get_origin( );

		vec_t min = this->get_collision( )->vec_mins( ) + vec_origin;
		vec_t max = this->get_collision( )->vec_maxs( ) + vec_origin;

		vec_t size = max - min;
		size /= 2.f;
		size += min;

		return size;
	}

	c_baseentity* bomb_defuser( void ) {
		constexpr fnv_t hash = fnv_hash( "CPlantedC4->m_hBombDefuser" );
		static uint16_t offset = 0;

		if ( !offset )
			offset = netvars->get_offset( hash );

		return ( c_baseentity* ) entity_list->get_client_entity_from_handle( ( void* ) ( *( uintptr_t* ) ( ( uintptr_t ) this + offset ) ) );
	}

	matrix3x4_t* get_bone_cache( void ) {
		return *( matrix3x4_t** ) ( ( uintptr_t ) this + 0x2900 );
	}

	float& get_cycle( void ) {
		return *( float* ) ( this + 0xA14 );
	}

	int& get_sequence( void ) {
		return *( int* ) ( this + 0x28AC );
	}

	float& get_duck_amount( void ) {
		return *( float* ) ( this + 0x2F9C );
	}

	float& get_duck_speed( void ) {
		return *( float* ) ( this + 0x2FA0 );
	}
	float max_desync( c_csgoplayeranimstate* override_animstate = nullptr, bool jitter = false ) {
		float max_desync_angle = 0.f;

		auto anim_state = override_animstate != nullptr ? override_animstate : this->get_anim_state( );
		if ( !anim_state )
			return max_desync_angle;

		float duck_amount = anim_state->duck_amount;
		float speed_fraction = std::max< float >( 0, std::min< float >( anim_state->feet_speed_forwards_or_sideways, 1 ) );
		float speed_factor = std::max< float >( 0, std::min< float >( 1, anim_state->feet_speed_unknown_forwards_or_sideways ) );

		float yaw_modifier = ( ( ( anim_state->stop_to_full_running_fraction * -0.3f ) - 0.2f ) * speed_fraction ) + 1.0f;

		if ( duck_amount > 0.f ) {
			yaw_modifier += ( ( duck_amount * speed_factor ) * ( 0.5f - yaw_modifier ) );
		}

		max_desync_angle = anim_state->velocity_subtract_y * yaw_modifier;
		auto yaw_feet_delta = anim_state->goal_feet_yaw - anim_state->eye_angles_y;
		if ( jitter ) {
			if ( yaw_feet_delta < max_desync_angle ) {
				max_desync_angle = 180.f;
			}
		}

		return max_desync_angle;
	}
	//bool& is_ghost( void ) {
	//	return *( bool* ) ( this + 0x3A11 );
	//}

	vec_t crouch_additive( void ) {
		return vec_t( 0.0f, 0.0f, get_view_offset( ).z );
	}

	int lookup_pose_parameter( const char* pose_name );

	bool handle_bone_setup( int32_t boneMask, matrix3x4_t * boneOut, float_t curtime );

	bool can_shoot( );

	uintptr_t& get_inverse_kinematics( void ) {
		return *( uintptr_t* ) ( ( uintptr_t ) this + 0x68 );
	}

	uintptr_t& get_effects( void ) {
		return *( uintptr_t* ) ( ( uintptr_t ) this + 0xEC );
	}

	void force_bone_recalculation( void ) {
		*( int* ) ( ( uintptr_t ) this + 0xA30 ) = globals->frame_count;
		*( int* ) ( &get_force_bone( ) + 0x20 ) = 0;
		*( int* ) ( &get_force_bone( ) + 0x4 ) = -1;
	}

	auto& get_ragdoll_pos( void ) {
		constexpr fnv_t hash = fnv_hash( "CRagdollProp->m_ragPos" );
		static uint16_t offset = 0;

		if ( !offset )
			offset = netvars->get_offset( hash );

		return *( std::array< float, 24 >* ) ( ( uintptr_t ) this + offset );
	}

	auto& get_poseparameter( void ) {
		constexpr fnv_t hash = fnv_hash( "CBaseAnimating->m_flPoseParameter" );
		static uint16_t offset = 0;

		if ( !offset )
			offset = netvars->get_offset( hash );

		return *( std::array< float, 24 >* ) ( ( uintptr_t ) this + offset );
	}

	inline bool is_enemy( void ) {
		return local_player->get_team( ) != get_team( );
	}

	inline bool is_teammate( void ) {
		return local_player->get_team( ) == get_team( );
	}

	inline const char* get_classname( void ) {
		return ( ( const char*( __thiscall* )( c_baseentity* ) ) *( uintptr_t* ) ( *( uintptr_t* ) this + 556 ) )( this );
	}

	bool is_valid_player( void ) {

		if ( !this )
			return false;

		if ( this->get_dormant( ) || !this->is_alive( ) )
			return false;

		if ( !this->is_enemy( ) )
			return false;

		if ( this->get_client_class( )->class_id != 40 )
			return false;

		if ( this == local_player )
			return false;

		if ( this->get_immune( ) )
			return false;


		return true;

	}
	bool is_valid( void ) {

		if ( !this )
			return false;

		if ( !this->is_enemy( ) )
			return false;

		if ( this->get_client_class( )->class_id != 40 )
			return false;

		if ( this == local_player )
			return false;

		if ( this->get_immune( ) )
			return false;


		return true;

	}
	uint8_t& get_movetype( void ) {
		return *( uint8_t* ) ( ( uintptr_t ) this + 0x258 );
	}

	int& get_takedamage( void ) {
		return *( int* ) ( ( uintptr_t ) this + 0x280 );
	}

	vec_t& get_playerstate_viewangles( void ) {
		return *( vec_t* ) ( ( uintptr_t ) &get_deadflag( ) + 0x4 );
	}

	float& get_old_simulation_time( void ) {
		static fnv_t hash = fnv_hash( "CBaseEntity->m_flSimulationTime" );
		static uint16_t offset = 0;

		if ( !offset )
			offset = netvars->get_offset( hash );

		return *( float* ) ( ( uintptr_t ) this + offset + 0x4 );
	}

	vec_t& get_abs_rotation( void ) {
		return *( vec_t* ) ( ( uintptr_t ) &get_rotation( ) - 0xC );
	}

	void set_pose_angles( float yaw, float pitch ) {
		get_poseparameter( ).at( 11 ) = ( pitch + 90.0f ) / 180.0f;
		get_poseparameter( ).at( 2 ) = ( yaw + 180.0f ) / 360.0f;
	}

	bool is_moving( void ) {
		return this->get_velocity( ).length_2d( ) > minimum_moving_speed;
	}

	float get_spawn_time( void ) {
		if ( !this )
			return 0.0f;

		return *( float* ) ( ( uintptr_t ) this + 0xA2A0 );
	}

	vec_t get_abs_origin( void ) {
		if ( !this )
			return vec_t( );

		return ( ( vec_t&( __thiscall* )( void* ) ) get_vfunc( this, 10 ) )( this );
	}

	vec_t get_abs_angles( void );
	void set_abs_origin( const vec_t& value );
	void set_abs_angles( const vec_t& angles );

	int draw_model( void ) {
		if ( !this )
			return -1;

		void* renderable = get_renderable( );

		if ( !renderable )
			return -1;

		return ( ( int( __thiscall* )( void*, int, byte ) ) get_vfunc( renderable, 9 ) )( renderable, 1, 255 );
	}

	bool is_player( void ) {
		return ( ( bool( __thiscall* )( void* ) ) get_vfunc( this, 155 ) )( this );
	}

	bool& is_self_animating( void ) {
		return ( ( bool&( __thiscall* )( void* ) ) get_vfunc( this, 152 ) )( this );
	}

	bool setup_bones( matrix3x4_t* out, int max_bones = 128, int bone_mask = 256, float current_time = 0.0f ) {
		if ( !this )
			return false;

		auto renderable = get_renderable( );

		if ( !renderable )
			return false;

		force_bone_recalculation( );


		in_setup_bones = true;
		/*		int Backup = *( int* )( ( uintptr_t )this + 0x228 );
				*( int* )( ( uintptr_t )this + 0x228 ) = 0;
				*( int* )( ( uintptr_t )this + 0x53 ) |= 8;
				*/
	//	int Backup = *( int* ) ( ( uintptr_t ) this + 0x274 );
		//*( int* ) ( ( uintptr_t ) this + 0x274 ) = 0;

		//*( int* )( ( uintptr_t )this + 0x53 ) &= 0xFFFFFFF7;
		//*( int* ) ( ( uintptr_t ) this + 0x274 ) = Backup;
		const auto Backup = *( int* ) ( uintptr_t( this ) + ptrdiff_t( 0x272 ) );

		*( int* ) ( uintptr_t( this ) + ptrdiff_t( 0x272 ) ) = -1;
		auto ret = ( ( bool( __thiscall* )( void*, matrix3x4_t*, int, int, float ) ) get_vfunc( renderable, 13 ) )( renderable, out, max_bones, 0x00000100 | 0x200, current_time );

		*( int* ) ( uintptr_t( this ) + ptrdiff_t( 0x272 ) ) = Backup;

		in_setup_bones = false;
		return ret;
	}

	const model_t* get_model( void ) {
		if ( !this )
			return nullptr;

		void* renderable = get_renderable( );

		if ( !renderable )
			return nullptr;

		return ( ( const model_t*( __thiscall* )( void* ) ) get_vfunc( renderable, 8 ) )( renderable );
	}

	client_class* get_client_class( void ) {
		if ( !this )
			return nullptr;

		void* networked = get_networkable( );

		if ( !networked )
			return nullptr;

		return ( ( client_class*( __thiscall* )( void* ) ) get_vfunc( networked, 2 ) )( networked );
	}

	void* get_data_table_base_ptr( void ) {
		if ( !this )
			return nullptr;

		void* networked = get_networkable( );

		if ( !networked )
			return nullptr;

		return ( ( void*( __thiscall* )( void* ) ) get_vfunc( networked, 12 ) )( networked );
	}

	bool is_alive( void ) {
		return ( byte ) get_lifestate( ) == ( byte ) life_state_t::life_alive;
	}

	bool has_c4( void );
	vec_t get_eye_pos( void );
	void invalidate_bone_cache( void );
	void update_clientside_animation( void );

	void reset_animation_state( c_csgoplayeranimstate* state );
	void set_current_command( c_usercmd * cmd );

	void update_animation_state( c_csgoplayeranimstate* state, vec_t angle );
	void create_animation_state( c_csgoplayeranimstate* state );

	c_csgoplayeranimstate* get_anim_state( void );
	int get_sequence_activity( int sequence );

	c_bone_accessor * get_bone_accessor( );

	c_studio_hdr * get_model_ptr( );

	void standard_bleding_rules( c_studio_hdr * hdr, vec_t * pos, quaternion * q, float_t curtime, int32_t boneMask );

	void build_transformations( c_studio_hdr * hdr, vec_t * pos, quaternion * q, const matrix3x4_t & cameraTransform, int32_t boneMask, byte * computed );

	void build_bones( matrix3x4_t * boneOut );

	c_baseviewmodel* get_viewmodel( void ) {
		constexpr fnv_t hash = fnv_hash( "CBasePlayer->m_hViewModel[0]" );
		static uint16_t offset = 0;

		if ( !offset )
			offset = netvars->get_offset( hash );

		return ( c_baseviewmodel* ) entity_list->get_client_entity_from_handle( ( void* ) ( *( uintptr_t* ) ( ( uintptr_t ) this + offset ) ) );
	}

	c_baseentity* get_observer_target( void ) {
		constexpr fnv_t hash = fnv_hash( "CBasePlayer->m_hObserverTarget" );
		static uint16_t offset = 0;

		if ( !offset )
			offset = netvars->get_offset( hash );

		return ( c_baseentity* ) entity_list->get_client_entity_from_handle( ( void* ) ( *( uintptr_t* ) ( ( uintptr_t ) this + offset ) ) );
	}

	c_baseweapon* get_active_weapon( void ) {
		constexpr fnv_t hash = fnv_hash( "CBaseCombatCharacter->m_hActiveWeapon" );
		static uint16_t offset = 0;

		if ( !offset )
			offset = netvars->get_offset( hash );

		return ( c_baseweapon* ) entity_list->get_client_entity_from_handle( ( void* ) ( *( uintptr_t* ) ( ( uintptr_t ) this + offset ) ) );
	}

	void force_angle( vec_t angle ) {
		get_eyeangles( ) = angle;
		get_abs_rotation( ) = vec_t( 0.0f, 0.0f, 0.0f );
		set_pose_angles( angle.y, angle.x );
		set_abs_angles( vec_t( 0.0f, angle.y, 0.0f ) );
	}

	int get_num_anim_overlays( void ) {
		return *( int* ) ( ( uintptr_t ) this + 0x298C );
	}

	animationlayer* get_anim_overlays( void ) {


		return *( animationlayer** ) ( ( uintptr_t ) this + 0x2980 );
	}

	animationlayer& get_anim_overlay( int index )
	{
		return ( *( animationlayer** ) ( ( DWORD ) this + 0x2980 ) ) [ index ];
	}

	const matrix3x4_t& get_bone_matrix( int bone ) {
		if ( !this || !this->is_alive( ) || this->get_dormant( ) )
			return matrix3x4_t( );

		uintptr_t offset = *( uintptr_t* ) ( ( uintptr_t ) this + 0x26A8 );

		if ( offset )
			return *( matrix3x4_t* ) ( offset + 0x30 * bone );

		return matrix3x4_t( );
	}

	vec_t get_bone_pos( int bone ) {


		return get_bone_matrix( bone ).at( 3 );
	}

	void set_local_viewangles( vec_t &angle ) {
		( ( void( __thiscall* )( void *, vec_t & ) ) get_vfunc( this, 369 ) )( this, angle);
	}
	
	

	void think( ) {
		( ( void( __thiscall* )( void* ) ) get_vfunc( this, 138 ) )( this );
	}

	void pre_think( ) {
		( ( void( __thiscall* )( void* ) ) get_vfunc( this, 314 ) )( this );
	}

	void post_think( ) {
		( ( void( __thiscall* )( void* ) ) get_vfunc( this, 315 ) )( this );
	}
	const char* get_name( ) {
		if ( is_player( ) ) {
			player_info_t info;
			engine->get_player_info( get_index( ), &info );
			return info.name;
		}
		else {
			if ( is_weapon( ) )
				return "bomb";
			else
				return "environment";
		}
	}
};

class c_baseweapon : public c_client_entity {
public:
	__declspec( noinline ) static c_baseweapon* get_client_entity( int ent_num ) {
		return ( c_baseweapon* ) entity_list->get_client_entity( ent_num );
	}

	netvar( bool, get_initialized, "CBaseAttributableItem->m_bInitialized" );
	netvar( short, get_item_definition_index, "CBaseAttributableItem->m_iItemDefinitionIndex" );
	netvar( int, get_entity_level, "CBaseAttributableItem->m_iEntityLevel" );
	netvar( int, get_account_id, "CBaseAttributableItem->m_iAccountID" );
	netvar( int, get_item_id_low, "CBaseAttributableItem->m_iItemIDLow" );
	netvar( int, get_item_id_high, "CBaseAttributableItem->m_iItemIDHigh" );
	netvar( int, get_entity_quality, "CBaseAttributableItem->m_iEntityQuality" );
	netvar( float, get_next_primary_attack, "CBaseCombatWeapon->m_flNextPrimaryAttack" );
	netvar( float, get_next_secondary_attack, "CBaseCombatWeapon->m_flNextSecondaryAttack" );
	netvar( float, last_shot_time, "CWeaponCSBase->m_fLastShotTime" );
	netvar( int, get_clip_1, "CBaseCombatWeapon->m_iClip1" );
	netvar( int, get_clip_2, "CBaseCombatWeapon->m_iClip2" );
	netvar( float, get_recoil_index, "CWeaponCSBase->m_flRecoilIndex" );
	netvar( int, get_world_model_index, "CBaseCombatWeapon->m_iWorldModelIndex" );
	netvar( int, get_world_dropped_model_index, "CBaseCombatWeapon->m_iWorldDroppedModelIndex" );
	netvar( int, get_view_model_index, "CBaseCombatWeapon->m_iViewModelIndex" );
	netvar( int, get_model_index, "CBaseEntity->m_nModelIndex" );
	netvar( c_baseweapon*, get_weapon_world_model, "CBaseCombatWeapon->m_hWeaponWorldModel" );
	netvar( unsigned int, get_paintkit, "CBaseAttributableItem->m_nFallbackPaintKit" );
	netvar( unsigned int, get_stattrak, "CBaseAttributableItem->m_nFallbackStatTrak" );
	netvar( unsigned int, get_seed, "CBaseAttributableItem->m_nFallbackSeed" );
	netvar( float, get_wear, "CBaseAttributableItem->m_flFallbackWear" );
	netvar( int, get_original_owner_xuid_low, "CBaseAttributableItem->m_OriginalOwnerXuidLow" );
	netvar( float, get_throw_time, "CBaseCSGrenade->m_fThrowTime" );
	netvar( bool, get_pin_pulled, "CBaseCSGrenade->m_bPinPulled" );
	netvar( bool, get_is_held_by_player, "CBaseCSGrenade->m_bIsHeldByPlayer" );
	netvar( float, get_throw_strength, "CBaseCSGrenade->m_flThrowStrength" );
	netvar( float, get_postpone_fire_ready_time, "CWeaponCSBase->m_flPostponeFireReadyTime" );

	uint64_t& get_item_id( void ) {
		return *( uint64_t* ) ( ( uintptr_t ) &get_item_id_high( ) - 0x8 );
	}

	inline bool is_revolver( void ) {
		if ( !this )
			return false;

		return get_item_definition_index( ) == ( int ) item_definition_index::weapon_revolver;
	}

	inline bool is_planted_c4( void ) {
		if ( !this )
			return false;

		return get_client_class( )->class_id == 108;
	}

	inline bool is_defuse_kit( void ) {
		if ( !this )
			return false;

		return get_client_class( )->class_id == 2;
	}

	c_weapon_system* get_weapon_system( void );

	c_csweapon_info* get_cs_weapon_data( void );

	bool is_reloading( void );

	inline bool has_bullets( void ) {
		return !is_reloading( ) && get_clip_1( );
	}

	inline bool is_grenade( void ) {
		switch ( get_item_definition_index( ) ) {
		case ( short ) item_definition_index::weapon_hegrenade:
		case ( short ) item_definition_index::weapon_molotov:
		case ( short ) item_definition_index::weapon_smokegrenade:
		case ( short ) item_definition_index::weapon_decoy:
		case ( short ) item_definition_index::weapon_incgrenade:
		case ( short ) item_definition_index::weapon_flashbang:
			return true;
		default:
			return false;
		}
	}

	inline bool is_c4( void ) {
		return get_cs_weapon_data( )->weapon_type == ( int ) cs_weapon_t::weapontype_c4;
	}

	inline bool is_knife( void ) {
		switch ( get_item_definition_index( ) ) {
		case ( short ) item_definition_index::weapon_knife:
		case ( short ) item_definition_index::weapon_knifegg:
		case ( short ) item_definition_index::weapon_knife_bayonet:
		case ( short ) item_definition_index::weapon_knife_bowie:
		case ( short ) item_definition_index::weapon_knife_butterfly:
		case ( short ) item_definition_index::weapon_knife_falchion:
		case ( short ) item_definition_index::weapon_knife_flip:
		case ( short ) item_definition_index::weapon_knife_gut:
		case ( short ) item_definition_index::weapon_knife_karambit:
		case ( short ) item_definition_index::weapon_knife_m9_bayonet:
		case ( short ) item_definition_index::weapon_knife_navaja:
		case ( short ) item_definition_index::weapon_knife_shadowdaggers:
		case ( short ) item_definition_index::weapon_knife_stiletto:
		case ( short ) item_definition_index::weapon_knife_tactical:
		case ( short ) item_definition_index::weapon_knife_ursus:
		case ( short ) item_definition_index::weapon_knife_widowmaker:
		case ( short ) item_definition_index::weapon_knife_t:
			return true;
		default:
			return false;
		}
	}

	bool is_pistol( void ) {
		return get_cs_weapon_data( )->weapon_type == ( int ) cs_weapon_t::weapontype_pistol;
	}

	bool is_sniper( void ) {
		switch ( get_item_definition_index( ) ) {
		case ( short ) item_definition_index::weapon_awp:
		case ( short ) item_definition_index::weapon_g3sg1:
		case ( short ) item_definition_index::weapon_ssg08:
		case ( short ) item_definition_index::weapon_scar20:
			return true;
		default:
			return false;
		}
	}

	inline bool is_rifle( void ) {
		switch ( get_cs_weapon_data( )->weapon_type ) {
		case ( int ) cs_weapon_t::weapontype_rifle:
		case ( int ) cs_weapon_t::weapontype_shotgun:
			return true;
		default:
			return false;
		}
	}

	inline float get_innacuracy( void ) {
		if ( !this )
			return FLT_MAX;

		return ( ( float( __thiscall* )( void* ) ) get_vfunc( this, 476 ) )( this );
	}

	inline float get_spread( void ) {
		if ( !this )
			return FLT_MAX;

		return ( ( float( __thiscall* )( void* ) ) get_vfunc( this, 446 ) )( this );
	}

	inline void update_accuracy_penalty( void ) {
		if ( !this )
			return;

		( ( void( __thiscall* )( void* ) ) get_vfunc( this, 477 ) )( this );
	}

	inline float get_spread_cone( void ) {
		if ( !this )
			return FLT_MAX;

		return ( ( float( __thiscall* )( void* ) ) get_vfunc( this, 476 ) )( this );
	}

	inline bool has_scope( void ) {
		if ( !this )
			return false;

		short item_id = get_item_definition_index( );

		if ( item_id == ( short ) item_definition_index::weapon_awp
			|| item_id == ( short ) item_definition_index::weapon_scar20
			|| item_id == ( short ) item_definition_index::weapon_g3sg1
			|| item_id == ( short ) item_definition_index::weapon_ssg08
			|| item_id == ( short ) item_definition_index::weapon_aug
			|| item_id == ( short ) item_definition_index::weapon_sg556 )
			return true;

		return false;
	}

	inline bool is_in_grenade_throw( c_usercmd* cmd ) {
		if ( get_cs_weapon_data( )->weapon_type != ( int ) cs_weapon_t::weapontype_grenade )
			return false;

		if ( !get_pin_pulled( )
			|| cmd->buttons & ( int ) buttons_t::in_attack
			|| cmd->buttons & ( int ) buttons_t::in_attack2 ) {
			float throw_time = get_throw_time( );

			if ( throw_time > 0.0f && throw_time < globals->curtime )
				return true;
		}

		return false;
	}

	void draw_crosshair( void );
};

struct c_model_render_info {
	vec_t origin;
	vec_t angles;
	byte pad_0x8 [ 0x04 ];
	void* renderable;
	const model_t* model;
	const matrix3x4_t* model_to_world;
	const matrix3x4_t* lighting_offset;
	const vec_t* lighting_origin;
	int flags;
	int entity_index;
	int skin;
	int body;
	int hitbox_set;
	unsigned short instance;

	c_model_render_info( )
	{
		model_to_world = 0;
		lighting_offset = 0;
		lighting_origin = 0;
	}
};

class bf_write;
class bf_read;
class c_game_event {
public:
	virtual ~c_game_event( ) = 0;
	virtual const char* get_name( ) const = 0;

	virtual bool is_reliable( ) const = 0;
	virtual bool is_local( ) const = 0;
	virtual bool is_empty( const char *keyName = nullptr ) = 0;

	virtual bool get_bool( const char *keyName = nullptr, bool defaultValue = false ) = 0;
	virtual int get_int( const char *keyName = nullptr, int defaultValue = 0 ) = 0;
	virtual uint64_t get_uint_64( const char *keyName = nullptr, unsigned long defaultValue = 0 ) = 0;
	virtual float get_float( const char *keyName = nullptr, float defaultValue = 0.0f ) = 0;
	virtual const char* get_string( const char *keyName = nullptr, const char *defaultValue = "" ) = 0;
	virtual const wchar_t* get_w_string( const char *keyName, const wchar_t *defaultValue = L"" ) = 0;

	virtual void set_bool( const char *keyName, bool value ) = 0;
	virtual void set_int( const char *keyName, int value ) = 0;
	virtual void set_uint_64( const char *keyName, unsigned long value ) = 0;
	virtual void set_float( const char *keyName, float value ) = 0;
	virtual void set_string( const char *keyName, const char *value ) = 0;
	virtual void set_w_string( const char *keyName, const wchar_t *value ) = 0;
};

class c_game_event_listener_2 {
public:
	virtual ~c_game_event_listener_2( void ) {}

	virtual void fire_game_event( c_game_event *event ) = 0;
	virtual int get_event_debug_id( )
	{
		return 42;
	}

public:
	int debug_id;
};

class c_game_event_manager_2 {
public:
	virtual ~c_game_event_manager_2( void ) = 0;
	virtual int load_events_from_file( const char* filename ) = 0;
	virtual void reset( void ) = 0;
	virtual bool add_listener( c_game_event_listener_2* listener, const char* name, bool server_side ) = 0;
	virtual bool find_listener( c_game_event_listener_2* listener, const char* name ) = 0;
	virtual int remove_listener( c_game_event_listener_2* listener ) = 0;
	virtual c_game_event* create_event( const char* name, bool force, unsigned int unk ) = 0;
	virtual bool fire_event( c_game_event* event, bool dont_brodcast = false ) = 0;
	virtual bool fire_event_client_side( c_game_event* event ) = 0;
	virtual c_game_event* duplicate_event( c_game_event* event ) = 0;
	virtual void free_event( c_game_event* event ) = 0;
	virtual bool serialize_event( c_game_event* event, bf_write* buf ) = 0;
	virtual c_game_event* unserialize_event( bf_read* buf ) = 0;
};

extern inline int time_to_ticks( float time );
extern inline float ticks_to_time( int ticks );

namespace sdk {
	extern void initialize( void );
}