#include "sdk.h"

/*
*	credits to namazo for this super fast and easy-to-use nevar manager ;3
*/

c_netvar_manager::c_netvar_manager( ) {
	for ( client_class* _class = client->get_all_classes( ); _class; _class = _class->next ) {
		if ( _class->p_recv_table ) {
			dump_recursive( _class->network_name, _class->p_recv_table, 0 );
		}
	}
}

void c_netvar_manager::dump_recursive( const char* base_class, recv_table* table, uint16_t offset ) {
	for ( auto i = 0; i < table->i_props; ++i ) {
		auto prop_ptr = &table->props[ i ];

		if ( !prop_ptr || isdigit( prop_ptr->var_name[ 0 ] ) )
			continue;

		if ( strcmp( prop_ptr->var_name, "baseclass" ) == 0 )
			continue;

		if ( prop_ptr->recv_type == send_prop_t::dpt_datatable && prop_ptr->data_table && prop_ptr->data_table->net_table_name[ 0 ] == 'D' ) {
			dump_recursive( base_class, prop_ptr->data_table, offset + prop_ptr->offset );
		}

		char hash_name[ 256 ];

		strcpy_s( hash_name, base_class );
		strcat_s( hash_name, "->" );
		strcat_s( hash_name, prop_ptr->var_name );

		auto hash = fnv_hash( ( const char* ) hash_name );

		props[ hash ] = { prop_ptr, ( uint16_t ) ( offset + prop_ptr->offset ) };
	}
}