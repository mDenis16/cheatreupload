#pragma once
#include "sdk.h"
#include <xmmintrin.h>
#include <cstdint>
#include <limits>

constexpr float pi = 3.14159265358979323846f;
constexpr float flt_max = 3.402823466e+38f;

class c_mathematics {
public:
	float deg_to_rad( float deg );
	float distance_to_ray( const vec_t & pos, const vec_t & ray_start, const vec_t & ray_end, float * along = 0, vec_t * point_on_ray = 0 );
	float rad_to_deg( float rad );
	void vector_substract( const vec_t & a, const vec_t & b, vec_t & c );
	float quick_normalize( float degree, const float min, const float max );
	void smooth_angle( vec_t src, vec_t & dst, float factor );
	float calc_distance( const vec_t src, const vec_t dst );
	bool screen_transform( const vec_t& in, vec_t& out );
	bool normalize_angles( vec_t & angles );

	bool world_to_screen( vec_t& in, vec_t& out );
	vec_t calc_angle( vec_t src, vec_t dst );
	float get_fov_player( vec_t ViewOffSet, vec_t View, c_baseentity * entity, int hitbox );
	float get_fov( vec_t viewangle, vec_t aim_angle );
	void clamp( vec_t& angles );
	float clamp_yaw( float yaw );
	void vector_itransform( const vec_t * in1, const matrix3x4_t & in2, vec_t * out );
	void vector_irotate( const vec_t * in1, const matrix3x4_t & in2, vec_t * out );
	bool intersects_rays_with_abb( vec_t & origin, vec_t & dir, vec_t & min, vec_t & max );
	bool intersected_ray_obb( const ray_t & ray, vec_t & vec_bb_min, vec_t & vec_bb_max, const matrix3x4_t & bone_matrix );
	float angle_diff( float destAngle, float srcAngle );
	float fl_angle_mod( float flAngle );
	float fl_approach_angle( float flTarget, float flValue, float flSpeed );
	void correct_movement( c_usercmd* cmd, vec_t o_angles, float o_forwardmove, float o_sidemove );
	vec_t vector_transform( const vec_t& in, const matrix3x4_t& matrix );
	float normalize_yaw( float yaw );
	void sin_cos( float a, float* s, float* c );
	vec_t vector_angles( const vec_t& forward );
	void normalize_num( vec_t & vIn, vec_t & vOut );
	void vector_angles( const vec_t & forward, vec_t & up, vec_t & angles );
	vec_t angle_vectors( const vec_t& angles );
	void angle_vectors( const vec_t& angles, vec_t* forward, vec_t* right, vec_t* up );
	float get_angle_delta( float src, float dst );
	bool get_hitchance( vec_t angles, c_baseentity* entity, float hc );
	void angle_matrix( const vec_t& angles, const vec_t& position, matrix3x4_t& matrix );
	void matrix_set_column( const vec_t& in, int column, matrix3x4_t& out );
	void angle_matrix( const vec_t& angles, matrix3x4_t& matrix );
	void matrix_copy( const matrix3x4_t& src, matrix3x4_t& dst );
	void matrix_multiply( const matrix3x4_t& src1, const matrix3x4_t& src2, matrix3x4_t& dst );
};

extern std::unique_ptr< c_mathematics > math;