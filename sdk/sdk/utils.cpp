#include "utils.h"
#include "mathematics.h"

std::unique_ptr< c_utils > utils = std::unique_ptr< c_utils >( );

void* c_utils::grab_interface( const char* module_name, const char* interf, bool bruteforce ) {
	auto create_interface = ( void* ( *)( const char*, int ) ) GetProcAddress( GetModuleHandleA( module_name ), "CreateInterface" );

	std::string str_interface, str_interface_version = "0";

	if ( bruteforce ) {
		for ( auto i = 0; i <= 99; i++ ) {
			str_interface = interf;
			str_interface += str_interface_version;
			str_interface += std::to_string( i );

			void* func_ptr = create_interface( str_interface.c_str( ), 0 );

			if ( func_ptr )
				return func_ptr;

			if ( i == 99 && str_interface_version == "0" ) {
				str_interface_version = "00";
				i = 0;
			}
			else if ( i == 99 && str_interface_version == "00" )
				return nullptr;
		}
	}
	else {
		void* ptr = create_interface( interf, 0 );

		if ( ptr )
			return ptr;
		else
			return nullptr;
	}

	return nullptr;
}

void* c_utils::find_pattern( const char* module_name, const char* pattern ) {
	const char* pat = pattern;
	uintptr_t range_start = ( uintptr_t ) GetModuleHandleA( module_name );
	MODULEINFO miModInfo;
	K32GetModuleInformation( GetCurrentProcess( ), ( HMODULE ) range_start, &miModInfo, sizeof( MODULEINFO ) );
	uintptr_t rangeEnd = range_start + miModInfo.SizeOfImage;

	void* first_match = 0;

	for ( uintptr_t current_address = range_start; current_address < rangeEnd; current_address++ ) {
		if ( !*pat )
			return first_match;

		if ( *( byte* ) pat == '\?' || *( byte* ) current_address == get_byte( pat ) ) {
			if ( !first_match )
				first_match = ( void* ) current_address;

			if ( !pat [ 2 ] )
				return first_match;

			if ( *( uint16_t* ) pat == '\?\?' || *( byte* ) pat != '\?' )
				pat += 3;
			else
				pat += 2;
		}
		else {
			pat = pattern;
			first_match = 0;
		}
	}

	return nullptr;
}

float c_utils::clamp( float x, float min, float max ) {
	float buf = x;

	if ( x > max )
		buf = max;

	if ( x < min )
		buf = min;

	return buf;
}
void c_utils::random_seed(int seed)
{
	static auto random_seed = reinterpret_cast< void( * )( int ) >( GetProcAddress( GetModuleHandleA ( "vstdlib.dll"), "RandomSeed"));

	random_seed(seed);
}
float c_utils::random_float( float min, float max ) {
	static auto random_float_fn = ( float( *)( float, float ) ) GetProcAddress( GetModuleHandleA( "vstdlib.dll" ), "RandomFloat" );

	return random_float_fn( min, max );
}

int c_utils::random_int( int min, int max ) {
	static auto random_int_fn = ( int( *)( int, int ) ) GetProcAddress( GetModuleHandleA( "vstdlib.dll" ), "RandomInt" );

	return random_int_fn( min, max );
}

void c_utils::force_update( void ) {
	static auto force_update_fn = ( void( __cdecl* )( ) ) this->find_pattern( "engine.dll", "A1 ? ? ? ? B9 ? ? ? ? 56 FF 50 14 8B 34 85" );

	force_update_fn( );
}
c_baseentity* c_utils::util_player_by_index(int index)
{
	typedef c_baseentity*(__fastcall* player_index)(int);
	static player_index c_index = (player_index)this->find_pattern("server.dll", "85 C9 7E 2A A1");

	if (!c_index)
		return false;

	return c_index(index);
}

float c_utils::fov_player( vec_t ViewOffSet, vec_t View, c_baseentity* entity, int hitbox )
{

	vec_t Angles = View, Origin = ViewOffSet;
	vec_t Delta( 0, 0, 0 ), Forward( 0, 0, 0 );
	vec_t AimPos = entity->get_bone_pos( hitbox );

	Forward = math->angle_vectors( Angles );
	math->vector_substract( AimPos, Origin, Delta );
	math->normalize_num( Delta, Delta );

	float DotProduct = Forward.dot_product( Delta );
	return ( acos( DotProduct ) * ( 180.0f / D3DX_PI ) );
}
float c_utils::get_lerp_time( void ) {
	static auto cl_ud_rate = cvar->find_var( "cl_updaterate" );
	static auto min_ud_rate = cvar->find_var( "sv_minupdaterate" );
	static auto max_ud_rate = cvar->find_var( "sv_maxupdaterate" );

	int ud_rate = 64;
	if ( cl_ud_rate )
		ud_rate = cl_ud_rate->get_int( );

	if ( min_ud_rate && max_ud_rate )
		ud_rate = max_ud_rate->get_int( );

	float ratio = 1.f;
	static auto cl_interp_ratio = cvar->find_var( "cl_interp_ratio" );
	if ( cl_interp_ratio )
		ratio = cl_interp_ratio->get_float( );

	static auto cl_interp = cvar->find_var( "cl_interp" );
	static auto c_min_ratio = cvar->find_var( "sv_client_min_interp_ratio" );
	static auto c_max_ratio = cvar->find_var( "sv_client_max_interp_ratio" );

	float lerp = globals->interval_per_tick;
	if ( cl_interp )
		lerp = cl_interp->get_float( );

	if ( c_min_ratio && c_max_ratio && c_min_ratio->get_float( ) != 1 )
		ratio = utils->clamp( ratio, c_min_ratio->get_float( ), c_max_ratio->get_float( ) );

	return max( lerp, ratio / ud_rate );
}

bool c_utils::hovered( int x, int y, int w, int h ) {
	POINT p;
	GetCursorPos( &p );
	return p.x >= x && p.y >= y && p.x <= x + w && p.y <= y + h;
}

bool c_utils::is_valid_tick( const float & simtime, float time ) {
	auto nci = engine->get_net_channel_info( );

	static auto sv_maxunlag = cvar->find_var( "sv_maxunlag" );

	if ( !nci || !sv_maxunlag )
		return false;

	const auto correct = clamp( nci->get_latency( 0 ) + nci->get_latency( 1 ) + get_lerp_time( ), 0.0f, 1.0f );
	const auto current_time = globals->interval_per_tick * local_player->get_tickbase( );
	const auto delta = correct - ( current_time - simtime );
	//msg( "backtrack stuff: correct %f, current_time %f, delta %f \n", std::abs( correct ), current_time, std::abs( delta ) );
	return std::abs( delta ) <= time;
}

color c_utils::rainbow( float ct, int a ) {
	int r = static_cast< int >( ( std::cosf( ct ) * 0.5f + 0.5f ) * 255.0f );
	int g = static_cast< int >( ( std::cosf( ct - 2.0f * pi / 3.0f ) * 0.5f + 0.5f )* 255.0f );
	int b = static_cast< int >( ( std::cosf( ct - 4.0f * pi / 3.0f ) * 0.5f + 0.5f ) * 255.0f );

	return color( ( byte ) r, ( byte ) g, ( byte ) b, ( byte ) a );
}
int  c_utils::ticks_choked( c_baseentity* pEntity )
{
	if (!engine->get_net_channel_info( ) ) return 0;

	double flSimulationTime = pEntity->get_simulation_time( );
	double flSimDiff =  globals->curtime - flSimulationTime;
	return time_to_ticks( max( 0.0f, flSimDiff - engine->get_net_channel_info()->get_latency(1) ) );
}

std::string c_utils::get_gun_image( item_definition_index index )
{
	std::string icon_letter( "null" );
	switch ( ( int ) index )
	{
	case ( int ) item_definition_index::weapon_deagle:
		icon_letter = 'f';
		break;
	case ( int ) item_definition_index::weapon_knife:
	case ( int ) item_definition_index::weapon_knife_t:
		icon_letter = 'j';
		break;
	case ( int ) item_definition_index::weapon_aug:
		icon_letter = 'e';
		break;

	case ( int ) item_definition_index::weapon_g3sg1:
		icon_letter = 'i';
		break;
	case ( int ) item_definition_index::weapon_mac10:
		icon_letter = 'l';
		break;
	case ( int ) item_definition_index::weapon_p90:
		icon_letter = 'm';
		break;
	case ( int ) item_definition_index::weapon_ssg08:
		icon_letter = 'n';
		break;
	case ( int ) item_definition_index::weapon_scar20:
		icon_letter = 'o';
		break;
	case ( int ) item_definition_index::weapon_ump45:
		icon_letter = 'q';
		break;
	case ( int ) item_definition_index::weapon_elite:
		icon_letter = 's';
		break;
	case ( int ) item_definition_index::weapon_famas:
		icon_letter = 't';
		break;
	case ( int ) item_definition_index::weapon_fiveseven:
		icon_letter = 'u';
		break;
	case ( int ) item_definition_index::weapon_galilar:
		icon_letter = 'v';
		break;
	case ( int ) item_definition_index::weapon_m4a1:
		icon_letter = 'w';
		break;
	case ( int ) item_definition_index::weapon_p250:
		icon_letter = 'y';
		break;
	case ( int ) item_definition_index::weapon_m249:
		icon_letter = 'z';
		break;
	case ( int ) item_definition_index::weapon_xm1014:
		icon_letter = ']';
		break;
	case ( int ) item_definition_index::weapon_c4:
		icon_letter = 'd';
		break;
	case ( int ) item_definition_index::weapon_glock:
		icon_letter = 'c';
		break;
	case ( int ) item_definition_index::weapon_hkp2000:
		icon_letter = 'y';
		break;
	}
	return icon_letter;
}
int c_utils::closest_to_crosshair( ) /* hotfix bcs i want to play*/
{
	int index = -1;
	float lowest_fov = INT_MAX;

	if ( !local_player )
		return -1;

	vec_t local_position = local_player->get_eye_pos( );
	vec_t angles; engine->get_viewangles( angles );

	for ( int i = 1; i <= globals->max_clients; i++ )
	{
		c_baseentity * entity = ( c_baseentity* )entity_list->get_client_entity( i );

		if ( !entity->is_valid_player( ) )
			continue;

		float fov = math->get_fov_player ( local_position, angles, entity, 0 );

		if ( fov < lowest_fov )
		{
			lowest_fov = fov;
			index = i;
		}
	}
	return index;
}

float c_utils::get_hitchance( void ) {
	if ( !local_player )
		return 0.0f;

	auto weapon = local_player->get_active_weapon( );

	if ( !weapon )
		return 0.0f;

	float inaccuracy = weapon->get_innacuracy( );

	if ( inaccuracy == 0.0f )
		inaccuracy = 0.0000001f;

	return 1.0f / inaccuracy;
}

bool c_utils::is_visible( vec_t pos_notangles, bool smokecheck )
{
	auto start = local_player->get_eye_pos( );
	auto angle = math->calc_angle( start, pos_notangles );
	auto end = vec_t { };

	if ( !local_player->is_alive( ) || local_player->get_dormant( ) || !engine->is_connected( ) )
		return false;

	angle = math->angle_vectors( angle );
	angle *= local_player->get_active_weapon( )->get_cs_weapon_data( )->range;
	end = start + angle;

	trace_t trace;
	c_trace_filter filter( local_player );
	ray_t ray; ray.init( start, end );

	static auto line_goes_through_smoke = ( bool( __cdecl* )( vec_t, vec_t, int16_t ) )( this->find_pattern( "client_panorama.dll", "55 8B EC 83 EC 08 8B 15 ? ? ? ? 0F 57 C0" ) );

	if ( line_goes_through_smoke( start, end, 1 ) && smokecheck )
		return false;

	engine_trace->trace_ray( ray, mask_solid, &filter, &trace );

	auto hit_entity = ( c_baseentity* ) ( trace.hit_entity );

	if ( !hit_entity || !hit_entity->is_player( ) || !hit_entity->is_alive( ) || hit_entity->get_dormant( ) || hit_entity->get_team( ) == local_player->get_team( ) )
		return false;

	return true;
}