#include <algorithm>
#include <DirectXMath.h>
#include "mathematics.h"
#include "utils.h"
#include "..\hacks\ragebot.h"

std::unique_ptr< c_mathematics > math = std::unique_ptr< c_mathematics >( );

float c_mathematics::deg_to_rad( float deg ) {
	return DirectX::XMConvertToRadians( deg );
} 
/* thx to valve sdk*/
float c_mathematics::distance_to_ray( const  vec_t &pos, const vec_t &ray_start, const  vec_t &ray_end, float *along, vec_t *point_on_ray )
{
	vec_t to = pos - ray_start;
	vec_t dir = ray_end - ray_start;
	float length = dir.normalized( ).length( );

	float range_along = dir.dot_product( to );
	if ( along )
		*along = range_along;

	float range;

	if ( range_along < 0.0f )
	{
		range = -( pos - ray_start ).length( );

		if ( point_on_ray )
			*point_on_ray = ray_start;
	}
	else if ( range_along > length )
	{
		range = -( pos - ray_end ).length( );

		if ( point_on_ray )
			*point_on_ray = ray_end;
	}
	else
	{
	 vec_t on_ray = ray_start + range_along * dir;
		range = ( pos - on_ray ).length( );

		if ( point_on_ray )
			*point_on_ray = on_ray;
	}

	return range;
}
float c_mathematics::rad_to_deg( float rad ) {
	return DirectX::XMConvertToDegrees( rad );
}
void c_mathematics::vector_substract( const vec_t& a, const vec_t& b, vec_t& c )
{
	c.x = a.x - b.x;
	c.y = a.y - b.y;
	c.z = a.z - b.z;
}
float c_mathematics::quick_normalize( float degree, const float min, const float max ) {
	while ( degree < min )
		degree += max - min;
	while ( degree > max )
		degree -= max - min;

	return degree;
}
void c_mathematics::smooth_angle( vec_t src, vec_t &dst, float factor )
{
	if ( factor == 0 ) return;

	factor *= 3;
	vec_t diff = dst - src;

	auto normalize = [ & ] ( )
	{
		while ( diff.x > 89.0f )
			diff.x -= 180.f;

		while ( diff.x < -89.0f )
			diff.x += 180.f;

		while ( diff.y > 180.f )
			diff.y -= 360.f;

		while ( diff.y < -180.f )
			diff.y += 360.f;
	};
	normalize( );
	auto calced = diff / std::powf( factor, 0.8f );
	dst = src + calced;
}

float c_mathematics::calc_distance( const vec_t src, const vec_t dst )
{
	return sqrtf( pow( ( src.y - dst.y ), 2 ) + pow( ( src.x - dst.x ), 2 ) + pow( ( src.z - dst.z ), 2 ) );
}

bool c_mathematics::screen_transform( const vec_t& in, vec_t& out ) {
	static std::uint32_t pmatrix = *( std::uint32_t* ) ( ( std::uint32_t ) utils->find_pattern( "client_panorama.dll", "0F 10 05 ? ? ? ? 8D 85 ? ? ? ? B9" ) + 3 ) + 176;

	auto& matrix = *( matrix3x4_t* ) pmatrix;

	out.x = matrix.values [ 0 ] [ 0 ] * in.x + matrix.values [ 0 ] [ 1 ] * in.y + matrix.values [ 0 ] [ 2 ] * in.z + matrix.values [ 0 ] [ 3 ];
	out.y = matrix.values [ 1 ] [ 0 ] * in.x + matrix.values [ 1 ] [ 1 ] * in.y + matrix.values [ 1 ] [ 2 ] * in.z + matrix.values [ 1 ] [ 3 ];
	out.z = 0.0f;

	float w = matrix.values [ 3 ] [ 0 ] * in.x + matrix.values [ 3 ] [ 1 ] * in.y + matrix.values [ 3 ] [ 2 ] * in.z + matrix.values [ 3 ] [ 3 ];

	if ( w < 0.001f ) {
		out.x *= 100000.0f;
		out.y *= 100000.0f;

		return true;
	}
	else {
		auto invw = 1.0f / w;
		out.x *= invw;
		out.y *= invw;

		return false;
	}

	return false;
}
bool c_mathematics::normalize_angles( vec_t &angles ) {
	if ( std::isfinite( angles.x ) && std::isfinite( angles.y ) && std::isfinite( angles.z ) ) {
		angles.x = std::remainder( angles.x, 360.f );
		angles.y = std::remainder( angles.y, 360.f );
		return true;
	}

	return false;
}

bool c_mathematics::world_to_screen( vec_t& in, vec_t& out ) {
	if ( !screen_transform( in, out ) ) {
		int w, h;
		engine->get_screen_size( w, h );
		float x = w / 2.f;
		float y = h / 2.f;
		x += 0.5f * out.x * w + 0.5f;
		y -= 0.5f * out.y * h + 0.5f;
		out.x = x;
		out.y = y;

		return true;
	}

	return false;
}
#define py ((FLOAT) 3.141592653589793238462643383279502884197169399375105820974944592307816406286)
vec_t c_mathematics::calc_angle( vec_t src, vec_t dst ) { /* oh, 57.2957795131f the magic number*/
	vec_t angles = vec_t( );  vec_t delta = vec_t( );

	math->vector_substract( src, dst, delta );

	float hyp = sqrtf( delta [ 0 ] * delta [ 0 ] + delta [ 1 ] * delta [ 1 ] );
	angles [ 0 ] = ( float )( atan( delta [ 2 ] / hyp ) * ( 180.0f / D3DX_PI ) );
	angles [ 1 ] = ( float )( atan( delta [ 1 ] / delta [ 0 ] ) * ( 180.0f / D3DX_PI ) );
	angles [ 2 ] = 0.f;
	if ( delta [ 0 ] >= 0.f )
		angles [ 1 ] += 180.f;

	return angles;
}
float c_mathematics::get_fov_player( vec_t ViewOffSet, vec_t View, c_baseentity* entity, int hitbox )
{
	const float MaxDegrees = 180.0f;
	vec_t angles = View, origin = ViewOffSet;
	vec_t delta( 0, 0, 0 ), forward( 0, 0, 0 );
	vec_t aim_pos = ragebot->cached_bones[entity->get_index()][ 8 ].get_origin( );
	angles = angle_vectors( forward );
	vector_substract( aim_pos, origin, delta );
	normalize_num( delta, delta );
	float dot_product = forward.dot_product( delta );
	return ( acos( dot_product ) * ( MaxDegrees / D3DX_PI ) );
}
float c_mathematics::get_fov( vec_t viewangle, vec_t aim_angle ) {
	vec_t delta = aim_angle - viewangle;
	clamp( delta );
	return sqrtf( powf( delta.x, 2.0f ) + powf( delta.y, 2.0f ) );
}

void c_mathematics::clamp( vec_t& angles ) {
	while ( angles.x > 89.0f )
		angles.x -= 180.f;

	while ( angles.x < -89.0f )
		angles.x += 180.f;

	while ( angles.y > 180.f )
		angles.y -= 360.f;

	while ( angles.y < -180.f )
		angles.y += 360.f;

	if ( angles.x > 89.0f )
		angles.x = 89.0f;
	else if ( angles.x < -89.0f )
		angles.x = -89.0f;

	if ( angles.y > 180.0f )
		angles.y = 180.0f;
	else if ( angles.y < -180.0f )
		angles.y = -180.0f;

	angles.z = 0.0f;
}
__forceinline float dot_product( const float *v1, const float *v2 )
{
	return v1 [ 0 ] * v2 [ 0 ] + v1 [ 1 ] * v2 [ 1 ] + v1 [ 2 ] * v2 [ 2 ];
}
#define CHECK_VALID( _v ) 0
__forceinline float dot_product( const vec_t& a, const vec_t& b )
{
    CHECK_VALID( a );
	CHECK_VALID( b );
	return( a.x*b.x + a.y*b.y + a.z*b.z );
}

float c_mathematics::clamp_yaw( float yaw ) {
	auto angles = yaw;

	while ( angles > 180.0f )
		angles -= 360.0f;

	while ( angles < -180.0f )
		angles += 360.0f;

	if ( angles > 180.0f )
		angles = 180.0f;
	else if ( angles < -180.0f )
		angles = -180.0f;

	return angles;
}
void c_mathematics::vector_itransform( const vec_t *in1, const matrix3x4_t& in2, vec_t *out )
{
	float in1t [ 3 ];

	in1t [ 0 ] = in1->x - in2 [ 0 ] [ 3 ];
	in1t [ 1 ] = in1->y - in2 [ 1 ] [ 3 ];
	in1t [ 2 ] = in1->z - in2 [ 2 ] [ 3 ];

	out->x = in1t [ 0 ] * in2 [ 0 ] [ 0 ] + in1t [ 1 ] * in2 [ 1 ] [ 0 ] + in1t [ 2 ] * in2 [ 2 ] [ 0 ];
	out->y = in1t [ 0 ] * in2 [ 0 ] [ 1 ] + in1t [ 1 ] * in2 [ 1 ] [ 1 ] + in1t [ 2 ] * in2 [ 2 ] [ 1 ];
	out->z = in1t [ 0 ] * in2 [ 0 ] [ 2 ] + in1t [ 1 ] * in2 [ 1 ] [ 2 ] + in1t [ 2 ] * in2 [ 2 ] [ 2 ];
}

// assume in2 is a rotation and rotate the input vector
void  c_mathematics::vector_irotate( const vec_t *in1, const matrix3x4_t& in2,  vec_t *out )
{
	
	assert( in1 != out );

	out->x = dot_product( ( const float* ) in1, in2 [ 0 ] );
	out->y = dot_product( ( const float* ) in1, in2 [ 1 ] );
	out->z = dot_product( ( const float* ) in1, in2 [ 2 ] );
}

bool  c_mathematics::intersects_rays_with_abb( vec_t& origin, vec_t& dir, vec_t& min, vec_t& max )
{
	float tmin, tmax, tymin, tymax, tzmin, tzmax;

	if ( dir.x >= 0 )
	{
		tmin = ( min.x - origin.x ) / dir.x;
		tmax = ( max.x - origin.x ) / dir.x;
	}
	else
	{
		tmin = ( max.x - origin.x ) / dir.x;
		tmax = ( min.x - origin.x ) / dir.x;
	}

	if ( dir.y >= 0 )
	{
		tymin = ( min.y - origin.y ) / dir.y;
		tymax = ( max.y - origin.y ) / dir.y;
	}
	else
	{
		tymin = ( max.y - origin.y ) / dir.y;
		tymax = ( min.y - origin.y ) / dir.y;
	}

	if ( tmin > tymax || tymin > tmax )
		return false;

	if ( tymin > tmin )
		tmin = tymin;

	if ( tymax < tmax )
		tmax = tymax;

	if ( dir.z >= 0 )
	{
		tzmin = ( min.z - origin.z ) / dir.z;
		tzmax = ( max.z - origin.z ) / dir.z;
	}
	else
	{
		tzmin = ( max.z - origin.z ) / dir.z;
		tzmax = ( min.z - origin.z ) / dir.z;
	}

	if ( tmin > tzmax || tzmin > tmax )
		return false;

	//behind us
	if ( tmin < 0 || tmax < 0 )
		return false;

	return true;
}

bool c_mathematics::intersected_ray_obb( const ray_t& ray, vec_t& vec_bb_min, vec_t& vec_bb_max, const matrix3x4_t &bone_matrix )
{
	//Transform ray into model space of hitbox so we only have to deal with an AABB instead of OBB
	vec_t ray_trans, dir_trans;
	vector_itransform( &ray.m_Start, bone_matrix, &ray_trans );
	vector_irotate( &ray.m_Delta, bone_matrix, &dir_trans ); //only rotate direction vector! no translation!

	return intersects_rays_with_abb( ray_trans, dir_trans, vec_bb_min, vec_bb_max );
}

float c_mathematics::angle_diff( float destAngle, float srcAngle )
{
	float delta = 0.f;

	delta = fmodf( destAngle - srcAngle, 360.0f );
	if ( destAngle > srcAngle )
	{
		if ( delta >= 180 )
			delta -= 360;
	}
	else
	{
		if ( delta <= -180 )
			delta += 360;
	}
	return delta;
}
float c_mathematics::fl_angle_mod( float fl_angle )
{
	return( ( 360.0f / 65536.0f ) * ( ( int32_t )( fl_angle * ( 65536.0f / 360.0f ) ) & 65535 ) );
}

float c_mathematics::fl_approach_angle( float fl_target, float fl_value, float fl_speed )
{
	float fl_adjusted_speed = fl_speed;
	if ( fl_adjusted_speed < 0.0f )
		fl_adjusted_speed *= -1.0f;
	

	float fl_angle_mod_target = fl_angle_mod( fl_target );
	float fl_angle_mod_value = fl_angle_mod( fl_value );

	float fl_delta = ( fl_angle_mod_target - fl_angle_mod_value );
	if ( fl_delta >= -180.0f )
	{
		if ( fl_delta >= 180.0f )
		  fl_delta -= 360.0f;
	}
	else
	{
		if ( fl_delta <= -180.0f )
			fl_delta += 360.0f;
	}

	float fl_return;

	if ( fl_delta <= fl_adjusted_speed )
	{
		if ( ( fl_adjusted_speed* -1.0f ) <= fl_delta )
			fl_return = fl_angle_mod_target;
		else
			fl_return = ( fl_angle_mod_value - fl_adjusted_speed );
	}
	else
		fl_return = ( fl_angle_mod_value + fl_adjusted_speed );
	
	return fl_return;
}
void c_mathematics::correct_movement( c_usercmd* cmd, vec_t o_angles, float o_forwardmove, float o_sidemove ) {
	float dv;
	float f1;
	float f2;

	if ( o_angles [ 1 ] < 0.f )
		f1 = 360.0f + o_angles [ 1 ];
	else
		f1 = o_angles [ 1 ];

	if ( cmd->viewangles.y < 0.0f )
		f2 = 360.0f + cmd->viewangles.y;
	else
		f2 = cmd->viewangles.y;

	if ( f2 < f1 )
		dv = abs( f2 - f1 );
	else
		dv = 360.0f - abs( f1 - f2 );

	dv = 360.0f - dv;

	cmd->forwardmove = cos( deg_to_rad( dv ) ) * o_forwardmove + cos( deg_to_rad( dv + 90.f ) ) * o_sidemove;
	cmd->sidemove = sin( deg_to_rad( dv ) ) * o_forwardmove + sin( deg_to_rad( dv + 90.f ) ) * o_sidemove;
}

vec_t c_mathematics::vector_transform( const vec_t& in, const matrix3x4_t& matrix ) {
	vec_t out;

	for ( int i = 0; i < 3; i++ )
		out [ i ] = in.dot_product( vec_t( matrix [ i ] [ 0 ], matrix [ i ] [ 1 ], matrix [ i ] [ 2 ] ) ) + matrix [ i ] [ 3 ];

	return out;
}
float c_mathematics::normalize_yaw( float yaw )
{
	if ( yaw > 180 )
		yaw -= ( round( yaw / 360 ) * 360.f );
	else if ( yaw < -180 )
		yaw += ( round( yaw / 360 ) * -360.f );

	return yaw;
}
void c_mathematics :: normalize_num( vec_t &vIn, vec_t &vOut )
{
	float flLen = vIn.length( );
	if ( flLen == 0 ) {
		vOut.init( 0, 0, 1 );
		return;
	}
	flLen = 1 / flLen;
	vOut.init( vIn.x * flLen, vIn.y * flLen, vIn.z * flLen );
}
void c_mathematics::sin_cos( float a, float* s, float* c ) {
	*s = sin( a );
	*c = cos( a );
}

vec_t c_mathematics::vector_angles( const vec_t& forward ) {
	vec_t angles;
	float tmp, yaw, pitch;

	if ( forward [ 1 ] == 0 && forward [ 0 ] == 0 ) {
		yaw = 0;
		if ( forward [ 2 ] > 0 )
			pitch = 270;
		else
			pitch = 90;
	}
	else {
		yaw = ( atan2( forward [ 1 ], forward [ 0 ] ) * 180 / DirectX::XM_PI );

		if ( yaw < 0 )
			yaw += 360;

		tmp = sqrt( forward [ 0 ] * forward [ 0 ] + forward [ 1 ] * forward [ 1 ] );
		pitch = ( atan2( -forward [ 2 ], tmp ) * 180 / DirectX::XM_PI );

		if ( pitch < 0 )
			pitch += 360;
	}

	angles [ 0 ] = pitch;
	angles [ 1 ] = yaw;
	angles [ 2 ] = 0;
	return angles;
}

void c_mathematics::vector_angles( const vec_t& forward, vec_t& up, vec_t& angles )
{
	static auto cross_product = [ & ] ( const vec_t &a, const vec_t &b )
	{
		return vec_t( a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x );
	};

	vec_t left = cross_product( up, forward );
	left.normalize( );

	float forwardDist = forward.length_2d( );

	if ( forwardDist > 0.001f )
	{
		angles.x = atan2f( -forward.z, forwardDist ) * 180 / ( ( float ) ( 3.14159265358979323846f ) );
		angles.y = atan2f( forward.y, forward.x ) * 180 / ( ( float ) ( 3.14159265358979323846f ) );

		float upZ = ( left.y * forward.x ) - ( left.x * forward.y );
		angles.z = atan2f( left.z, upZ ) * 180 / ( ( float ) ( 3.14159265358979323846f ) );
	}
	else
	{
		angles.x = atan2f( -forward.z, forwardDist ) * 180 / ( ( float ) ( 3.14159265358979323846f ) );
		angles.y = atan2f( -left.x, left.y ) * 180 / ( ( float ) ( 3.14159265358979323846f ) );
		angles.z = 0;
	}
}

vec_t c_mathematics::angle_vectors( const vec_t& angles ) {
	float sp, sy, cp, cy;

	DirectX::XMScalarSinCos( &sp, &cp, deg_to_rad( angles [ 0 ] ) );
	DirectX::XMScalarSinCos( &sy, &cy, deg_to_rad( angles [ 1 ] ) );

	vec_t forward;
	forward.x = cp * cy;
	forward.y = cp * sy;
	forward.z = -sp;
	return forward;
}

void c_mathematics::angle_vectors( const vec_t& angles, vec_t* forward, vec_t* right, vec_t* up ) {
	float sp, sy, sr, cp, cy, cr;

	sin_cos( deg_to_rad( angles.x ), &sp, &cp );
	sin_cos( deg_to_rad( angles.y ), &sy, &cy );
	sin_cos( deg_to_rad( angles.z ), &sr, &cr );

	if ( forward ) {
		forward->x = cp * cy;
		forward->y = cp * sy;
		forward->z = -sp;
	}

	if ( right ) {
		right->x = -1.0f * sr * sp * cy + -1.0f * cr * -sy;
		right->y = -1.0f * sr * sp * sy + -1.0f * cr * cy;
		right->z = -1.0f * sr * cp;
	}

	if ( up ) {
		up->x = cr * sp * cy + -sr * -sy;
		up->y = cr * sp * sy + -sr * cy;
		up->z = cr * cp;
	}
}

float c_mathematics::get_angle_delta( float src, float dst ) {
	vec_t v_src = vec_t( 0.0f, src, 0.0f );
	vec_t v_dst = vec_t( 0.0f, dst, 0.0f );
	clamp( v_src );
	clamp( v_dst );
	vec_t delta_ang = vec_t( 0.0f, v_src.y - v_dst.y, 0.0f );
	clamp( delta_ang );
	float delta = delta_ang.y;
	delta += ( delta > 180.0f ) ? -360.0f : ( delta < -180.0f ) ? 360.0f : 0.0f;
	return delta;
}

bool c_mathematics::get_hitchance( vec_t angles, c_baseentity* entity, float hc ) {
	if (  !local_player->get_active_weapon( ) )
		return 0.0f;

	auto weapon = local_player->get_active_weapon( );

	vec_t forward, right, up;
	vec_t src = local_player->get_eye_pos( );
	this->angle_vectors( angles, &forward, &right, &up );

	int cHits = 0;
	int cNeededHits = static_cast< int >( 150.f * ( hc / 100.f ) );

	weapon->update_accuracy_penalty( );
	float weap_spread = weapon->get_spread( );
	float weap_inaccuracy = weapon->get_innacuracy( );

	for ( int i = 0; i < 150; i++ )
	{
		float a = utils->random_float( 0.f, 1.f );
		float b = utils->random_float( 0.f, 2.f * ( ( float ) ( 3.14159265358979323846f ) ) );
		float c = utils->random_float( 0.f, 1.f );
		float d = utils->random_float( 0.f, 2.f * ( ( float ) ( 3.14159265358979323846f ) ) );

		float inaccuracy = a * weap_inaccuracy;
		float spread = c * weap_spread;

		if ( weapon->get_item_definition_index( ) == 64 )
		{
			a = 1.f - a * a;
			a = 1.f - c * c;
		}

		vec_t spreadView( ( cos( b ) * inaccuracy ) + ( cos( d ) * spread ), ( sin( b ) * inaccuracy ) + ( sin( d ) * spread ), 0 ), direction;

		direction.x = forward.x + ( spreadView.x * right.x ) + ( spreadView.y * up.x );
		direction.y = forward.y + ( spreadView.x * right.y ) + ( spreadView.y * up.y );
		direction.z = forward.z + ( spreadView.x * right.z ) + ( spreadView.y * up.z );
		direction.normalized( );

		vec_t viewAnglesSpread;
		this->vector_angles( direction, up, viewAnglesSpread );
		this->clamp( viewAnglesSpread );

		vec_t viewForward;
		viewForward = this->angle_vectors( viewAnglesSpread );
		viewForward.normalize( );

		viewForward = src + ( viewForward * weapon->get_cs_weapon_data( )->range );

		trace_t tr;
		ray_t ray;

		ray.init( src, viewForward );
		engine_trace->clip_ray_to_entity( ray, mask_shot | contents_grate, entity, &tr );

		if ( ( c_baseentity* ) tr.hit_entity == entity )
			++cHits;

		if ( int( ( ( ( float ) ( cHits ) / 150.f ) * 100.f ) >= hc ) )
			return true;

		if ( ( 150 - i + cHits ) < cNeededHits )
			return false;
	}
	return false;
}

void c_mathematics::angle_matrix( const vec_t& angles, const vec_t& position, matrix3x4_t& matrix ) {
	angle_matrix( angles, matrix );
	matrix_set_column( position, 3, matrix );
}

void c_mathematics::matrix_set_column( const vec_t& in, int column, matrix3x4_t& out ) {
	out [ 0 ] [ column ] = in.x;
	out [ 1 ] [ column ] = in.y;
	out [ 2 ] [ column ] = in.z;
}

void c_mathematics::angle_matrix( const vec_t& angles, matrix3x4_t& matrix ) {
	float sr, sp, sy, cr, cp, cy;

	sin_cos( deg_to_rad( angles [ 1 ] ), &sy, &cy );
	sin_cos( deg_to_rad( angles [ 0 ] ), &sp, &cp );
	sin_cos( deg_to_rad( angles [ 2 ] ), &sr, &cr );

	matrix [ 0 ] [ 0 ] = cp * cy;
	matrix [ 1 ] [ 0 ] = cp * sy;
	matrix [ 2 ] [ 0 ] = -sp;

	float crcy = cr * cy;
	float crsy = cr * sy;
	float srcy = sr * cy;
	float srsy = sr * sy;

	matrix [ 0 ] [ 1 ] = sp * srcy - crsy;
	matrix [ 1 ] [ 1 ] = sp * srsy + crcy;
	matrix [ 2 ] [ 1 ] = sr * cp;

	matrix [ 0 ] [ 2 ] = ( sp * crcy + srsy );
	matrix [ 1 ] [ 2 ] = ( sp * crsy - srcy );
	matrix [ 2 ] [ 2 ] = cr * cp;

	matrix [ 0 ] [ 3 ] = 0.0f;
	matrix [ 1 ] [ 3 ] = 0.0f;
	matrix [ 2 ] [ 3 ] = 0.0f;
}

void c_mathematics::matrix_copy( const matrix3x4_t& src, matrix3x4_t& dst ) {
	if ( &src != &dst )
		std::memcpy( dst.values, src.values, 16 * sizeof( float ) );
}

void c_mathematics::matrix_multiply( const matrix3x4_t& src1, const matrix3x4_t& src2, matrix3x4_t& dst ) {
	typedef float raw_matrix [ 4 ];

	matrix3x4_t tmp1, tmp2;
	const raw_matrix* s1 = ( &src1 == &dst ) ? tmp1.values : src1.values;
	const raw_matrix* s2 = ( &src2 == &dst ) ? tmp2.values : src2.values;

	if ( &src1 == &dst )
		matrix_copy( src1, tmp1 );

	if ( &src2 == &dst )
		matrix_copy( src2, tmp2 );

	dst [ 0 ] [ 0 ] = s1 [ 0 ] [ 0 ] * s2 [ 0 ] [ 0 ] + s1 [ 0 ] [ 1 ] * s2 [ 1 ] [ 0 ] + s1 [ 0 ] [ 2 ] * s2 [ 2 ] [ 0 ] + s1 [ 0 ] [ 3 ] * s2 [ 3 ] [ 0 ];
	dst [ 0 ] [ 1 ] = s1 [ 0 ] [ 0 ] * s2 [ 0 ] [ 1 ] + s1 [ 0 ] [ 1 ] * s2 [ 1 ] [ 1 ] + s1 [ 0 ] [ 2 ] * s2 [ 2 ] [ 1 ] + s1 [ 0 ] [ 3 ] * s2 [ 3 ] [ 1 ];
	dst [ 0 ] [ 2 ] = s1 [ 0 ] [ 0 ] * s2 [ 0 ] [ 2 ] + s1 [ 0 ] [ 1 ] * s2 [ 1 ] [ 2 ] + s1 [ 0 ] [ 2 ] * s2 [ 2 ] [ 2 ] + s1 [ 0 ] [ 3 ] * s2 [ 3 ] [ 2 ];
	dst [ 0 ] [ 3 ] = s1 [ 0 ] [ 0 ] * s2 [ 0 ] [ 3 ] + s1 [ 0 ] [ 1 ] * s2 [ 1 ] [ 3 ] + s1 [ 0 ] [ 2 ] * s2 [ 2 ] [ 3 ] + s1 [ 0 ] [ 3 ] * s2 [ 3 ] [ 3 ];

	dst [ 1 ] [ 0 ] = s1 [ 1 ] [ 0 ] * s2 [ 0 ] [ 0 ] + s1 [ 1 ] [ 1 ] * s2 [ 1 ] [ 0 ] + s1 [ 1 ] [ 2 ] * s2 [ 2 ] [ 0 ] + s1 [ 1 ] [ 3 ] * s2 [ 3 ] [ 0 ];
	dst [ 1 ] [ 1 ] = s1 [ 1 ] [ 0 ] * s2 [ 0 ] [ 1 ] + s1 [ 1 ] [ 1 ] * s2 [ 1 ] [ 1 ] + s1 [ 1 ] [ 2 ] * s2 [ 2 ] [ 1 ] + s1 [ 1 ] [ 3 ] * s2 [ 3 ] [ 1 ];
	dst [ 1 ] [ 2 ] = s1 [ 1 ] [ 0 ] * s2 [ 0 ] [ 2 ] + s1 [ 1 ] [ 1 ] * s2 [ 1 ] [ 2 ] + s1 [ 1 ] [ 2 ] * s2 [ 2 ] [ 2 ] + s1 [ 1 ] [ 3 ] * s2 [ 3 ] [ 2 ];
	dst [ 1 ] [ 3 ] = s1 [ 1 ] [ 0 ] * s2 [ 0 ] [ 3 ] + s1 [ 1 ] [ 1 ] * s2 [ 1 ] [ 3 ] + s1 [ 1 ] [ 2 ] * s2 [ 2 ] [ 3 ] + s1 [ 1 ] [ 3 ] * s2 [ 3 ] [ 3 ];

	dst [ 2 ] [ 0 ] = s1 [ 2 ] [ 0 ] * s2 [ 0 ] [ 0 ] + s1 [ 2 ] [ 1 ] * s2 [ 1 ] [ 0 ] + s1 [ 2 ] [ 2 ] * s2 [ 2 ] [ 0 ] + s1 [ 2 ] [ 3 ] * s2 [ 3 ] [ 0 ];
	dst [ 2 ] [ 1 ] = s1 [ 2 ] [ 0 ] * s2 [ 0 ] [ 1 ] + s1 [ 2 ] [ 1 ] * s2 [ 1 ] [ 1 ] + s1 [ 2 ] [ 2 ] * s2 [ 2 ] [ 1 ] + s1 [ 2 ] [ 3 ] * s2 [ 3 ] [ 1 ];
	dst [ 2 ] [ 2 ] = s1 [ 2 ] [ 0 ] * s2 [ 0 ] [ 2 ] + s1 [ 2 ] [ 1 ] * s2 [ 1 ] [ 2 ] + s1 [ 2 ] [ 2 ] * s2 [ 2 ] [ 2 ] + s1 [ 2 ] [ 3 ] * s2 [ 3 ] [ 2 ];
	dst [ 2 ] [ 3 ] = s1 [ 2 ] [ 0 ] * s2 [ 0 ] [ 3 ] + s1 [ 2 ] [ 1 ] * s2 [ 1 ] [ 3 ] + s1 [ 2 ] [ 2 ] * s2 [ 2 ] [ 3 ] + s1 [ 2 ] [ 3 ] * s2 [ 3 ] [ 3 ];

	dst [ 3 ] [ 0 ] = s1 [ 3 ] [ 0 ] * s2 [ 0 ] [ 0 ] + s1 [ 3 ] [ 1 ] * s2 [ 1 ] [ 0 ] + s1 [ 3 ] [ 2 ] * s2 [ 2 ] [ 0 ] + s1 [ 3 ] [ 3 ] * s2 [ 3 ] [ 0 ];
	dst [ 3 ] [ 1 ] = s1 [ 3 ] [ 0 ] * s2 [ 0 ] [ 1 ] + s1 [ 3 ] [ 1 ] * s2 [ 1 ] [ 1 ] + s1 [ 3 ] [ 2 ] * s2 [ 2 ] [ 1 ] + s1 [ 3 ] [ 3 ] * s2 [ 3 ] [ 1 ];
	dst [ 3 ] [ 2 ] = s1 [ 3 ] [ 0 ] * s2 [ 0 ] [ 2 ] + s1 [ 3 ] [ 1 ] * s2 [ 1 ] [ 2 ] + s1 [ 3 ] [ 2 ] * s2 [ 2 ] [ 2 ] + s1 [ 3 ] [ 3 ] * s2 [ 3 ] [ 2 ];
	dst [ 3 ] [ 3 ] = s1 [ 3 ] [ 0 ] * s2 [ 0 ] [ 3 ] + s1 [ 3 ] [ 1 ] * s2 [ 1 ] [ 3 ] + s1 [ 3 ] [ 2 ] * s2 [ 2 ] [ 3 ] + s1 [ 3 ] [ 3 ] * s2 [ 3 ] [ 3 ];
}
