@@ - 0, 0 + 1, 79 @@
#pragma once
#include <d3d9.h>
#include <d3dx9.h>
#include <memory>
#include <vector>
#include "sdk/sdk.h"
#include "zgui.hh"
enum font_orient_t {
	font_left,
	font_center,
	font_right
};

#define get_alpha(col) (((col)&0xff000000)>>24)
#define get_red(col) (((col)&0x00ff0000)>>16)
#define get_green(col) (((col)&0x0000ff00)>>8)
#define get_blue(col) ((col)&0x000000ff)

class vec_t;

enum circle_type_t {
	full, half, quarter
};

struct vertex {
	float x, y, z, rhw;
	std::uint32_t _color;
};

struct font_t {
	ID3DXFont* normal_selected;
	ID3DXFont* normal;
	ID3DXFont* esp;
	ID3DXFont* tabs;
	ID3DXFont* tabs_out;
	ID3DXFont* main;
	ID3DXFont* weapon_icon;
};

class c_renderer {
	IDirect3DDevice9* device;
	IDirect3DVertexBuffer9* vb;
	IDirect3DIndexBuffer9* ib;

public:
	font_t fonts;
	void initialize( IDirect3DDevice9* device );
	void reset( void );

	int string_width( ID3DXFont* font, const char* string );
	float get_frametime( );
	int string_height( ID3DXFont* font, const char* string );
	RECT string_rect( ID3DXFont * font, const char * string );
	void line( float x, float y, float x2, float y2, color _color );



	void filled_box_zgui( float x, float y, float width, float height, color _color );
	void filled_box_outlined_zgui( float x, float y, float width, float height, color _color, color outline_color, float thickness );
	void filled_box( float x, float y, float width, float height, color _color );
	void filled_box_outlined( float x, float y, float width, float height, color colour, color outline_color, float thickness = 1.0f );
	void bordered_box( float x, float y, float width, float height, color _color, float thickness = 1.0f );
	void bordered_box_zgui( float x, float y, float width, float height, color _color, float thickness );
	void bordered_box_outlined( float x, float y, float width, float height, color colour, color outline_color, float thickness = 1.0f );
	void gradient_box( float x, float y, float width, float height, color _color, color _color2, bool vertical );
	void gradient_box_zgui( float x, float y, float width, float height, color _color, color _color2, bool vertical );
	void gradient_box_outlined( float x, float y, float width, float height, float thickness, color colour, color _color2, color outline_color, bool vertical );
	void circle( float x, float y, float radius, float pofloats, color _color );
	void filled_circle( float x, float y, float radius, float pofloats, color _color );
	void filled_circle_outlined( float x, float y, float radius, float pofloats, color _color, color outline_color );
	void string( bool borderedl, ID3DXFont * font, float x, float y, color _color, int orientation, const char * input, ... );
	void filled_circle_gradient( float x, float y, float rad, float angle, float fraction, float resolution, color _color, color _color2 );
	void polygon( std::vector<vertex_t>, float x, float y, float resolution, color _color );
	void string( bool borderedl, ID3DXFont* font, float x, float y, color _color, float orientation, const char *input, ... );

	void release( void );
};

extern std::unique_ptr< c_renderer > renderer;