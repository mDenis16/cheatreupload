#pragma once

#include <Windows.h>
#include <vector>
#include <string>
#include <iostream>

namespace base64 {
	std::string encode( byte const* buf, unsigned int bufLen );
	std::vector< byte > decode( std::string const& );
}