@@ - 0, 0 + 1, 31 @@
#include <Windows.h>
#include <thread>
#include "hooks.h"

/*
*	initializes cheat
*/

void init( HMODULE module ) {
	while ( !GetModuleHandleA( "serverbrowser.dll" ) )
		std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

	sdk::initialize( );
	hooks::initialize( );
}

bool __stdcall DllMain( HMODULE module, uintptr_t reason, void* reserved ) {
	DisableThreadLibraryCalls( module );

	switch ( reason ) {
	case 1: {
		pSAMP = new SAMPFramework( GetModuleHandle( "samp.dll" ) );
		CreateThread( nullptr, 0, ( unsigned long( __stdcall* )( void* ) ) init, module, 0, nullptr );
	} break;
	case 0: {

	} break;
	}

	return true;
}