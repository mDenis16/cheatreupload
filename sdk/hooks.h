
#pragma once
#include "sdk/sdk.h"

namespace hooks {
	extern WNDPROC o_wndproc;

	extern void* o_endscene,
		*o_reset,
		*o_lockcursor,
		*o_framestagenotify,
		*o_painttraverse,
		*o_sceneend,
		*o_drawmodelexecute,
		*o_createmove,
		*o_do_procedural_foot_plant,
		*o_setup_bones;
	extern void __fastcall do_procedural_foot_plant_hk( void* a1, void* _edx, int a2, int a3, int a4, int a5 );
	extern bool __fastcall setup_bones_hk( void* ecx, void* edx, matrix3x4_t* a1, int a2, int a3, float a4 );
	extern void __fastcall framestagenotify_hk( void* ecx, void* edx, client_frame_stage_t stage );
	extern long __fastcall endscene_hk( void* ecx, void* edx, IDirect3DDevice9* device );
	extern long __fastcall reset_hk( void* ecx, void* edx, IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* presentation_parameters );
	extern void __fastcall hk_drawmodelexecute( void* ecx, void* edx, void* ctx, void* state, const c_model_render_info& info, matrix3x4_t* bone_to_world );


	extern void initialize( void );
};