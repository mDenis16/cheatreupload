//hello 𝗃𝖾𝗐𝗂𝖾
#include <d3d9.h>
#include <d3dx9.h>
#include <intrin.h>
#include "hooks.h"
#include "minhook/minhook.h"
#include "draw_mgr.h"
#include "menu.h"
#include "hacks/esp.h"
#include "sdk/utils.h"
#include "sdk/mathematics.h"
#include "options.h"
#include "sdk/sdk.h"
#include "guy.h"
#include "configs.h"
#include "config_items.h"
#include "..\hacks\entity_list.h"
#include <random>
#pragma comment(lib, "urlmon.lib")


#define _SOLVEY(a, b, c, d, e, f) ((c * b - d * a) / (c * f - d * e))
#define SOLVEY(...) _SOLVEY(?, ?, ?, ?, ?, ?)
#define SOLVEX(y, world, forward, right) ((world.x - right.x * y) / forward.x)
#include <iostream>
#include <algorithm>

/*
*	cheat modules
*/

#include "hacks/antiaim.h"
#include "hacks/thirdperson.h"
#include "hacks/animfix.h"
#include "hacks/ragebot.h"
#include "hacks/engine_prediction.h"
#include "hacks/backtrack.h"
#include "hacks/bhop.h"
#include "hacks/engine_prediction.h"
#include "hacks/slowwalk.h"
#include "hacks/resolver.h"

namespace hooks {
	/*
	*	original functions
	*/


	void* o_endscene = nullptr,
		*o_reset = nullptr,
		*o_set_stream_source = nullptr,
		*o_draw_indexed_primitive = nullptr,
		*o_lockcursor = nullptr,
		*o_framestagenotify = nullptr,
		*o_sceneend = nullptr,
		*o_painttraverse = nullptr,
		*o_drawmodelexecute = nullptr,
		*o_get_bool_sv_cheats = nullptr,
		*o_createmove = nullptr,
		*o_do_procedural_foot_plant = nullptr,
		*o_setup_bones = nullptr,
		*o_is_hltv = nullptr,
		*o_update_clientside_animation = nullptr,
		*o_do_post_screen_space_effects = nullptr,
		*o_in_prediction = nullptr;
	/*
	*	hook functions
	*/

	void __fastcall do_procedural_foot_plant_hk( void* a1, void* _edx, int a2, int a3, int a4, int a5 ) {
		auto orig = *( std::uint32_t* ) ( ( uintptr_t ) a1 + 96 );
		*( std::uint32_t* ) ( ( uintptr_t ) a1 + 96 ) = 0;

		__asm {
			mov ecx, a1
			push a5
			push a4
			push a3
			push a2
			call o_do_procedural_foot_plant
		}

		*( std::uint32_t* ) ( ( uintptr_t ) a1 + 96 ) = orig;
	}

	bool __fastcall is_hltv_hk( uintptr_t ecx, uintptr_t edx ) {

		static auto const anim_lod = utils->find_pattern( "client_panorama.dll", "84 C0 0F 85 ? ? ? ? A1 ? ? ? ? 8B B7" );

		uintptr_t player;

		__asm
		{
			mov player, edi
		}

		if ( _ReturnAddress( ) != anim_lod )
			return ( ( bool( __thiscall* )( uintptr_t ) ) is_hltv_hk )( ecx );

		if ( !player || player == 0x000FFFF )
			return ( ( bool( __thiscall* )( uintptr_t ) ) is_hltv_hk )( ecx );



		*( int32_t* ) ( player + 2596 ) = -1;
		*( int32_t* ) ( player + 0xA2C ) = *( int32_t* ) ( player + 0xA28 );
		*( int32_t* ) ( player + 0xA28 ) = 0;

		return true;

	}

	bool __fastcall setup_bones_hk( void* ecx, void* edx, matrix3x4_t* a1, int a2, int a3, float a4 ) {

		auto entity = reinterpret_cast< c_baseentity* >( ( std::uintptr_t )ecx - 0x4 );

		if ( !entity )
			return false;

		if ( !in_setup_bones [ entity->get_index( ) ] )
			return false;

		return false;//  ( ( bool( __thiscall* )( void*, matrix3x4_t*, int, int, float ) ) o_setup_bones )( null );ret;
	}
	void __fastcall update_clientside_animation_hk( void* ecx, void* edx ) {

		if ( !in_update_client_side_animations )
			return;

		( ( void( __thiscall* )( void* ) ) o_update_clientside_animation )( ecx );

	}

	void __fastcall painttraverse_hk( PVOID pPanels, int edx, unsigned int vguiPanel, bool forceRepaint, bool allowForce )
	{

		static unsigned int panelID, panelHudID;

		if ( !panelHudID )
			if ( !strcmp( "HudZoom", vgui_panel->get_name( vguiPanel ) ) )
			{
				panelHudID = vguiPanel;
			}

		if ( panelHudID == vguiPanel && local_player && local_player->is_alive( ) )
		{
			if ( local_player->get_is_scoped( ) )
				return;
		}

		_asm
		{
			PUSH allowForce
			PUSH forceRepaint
			PUSH vguiPanel
			MOV ECX, vgui_panel
			CALL o_painttraverse
		}

	}
	bool __fastcall in_prediction_hk( void* ecx, void* )
	{
		static auto target_ret_addr = _ReturnAddress( );





		return ( ( bool( __thiscall* )( void* ) ) o_in_prediction )( ecx );
	}

	bool __fastcall createmove_hk( void* ecx, void* edx, int input_sample_frametime, c_usercmd* cmd ) {
		void* _ebp;
		__asm mov _ebp, ebp;
		bool& send_packet = *( bool* ) ( *( uintptr_t* ) _ebp - 0x1C );

		if ( !cmd || !cmd->command_number )
			return true;

		if ( !engine->is_in_game( ) && !engine->is_connected( ) || !local_player )
			return;

		closest_target = utils->closest_to_crosshair( );

		aa->fake_lag( cmd, send_packet );
		bhop->createmove( cmd );
		slowmo->createmove( cmd );

		auto o_viewangles = cmd->viewangles;
		auto o_sidemove = cmd->sidemove;
		auto o_forwardmove = cmd->forwardmove;

		engine_prediction->run_engine_prediction( cmd );
		aa->predict_lby_update( cmd, send_packet );

		animfix->fix_local_player_animations( );
		backtrack->manage_records_cmd( );
		ragebot->createmove( cmd, send_packet );
		aa->createmove( cmd, send_packet );

		engine_prediction->end_engine_prediction( );

		math->correct_movement( cmd, o_viewangles, o_forwardmove, o_sidemove );


		math->clamp( cmd->viewangles );

		if ( send_packet )
			thirdperson->thirdperson_angle = cmd->viewangles;

		if ( send_packet )
			last_origin = local_player->get_abs_origin( );

		return false;
	}


	void __fastcall sceneend_hk( void* ecx, void* edx ) {


		auto chams = c_config::get( ).esp.chams;



		if ( !engine->is_connected( ) && !engine->is_in_game( ) )
			return;

		static const auto mat = material_system->find_material( "dev/glow_armsrace.vmt", nullptr );
		if ( local_player )
		{

			vec_t back = local_player->get_abs_angles( );
			vec_t pos = local_player->get_abs_origin( );
			local_player->set_abs_angles( vec_t( 0.f, resolver->data [ local_player->get_index( ) ].fl_goalfeetyaw, 0.f ) );
			modelrender->forced_material_override( mat );
			local_player->set_abs_origin( last_origin );
			local_player->draw_model( );
			modelrender->forced_material_override( nullptr );
			local_player->set_abs_origin( pos );
			local_player->set_abs_angles( back );

		}

		__asm {
			call o_sceneend
		}
	}
	bool  __fastcall do_post_screen_space_effects_hk( void* ecx, void* edx, void* setup ) {

		static auto ret = ( ( bool( __thiscall* )( void*, void* ) ) o_do_post_screen_space_effects )( ecx, setup );


		return ( ( bool( __thiscall* )( void*, void* ) ) o_do_post_screen_space_effects )( ecx, setup );
	}

	void __fastcall framestagenotify_hk( void* ecx, void* edx, client_frame_stage_t stage ) {
		local_player = engine->is_in_game( ) && engine->is_connected( )?c_baseentity::get_client_entity( engine->get_local_player( ) ):nullptr;
		if ( local_player ) local_weapon = local_player->get_active_weapon( );


		thirdperson->pre_framestagenotify( stage );


		resolver->fns( stage );
		animfix->re_work( stage );


		__asm {
			push stage
			call o_framestagenotify
		}

		thirdperson->framestagenotify( stage );
	}
	bool __fastcall get_bool_sv_cheats_hk( void* ecx, void* edx ) {
		if ( !ecx )
			return false;

		if ( local_player && local_player->is_alive( ) ) {
			static auto cam_think = utils->find_pattern( "client_panorama.dll", "85 C0 75 30 38 86" );

			if ( _ReturnAddress( ) == cam_think )
				return true;
		}

		( ( bool( __thiscall* )( void* ) ) o_get_bool_sv_cheats )( ecx );
	}



	long __fastcall endscene_hk( void* ecx, void* edx, IDirect3DDevice9* device ) {
		static auto target_ret_addr = _ReturnAddress( );

		if ( target_ret_addr == _ReturnAddress( ) ) {
			if ( !renderer->fonts.normal )
				renderer->initialize( device );

			IDirect3DStateBlock9* pixel_state = nullptr;
			IDirect3DVertexDeclaration9* vertex_decleration = nullptr;
			IDirect3DVertexShader9* vertex_shader = nullptr;

			device->CreateStateBlock( D3DSBT_PIXELSTATE, &pixel_state );
			device->GetVertexDeclaration( &vertex_decleration );
			device->GetVertexShader( &vertex_shader );

			renderer->reset( );
			esp->render( );
			gui->render_menu( );

			pixel_state->Apply( );
			pixel_state->Release( );
			device->SetVertexDeclaration( vertex_decleration );
			device->SetVertexShader( vertex_shader );
		}

		__asm {
			push device
			call o_endscene
		}
	}

	long __fastcall reset_hk( void* ecx, void* edx, IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* presentation_parameters ) {
		auto o_rst = ( long( __stdcall* )( IDirect3DDevice9*, D3DPRESENT_PARAMETERS* ) ) o_reset;

		if ( !device )
			return o_rst( device, presentation_parameters );

		renderer->release( );

		auto ret = o_rst( device, presentation_parameters );

		if ( ret >= 0 )
			renderer->initialize( device );

		return ret;
	}

	long __fastcall set_stream_source_hk( void* ecx, void* edx, IDirect3DDevice9* device, uintptr_t stream_number, IDirect3DVertexBuffer9* stream_data, uintptr_t offset_in_bytes, uintptr_t stride ) {
		__asm {
			push stride
			push offset_in_bytes
			push stream_data
			push stream_number
			push device
			call o_set_stream_source
		}
	}



	void __stdcall lockcursor_hk( void ) {


		( ( void( __thiscall* )( void* ) ) o_lockcursor )( vgui_surface );
	}

	void __fastcall hk_drawmodelexecute( void* ecx, void* edx, void* ctx, void* state, const c_model_render_info& info, matrix3x4_t* bone_to_world ) {
		esp->run( ecx, edx, ctx, state, info, bone_to_world, o_drawmodelexecute );
		( ( void( __fastcall* )( void*, void*, void*, void*, const c_model_render_info&, matrix3x4_t* ) ) o_drawmodelexecute )( ecx, edx, ctx, state, info, bone_to_world );
	}



	void initialize( void ) {


		auto do_procedural_foot_plant_fn = utils->find_pattern( "client_panorama.dll", "55 8B EC 83 E4 F0 83 EC 78 56 8B F1 57 8B 56 60" );
		auto render_able_player = ( void* ) ( ( ( std::uintptr_t ) utils->find_pattern( "client_panorama.dll", "55 8B EC 83 E4 F8 83 EC 18 56 57 8B F9 89 7C 24 0C" ) + 0x4E ) );
		auto createmove_fn = ( void* ) get_vfunc( clientmode, 24 );
		auto sceneend_fn = ( void* ) get_vfunc( renderview, 9 );
		auto painttraverse_fn = ( void* ) get_vfunc( vgui_panel, 41 );
		auto fsn_fn = ( void* ) get_vfunc( client, 37 );
		auto endscene_fn = ( void* ) get_vfunc( d3d_device, 42 );
		auto reset_fn = ( void* ) get_vfunc( d3d_device, 16 );
		auto set_stream_source_fn = ( void* ) get_vfunc( d3d_device, 100 );
		auto setup_bones_fn = ( void* ) get_vfunc( render_able_player, 13 );
		auto update_clientside_animation_fn = utils->find_pattern( "client_panorama.dll", "55 8B EC 51 56 8B F1 80 BE ? ? ? ? 00 74 ? 8B 06 FF" );
		auto in_prediction_fn = ( void* ) get_vfunc( prediction, 14 );
		auto lockcursor_fn = ( void* ) get_vfunc( vgui_surface, 67 );
		auto drawmodelexecute_fn = ( void* ) get_vfunc( modelrender, 21 );
		auto get_bool_sv_cheats_fn = ( void* ) get_vfunc( cvar->find_var( "sv_cheats" ), 13 );
		auto do_post_screen_effects_fn = ( void* ) get_vfunc( clientmode, 44 );
		auto is_hltv_fn = ( void* ) get_vfunc( engine, 93 );

		MH_Initialize( );

		MH_CreateHook( createmove_fn, &createmove_hk, ( void** ) &o_createmove );
		MH_CreateHook( painttraverse_fn, &painttraverse_hk, ( void** ) &o_painttraverse );
		MH_CreateHook( sceneend_fn, &sceneend_hk, ( void** ) &o_sceneend );
		MH_CreateHook( fsn_fn, &framestagenotify_hk, ( void** ) &o_framestagenotify );
		MH_CreateHook( endscene_fn, &endscene_hk, ( void** ) &o_endscene );
		MH_CreateHook( reset_fn, &reset_hk, ( void** ) &o_reset );
		MH_CreateHook( set_stream_source_fn, &set_stream_source_hk, ( void** ) &o_set_stream_source );
		MH_CreateHook( in_prediction_fn, &in_prediction_hk, ( void** ) &o_in_prediction );
		MH_CreateHook( lockcursor_fn, &lockcursor_hk, ( void** ) &o_lockcursor );
		MH_CreateHook( drawmodelexecute_fn, &hk_drawmodelexecute, ( void** ) &o_drawmodelexecute );
		MH_CreateHook( get_bool_sv_cheats_fn, &get_bool_sv_cheats_hk, ( void** ) &o_get_bool_sv_cheats );
		MH_CreateHook( setup_bones_fn, &setup_bones_hk, ( void** ) &o_setup_bones );
		MH_CreateHook( update_clientside_animation_fn, &update_clientside_animation_hk, ( void** ) &o_update_clientside_animation );
		MH_CreateHook( do_procedural_foot_plant_fn, &do_procedural_foot_plant_hk, ( void** ) &o_do_procedural_foot_plant );
		MH_CreateHook( do_post_screen_effects_fn, &do_post_screen_space_effects_hk, ( void** ) &o_do_post_screen_space_effects );
		MH_CreateHook( is_hltv_fn, &is_hltv_hk, ( void** ) &o_is_hltv );

		MH_EnableHook( createmove_fn );
		MH_EnableHook( sceneend_fn );
		MH_EnableHook( fsn_fn );
		MH_EnableHook( painttraverse_fn );
		MH_EnableHook( endscene_fn );
		MH_EnableHook( reset_fn );
		MH_EnableHook( set_stream_source_fn );
		MH_EnableHook( in_prediction_fn );
		MH_EnableHook( lockcursor_fn );
		MH_EnableHook( drawmodelexecute_fn );
		MH_EnableHook( get_bool_sv_cheats_fn );
		MH_EnableHook( setup_bones_fn );
		MH_EnableHook( do_procedural_foot_plant_fn );
		MH_EnableHook( update_clientside_animation_fn );
		MH_EnableHook( do_post_screen_effects_fn );


	}
}