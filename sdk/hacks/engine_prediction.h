
#pragma once
#include <memory>
#include "..\sdk\sdk.h"

class c_engine_pred {
	float o_curtime;
	float o_frametime;
	int* pred_rand_seed;
	int* set_pred_player;
	vec_t backup_angle;

	struct globals_t {
		float frametime;
		float curtime;
		int tickcount;
	} old_globals;

	struct player_t {
		int flags;
		vec_t velocity;
	} old_player;
public:
	void* move_data;

	int prediction_player;
	int prediction_seed;

	int m_corrected_tickbase;

	void run_engine_prediction( c_usercmd * cmd );

	void run_engine_pred( c_usercmd * cmd );

	void end_engine_prediction( void );

	void end_engine_prediction( c_usercmd * ucmd );


	c_usercmd* m_last_ucmd = nullptr;

};

extern std::unique_ptr< c_engine_pred > engine_prediction;