
#include "autowall.h"
#include "../sdk/utils.h"
#include "../sdk/mathematics.h"
#include "ragebot.h"
std::unique_ptr< c_autowall > autowall = std::make_unique< c_autowall >( );

void c_autowall::clip_trace_to_players( const vec_t& start, const vec_t& end, std::uint32_t mask, c_trace_filter* filter, trace_t* trace ) {
	trace_t player_trace;
	ray_t ray;
	float smallest_fraction = trace->fraction;
	const float max_range = 60.0f;

	ray.init( start, end );
	if ( !trace->hit_entity ) return;
	if ( filter && filter->should_hit_entity( trace->hit_entity, mask ) == false )
		return;


	float range = math->distance_to_ray( ( ( c_baseentity* ) ( trace->hit_entity ) )->world_space_center( ), start, end );
	if ( range >= 0.0f || range <= max_range )
	{
		engine_trace->clip_ray_to_entity( ray, mask | contents_hitbox, ( ( c_baseentity* ) ( trace->hit_entity ) ), &player_trace );
		if ( player_trace.fraction < smallest_fraction )
		{
			*trace = player_trace;
			smallest_fraction = player_trace.fraction;
		}
	}
};
/*void c_autowall::clip_trace_to_players( const vec_t& start, const vec_t& end, std::uint32_t mask, c_trace_filter* filter, trace_t* trace ) {
	static auto clip_trace_to_players_add = ( std::uint32_t ) utils->find_pattern( "client_panorama.dll", "53 8B DC 83 EC 08 83 E4 F0 83 C4 04 55 8B 6B 04 89 6C 24 04 8B EC 81 EC D8 ? ? ? 0F 57 C9" );

	__asm {
		mov eax, filter
		lea ecx, trace
		push ecx
		push eax
		push mask
		lea edx, end
		lea ecx, start
		call clip_trace_to_players_add
		add esp, 0xC
	}
};*/
void c_autowall::scale_dmg( c_baseentity* entity, c_csweapon_info* weapon_info, int hitgroup, float& current_damage ) {

	bool has_heavy_armor = false;
	if ( !entity ) return;
	static auto armored = [ & ] ( void ) {
		switch ( hitgroup ) {
		case ( int ) hitgroup_t::hitgroup_head: return entity->get_has_helmet( );
		case ( int ) hitgroup_t::hitgroup_generic:
		case ( int ) hitgroup_t::hitgroup_chest:
		case ( int ) hitgroup_t::hitgroup_stomach:
		case ( int ) hitgroup_t::hitgroup_leftarm:
		case ( int ) hitgroup_t::hitgroup_rightarm: return true;
		default: return false;
		}
	};

	switch ( hitgroup ) {
	case ( int ) hitgroup_t::hitgroup_head: current_damage *= has_heavy_armor?2.0f:4.0f; break;
	case ( int ) hitgroup_t::hitgroup_stomach: current_damage *= 1.25f; break;
	case ( int ) hitgroup_t::hitgroup_leftleg:
	case ( int ) hitgroup_t::hitgroup_rightleg: current_damage *= 0.75f; break;
	default: break;
	}

	if ( entity->get_armor( ) > 0 && armored( ) ) {
		auto bonus_val = 1.0f;
		auto armor_bonus_rat = 0.5f;
		auto armor_rat = weapon_info->armor_ratio / 2.0f;

		if ( has_heavy_armor ) {
			armor_bonus_rat = 0.33f;
			armor_rat *= 0.5f;
			bonus_val = 0.33f;
		}

		auto dmg = current_damage * armor_rat;

		if ( has_heavy_armor )
			dmg *= 0.85f;

		if ( ( ( current_damage - ( current_damage * armor_rat ) ) * ( bonus_val * armor_bonus_rat ) ) > entity->get_armor( ) )
			dmg = current_damage - ( entity->get_armor( ) / armor_bonus_rat );

		current_damage = dmg;
	}
}

bool c_autowall::fire_bullet( c_baseweapon* pWeapon, vec_t& direction, float& current_damage, fire_bullet_data_t& data )
{
	if ( !pWeapon )
		return false;

	c_csweapon_info* weapon_data = pWeapon->get_cs_weapon_data( );
	if ( !weapon_data )
		return false;

	bool sv_penetration_type;

	float current_distance = 0.f, penetration_power, penetration_distance, max_range, ff_damage_reduction_bullets, ff_damage_bullet_penetration, ray_extension = 40.f;


	static c_convar* penetrationSystem = cvar->find_var( ( "sv_penetration_type" ) );
	static c_convar* damageReductionBullets = cvar->find_var( ( "ff_damage_reduction_bullets" ) );
	static c_convar* damageBulletPenetration = cvar->find_var( ( "ff_damage_bullet_penetration" ) );

	sv_penetration_type = penetrationSystem->get_bool( );
	ff_damage_reduction_bullets = damageReductionBullets->get_float( );
	ff_damage_bullet_penetration = damageBulletPenetration->get_float( );

	trace_t enter_trace;


	c_trace_filter filter;
	filter.skip = local_player;


	max_range = weapon_data->range;

	get_bullet_type( penetration_power, penetration_distance, weapon_data->hud_name, sv_penetration_type );

	if ( sv_penetration_type )
		penetration_power = weapon_data->penetration;

	int possible_hits_remaining = 4;

	current_damage = weapon_data->damage;

	while ( possible_hits_remaining > 0 && current_damage >= 1.f )
	{

		max_range -= current_distance;

		vec_t end = data.src + direction * max_range;

		trace_line( data.src, end, mask_shot_hull | contents_hitbox, local_player, &enter_trace );
		clip_trace_to_players( data.src, end + direction * ray_extension, mask_shot_hull | contents_hitbox, &data.filter, &enter_trace );

		surfacedata_t *enter_surface_data = physics->get_surface_data( enter_trace.surface.surface_props );
		float enter_surf_penetration_modifier = enter_surface_data->game.penetration_modifier;
		int enter_material = enter_surface_data->game.material;

		if ( enter_trace.fraction == 1.f )
			break;

		current_distance += enter_trace.fraction * max_range;
		current_damage *= pow( weapon_data->range_modifier, ( current_distance / 500.f ) );

		if ( current_distance > penetration_distance && weapon_data->penetration > 0.f || enter_surf_penetration_modifier < 0.1f )
			break;


		bool can_do_damage = ( enter_trace.hitgroup != ( int ) hitgroup_t::hitgroup_generic && enter_trace.hitgroup != ( int ) hitgroup_t::hitgroup_generic );

		bool is_enemy = ( ( c_baseentity* ) enter_trace.hit_entity )->is_enemy( );

		if ( ( can_do_damage &&  is_enemy ) )
		{
			scale_dmg( ( c_baseentity* ) enter_trace.hit_entity, weapon_data, enter_trace.hitgroup, current_damage );
			return true;
		}

		if ( !handle_bullet_penetration( weapon_data, enter_trace, data.src, direction, possible_hits_remaining, current_damage, penetration_power, sv_penetration_type, ff_damage_reduction_bullets, ff_damage_bullet_penetration, data ) )
			break;
	}
	return false;
}
bool c_autowall::did_hit_non_world_entity( c_baseentity* entity )
{
	return entity != nullptr && entity->get_index( ) != 0;
}

bool c_autowall::handle_bullet_penetration( c_csweapon_info* weapon_data, trace_t& enter_trace, vec_t& eye_position, vec_t direction, int& possible_hits_remaining, float& current_damage, float penetration_power, bool sv_penetration_type, float ff_damage_reduction_bullets, float ff_damage_bullet_penetration, fire_bullet_data_t& data )
{

	if ( &current_damage == nullptr )
		throw std::invalid_argument( "current damage is null!" );

	trace_t exit_trace;
	auto enemy = ( c_baseentity* ) enter_trace.hit_entity;
	surfacedata_t *enter_surface_data = physics->get_surface_data( enter_trace.surface.surface_props );
	int enter_material = enter_surface_data->game.material;

	float enter_surf_penetration_modifier = enter_surface_data->game.penetration_modifier;
	float enter_damage_modifier = enter_surface_data->game.damage_modifier;
	float thickness, modifier, lost_damage, final_damage_modifier, combined_penetration_modifier;
	bool is_solid_surface = ( ( enter_trace.contents >> 3 ) & contents_solid );
	bool is_light_surface = ( ( enter_trace.surface.flags >> 7 ) & surf_light );

	if ( possible_hits_remaining <= 0

		|| ( !possible_hits_remaining && !is_light_surface && !is_solid_surface && enter_material != ( int ) material_character_identifiers_t::char_tex_grate && enter_material != ( int ) material_character_identifiers_t::char_tex_glass )
		|| weapon_data->penetration <= 0.f
		|| !trace_to_exit( enter_trace, exit_trace, enter_trace.end_pos, direction )
		&& !( engine_trace->get_point_contents( enter_trace.end_pos, mask_shot_hull, nullptr ) & mask_shot_hull ) )
		return false;

	surfacedata_t *exit_surface_data = physics->get_surface_data( exit_trace.surface.surface_props );
	int exit_material = exit_surface_data->game.material;
	float exit_surf_penetration_modifier = exit_surface_data->game.penetration_modifier;
	float exit_damage_modifier = exit_surface_data->game.damage_modifier;


	if ( sv_penetration_type )
	{
		if ( enter_material == ( int ) material_character_identifiers_t::char_tex_grate || enter_material == ( int ) material_character_identifiers_t::char_tex_glass )
		{
			combined_penetration_modifier = 3.f;
			final_damage_modifier = 0.05f;
		}
		else if ( is_solid_surface || is_light_surface )
		{
			combined_penetration_modifier = 1.f;
			final_damage_modifier = 0.16f;
		}
		else if ( enter_material == ( int ) material_character_identifiers_t::char_tex_flesh && ( enemy->is_teammate( ) && ff_damage_reduction_bullets == 0.f ) )
		{
			if ( ff_damage_bullet_penetration == 0.f )
				return false;

			combined_penetration_modifier = ff_damage_bullet_penetration;
			final_damage_modifier = 0.16f;
		}
		else
		{
			combined_penetration_modifier = ( enter_surf_penetration_modifier + exit_surf_penetration_modifier ) / 2.f;
			final_damage_modifier = 0.16f;
		}


		if ( enter_material == exit_material )
		{
			if ( exit_material == ( int ) material_character_identifiers_t::char_tex_cardboard || exit_material == ( int ) material_character_identifiers_t::char_tex_wood )
				combined_penetration_modifier = 3.f;
			else if ( exit_material == ( int ) material_character_identifiers_t::char_tex_plastic )
				combined_penetration_modifier = 2.f;
		}


		thickness = ( exit_trace.end_pos - enter_trace.end_pos ).length_sqr( );
		modifier = fmaxf( 1.f / combined_penetration_modifier, 0.f );

		lost_damage = fmaxf(
			( ( modifier * thickness ) / 24.f )
			+ ( ( current_damage * final_damage_modifier )
				+ ( fmaxf( 3.75f / penetration_power, 0.f ) * 3.f * modifier ) ), 0.f );


		if ( lost_damage > current_damage )
			return false;

		if ( lost_damage > 0.f )
			current_damage -= lost_damage;


		if ( current_damage < 1.f )
			return false;

		eye_position = exit_trace.end_pos;
		--possible_hits_remaining;

		return true;
	}
	else
	{
		combined_penetration_modifier = 1.f;

		if ( is_solid_surface || is_light_surface )
			final_damage_modifier = 0.99f;
		else
		{
			final_damage_modifier = fminf( enter_damage_modifier, exit_damage_modifier );
			combined_penetration_modifier = fminf( enter_surf_penetration_modifier, exit_surf_penetration_modifier );
		}

		if ( enter_material == exit_material && ( exit_material == ( int ) material_character_identifiers_t::char_tex_metal || exit_material == ( int ) material_character_identifiers_t::char_tex_wood ) )
			combined_penetration_modifier += combined_penetration_modifier;

		thickness = ( exit_trace.end_pos - enter_trace.end_pos ).length_sqr( );

		if ( sqrt( thickness ) <= combined_penetration_modifier * penetration_power )
		{
			current_damage *= final_damage_modifier;
			eye_position = exit_trace.end_pos;
			--possible_hits_remaining;

			return true;
		}

		return false;
	}
}
bool c_autowall::classname_is( c_baseentity* entity, const char* class_name ) {
	return fnv_hash( entity->get_classname( ) ) == fnv_hash( class_name );
}

bool c_autowall::is_breakable( c_baseentity* entity ) {
	static auto __rtdynamiccast_fn = utils->find_pattern( "client_panorama.dll", "6A 18 68 ? ? ? ? E8 ? ? ? ? 8B 7D 08" );
	static auto is_breakable_entity_fn = utils->find_pattern( "client_panorama.dll", "55 8B EC 51 56 8B F1 85 F6 74 68 83 BE" );
	static auto multiplayerphysics_rtti_desc = *( uintptr_t* ) ( ( uintptr_t ) is_breakable_entity_fn + 0x50 );
	static auto baseentity_rtti_desc = *( uintptr_t* ) ( ( uintptr_t ) is_breakable_entity_fn + 0x55 );
	static auto breakablewithpropdata_rtti_desc = *( uintptr_t* ) ( ( uintptr_t ) is_breakable_entity_fn + 0xD5 );

	int( __thiscall ***v4 )( c_baseentity* );
	int v5;

	if ( entity && ( *( DWORD * ) ( entity + 256 ) >= 0 || ( *( int( ** )( void ) )( *( DWORD * ) entity + 122 ) )( ) <= 0 ) && *( BYTE * ) ( *( DWORD * ) entity + 640 ) == 2 ) {
		auto v3 = *( DWORD* ) ( *( DWORD * ) entity + 1136 );

		if ( v3 != 17 && v3 != 6 && v3 )
			return false;

		if ( *( DWORD * ) ( *( DWORD * ) entity + 256 ) > 200 )
			return false;

		__asm {
			push 0
			push multiplayerphysics_rtti_desc
			push baseentity_rtti_desc
			push 0
			push entity
			call __rtdynamiccast_fn
			add esp, 20
			mov v4, eax
		}

		if ( v4 ) {
			if ( ( **v4 )( entity ) != 1 )
				return false;

			goto label_18;
		}

		if ( !classname_is( entity, "func_breakable" ) && !classname_is( entity, "func_breakable_surf" ) ) {
			if ( ( *( ( int( __thiscall** )( c_baseentity* ) )*( DWORD * ) entity + 592 ) )( entity ) )
				return false;

			goto label_18;
		}

		if ( !classname_is( entity, "func_breakable_surf" ) || !*( ( uint8_t* ) entity + 2564 ) ) {
		label_18:
			__asm {
				push 0
				push breakablewithpropdata_rtti_desc
				push baseentity_rtti_desc
				push 0
				push entity
				call __rtdynamiccast_fn
				add esp, 20
				mov v5, eax
			}

			if ( v5 && ( ( float( __thiscall* )( uintptr_t ) ) *( uintptr_t* ) ( *( uintptr_t* ) v5 + 12 ) )( v5 ) <= 0.0f )
				return true;
		}
	}

	return false;
}

void c_autowall::get_bullet_type( float& max_range, float& max_distance, char* bullet_type, bool sv_penetration_type )
{
	if ( sv_penetration_type )
	{
		max_range = 35.0;
		max_distance = 3000.0;
	}
	else
	{
		switch ( fnv_hash( bullet_type ) )
		{
		case fnv_hash( "BULLET_PLAYER_338MAG" ):
			max_range = 45.0;
			max_distance = 8000.0;
			break;
		case fnv_hash( "BULLET_PLAYER_762MM" ):
			max_range = 39.0;
			max_distance = 5000.0;
			break;
		case fnv_hash( "BULLET_PLAYER_556MM" ):
			max_range = 35.0;
			max_distance = 4000.0;
			break;
		case fnv_hash( "BULLET_PLAYER_556MM_SMALL" ):
			max_range = 35.0;
			max_distance = 4000.0;
			break;
		case fnv_hash( "BULLET_PLAYER_556MM_BOX" ):
			max_range = 35.0;
			max_distance = 4000.0;
			break;
		case fnv_hash( "BULLET_PLAYER_57MM" ):
			max_range = 30.0;
			max_distance = 2000.0;
			break;
		case fnv_hash( "BULLET_PLAYER_50AE" ):
			max_range = 30.0;
			max_distance = 1000.0;
			break;
		case fnv_hash( "BULLET_PLAYER_357SIG" ):
			max_range = 25.0;
			max_distance = 800.0;
			break;
		case fnv_hash( "BULLET_PLAYER_357SIG_SMALL" ):
			max_range = 25.0;
			max_distance = 800.0;
			break;
		case fnv_hash( "BULLET_PLAYER_357SIG_P250" ):
			max_range = 25.0;
			max_distance = 800.0;
			break;
		case fnv_hash( "BULLET_PLAYER_357SIG_MIN" ):
			max_range = 25.0;
			max_distance = 800.0;
			break;
		case fnv_hash( "BULLET_PLAYER_9MM" ):
			max_range = 21.0;
			max_distance = 800.0;
			break;
		case fnv_hash( "BULLET_PLAYER_45ACP" ):
			max_range = 15.0;
			max_distance = 500.0;
			break;
		case fnv_hash( "BULLET_PLAYER_BUCKSHOT" ):
			max_range = 0.0;
			max_distance = 0.0;
			break;
		}
	}
}


bool c_autowall::trace_to_exit_short( vec_t &point, vec_t &dir, const float step_size, float max_distance )
{
	float flDistance = 0;

	while ( flDistance <= max_distance )
	{
		flDistance += step_size;

		point += dir * flDistance;
		int point_contents = engine_trace->get_point_contents( point, mask_shot_hull );
		if ( !( point_contents & mask_shot_hull ) )
			return true;

	}
	return false;
}

float c_autowall::get_thickness( vec_t& start, vec_t& end ) {
	vec_t dir = end - start;
	vec_t step = start;
	dir /= dir.length( );
	c_trace_filter filter( nullptr );
	trace_t trace;

	float thickness = 0;
	while ( true ) {
		ray_t ceau;
		ceau.init( step, end );
		engine_trace->trace_ray( ceau, mask_solid, &filter, &trace );

		if ( !trace.did_hit( ) )
			break;

		const vec_t lastStep = trace.end_pos;
		step = trace.end_pos;

		if ( ( end - start ).length( ) <= ( step - start ).length( ) )
			break;

		if ( !trace_to_exit_short( step, dir, 5, 90 ) )
			return 0.f;

		thickness += ( step - lastStep ).length( );
	}
	return thickness;
}
bool c_autowall::trace_to_exit( trace_t& enter_trace, trace_t& exit_trace, vec_t start_position, vec_t direction )
{
	vec_t start, end;
	float max_distance = 90.f, ray_extension = 4.f, current_distance = 0;
	int first_contents = 0;

	while ( current_distance <= max_distance )
	{
		current_distance += ray_extension;
		start = start_position + direction * current_distance;

		if ( !first_contents )
			first_contents = engine_trace->get_point_contents( start, mask_shot_hull | contents_hitbox, nullptr );
		int point_contents = engine_trace->get_point_contents( start, mask_shot_hull | contents_hitbox, nullptr );

		if ( !( point_contents & mask_shot_hull ) || point_contents & contents_hitbox && point_contents != first_contents )
		{

			end = start - ( direction * ray_extension );

			trace_line( start, end, mask_shot_hull | contents_hitbox, nullptr, &exit_trace );


			if ( exit_trace.start_solid && exit_trace.surface.flags & surf_hitbox )
			{
				trace_line( start, start_position, mask_shot_hull, ( c_baseentity* ) exit_trace.hit_entity, &exit_trace );

				if ( exit_trace.did_hit( ) && !exit_trace.start_solid )
				{
					start = exit_trace.end_pos;
					return true;
				}

				continue;
			}

			if ( exit_trace.did_hit( ) && !exit_trace.start_solid )
			{
				if ( is_breakable( ( c_baseentity* ) enter_trace.hit_entity ) && is_breakable( ( c_baseentity* ) exit_trace.hit_entity ) )
					return true;

				if ( enter_trace.surface.flags & surf_nodraw || !( exit_trace.surface.flags & surf_nodraw ) && ( exit_trace.plane.normal.dot_product( direction ) <= 1.f ) )
				{
					float mult_amount = exit_trace.fraction * 4.f;
					start -= direction * mult_amount;
					return true;
				}

				continue;
			}

			if ( !exit_trace.did_hit( ) || exit_trace.start_solid )
			{
				if ( enter_trace.did_hit( ) && is_breakable( ( c_baseentity* ) enter_trace.hit_entity ) )
				{
					exit_trace = enter_trace;
					exit_trace.end_pos = start + direction;
					return true;
				}
				continue;
			}
		}
	}
	return false;
}



bool c_autowall::is_armored( c_baseentity *player, int armor, int hitgroup ) {
	if ( !player )
		return false;

	bool result = false;

	if ( armor > 0 ) {
		switch ( hitgroup ) {
		case ( int ) hitgroup_t::hitgroup_generic:
		case ( int ) hitgroup_t::hitgroup_chest:
		case ( int ) hitgroup_t::hitgroup_stomach:
		case ( int ) hitgroup_t::hitgroup_leftarm:
		case ( int ) hitgroup_t::hitgroup_rightarm:
			result = true;
			break;
		case ( int ) hitgroup_t::hitgroup_head:
			result = player->get_has_helmet( );
			break;
		}
	}

	return result;
}


float c_autowall::get_damage_vec( vec_t point ) {

	fire_bullet_data_t data;
	data.filter = c_trace_filter( );
	data.filter.skip = local_player;
	data.src = local_player->get_eye_pos( );
	vec_t angles, direction;
	vec_t tmp = point - data.src;
	float currentDamage = 0;
	angles = math->vector_angles( tmp );
	direction = math->angle_vectors( angles );

	direction.normalize_place( );

	if ( fire_bullet( local_player->get_active_weapon( ), direction, currentDamage, data ) )
		return currentDamage;
	return 0.f;
}


void c_autowall::trace_line( vec_t& vec_abs_start, vec_t& vec_abs_end, unsigned int mask, c_baseentity *ignore, trace_t *ptr )
{
	c_trace_filter filter( ignore ); ray_t samp; samp.init( vec_abs_start, vec_abs_end );
	engine_trace->trace_ray( samp, mask, &filter, ptr );

}

