
#include "engine_prediction.h"
#include "..\sdk\sdk.h"
#include "..\sdk\mathematics.h"
#include "..\sdk\utils.h"
#include "..\options.h"

std::unique_ptr< c_engine_pred > engine_prediction = std::make_unique< c_engine_pred >( );
/*

int c_engine_pred::post_think( c_baseentity *player ) const {
	static auto post_think_vphysics = reinterpret_cast< bool( __thiscall* )( c_baseentity* ) >(utils->find_pattern( "client_panorama.dll", "55 8B EC 83 E4 F8 81 EC ? ? ? ? 53 8B D9 56 57 83 BB ? ? ? ? ? 75 50" ) ) ;
	static auto simulate_player_simulated_entities = reinterpret_cast< void( __thiscall* )( c_baseentity* ) >(utils->find_pattern( "client_panorama.dll", "56 8B F1 57 8B BE ? ? ? ? 83 EF 01 78 72 90 8B 86" ));

	( ( void( __thiscall* )( void* ) ) get_vfunc( model_cache, 33 ) )( model_cache );

	if ( player->is_alive( ) ) {

		( ( void( __thiscall* )( void* ) ) get_vfunc( player, 336 ) )( player );

		if ( player->get_flags( ) & (int)flags_t::fl_onground)
			*reinterpret_cast< uintptr_t * >( uintptr_t( player ) + 0x3014 ) = 0;

		if ( *reinterpret_cast< int * >( uintptr_t( player ) + 0x28BC ) == -1 )

		( ( void( __thiscall* )( void*, int ) ) get_vfunc( player, 216 ) )( player, 0 );

		( ( void( __thiscall* )( void* ) ) get_vfunc( player, 217 ) )( player );

		post_think_vphysics( player );
	}
	simulate_player_simulated_entities( player );

	return ( ( int( __thiscall* )( void* ) ) get_vfunc( model_cache, 33 ) )( model_cache );
}

void c_engine_pred::pre_start( ) {
	if ( !engine->is_in_game( ) || !clientstate )
		return;

	if ( clientstate->m_nDeltaTick > 0 ) {
		 prediction->update( clientstate->m_nDeltaTick, clientstate->m_nDeltaTick > 0,
			clientstate->last_command_ack, clientstate->lastoutgoingcommand+ clientstate->chokedcommands );
	}
}
bool physics_run_think( c_baseentity* entity, int unk01 ) {
	static auto post_run_think = reinterpret_cast< bool( __thiscall* )( c_baseentity*, int ) >( utils->find_pattern( "client_panorama.dll", "55 8B EC 83 EC 10 53 56 57 8B F9 8B 87 ? ? ? ? C1 E8 16" ) );

	return post_run_think( entity, unk01 );
}
void c_engine_pred::start( c_usercmd *cmd ) {
	if ( !cmd || !move_helper || !local_player )
		return;

	if ( !move_data )
		move_data = malloc( 182 );

	if ( !prediction_player || !prediction_seed) {
		prediction_seed = *reinterpret_cast< int* >( ( std::uint32_t ) utils->find_pattern( "client_panorama.dll", "8B 0D ? ? ? ? BA ? ? ? ? E8 ? ? ? ? 83 C4 04" ) + 2 );
		prediction_player = *reinterpret_cast < int* >( ( std::uint32_t ) utils->find_pattern( "client_panorama.dll", "89 35 ? ? ? ? F3 0F 10 46" ) + 2 );
	}

	//	CPrediction::StartCommand
	{
		*reinterpret_cast< int * >( prediction_seed) = cmd ? cmd->random_seed : -1;
		*reinterpret_cast< int * >( prediction_player ) = reinterpret_cast< int >( local_player );

		*reinterpret_cast< c_usercmd ** >( uint32_t( local_player ) + 0x3334 ) = cmd; // pCurrentCommand
		*reinterpret_cast< c_usercmd ** >( uint32_t( local_player ) + 0x3288 ) = cmd; // unk01
	}

	//	backup player variables
	old_player.flags = local_player->get_flags( );
	old_player.velocity = local_player->get_velocity( );

	//	backup globals
	old_globals.curtime = globals->curtime;
	old_globals.frametime = globals->frametime;
	old_globals.tickcount = globals->tickcount;

	//	backup tick base
	const int old_tickbase = local_player->get_tickbase( );

	//	backup prediction variables
	const bool old_in_prediction = prediction->in_prediction;
	const bool old_first_prediction = prediction->is_first_time_predicted;

	//	set globals correctly
	globals->curtime = local_player->get_tickbase() * globals->interval_per_tick;
	globals->frametime = prediction->engine_paused ? 0 : globals->interval_per_tick;
	globals->tickcount = local_player->get_tickbase( );

	//	setup prediction
	prediction->is_first_time_predicted = false;
	prediction->in_prediction = true;

	if ( cmd->impulse )
		*reinterpret_cast< uint32_t * >( uint32_t( local_player ) + 0x31FC ) = cmd->impulse;

	//	CBasePlayer::UpdateButtonState
	{
		cmd->buttons |= *reinterpret_cast< uint32_t * >( uint32_t( local_player ) + 0x3330 );

		const int v16 = cmd->buttons;
		int *unk02 = reinterpret_cast< int * >( uint32_t( local_player ) + 0x31F8 );
		const int v17 = v16 ^ *unk02;

		*reinterpret_cast< int * >( uint32_t( local_player ) + 0x31EC ) = *unk02;
		*reinterpret_cast< int * >( uint32_t( local_player ) + 0x31F8 ) = v16;
		*reinterpret_cast< int * >( uint32_t( local_player ) + 0x31F0 ) = v16 & v17;
		*reinterpret_cast< int * >( uint32_t( local_player ) + 0x31F4 ) = v17 & ~v16;
	}

	//	check if player is standing on moving ground
	prediction->check_moving_ground( local_player, globals->frame_count );

	//	copy from command to player
	local_player->set_local_viewangles( cmd->viewangles );

	//	CPrediction::RunPreThink
	{
		//	THINK_FIRE_ALL_FUNCTIONS
		if ( physics_run_think(local_player,  0 ) ) {
			 local_player->pre_think( );
		}
	}

	//	CPrediction::RunThink
	{
		const auto next_think = reinterpret_cast< int * >( uint32_t( local_player ) + 0xFC );
		if ( *next_think > 0 && *next_think <= local_player->get_tickbase( ) ) {
			//	TICK_NEVER_THINK
			*next_think = -1;

			local_player->think( );
		}
	}

	//	set host
	move_helper->set_host( local_player );

	//	setup input
	prediction->setup_move( local_player, cmd, move_helper, move_data );

	//	run movement
	game_movement->process_movement( local_player, (c_move_data*)move_data );

	//	finish prediction
	prediction->finish_move( local_player, cmd, move_data );

	//	invoke impact functions
	move_helper->process_impacts( );

	//	CPrediction::RunPostThink
	{
		post_think( local_player );
	}

	//	restore tickbase
	local_player->get_tickbase() = old_tickbase;

	//	restore prediction
	prediction->is_first_time_predicted = old_first_prediction;
	prediction->in_prediction = old_in_prediction;

}

void c_engine_pred::end( ) const {

	if ( !local_player || !move_helper )
		return;

	globals->curtime = old_globals.curtime;
	globals->frametime = old_globals.frametime;
	globals->tickcount = old_globals.tickcount;

	//	CPrediction::FinishCommand
	{
		*reinterpret_cast< uint32_t * >( reinterpret_cast< uint32_t >( local_player ) + 0x3334 ) = 0;

		*reinterpret_cast< int * >( prediction_seed) = static_cast< uint32_t >( -1 );
		*reinterpret_cast< int * >( prediction_player ) = 0;
	}

	game_movement->reset( );
}

*/
void c_engine_pred::run_engine_prediction( c_usercmd* cmd ) {
	if ( !local_player || !local_player->is_alive( ) || !move_helper )
		return;

	if ( !pred_rand_seed || !set_pred_player ) {
		pred_rand_seed = *reinterpret_cast< int** >( ( std::uint32_t ) utils->find_pattern( "client_panorama.dll", "8B 0D ? ? ? ? BA ? ? ? ? E8 ? ? ? ? 83 C4 04" ) + 2 );
		set_pred_player = *reinterpret_cast < int** >( ( std::uint32_t ) utils->find_pattern( "client_panorama.dll", "89 35 ? ? ? ? F3 0F 10 46" ) + 2 );
	}

	c_move_data data;
	memset( &data, 0, sizeof( c_move_data ) );
	local_player->set_current_command( cmd );
	move_helper->set_host( local_player );

	*pred_rand_seed = cmd->random_seed;
	*set_pred_player = uintptr_t( local_player );

	*reinterpret_cast< uint32_t* >( reinterpret_cast< uint32_t >( local_player ) + 0x3314 ) = reinterpret_cast< uint32_t >( cmd );
	*reinterpret_cast< uint32_t* >( reinterpret_cast< uint32_t >( local_player ) + 0x326C ) = reinterpret_cast< uint32_t >( cmd );

	o_curtime = globals->curtime;
	o_frametime = globals->frametime;

	u_random_seed = *pred_rand_seed;
	globals->curtime = local_player->get_tickbase( ) * globals->interval_per_tick;
	globals->frametime = globals->interval_per_tick;

	if ( cmd->impulse )
		*reinterpret_cast< uint32_t * >( uint32_t( local_player ) + 0x31FC ) = cmd->impulse;

	{
		cmd->buttons |= *reinterpret_cast< uint32_t * >( uint32_t( local_player ) + 0x3330 );

		const int v16 = cmd->buttons;
		int *unk02 = reinterpret_cast< int * >( uint32_t( local_player ) + 0x31F8 );
		const int v17 = v16 ^ *unk02;

		*reinterpret_cast< int * >( uint32_t( local_player ) + 0x31EC ) = *unk02;
		*reinterpret_cast< int * >( uint32_t( local_player ) + 0x31F8 ) = v16;
		*reinterpret_cast< int * >( uint32_t( local_player ) + 0x31F0 ) = v16 & v17;
		*reinterpret_cast< int * >( uint32_t( local_player ) + 0x31F4 ) = v17 & ~v16;
	}

	game_movement->start_track_prediction_errors( local_player );

	prediction->setup_move( local_player, cmd, move_helper, &data );
	game_movement->process_movement( local_player, &data );
	prediction->finish_move( local_player, cmd, &data );

	if ( local_player->get_active_weapon( ) )
		local_player->get_active_weapon( )->update_accuracy_penalty( );
}

void c_engine_pred::end_engine_prediction( void ) {
	if ( !local_player || !local_player->is_alive( ) || !move_helper )
		return;

	game_movement->finish_track_prediction_errors( local_player );
	move_helper->set_host( nullptr );

	if ( pred_rand_seed || set_pred_player ) {
		*pred_rand_seed = -1;
		*set_pred_player = 0;
	}

	globals->curtime = o_curtime;
	globals->frametime = o_frametime;

	local_player->set_current_command( nullptr );
}


bool unk1( void* local ) {
	auto v1 = local;

	if ( *( std::uintptr_t* ) ( ( std::uintptr_t ) local + 252 ) > 0 )
		return true;

	auto v3 = *( std::uintptr_t* ) ( ( std::uintptr_t ) local + 700 );
	auto v4 = 0;

	if ( v3 > 0 ) {
		auto v5 = ( std::uintptr_t* ) ( *( std::uintptr_t* )( ( std::uintptr_t ) v1 + 688 ) + 20 );

		while ( *v5 <= 0 ) {
			++v4;
			v5 += 8;

			if ( v4 >= v3 )
				return false;
		}

		return true;
	}

	return false;
}

bool unk( void* local, char a2 ) {
	auto v2 = local;
	auto v3 = *( std::uintptr_t* ) ( ( std::uintptr_t ) local + 232 );
	auto result = ( *( std::uintptr_t* ) ( ( std::uintptr_t ) local + 232 ) >> 22 ) & 1;

	if ( result ) {
		if ( a2 ) {
			*( std::uintptr_t* ) ( ( std::uintptr_t ) local + 232 ) = v3 & 0xFFBFFFFF;
			return result;
		}
	}
	else if ( a2 )
		return result;

	if ( !result ) {
		result = unk1( local );

		if ( !result )
			*( std::uintptr_t* ) ( ( std::uintptr_t ) v2 + 232 ) = v3 | 0x400000;
	}

	return result;
}
/*
void c_engine_pred::run_engine_prediction( c_usercmd* ucmd ) {

	if ( !local_player || !move_helper )
		return;

	if ( !this->pred_rand_seed )
		this->pred_rand_seed = *reinterpret_cast< int** >( ( std::uint32_t ) utils->find_pattern( "client_panorama.dll", "8B 0D ? ? ? ? BA ? ? ? ? E8 ? ? ? ? 83 C4 04" ) + 2 );

	if ( !this->set_pred_player )
		this->set_pred_player = *reinterpret_cast < int** >( ( std::uint32_t ) utils->find_pattern( "client_panorama.dll", "89 35 ? ? ? ? F3 0F 10 46" ) + 2 );


	std::memset( &this->move_data, 0, sizeof( move_data ) );

	this->o_curtime = globals->curtime;
	this->o_frametime = globals->frametime;

	*( c_usercmd** ) ( ( std::uintptr_t ) local_player + 13108 ) = ucmd;
	*( c_usercmd** ) ( ( std::uintptr_t ) local_player + 12936 ) = ucmd;

	const auto getRandomSeed = [ & ] ( )
	{
		using MD5_PseudoRandomFn = unsigned long( __cdecl* )( std::uintptr_t );
		static auto offset = utils->find_pattern( "client_panorama.dll", "55 8B EC 83 E4 F8 83 EC 70 6A 58" );
		static auto MD5_PseudoRandom = reinterpret_cast< MD5_PseudoRandomFn >( offset );
		return MD5_PseudoRandom( ucmd->command_number ) & 0x7FFFFFFF;
	};

	*this->pred_rand_seed = getRandomSeed( );
	*this->set_pred_player = ( std::uintptr_t ) local_player;

	globals->curtime = local_player->get_tickbase( ) * globals->interval_per_tick;
	globals->frametime = globals->interval_per_tick;

	game_movement->start_track_prediction_errors( local_player );

	*( std::uintptr_t* ) ( ( std::uintptr_t ) ucmd + 48 ) |= *( std::uintptr_t * )( ( std::uintptr_t ) local_player + 13104 );

	auto v16 = *( std::uintptr_t* )( ( std::uintptr_t ) ucmd + 48 );
	auto v17 = v16 ^ *( std::uintptr_t* )( ( std::uintptr_t ) local_player + 12792 );
	*( std::uintptr_t* ) ( ( std::uintptr_t ) local_player + 12780 ) = *( std::uintptr_t * )( ( std::uintptr_t ) local_player + 12792 );
	*( std::uintptr_t* ) ( ( std::uintptr_t ) local_player + 12792 ) = v16;
	*( std::uintptr_t* ) ( ( std::uintptr_t ) local_player + 12784 ) = v16 & v17;
	*( std::uintptr_t* ) ( ( std::uintptr_t ) local_player + 12788 ) = v17 & ~v16;

	local_player->set_local_viewangles( ucmd->viewangles );

	auto v20 = *( std::uintptr_t * ) ( ( std::uintptr_t ) local_player + 252 );

	if ( v20 != -1 && v20 > 0 && v20 <= *( std::uintptr_t * )( ( std::uintptr_t ) local_player + 13356 ) ) {
		*( std::uintptr_t * ) ( ( std::uintptr_t ) local_player + 252 ) = -1;
		unk( local_player, 0 );
		( *( void( __thiscall ** )( c_baseentity* ) ) ( *( std::uintptr_t * ) ( ( std::uintptr_t ) local_player + 552 ) ) )( local_player );
	}

	move_helper->set_host( local_player );

	prediction->setup_move( local_player, ucmd, move_helper, &this->move_data );
	game_movement->process_movement( local_player, &this->move_data );
	prediction->finish_move( local_player, ucmd, &this->move_data );

}


/*
void c_engine_pred::end_engine_prediction( c_usercmd* ucmd ) {

	if ( !local_player || !move_helper )
		return;

	game_movement->finish_track_prediction_errors( local_player );
	move_helper->set_host( nullptr );
	//(*(void(**)(void))(csgo->m_move_helper + 16))();
	( *( void( __thiscall ** )( c_baseentity* ) )( *( std::uintptr_t* ) local_player + 1260 ) )( local_player );
	//(*(void(__thiscall **)(void*, player_t*))(csgo->m_movement + 16))(&csgo->m_movement, local);
	*( std::uintptr_t* ) ( ( std::uintptr_t ) local_player + 13108 ) = 0;

	if ( this->pred_rand_seed )
		*this->pred_rand_seed = -1;

	if ( this->set_pred_player )
		*this->set_pred_player = 0;

	globals->curtime = this->o_curtime;
	globals->frametime = this->o_frametime;

	//*( ucmd_t** ) ( ( std::uintptr_t ) local + 0x3334 ) = nullptr;
}
*/