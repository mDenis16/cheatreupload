
#pragma once
#include <memory>
#include "../sdk/sdk.h"
#include "backtrack.h"

class c_ragebot {

	bool bounding( c_baseentity * entity, vec_t bb_min, vec_t bb_max, vec_t origin, matrix3x4_t matrix [ 128 ] );
	std::pair< c_backtrack::backtrack_records_t, bool > find_best_tick( c_baseentity* entity );



public:

	void select_targets( );

	bool hit_chance( vec_t point, vec_t angle, c_baseentity * ent, float hitchance );

	void createmove( c_usercmd * cmd, bool & send_packet );


	c_baseentity* last_target_global = nullptr;

	matrix3x4_t cached_bones [ 65 ] [ 128 ];
	c_backtrack::backtrack_records_t best_tick_global [ 65 ];
	bool can_backtrack [ 65 ];


	vec_t get_point( matrix3x4_t * matrix, c_baseentity * entity, const int & hitbox );


	float get_config_pointscale( const int & hit_box );

	bool multi_point( matrix3x4_t * matrix, c_baseentity * entity, const int & hitbox, std::vector<vec_t>& points );

	int prioritize_hitbox( );

	std::vector<int> hitscan_list( c_baseentity * entity );
	std::vector< int > calculed_hitscan [ 65 ];

	std::pair<vec_t, float> get_best_point_backtrack( c_baseentity * entity, float min_dmg );


	std::pair<vec_t, std::pair<bool, float>> get_best_player( c_baseentity * entity, float min_dmg );

	std::string hitbox_to_string( int i );

	std::pair<vec_t, float> get_best_point( matrix3x4_t * matrix, c_baseentity * entity, float min_dmg );


};

extern std::unique_ptr< c_ragebot > ragebot;