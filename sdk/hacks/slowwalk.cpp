
#include "slowwalk.h"
#include "../options.h"
#include "..\config_items.h"
#include "..\configs.h"
#include "..\sdk\utils.h"
std::unique_ptr< c_slowwalk > slowmo = std::make_unique< c_slowwalk >( );
void c_slowwalk::auto_bhop( c_usercmd* cmd )
{
	if ( !c_config::get( ).misc.auto_bhop )
		return;
	if ( local_player->get_movetype( ) == ( uint8_t ) movetype_t::movetype_ladder )
		return;

	static bool last_jumped = false;
	static bool should_fake = false;

	if ( !last_jumped && should_fake )
	{
		should_fake = false;
		cmd->buttons |= ( int ) buttons_t::in_jump;
	}
	else if ( cmd->buttons & ( int ) buttons_t::in_jump )
	{
		if ( local_player->get_flags( ) & ( int ) flags_t::fl_onground )
			should_fake = last_jumped = true;
		else
		{
			cmd->buttons &= ~( int ) buttons_t::in_jump;
			last_jumped = false;
		}
	}
	else
		should_fake = last_jumped = false;
}

void c_slowwalk::strafer( c_usercmd* cmd ) {
	if ( !c_config::get( ).misc.auto_strafe )
		return;

	if ( local_player->get_movetype( ) == ( int ) movetype_t::movetype_noclip || local_player->get_movetype( ) == ( int ) movetype_t::movetype_ladder )
		return;

	if ( !GetAsyncKeyState( VK_SPACE ) || local_player->get_velocity( ).length_2d( ) < 0.5 )
		return;

	if ( !( local_player->get_flags( ) & ( int ) flags_t::fl_onground ) ) {
		static float cl_sidespeed = cvar->find_var( "cl_sidespeed" )->get_float( );
		if ( fabsf( cmd->mousedx > 2 ) ) {
			cmd->sidemove = ( cmd->mousedx < 0.f )?-cl_sidespeed:cl_sidespeed;
			return;
		}

		if ( !local_player->get_velocity( ).length_2d( ) > 0.5 || local_player->get_velocity( ).length_2d( ) == NAN || local_player->get_velocity( ).length_2d( ) == INFINITE )
		{
			cmd->forwardmove = 400;
			return;
		}

		cmd->forwardmove = utils->clamp( 5850.f / local_player->get_velocity( ).length_2d( ), -400, 400 );
		if ( ( cmd->forwardmove < -400 || cmd->forwardmove > 400 ) )
			cmd->forwardmove = 0;

		const auto vel = local_player->get_velocity( );
		const float y_vel = math->rad_to_deg( atan2( vel.y, vel.x ) );
		const float diff_ang = math->normalize_yaw( cmd->viewangles.y - y_vel );

		cmd->sidemove = ( diff_ang > 0.0 )?-cl_sidespeed:cl_sidespeed;
		cmd->viewangles.y = math->normalize_yaw( cmd->viewangles.y - diff_ang );
	}
}

void  c_slowwalk::stop_movement( c_usercmd& cmd )
{
	vec_t velocity = local_player->get_velocity( ); velocity.z = 0;
	float speed = velocity.length_2d( );

	if ( speed < 1.f )
	{
		cmd.forwardmove = 0.f;
		cmd.sidemove = 0.f;
		return;
	}

	float accel = 5.5f;
	float maxSpeed = 320.f;
	float playerSurfaceFriction = 1.0f;
	float max_accelspeed = accel * globals->interval_per_tick * maxSpeed * playerSurfaceFriction;

	float wishspeed = 0.f;
	speed - max_accelspeed <= -1.f?wishspeed = max_accelspeed / ( speed / ( accel * globals->interval_per_tick ) ):wishspeed = max_accelspeed;

	vec_t direction = math->vector_angles( velocity * -1.f );
	direction.y = cmd.viewangles.y - direction.y;
	direction = math->angle_vectors( direction );

	cmd.forwardmove = direction.x * wishspeed;
	cmd.sidemove = direction.y * wishspeed;
}
void c_slowwalk::createmove( c_usercmd* cmd ) {


	auto_bhop( cmd );
	strafer( cmd );

	if ( !c_config::get( ).misc.slow_walk || !( GetKeyState( c_config::get( ).misc.slow_wakk_key ) & 0x8000 ) )
		return;

	auto w = local_player->get_active_weapon( );

	if ( !w )
		return;

	auto fastest = ( local_player->get_is_scoped( )?w->get_cs_weapon_data( )->max_speed_alt:w->get_cs_weapon_data( )->max_speed ) * 0.34f;

	if ( local_player->get_velocity( ).length_2d( ) < fastest )
		return;

	stop_movement( *cmd );


}