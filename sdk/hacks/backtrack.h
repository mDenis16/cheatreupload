#pragma once
#include <memory>
#include <algorithm>
#include "../sdk/sdk.h"
#include "resolver.h"
class c_backtrack {
public:
	struct backtrack_records_t {
		bool shoot, moving, slow_walking, lby_update;
		int flags;
	
		float simtime, duckamount, curtime, lby;
		std::array< float, 24 > pose_params;
		vec_t absorigin, origin, eyeangles, absangles, obbmin, obbmax, velocity;
		c_baseentity* entity;
		matrix3x4_t cached_bones [ 128 ];
		c_csgoplayeranimstate state;
		
		c_resolver::player_data_t resolver;

		animationlayer anim_layer [ 15 ];
	};


	void framestagenotify( client_frame_stage_t stage );

float get_lerp_time( );

float interpolation_time( );

bool is_tick_valid( backtrack_records_t record );

	void backup_player( c_baseentity * entity );



	void restore_record( c_baseentity* entity, backtrack_records_t record );
	void restore_player( c_baseentity * entity );
	void recieve_record( c_baseentity * entity, backtrack_records_t & record );
	float lerp_time = 0.f;
	backtrack_records_t backup_data [ 64 ];
	std::deque< backtrack_records_t > backtrack_records [ 64 ];
};

extern std::unique_ptr< c_backtrack > backtrack;