
#pragma once
#include "../sdk/sdk.h"

struct fire_bullet_data_t {

	vec_t src;
	trace_t enter_trace;
	vec_t direction;
	c_trace_filter filter;
	float trace_length;
	float trace_length_remaining;
	float current_damage;
	int penetrate_count;
};

class c_autowall {
public:
	bool handle_bullet_penetration( c_csweapon_info * weapon_data, trace_t & enter_trace, vec_t & eye_position, vec_t direction, int & possible_hits_remaining, float & current_damage, float penetration_power, bool sv_penetration_type, float ff_damage_reduction_bullets, float ff_damage_bullet_penetration, fire_bullet_data_t & data );

	bool classname_is( c_baseentity* entity, const char* class_name );
	bool is_breakable( c_baseentity * entity );

	void get_bullet_type( float & max_range, float & max_distance, char * bullet_type, bool sv_penetration_type );

	void clip_trace_to_players( const vec_t & start, const vec_t & end, std::uint32_t mask, c_trace_filter * filter, trace_t * trace );
	void scale_dmg( c_baseentity* entity, c_csweapon_info* weapon_info, int hitgroup, float& current_damage );


	bool fire_bullet( c_baseweapon * pWeapon, vec_t & direction, float & current_damage, fire_bullet_data_t & data );

	bool did_hit_non_world_entity( c_baseentity * entity );

	bool trace_to_exit_short( vec_t & point, vec_t & dir, const float step_size, float max_distance );
	float get_thickness( vec_t & start, vec_t & end );

	bool trace_to_exit( trace_t & enter_trace, trace_t & exit_trace, vec_t start_position, vec_t direction );

	bool is_armored( c_baseentity* player, int armor, int hitgroup );
	float get_damage_vec( vec_t point );


	void trace_line( vec_t & vec_abs_start, vec_t & vec_abs_end, unsigned int mask, c_baseentity * ignore, trace_t * ptr );


};

extern std::unique_ptr< c_autowall > autowall;