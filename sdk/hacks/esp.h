
#pragma once
#include "../sdk/sdk.h"

struct esp_record_t {
	bool dormant;
	vec_t origin, min, max;
	int health;
	float time, top, left, right, bottom;
	item_definition_index weapon_item;
	c_baseweapon* weapon;
	char name [ 128 ];
};

class c_esp {
private:
	std::array< esp_record_t, 500 > esp_records;

public:
	void scope_lines( );
	void draw_angles( vec_t angle, color color, const char * input );

	void render( void );
	void draw_esp_box( c_baseentity * entity, esp_record_t esp_box );

	void draw_world_esp( );

	void serverboxes( c_baseentity * entity, matrix3x4_t pBoneToWorldOut [ 128 ] );

	void capsule_overlay( c_baseentity *entity, float duration, matrix3x4_t pBoneToWorldOut [ 128 ] );
	void removals( );
	void run( void* ecx, void* edx, void* context, void* state, const c_model_render_info& info, matrix3x4_t* bone_to_world, void* o_drawmodelexecute );
	void fov_arrows( c_baseentity * entity );
};

extern std::unique_ptr< c_esp > esp;