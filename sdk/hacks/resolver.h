
#pragma once
#include <memory>
#include "../sdk/sdk.h"


enum desync_type {
	static_left_desync,
	static_right_desync,
	static_desync_auto,
	jittering
};

enum resolve_type {
	overide,
	bruteforce,
	baim
};


class c_resolver {

public:

	class player_data_t {
	public:
		float calculated_angle_on_client = 0.f;
		float last_lby = 0.f;
		float lby = 0.f;
		float last_networked_lby = 0.f;
		float last_networked_goal_feet_client_calc = 0.f;
		float last_network_goal_feet_client_side = 0.f;
		float last_networked_pitch = 0.f;
		float last_networked_angle = 0.f;
		float max_desync_delta = 0.f;
		float speed = 0.f;
		float pitch_hit = 0.f;
		float fl_goalfeetyaw = 0.f;
		float fl_goalfeetyaw_calc = 0.f;
		float final_angle = 0.f;
		int resolving_method = 0;
		int antiaim_method = 0;
		int desync_side = 0;
		vec_t uinterpolated_angle = vec_t( );
		vec_t abs_rotation = vec_t( );
		vec_t rotation = vec_t( );
	};
	float server_goal_feet_yaw( c_baseentity* entity );

	float max_desync_delta( c_baseentity * entity );

	bool is_adjusting_balance( c_baseentity * player, animationlayer * layer );

	bool is_adjusting_980( c_baseentity * player, animationlayer * layer );

	void log_data( c_baseentity * entity );

	void get_type_of_antiaim( c_baseentity * entity );

	int desync_side( c_baseentity * entity );


	void resolving( c_baseentity * entity );

	void apply_angles( c_baseentity * entity );

	void fns( client_frame_stage_t stage );

	player_data_t data [ 64 ];
};

extern std::unique_ptr< c_resolver > resolver;