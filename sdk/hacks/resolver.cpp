#include "../sdk/utils.h"
#include "../options.h"
#include "resolver.h"
#include "backtrack.h"
#include "..\sdk\mathematics.h"
std::unique_ptr< c_resolver > resolver = std::make_unique< c_resolver >( );
/*thanks to sharklaser for setupvelocity code*/
float c_resolver::server_goal_feet_yaw( c_baseentity* entity )
{
	auto animstate = entity->get_anim_state( );
	if ( !animstate ) return 0.f;
	/* data */
	float ground_fraction = *( float * )( animstate + 0x11C );
	float duck_ammount = *( float * )( animstate + 0xA4 );
	float ducking_speed = max( 0, min( 1, *reinterpret_cast< float* > ( animstate + 0xFC ) ) );
	float running_speed = max( 0, min( *reinterpret_cast< float* >( animstate + 0xF8 ), 1 ) );
	/* offsets */
	int backup_eflags = entity->get_eflags( );

	entity->get_eflags( ) = ( 1 << 12 );
	vec_t abs_velocity = entity->get_abs_velocity( );
	entity->get_eflags( ) = backup_eflags;

	float speed = std::fmin( abs_velocity.length( ), 260.0f );
	

	float goal_feet_yaw = animstate->goal_feet_yaw;

	float eye_feet_delta = math->angle_diff( animstate->eye_angles_y, goal_feet_yaw );



	float flYawModifier = ( ( ( ground_fraction * -0.3f ) - 0.2f ) * running_speed ) + 1.0f;

	if ( duck_ammount > 0.0f )
		flYawModifier = flYawModifier + ( ( duck_ammount * ducking_speed ) * ( 0.5f - flYawModifier ) );


	float flMaxYawModifier = flYawModifier * 58.f;
	float flMinYawModifier = flYawModifier * -58.f;

	if ( eye_feet_delta <= flMaxYawModifier )
		if ( flMinYawModifier > eye_feet_delta )
			goal_feet_yaw = fabs( flMinYawModifier ) + animstate->eye_angles_y;

		else
			goal_feet_yaw = animstate->eye_angles_y - fabs( flMaxYawModifier );


	math->normalize_yaw( goal_feet_yaw );

	if ( speed > 0.1f || fabs( abs_velocity.z ) > 100.0f )
	{
		goal_feet_yaw = math->fl_approach_angle(
			animstate->eye_angles_y,
			goal_feet_yaw,
			( ( ground_fraction * 20.0f ) + 30.0f )
			* animstate->last_client_side_animation_update_time );
	}
	else
	{
		goal_feet_yaw = math->fl_approach_angle(
			entity->get_lby( ),
			goal_feet_yaw,
			animstate->last_client_side_animation_update_time * 100.0f );
	}

	math->normalize_yaw( goal_feet_yaw );
	
	return goal_feet_yaw;
}
float c_resolver::max_desync_delta( c_baseentity* entity )
{
	auto animstate = entity->get_anim_state( );
	
	return 0.f;
}
bool c_resolver::is_adjusting_balance( c_baseentity *player, animationlayer *layer )
{
	for ( int i = 0; i < 16; i++ )
	{
		const int activity = player->get_sequence_activity( layer [ i ].sequence );
		if ( activity == 979 )
		{
			layer = &player->get_anim_overlay( i );
			return true;
		}
	}
	return false;
}

bool c_resolver::is_adjusting_980( c_baseentity *player, animationlayer *layer )
{
	for ( int i = 0; i < 16; i++ )
	{
		const int activity = player->get_sequence_activity( layer [ i ].sequence );
		if ( activity == 980 )
		{
			layer = &player->get_anim_overlay( i );
			return true;
		}
	}
	return false;
}

void c_resolver::log_data( c_baseentity* entity )
{
	auto anim = entity->get_anim_state( ); if (!anim ) return;
	static float simtime [ 65 ];

	player_data_t &player = data[ entity->get_index( ) ];
	
	player.fl_goalfeetyaw_calc = server_goal_feet_yaw( entity );
	player.lby = entity->get_lby( );
	player.fl_goalfeetyaw = anim->goal_feet_yaw;
	player.max_desync_delta = max_desync_delta( entity );
	if ( simtime [ entity->get_index( ) ] != entity->get_simulation_time( ) )
	{
		player.last_networked_goal_feet_client_calc = player.fl_goalfeetyaw_calc;
		player.last_networked_lby = player.lby;
		player.last_network_goal_feet_client_side = anim->goal_feet_yaw;
		player.last_networked_pitch = entity->get_eyeangles( ).x;
		simtime [ entity->get_index( ) ] = entity->get_simulation_time( );
	}

}
void c_resolver::get_type_of_antiaim( c_baseentity* entity )
{
	player_data_t &player = data [ entity->get_index( ) ];


   animationlayer cur_balance;
	float lby_delta = abs( player.lby - player.last_networked_lby );
	if ( lby_delta > player.max_desync_delta )
		player.antiaim_method = resolve_type::SELF_CALC;
	else if ( is_adjusting_balance( entity, &cur_balance ) || is_adjusting_980( entity, &cur_balance))
		player.antiaim_method = resolve_type::SUBSTRACT_DELTA;

	if ( is_adjusting_balance( entity, &cur_balance ) )
	{
		msg( "====ANIMDATA====" );
		msg( "playrate: %f", cur_balance.playback_rate );
		msg( "cycle: %f", cur_balance.cycle );
		msg( " sequence: %i", cur_balance.sequence );
		msg( "====ANIMDATA====" );
	}
}
void c_resolver::resolving( c_baseentity* entity )
{
	if ( entity == local_player ) return;
	player_data_t &player = data [ entity->get_index( ) ];
//	if ( player.antiaim_method == resolve_type::SELF_CALC )
	if ( GetKeyState( VK_SHIFT ) & 0x8000 )
		player.final_angle = abs( resolver->server_goal_feet_yaw( entity ) - entity->max_desync() );
	else if ( GetKeyState( 0x46 ) & 0x8000 )
		player.final_angle = abs( resolver->server_goal_feet_yaw( entity ) + entity->max_desync( ) );
	else
		player.final_angle = entity->get_eyeangles( ).y;

/*	else if ( player.antiaim_method == resolve_type::LAST_NETWORKED_ANGLE_CALC )
		player.final_angle = resolver->server_goal_feet_yaw( entity ) - abs( player.last_networked_lby - player.fl_goalfeetyaw_calc );
	else  if ( player.antiaim_method == resolve_type::SUBSTRACT_DELTA )
		player.final_angle = abs(resolver->server_goal_feet_yaw( entity ) - resolver->max_desync_delta( entity ));
	else
		player.final_angle = entity->get_eyeangles( ).y;*/
}
void c_resolver::apply_angles( c_baseentity* entity )
{
	auto index = entity->get_index( );

	static bool shoot [ 65 ];
	static bool choking [ 65 ];
	static float shoot_time [ 65 ];
	if ( !entity->get_anim_overlays( ) ) return;

	if ( entity->get_active_weapon( ) )
	{
		if ( shoot_time [ index ] != entity->get_active_weapon( )->last_shot_time( ) )
		{
			shoot [ index ] = true;

			shoot_time [ index ] = entity->get_active_weapon( )->last_shot_time( );
		}
		else
			shoot [ index ] = false;
	}
	else
	{
		shoot [ index ] = false;
		shoot_time [ index ] = 0.f;
	}


	if ( entity == local_player ) return;
	static float pitch;
	if ( data [ entity->get_index( ) ].last_networked_pitch != 0.f )
		pitch = data [ entity->get_index( ) ].last_networked_pitch;

	entity->get_eyeangles( ).x = pitch;
	entity->get_eyeangles( ).y = data[entity->get_index()].final_angle;
}
void c_resolver::fns( client_frame_stage_t stage )
{

	for ( int i = 1; i <= globals->max_clients; ++i ) {
		c_baseentity * entity = ( c_baseentity* )entity_list->get_client_entity( i );

		if ( !entity->is_valid_player())
			continue;

		

		if ( stage == client_frame_stage_t::frame_net_update_postdataupdate_start )
		{
			log_data( entity );
			get_type_of_antiaim( entity );
			resolving( entity );
		
		}
		
		
	}

}