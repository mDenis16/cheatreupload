
#include "thirdperson.h"
#include "../sdk/mathematics.h"
#include "../options.h"
#include "../configs.h"
#include "../config_items.h"

std::unique_ptr< c_thirdperson > thirdperson = std::make_unique< c_thirdperson >( );

void c_thirdperson::pre_framestagenotify( client_frame_stage_t stage ) {
	if ( stage != client_frame_stage_t::frame_render_start )
		return;

	if ( !local_player || !local_player->is_alive( ) )
		return;

	math->clamp( thirdperson_angle );

	if ( input->camera_in_third_person )
		local_player->set_local_viewangles( thirdperson_angle );
}

void c_thirdperson::framestagenotify( client_frame_stage_t stage ) {

	auto tp_key = c_config::get( ).esp.local_player.thirdperson_key;
	auto tp = c_config::get( ).esp.local_player.thirdperson;
	auto fp_nades = c_config::get( ).esp.local_player.thirdperson_nades;

	if ( stage != client_frame_stage_t::frame_render_start )
		return;

	static bool was_in_thirdperson = false;
	static auto thirdperson = false;
	thirdperson = tp;

	static auto pressed = false;
	static auto key = false;

	if ( GetKeyState( tp_key ) & 0x8000 && !pressed )
		pressed = true;
	else if ( !( GetKeyState( tp_key ) & 0x8000 ) && pressed ) {
		pressed = false;
		key = !key;
	}

	if ( key )
		thirdperson = false;

	if ( was_in_thirdperson != thirdperson ) {
		static auto cam_ideallag = cvar->find_var( "cam_ideallag" );
		cam_ideallag->null_callback( );
		cam_ideallag->set_value( 0.0f );

		was_in_thirdperson = !was_in_thirdperson;
	}

	vec_t angles;
	engine->get_viewangles( angles );

	math->clamp( angles );

	if ( !local_player || !local_player->is_alive( ) || !thirdperson || ( fp_nades && local_player->get_active_weapon( ) && local_player->get_active_weapon( )->is_grenade( ) ) ) {
		input->camera_in_third_person = false;
		input->camera_offset = vec_t( angles.x, angles.y, 0.0f );
		was_in_thirdperson = false;

		return;
	}

	was_in_thirdperson = true;

	input->camera_in_third_person = true;
	input->camera_offset = vec_t( angles.x, angles.y, 150.0f );
}