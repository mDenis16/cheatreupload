@@ - 0, 0 + 1, 82 @@
#pragma once
#include "../sdk/sdk.h"

enum class antiaim_pitch_t : int {
	aa_pitch_none,
	aa_pitch_down,
	aa_pitch_zero,
	aa_pitch_up
};

enum class antiaim_yaw_t : int {
	aa_yaw_none,
	aa_yaw_backwards,
	aa_yaw_side,
	aa_yaw_spin,
	aa_yaw_180z,
	aa_yaw_manual,
	aa_yaw_auto
};

enum class manual_aa_side_t : int {
	manual_aa_left,
	manual_aa_right,
	manual_aa_back
};

class c_antiaim {
public:
	struct angle_data {
		float angle;
		float thickness;
		float curtime;
		float number;
		angle_data( const float angle, const float thickness, const float curtime, const int number ) : angle( angle ), thickness( thickness ), curtime( curtime ), number( number ) {}
	};

	vec_t last_real_angle = vec_t( );
	vec_t last_fake_angle = vec_t( );
	vec_t last_lby_angle = vec_t( );
	vec_t last_fake_origin = vec_t( );
	vec_t last_origin = vec_t( );


	manual_aa_side_t manual_aa_side = manual_aa_side_t::manual_aa_left;
	float next_lby_update = 0.0f;

	vec_t m_input = vec_t {};
	vec_t m_stored_input = vec_t {};

	bool choke_next_tick;


	int m_iRotate = 0;
	int m_iRotateIteration = 0;
	bool shoud_choke = false;


	bool should_update( c_usercmd * cmd );

	int get_desync_dirrection( c_baseentity * entity_to_freestand );

	void rudy_rtx( c_usercmd* cmd, bool& send_packet, int dirrection );

	void predict_lby_update( c_usercmd * cmd, bool & sendpacket );

	void fake_lag( c_usercmd * ucmd, bool & send_packet );

	void fake_lag_on_peek( bool & send_packet, c_usercmd * cmd );


	bool allow( c_usercmd * ucmd, bool& send_packet );
	void freestanding_desync( c_usercmd * cmd, float & dirrection, c_baseentity * p_entity, bool & send_packet, float max_desync );
	bool should_choke_shot = false;
	float real_yaw = 0.f;
	bool rotate( float rotation, int direction );
	bool desync_side( int & dir );
	void createmove( c_usercmd* cmd, bool& send_packet );
	float m_next_update = 0.0f;
	bool m_lby_update_pending = false;
};

extern std::unique_ptr< c_antiaim > aa;