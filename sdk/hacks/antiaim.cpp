@@ - 0, 0 + 1, 282 @@
#include "antiaim.h"
#include "thirdperson.h"
#include "../sdk/mathematics.h"
#include "../sdk/utils.h"
#include "../options.h"
#include "autowall.h"
#include "resolver.h"
#include "../config_items.h"
#include "../configs.h"
#include "ragebot.h"
#include "..\sdk\mathematics.h"
#include <DirectXMath.h>
#include <memoryapi.h>
#include "slowwalk.h"
std::unique_ptr< c_antiaim > aa = std::make_unique< c_antiaim >( );


void setMoveChokeClampLimit( ) {

	static auto clMoveChokeClamp = ( uintptr_t ) utils->find_pattern( "engine.dll", "B8 ? ? ? ? 3B F0 0F 4F F0 89 5D FC" ) + 1;

	unsigned long protect = 0;

	VirtualProtect( ( void* ) clMoveChokeClamp, 4, PAGE_EXECUTE_READWRITE, &protect );
	*( std::uint32_t* )clMoveChokeClamp = 62;
	VirtualProtect( ( void* ) clMoveChokeClamp, 4, protect, &protect );

}

void c_antiaim::fake_lag( c_usercmd *ucmd, bool& send_packet )
{
	setMoveChokeClampLimit( );

	if ( !local_player )
		return;

	auto weapon = local_player->get_active_weapon( );

	if ( !weapon )
		return;

	send_packet = engine->get_net_channel( )->choked_packets >= c_config::get( ).antiaim.fakelag.value;

	fake_lag_on_peek( send_packet, ucmd );



}
void c_antiaim::fake_lag_on_peek( bool& send_packet, c_usercmd* cmd )
{


}


bool c_antiaim::allow( c_usercmd *ucmd, bool& send_packet ) {


	auto local = local_player;
	if ( !local && !local->is_alive( ) )
		return false;

	const auto move_type = local->get_movetype( );
	if ( move_type == ( int ) movetype_t::movetype_ladder || move_type == ( int ) movetype_t::movetype_noclip )
		return false;

	if ( ucmd->buttons & ( int ) buttons_t::in_use )
		return false;

	auto weapon = local->get_active_weapon( );
	if ( !weapon )
		return false;

	if ( weapon->is_knife( ) ) {
		float next_secondary_attack = weapon->get_next_secondary_attack( ) - globals->curtime;
		float next_primary_attack = weapon->get_next_primary_attack( ) - globals->curtime;

		if ( ucmd->buttons & ( int ) buttons_t::in_attack && next_primary_attack <= 0.f || ucmd->buttons & ( int ) buttons_t::in_attack2 && next_secondary_attack <= 0.f )
			return false;
	}

	if ( ucmd->buttons & ( int ) buttons_t::in_attack && local->can_shoot( ) ) {
		choke_next_tick = true;
		send_packet = true;
		return false;
	}

	if ( local->get_active_weapon( )->is_in_grenade_throw( ucmd ) )
		return false;

	return true;
}

void c_antiaim::freestanding_desync( c_usercmd *cmd, float& dirrection, c_baseentity* p_entity, bool& send_packet, float max_desync ) {
	std::vector<angle_data> points;
	int side = 0;

	if ( !cmd )
		return;

	const auto local = local_player;

	if ( !local )
		return;

	if ( !local->is_alive( ) )
		return;

	const auto local_position = local->get_eye_pos( );



	if ( !p_entity ) return;

	const auto view = math->calc_angle( local_position, p_entity->get_eye_pos( ) );



	auto enemy_eyes = p_entity->get_eye_pos( );

	static matrix3x4_t local_player_cached_bones [ 128 ];

	if ( send_packet )
	{
		auto count = *( std::uint32_t* ) ( ( std::uint32_t ) local_player->get_renderable( ) + 0x2918 );
		std::memcpy( local_player_cached_bones, *( void** ) ( ( std::uint32_t ) local_player->get_renderable( ) + 0x290C ), sizeof( matrix3x4_t ) * count );
	}

	const auto head = ragebot->get_point( local_player_cached_bones, local_player, 0 );

	float radius = 100.f;

	for ( float a = 0; a < ( D3DX_PI * 2.0 ); a += D3DX_PI * 2.0 / 16 )
	{
		vec_t location( radius * cos( a ) + head.x, radius * sin( a ) + head.y, head.z );

		ray_t ray;
		trace_t trace;
		ray.init( head, location );
		c_trace_filter traceFilter;
		traceFilter.skip = local_player;

		engine_trace->trace_ray( ray, 0x4600400B, &traceFilter, &trace );
		debug_overlay->add_line_overlay( head, trace.end_pos, 255, 0, 0, 0, 0.1f );
		float tickness = autowall->get_thickness( enemy_eyes, trace.end_pos );
		debug_overlay->add_text_overlay( trace.end_pos, 0.1f, std::to_string( tickness ).c_str( ) );
		points.push_back( angle_data( math->clamp_yaw( math->rad_to_deg( a ) ), tickness, globals->curtime, a ) );

	}


	float l1 = 0.f; float l2 = 0.f; float lowest_angle, highest_angle = 0.f;
	for ( auto &i : points )
	{
		if ( i.thickness > 50.f && i.thickness != 0.f )
		{
			if ( i.thickness > l1 )
			{
				l1 = i.thickness;
				lowest_angle = i.angle;
			}
			if ( 30.f < i.thickness < l2 )
			{
				l2 = i.thickness;
				highest_angle = i.angle;
			}
		}

	}

	if ( desync_side( side ) )
	{
		if ( send_packet )
		{
			if ( side == 1 )
				dirrection = lowest_angle;
			else if ( side == 2 )
				dirrection = highest_angle;
		}
		else
		{
			if ( side == 1 )
			{
				if ( m_lby_update_pending )
					dirrection = lowest_angle - max_desync - 120.f;
				else
					dirrection = lowest_angle - max_desync;
			}
			else if ( side == 2 )
			{
				if ( m_lby_update_pending )
					dirrection = highest_angle + max_desync + 120.f;
				else
					dirrection = highest_angle + max_desync;
			}
		}
	}
}


bool c_antiaim::desync_side( int &dir )
{

	return true;
}


void c_antiaim::predict_lby_update( c_usercmd* cmd, bool& sendpacket ) {


}
void c_antiaim::createmove( c_usercmd* cmd, bool& send_packet )
{


	auto desync = c_config::get( ).antiaim.desync_type;


	if ( !c_config::get( ).antiaim.enable )
		return;

	auto weapon = local_player->get_active_weapon( );

	if ( !weapon )
		return;


	if ( allow( cmd, send_packet ) ) {
		auto animstate = local_player->get_anim_state( );

		if ( !animstate )
			return;

		m_input.x = 89.0f;
		float dirrection = float( );
		c_baseentity * p_entity = ( c_baseentity* ) entity_list->get_client_entity( closest_target );


		int desync_dirrection = get_desync_dirrection( p_entity );


		float max_desync = local_player->max_desync( nullptr, true );



		float server_goal_feet = resolver->server_goal_feet_yaw( local_player );


		freestanding_desync( cmd, dirrection, p_entity, send_packet, max_desync );


		switch ( c_config::get( ).antiaim.yaw )
		{
		case 1:


			break;
		case 2:


			break;
		case 3:
			if ( !send_packet )
			{
				if ( m_lby_update_pending )
					m_input.y = 180.f + max_desync + 120.f;
				else
					m_input.y = 180.f + max_desync;
			}
			else
				m_input.y = 180.f;
			break;
		default:
			break;
		}

		math->clamp( m_input );
		cmd->viewangles = m_input;


	}
}