@@ - 0, 0 + 1, 34 @@
#pragma once
#include "../sdk/sdk.h"
#include "../sdk/singleton.h"

class c_animfix : public singleton< c_animfix > {
public:

	bool           shoot [ 65 ];
	bool           hit [ 65 ];
	void local_player_fix_cm( );
	struct player_anims
	{
		animationlayer m_layers [ 15 ];
		std::array< float, 24 > pose_params;
		c_csgoplayeranimstate* animstate;


	};

	c_csgoplayeranimstate* animstate;
	std::array< bool, 64 > on_ground { false };
	std::array< bool, 64 > last_on_ground { false };

	std::array< vec_t, 64 > speed { vec_t( ) };
	std::array< vec_t, 64 > last_speed { vec_t( ) };

	void update_animations( c_baseentity* entity );
	void re_work( client_frame_stage_t stage );
	void fix_local_player_animations( );
	player_anims player_data [ 64 ];

};

extern std::unique_ptr< c_animfix > animfix;