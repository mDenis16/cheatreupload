
#include "autowall.h"
#include "ragebot.h"
#include "backtrack.h"
#include "antiaim.h"
#include "../sdk/sdk.h"
#include "../sdk/mathematics.h"
#include "../sdk/utils.h"
#include "../options.h"
#include "animfix.h"
#include "esp.h"
#include "entity_list.h"
#include <limits>
#include "..\configs.h"
#include "..\config_items.h"
#include <algorithm> 
std::unique_ptr< c_ragebot > ragebot = std::make_unique< c_ragebot >( );

vec_t c_ragebot::get_point( matrix3x4_t* matrix, c_baseentity* entity, const int &hitbox ) {


	const auto* model = entity->get_model( );

	if ( !model )
		return vec_t( );

	auto studio_model = model_info->get_studio_model( model );
	if ( !studio_model )
		vec_t( );

	mstudiohitboxset_t *s = studio_model->hitbox_set( 0 );
	if ( !s )
		return vec_t( );

	auto* hb = s->hitbox( hitbox );
	if ( !hb )
		return  vec_t( );

	auto& mat = matrix [ hb->bone ];

	auto mod = hb->radius != -1.0f?hb->radius:0.0f;

	vec_t mins = math->vector_transform( hb->bbmin - vec_t( mod, mod, mod ), mat );
	vec_t maxs = math->vector_transform( hb->bbmax + vec_t( mod, mod, mod ), mat );
	auto center = ( mins + maxs ) * 0.5f;

	return center;
}

float c_ragebot::get_config_pointscale( const int &hit_box )
{
	switch ( hit_box ) {
	case 0:
		return c_config::get( ).ragebot.pointscale [ 0 ];
		break;
	case 1:
		return c_config::get( ).ragebot.pointscale [ 1 ];
		break;
	case 2:
		return c_config::get( ).ragebot.pointscale [ 2 ];
		break;
	case 3:
		return c_config::get( ).ragebot.pointscale [ 3 ];
		break;
	case 4:
		return c_config::get( ).ragebot.pointscale [ 4 ];
		break;
	case 5:
		return c_config::get( ).ragebot.pointscale [ 5 ];
		break;
	default:
		return 0.f;
		break;
	}
}
bool c_ragebot::multi_point( matrix3x4_t* matrix, c_baseentity* entity, const int &hit_box, std::vector<vec_t>& points ) {

	auto point_scale = get_config_pointscale( hit_box ); point_scale /= 100.f;

	studiohdr_t* studio_model = model_info->get_studio_model( entity->get_model( ) );
	mstudiohitboxset_t* set = studio_model->hitbox_set( 0 );
	mstudiobbox_t *hitbox = set->hitbox( hit_box );

	vec_t max = math->vector_transform( hitbox->bbmax, matrix [ hitbox->bone ] );
	vec_t min = math->vector_transform( hitbox->bbmin, matrix [ hitbox->bone ] );

	auto center = ( min + max ) * 0.5f;

	vec_t current_angles = math->calc_angle( center, local_player->eye_pos( ) );

	vec_t forward = math->angle_vectors( current_angles );
	vec_t right = forward.cross_product( vec_t( 0, 0, 1 ) );
	vec_t left = vec_t( -right.x, -right.y, right.z );

	vec_t top = vec_t( 0, 0, 1 );
	vec_t bot = vec_t( 0, 0, -1 );

	if ( c_config::get( ).ragebot.save_fps && last_target_global->get_index( ) != entity->get_index( ) )
	{
		points.push_back( center );
		return true;
	}

	switch ( hit_box ) {
	case ( int ) hitbox_t::head:
	{


		points.push_back( center );
		if ( !utils->is_visible( center, false ) || point_scale < 0.1f ) {
			for ( int i = 0; i < 4; i++ )
				points.push_back( center );

			points [ 1 ] += top * ( hitbox->radius *  point_scale );
			points [ 2 ] += right * ( hitbox->radius *  point_scale );
			points [ 3 ] += left * ( hitbox->radius *  point_scale );


		}
		break;
	}
	case ( int ) hitbox_t::neck:
	{	points.push_back( center );
	if ( !utils->is_visible( center, false ) || point_scale < 0.1f ) {

		for ( int i = 0; i < 4; i++ )
			points.push_back( center );

		points [ 1 ] += top * ( hitbox->radius *  point_scale );
		points [ 2 ] += right * ( hitbox->radius * point_scale );
		points [ 3 ] += left * ( hitbox->radius *  point_scale );
	}

	break;
	}
	case ( int ) hitbox_t::upper_chest: {
		points.push_back( center );
		if ( !utils->is_visible( center, false ) || point_scale < 0.1f ) {
			for ( int i = 0; i < 3; i++ )
				points.push_back( center );

			points [ 1 ] += right * ( hitbox->radius *  point_scale );
			points [ 2 ] += left * ( hitbox->radius *  point_scale );
		}

		break;
	}
	case ( int ) hitbox_t::chest:
	{

		points.push_back( center );
		if ( !utils->is_visible( center, false ) || point_scale < 0.1f ) {
			for ( int i = 0; i < 3; i++ )
				points.push_back( center );

			points [ 1 ] += right * ( hitbox->radius *  point_scale );
			points [ 2 ] += left * ( hitbox->radius *  point_scale );
		}

		break;
	}
	case ( int ) hitbox_t::pelvis: {
		points.push_back( center );
		if ( !utils->is_visible( center, false ) || point_scale < 0.1f ) {
			for ( int i = 0; i < 3; i++ )
				points.push_back( center );

			points [ 1 ] += right * ( hitbox->radius *  point_scale );
			points [ 2 ] += left * ( hitbox->radius *  point_scale );
		}

		break;
	}
	case ( int ) hitbox_t::right_foot:
	case ( int ) hitbox_t::left_foot: {
		points.push_back( center );
		if ( !utils->is_visible( center, false ) ) {
			for ( int i = 0; i < 3; i++ )
				points.push_back( center );

			points [ 1 ] += right * ( hitbox->radius *  point_scale );
			points [ 2 ] += left * ( hitbox->radius * point_scale );
		}

		break;
	}
	default: {
		points.push_back( center );
		break;
	}
	}
	return true;
}
int c_ragebot::prioritize_hitbox( )
{
	int best_hitbox = 0;
	switch ( c_config::get( ).ragebot.prioritize_hitbox )
	{
	case  0:
		return ( int ) hitbox_t::head;
		break;
	case  1:
		return ( int ) hitbox_t::neck;
		break;
	case  2:
		return ( int ) hitbox_t::upper_chest;
		break;
	case  3:
		return ( int ) hitbox_t::chest;
		break;
	case  4:
		return ( int ) hitbox_t::pelvis;
		break;
	case  5:
		return ( int ) hitbox_t::left_foot;
		break;
	default:
		return 0;
		break;
	}
}
std::vector< int > c_ragebot::hitscan_list( c_baseentity* entity )
{
	auto best_hitbox = prioritize_hitbox( );
	std::vector<int> hitscan;
	hitscan.push_back( best_hitbox );

	hitscan.push_back( ( int ) hitbox_t::head );

	hitscan.push_back( ( int ) hitbox_t::neck );

	hitscan.push_back( ( int ) hitbox_t::upper_chest );

	hitscan.push_back( ( int ) hitbox_t::chest );

	hitscan.push_back( ( int ) hitbox_t::pelvis );

	{
		hitscan.push_back( ( int ) hitbox_t::left_foot );
		hitscan.push_back( ( int ) hitbox_t::right_foot );
		hitscan.push_back( ( int ) hitbox_t::right_thigh );
		hitscan.push_back( ( int ) hitbox_t::left_thigh );
		hitscan.push_back( ( int ) hitbox_t::left_calf );
		hitscan.push_back( ( int ) hitbox_t::right_calf );
	}

	{
		hitscan.push_back( ( int ) hitbox_t::right_upper_arm );
		hitscan.push_back( ( int ) hitbox_t::left_upper_arm );
		hitscan.push_back( ( int ) hitbox_t::left_forearm );
		hitscan.push_back( ( int ) hitbox_t::right_forearm );
		hitscan.push_back( ( int ) hitbox_t::left_hand );
		hitscan.push_back( ( int ) hitbox_t::right_hand );
	}
	/*
	if ( c_config::get( ).ragebot.hitscan [ 0 ] && best_hitbox != ( int ) hitbox_t::head )
		hitscan.push_back( ( int ) hitbox_t::head );
	else if ( c_config::get( ).ragebot.hitscan [ 1 ] && best_hitbox != ( int ) hitbox_t::neck )
		hitscan.push_back( ( int ) hitbox_t::neck );
	else if ( c_config::get( ).ragebot.hitscan [ 2 ] && best_hitbox != ( int ) hitbox_t::upper_chest )
		hitscan.push_back( ( int ) hitbox_t::upper_chest  );
	else if ( c_config::get( ).ragebot.hitscan [ 3 ] && best_hitbox != ( int ) hitbox_t::chest )
		hitscan.push_back( ( int ) hitbox_t::chest );
	else if ( c_config::get( ).ragebot.hitscan [ 4 ] && best_hitbox != ( int ) hitbox_t::pelvis )
		hitscan.push_back( ( int ) hitbox_t::pelvis );
	else if ( c_config::get( ).ragebot.hitscan [ 5 ] )
	{
		hitscan.push_back( ( int ) hitbox_t::left_foot );
		hitscan.push_back( ( int ) hitbox_t::right_foot );
		hitscan.push_back( ( int ) hitbox_t::right_thigh );
		hitscan.push_back( ( int ) hitbox_t::left_thigh );
		hitscan.push_back( ( int ) hitbox_t::left_calf );
		hitscan.push_back( ( int ) hitbox_t::right_calf );
	}
	else if ( c_config::get( ).ragebot.hitscan [ 6 ] && !( entity->is_moving( ) && c_config::get( ).ragebot.ignore_limbs_moving ) )
	{
		hitscan.push_back( ( int ) hitbox_t::right_upper_arm );
		hitscan.push_back( ( int ) hitbox_t::left_upper_arm );
		hitscan.push_back( ( int ) hitbox_t::left_forearm );
		hitscan.push_back( ( int ) hitbox_t::right_forearm );
		hitscan.push_back( ( int ) hitbox_t::left_hand );
		hitscan.push_back( ( int ) hitbox_t::right_hand );
	}
	*/


	return hitscan;
}
std::pair< vec_t, float > c_ragebot::get_best_point_backtrack( c_baseentity* entity, float min_dmg ) {
	std::pair< vec_t, float > best_point;

	auto best_tick = find_best_tick( entity );
	best_tick_global [ entity->get_index( ) ] = best_tick.first;
	if ( backtrack->is_tick_valid( best_tick.first ) )
	{
		if ( best_tick_global [ entity->get_index( ) ].shoot && c_config::get( ).ragebot.prioritize_pitch_flip )
		{
			auto point = get_point( best_tick_global [ entity->get_index( ) ].cached_bones, entity, 0 );
			backtrack->restore_record( entity, best_tick.first );
			float cur_damage = autowall->get_damage_vec( point );
			backtrack->restore_player( entity );

			if ( c_config::get( ).ragebot.fatal_pitch_flip && ( cur_damage >= ( entity->get_health( ) - 10 ) ) ) {
				best_point.first = point;
				best_point.second = cur_damage;
			}
			else if ( cur_damage >= ( min_dmg + 30 ) ) {
				best_point.first = point;
				best_point.second = cur_damage;
			}
		}
		else
		{

			for ( int i = 0; ( i ) < ( calculed_hitscan [ entity->get_index( ) ].size( ) ); i++ )
			{


				auto point = get_point( best_tick_global [ entity->get_index( ) ].cached_bones, entity, calculed_hitscan [ entity->get_index( ) ] [ i ] );

				backtrack->restore_record( entity, best_tick.first );
				float cur_damage = autowall->get_damage_vec( point );
				backtrack->restore_player( entity );

				if ( ( cur_damage >= best_point.second ) && ( cur_damage >= min_dmg ) ) {
					best_point.first = point;
					best_point.second = cur_damage;


				}
				else if ( cur_damage >= entity->get_health( ) ) {
					best_point.first = point;
					best_point.second = cur_damage;
				}


			}
		}
	}
	return best_point;
}
std::pair< vec_t, std::pair< bool, float > > c_ragebot::get_best_player( c_baseentity* entity, float min_dmg ) {

	std::pair< vec_t, std::pair< bool, float > > best_point;

	auto index = entity->get_index( );



	auto best_pos = get_best_point( cached_bones [ index ], entity, min_dmg );
	auto backtrack = get_best_point_backtrack( entity, min_dmg );

	float velocity = entity->get_velocity( ).length_2d( );
	if ( best_tick_global [ index ].shoot ) velocity = 300.f;

	if ( backtrack.second - 60 > best_pos.second && velocity > 35.f )
	{
		best_point.first = backtrack.first;
		best_point.second.second = backtrack.second;
		best_point.second.first = true;
	}
	else
	{
		best_point.first = best_pos.first;
		best_point.second.second = best_pos.second;
		best_point.second.first = false;
	}
	return best_point;
}

#define  hitbox (int)hitbox_t
#define  dmg_calc( dmg )		( (damage [ p_hitbox ] > dmg) - 5  && dmg >= min_dmg)
#define  dmg_comp( dmg, dmg2 )		( dmg > dmg2 )
#define dmg_health(dmg) ( ((dmg >= entity->get_health()) + 5)
#define more_than_body(hitboxx) ( damage [ hitboxx ] > damage [hitbox::thorax ] || (damage [ hitboxx ] > damage [hitbox::pelvis ]  || damage [ hitboxx ] > damage [hitbox::chest ] || damage [ hitboxx ] > damage [ hitbox::upper_chest ] || damage [ hitboxx ] > damage [ hitbox::body ])  )

std::string c_ragebot::hitbox_to_string( int i )
{
	std::string to( "null" );
	switch ( i )
	{
	case ( int ) hitbox_t::head: {
		to = std::string( "head" );
		break;
	}
	case ( int ) hitbox_t::neck: {
		to = std::string( "neck" );
		break;
	}
	case ( int ) hitbox_t::left_upper_arm: {
		to = std::string( "left uppper arm" );
		break;
	}
	case ( int ) hitbox_t::right_upper_arm: {
		to = std::string( "right upper arm" );
		break;
	}

	case ( int ) hitbox_t::upper_chest: {
		to = std::string( "upper chest" );
		break;
	}
	case ( int ) hitbox_t::chest: {
		to = std::string( "chest" );
		break;
	}
	case ( int ) hitbox_t::left_forearm: {
		to = std::string( "left forearm" );
		break;
	}
	case ( int ) hitbox_t::right_forearm: {
		to = std::string( "right forearm" );
		break;
	}
	case ( int ) hitbox_t::pelvis: {
		to = std::string( "pelvis" );
		break;
	}
	case ( int ) hitbox_t::left_calf: {
		to = std::string( "left calf" );
		break;
	}
	case ( int ) hitbox_t::right_calf: {
		to = std::string( "right calf" );
		break;
	}
	case ( int ) hitbox_t::left_foot: {
		to = std::string( "left foot" );
		break;
	}
	case ( int ) hitbox_t::right_foot: {
		to = std::string( "right foot" );
		break;
	}
	default:
		return std::string( "null" );
		break;
	}
	return to;
}


std::pair< vec_t, float > c_ragebot::get_best_point( matrix3x4_t* matrix, c_baseentity* entity, float min_dmg ) {
	std::pair< vec_t, float > best_point;
	/*option prioritize_hitbox = options->get_int( "prioritize hitbox" );
	option is_fatal = options->get_bool( "baim if fatal" );
	option save_fps = options->get_bool( "save fps" );

	int  p_hitbox = 0;
	switch ( prioritize_hitbox )
	{
	case 0:
		p_hitbox = ( int ) hitbox::head;
	case 1:
		p_hitbox = (int) hitbox::neck;
	case 2:
		p_hitbox = ( int ) hitbox::body;
	case 3:
		p_hitbox = ( int ) hitbox::pelvis;
	}



	if ( !bounding( entity, entity->get_collision( )->vec_mins( ), entity->get_collision( )->vec_maxs( ), entity->get_origin( ), cached_bones [ entity->get_index( ) ] ) && save_fps )
		return best_point;
		*/

		/*
			bool valid_player = false;

			std::vector< int > hitboxes = { hitbox::head,
					hitbox::pelvis,
					hitbox::upper_chest,
					hitbox::chest,
					hitbox::neck,
					hitbox::left_forearm,
					hitbox::right_forearm,
					hitbox::right_hand,
					hitbox::left_thigh,
					hitbox::right_thigh,
					hitbox::left_calf,
					hitbox::right_calf,
					hitbox::left_foot,
					hitbox::right_foot

			};




			float damage [ 24 ];

			if ( GetKeyState( 0x46 ) & 0x8000 )
			{
				hitboxes = { hitbox::pelvis };
			}
			std::vector< vec_t > points;

				for ( int i = 0; i < hitboxes.size( ); i++ )
				{
					std::vector< vec_t > points;


					if ( multi_point( matrix, entity, hitboxes [ i ], points ) )
					{
						for ( auto point : points ) {
							damage [ i ] = 0.f;

							damage[i] = autowall->get_damage_vec( point );
							if (!damage [ i ])
								continue;
							//msg( "damage[%s] %f \n", hitbox_to_string( i ).c_str( ), damage [ i ] );
						//	if ( damage [ i ] < min_dmg ) continue;

							p_hitbox = 0;

							if ( ( i == p_hitbox ) && damage [ p_hitbox ] > min_dmg && more_than_body(p_hitbox)) // go for head
							{
								best_point.first = point;
								best_point.second = damage [ p_hitbox ];
							}
							else if ( i == hitbox::pelvis || i == hitbox::body || i == hitbox::chest || i == hitbox::thorax || i == hitbox::upper_chest )
							{
								best_point.first = point;
								best_point.second = max_val( damage [ hitbox::pelvis ], damage [ hitbox::body ], damage [ hitbox::chest ], damage [ hitbox::thorax ], damage [ hitbox::upper_chest ] );
							}
							else if ( ( i == hitbox::pelvis ) &&  damage [ hitbox::pelvis ] >= ( entity->get_health( ) + 5 ) && is_fatal )
							{
								best_point.first = point;
								best_point.second = damage [ hitbox::pelvis ];
							}
							else if ( damage [ i ] > best_point.second &&  damage [ i ] > min_dmg )
							{
								best_point.first = point;
								best_point.second = damage [ i ];

							}

						}

					}
				}
				*/
	matrix3x4_t cache [ 128 ];
	auto index = entity->get_index( );


	for ( int i = 0; i < calculed_hitscan [ index ].size( ); i++ ) {
		std::vector< vec_t > points;

		if ( multi_point( matrix, entity, calculed_hitscan [ index ] [ i ], points ) )
		{
			for ( auto &point : points )
			{

				auto cur_damage = autowall->get_damage_vec( point );

				if ( cur_damage > min_dmg && cur_damage > best_point.second ) {
					best_point.first = point;
					best_point.second = cur_damage;

				}
				else if ( cur_damage >= entity->get_health( ) ) {
					best_point.first = point;
					best_point.second = cur_damage;
					return best_point;
				}
			}
		}
	}

	return best_point;
}

bool c_ragebot::bounding( c_baseentity * entity, vec_t bb_min, vec_t bb_max, vec_t origin, matrix3x4_t matrix [ 128 ] )
{
	const auto* model = entity->get_model( );
	studiohdr_t *studio_model = nullptr;
	if ( model )
		studio_model = model_info->get_studio_model( model );

	auto local = local_player->eye_pos( );
	auto body = get_point( matrix, entity, ( int ) hitbox_t::pelvis );
	if ( 1.f / globals->absolute_frametime < 70 && entity->get_index( ) != closest_target && c_config::get( ).ragebot.save_fps )
		return false;

	if ( c_config::get( ).ragebot.ignore_high_tickness && autowall->get_thickness( local, ( vec_t& ) body ) > 300.f )
		return false;

	const auto bbmin = bb_min + origin;
	const auto bbmax = bb_max + origin;

	vec_t points [ 7 ];

	points [ 0 ] = get_point( matrix, entity, 0 );
	points [ 1 ] = ( bbmin + bbmax ) * 0.5f;
	points [ 2 ] = vec_t( ( bbmax.x + bbmin.x ) * 0.5f, ( bbmax.y + bbmin.y ) * 0.5f, bbmin.z );

	points [ 3 ] = bbmax;
	points [ 4 ] = vec_t( bbmax.x, bbmin.y, bbmax.z );
	points [ 5 ] = vec_t( bbmin.x, bbmin.y, bbmax.z );
	points [ 6 ] = vec_t( bbmin.x, bbmax.y, bbmax.z );

	for ( const auto& point : points )
		if ( autowall->get_damage_vec( point ) > 0.f )
			return true;


	return false;

}
std::pair< c_backtrack::backtrack_records_t, bool > c_ragebot::find_best_tick( c_baseentity* entity )
{
	std::pair< c_backtrack::backtrack_records_t, bool > best_tick;// MAX );
	best_tick.second = 0.f;


	for ( int i = backtrack->backtrack_records [ entity->get_index( ) ].size( ) - 1; i >= 0; i-- ) {
		auto& record = backtrack->backtrack_records [ entity->get_index( ) ] [ i ];




		if ( record.shoot && c_config::get( ).ragebot.prioritize_pitch_flip )
		{
			best_tick.first = record;
			return best_tick;

		}

		switch ( c_config::get( ).ragebot.prioritize_record )
		{
		case 0:
			best_tick.first = backtrack->backtrack_records [ entity->get_index( ) ].at( 0 );
			break;
		case 1:
		{
			if ( backtrack->backtrack_records [ entity->get_index( ) ].size( ) > 4 )
				best_tick.first = backtrack->backtrack_records [ entity->get_index( ) ].at( backtrack->backtrack_records [ entity->get_index( ) ].size( ) - 3 );
			break;
		}
		case 2:
		{
			float velocity = 0.f;
			if ( record.velocity.length_2d( ) > velocity ) {
				best_tick.first = record;
				velocity = record.velocity.length_2d( );
			}
			break;
		}
		case 3:
		{
			backtrack->restore_record( entity, record );

			auto cur_damage = autowall->get_damage_vec( record.cached_bones [ 8 ].get_origin( ) );

			if ( cur_damage > best_tick.second ) {
				best_tick.first = record;
				best_tick.second = cur_damage;
			}

			if ( best_tick.second >= entity->get_health( ) ) {
				return best_tick;
			}

			cur_damage = autowall->get_damage_vec( record.cached_bones [ 0 ].get_origin( ) );

			if ( cur_damage > best_tick.second ) {
				best_tick.first = record;
				best_tick.second = cur_damage;
			}
		}
		break;
		default:
			break;
		}

	}

	return best_tick;
}


bool c_ragebot::hit_chance( vec_t point, vec_t angle, c_baseentity *ent, float hitchance ) {


	if ( !local_player || !local_weapon ) return false;
	int traces_hit = 0;
	int needed_hits = static_cast< int >( 150.f * ( hitchance / 100.f ) );
	vec_t forward, right, up;
	const vec_t eye_position = local_player->eye_pos( );
	math->angle_vectors( angle, &forward, &right, &up );

	c_baseweapon *weapon = local_player->get_active_weapon( );
	if ( !weapon )
		return false;

	weapon->update_accuracy_penalty( );
	float weapon_spread = weapon->get_spread( );
	float weapon_cone = weapon->get_innacuracy( );

	const auto get_bullet_location = [ & ] ( int seed ) {

		utils->random_seed( seed );

		float a = utils->random_float( 0.f, 1.f );
		float b = utils->random_float( 0.f, 2.f * pi );
		float c = utils->random_float( 0.f, 1.f );
		float d = utils->random_float( 0.f, 2.f * pi );

		const float generated_spread = a * weapon_spread;
		const float generated_cone = c * weapon_cone;

		const vec_t spread = vec_t(
			std::cos( b ) * generated_spread + std::cos( d ) * generated_cone,
			std::sin( b ) * generated_spread + std::sin( d ) * generated_cone,
			0.f
		);

		return vec_t( forward + right * -spread.x + up * -spread.y ).normalized( );
	};

	for ( int i = 1; i <= 1500; i++ ) {
		vec_t spread_angle;
		vec_t bullet_end;

		spread_angle = math->vector_angles( get_bullet_location( i ) );
		bullet_end = math->angle_vectors( angle - ( spread_angle - angle ) );

		trace_t trace;
		ray_t ray;
		ray.init( eye_position, eye_position + bullet_end * weapon->get_cs_weapon_data( )->range );

		engine_trace->clip_ray_to_entity( ray, mask_shot | contents_grate, ent, &trace );


		if ( trace.hit_entity == ent )
			++traces_hit;



		if ( static_cast< int >( ( static_cast< float >( traces_hit ) / 150.f ) * 100.f ) >= hitchance )
			return true;

		if ( ( 150 - i + traces_hit ) < needed_hits )
			return false;
	}

	return false;
}
void c_ragebot::createmove( c_usercmd* cmd, bool& send_packet ) {



	static bool shot = false;
	static c_baseentity* last_target = nullptr;

	if ( !c_config::get( ).ragebot.enable )
		return;

	std::pair< std::pair< vec_t, float >, std::pair< c_baseentity*, bool > > best_target;

	entity_listener->get_entities_fns( );

	for ( auto ent : entity_listener->player_data )
	{
		auto entity = ent.entity;
		auto i = ent.index;

		calculed_hitscan [ i ] = hitscan_list( entity );

		auto target_data = get_best_player( entity, c_config::get( ).ragebot.min_dmg );

		if ( !target_data.first.is_zero( ) && best_target.first.second <= target_data.second.second )
		{
			best_target.first.second = target_data.second.second; //dmg
			best_target.first.first = target_data.first; // point
			best_target.second.first = entity; //entity
			best_target.second.second = target_data.second.first; //backtrack

		}
	}
	int backup = c_config::get( ).antiaim.fakelag.value;
	if ( best_target.second.first && best_target.first.second != float {}  && local_player->can_shoot( ) ) {

		auto ent = best_target.second.first;
		auto& bt = best_tick_global [ ent->get_index( ) ];
		auto point = best_target.first.first;
		auto dmg = best_target.first.second;
		last_target = ent;

		auto ang = math->calc_angle( local_player->eye_pos( ), point );
		ang -= local_player->get_aimpunch_angle( ) * 2.f;
		math->clamp( ang );

		auto simtime = best_target.second.second?bt.simtime:ent->get_simulation_time( ); simtime += backtrack->interpolation_time( );

		if ( best_target.second.second ) backtrack->restore_record( ent, bt );
		if ( hit_chance( point, ang, ent, c_config::get( ).ragebot.hitchance ) ) {
			c_config::get( ).antiaim.fakelag.value = 0;
			if ( c_config::get( ).ragebot.silent_aim )
				cmd->viewangles = ang;
			else
				engine->set_viewangles( ang );

			if ( local_player->can_shoot( ) && c_config::get( ).ragebot.auto_fire )
				cmd->buttons |= ( int ) buttons_t::in_attack;
			else if ( !local_player->can_shoot( ) && c_config::get( ).ragebot.auto_fire )
				cmd->buttons &= ~( int ) buttons_t::in_attack;

			if ( cmd->buttons & ( int ) buttons_t::in_attack )
			{
				cmd->tick_count = time_to_ticks( simtime );
				esp->capsule_overlay( ent, 1.7f, best_target.second.second?bt.cached_bones:cached_bones [ ent->get_index( ) ] );
			}
		}
		if ( best_target.second.second ) backtrack->restore_player( ent );
	}

}