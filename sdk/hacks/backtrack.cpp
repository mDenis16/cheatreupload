@@ - 0, 0 + 1, 169 @@
#include "backtrack.h"
#include "ragebot.h"
#include "..\sdk\utils.h"
#include "animfix.h"
std::unique_ptr< c_backtrack > backtrack = std::make_unique< c_backtrack >( );

float c_backtrack::interpolation_time( ) {

	static const auto                 cl_updaterate = cvar->find_var( "cl_updaterate" );
	static const auto              sv_minupdaterate = cvar->find_var( "sv_minupdaterate" );
	static const auto              sv_maxupdaterate = cvar->find_var( "sv_maxupdaterate" );
	static const auto               cl_interp_ratio = cvar->find_var( "cl_interp_ratio" );
	static const auto                     cl_interp = cvar->find_var( "cl_interp" );
	static const auto    sv_client_min_interp_ratio = cvar->find_var( "sv_client_min_interp_ratio" );
	static const auto    sv_client_max_interp_ratio = cvar->find_var( "sv_client_max_interp_ratio" );

	const auto update_rate = utils->clamp( cl_updaterate->get_float( ),
		sv_minupdaterate->get_float( ),
		sv_maxupdaterate->get_float( ) );

	const auto base_interp_ratio = ( cl_interp_ratio->get_float( ) == 0.0f )?1.0f:cl_interp_ratio->get_float( );

	const auto interp_ratio = utils->clamp( base_interp_ratio,
		sv_client_min_interp_ratio->get_float( ),
		sv_client_max_interp_ratio->get_float( ) );

	const auto interp_latency = ( interp_ratio / update_rate );

	return std::max<float>( cl_interp->get_float( ), interp_latency );
}
bool c_backtrack::is_tick_valid( backtrack_records_t record )
{
	const auto nci = engine->get_net_channel_info( );

	if ( !nci )
		return false;

	constexpr auto max_unlag = 1.0f;
	constexpr auto max_delta = 0.2f;

	const auto   correct = utils->clamp( ( nci->get_latency( 0 ) + interpolation_time( ) ), 0.0f, max_unlag );
	const auto   delta = correct - ( globals->curtime - record.simtime + interpolation_time( ) );

	return fabsf( delta ) < max_delta;
}

void c_backtrack::manage_records_cmd( )
{

	for ( int i = 1; i <= globals->max_clients; i++ ) {
		c_baseentity * entity = ( c_baseentity* ) entity_list->get_client_entity( i );

		if ( !entity->is_valid_player( ) )
			continue;

		c_backtrack::backtrack_records_t record;

		animfix->update_animations( entity );
		backtrack->backup_player( entity );

		static float sim_time;
		if ( sim_time != entity->get_simulation_time( ) )
		{
			backtrack->recieve_record( entity, record );
			backtrack->backtrack_records [ i ].push_back( record );
			sim_time = entity->get_simulation_time( );
		}
	}

	for ( int player = 1; player <= globals->max_clients; player++ ) {
		c_baseentity * entity = ( c_baseentity* ) entity_list->get_client_entity( player );

		if ( !entity )
			continue;

		if ( !backtrack->backtrack_records [ player ].size( ) > 0 )
			continue;

		for ( int t = 0; ( t ) < ( backtrack->backtrack_records [ player ].size( ) ); t++ )
			if ( !backtrack->is_tick_valid( backtrack->backtrack_records [ player ].at( t ) ) )
				backtrack->backtrack_records [ player ].erase( backtrack->backtrack_records [ player ].begin( ) + t );
			else if ( entity->get_dormant( ) || !entity->is_alive( ) || !( entity->get_health( ) > 0 ) )
				backtrack->backtrack_records [ player ].clear( );

	}

}
void c_backtrack::backup_player( c_baseentity* entity )
{
	auto& record = backup_data [ entity->get_index( ) ];

	record.obbmin = entity->get_collision( )->vec_mins( );
	record.obbmax = entity->get_collision( )->vec_maxs( );

	record.absangles = entity->get_abs_angles( );
	record.absorigin = entity->get_abs_origin( );

	auto count = *( std::uint32_t* ) ( ( std::uint32_t ) entity->get_renderable( ) + 0x2918 );
	std::memcpy( &record.cached_bones, *( void** ) ( ( std::uint32_t ) entity->get_renderable( ) + 0x290C ), sizeof( matrix3x4_t ) * count );

	std::memcpy( &ragebot->cached_bones [ entity->get_index( ) ], record.cached_bones, sizeof( matrix3x4_t ) * count );

}
void c_backtrack::restore_record( c_baseentity* entity, backtrack_records_t record ) {
	entity->get_collision( )->vec_mins( ) = record.obbmin;
	entity->get_collision( )->vec_maxs( ) = record.obbmax;

	entity->set_abs_angles( record.absangles );
	entity->set_abs_origin( record.absorigin );

	entity->invalidate_bone_cache( );

	auto count = *( std::uint32_t* ) ( ( std::uint32_t ) entity->get_renderable( ) + 0x2918 );
	std::memcpy( *( void** ) ( ( std::uint32_t ) entity->get_renderable( ) + 0x290C ), record.cached_bones, sizeof( matrix3x4_t ) * count );
}
void c_backtrack::restore_player( c_baseentity* entity ) {
	auto& record = backup_data [ entity->get_index( ) ];

	entity->get_collision( )->vec_mins( ) = record.obbmin;
	entity->get_collision( )->vec_maxs( ) = record.obbmax;

	entity->set_abs_angles( record.absangles );
	entity->set_abs_origin( record.absorigin );

	entity->invalidate_bone_cache( );

	auto count = *( std::uint32_t* ) ( ( std::uint32_t ) entity->get_renderable( ) + 0x2918 );
	std::memcpy( *( void** ) ( ( std::uint32_t ) entity->get_renderable( ) + 0x290C ), record.cached_bones, sizeof( matrix3x4_t ) * count );
}
void c_backtrack::recieve_record( c_baseentity* entity, backtrack_records_t& record ) {

	static float lby [ 65 ];
	record.simtime = entity->get_simulation_time( );

	record.obbmin = entity->get_collision( )->vec_mins( );
	record.obbmax = entity->get_collision( )->vec_maxs( );

	record.absangles = entity->get_abs_angles( );
	record.absorigin = entity->get_abs_origin( );

	record.resolver = resolver->data [ entity->get_index( ) ];

	auto count = *( std::uint32_t* ) ( ( std::uint32_t ) entity->get_renderable( ) + 0x2918 );
	std::memcpy( &record.cached_bones, *( void** ) ( ( std::uint32_t ) entity->get_renderable( ) + 0x290C ), sizeof( matrix3x4_t ) * count );

	if ( lby [ entity->get_index( ) ] != entity->get_lby( ) || ( entity->get_flags( ) & ( int ) flags_t::fl_onground && entity->get_velocity( ).length_2d( ) > 29.f ) )
	{
		record.lby_update = true;
		lby [ entity->get_index( ) ] = entity->get_lby( );
	}

	static float shot [ 65 ];

	if ( entity->get_active_weapon( ) )
	{
		if ( shot [ entity->get_index( ) ] != entity->get_active_weapon( )->last_shot_time( ) )
		{
			record.shoot = true;
			shot [ entity->get_index( ) ] = entity->get_active_weapon( )->last_shot_time( );
		}
		else
			record.shoot = false;
	}
	else
	{
		record.shoot = false;
		shot [ entity->get_index( ) ] = 0.f;
	}
}