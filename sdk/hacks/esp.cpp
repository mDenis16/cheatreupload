
#include "esp.h"
#include "../options.h"
#include "../sdk/mathematics.h"
#include "../draw_mgr.h"
#include "../sdk/utils.h"
#include "backtrack.h"
#include "ragebot.h"
#include "resolver.h"
#include "event_logger.h"
#include "thirdperson.h"

#include "..\configs.h"
#include "..\config_items.h"
#include "resolver.h"
#include  "esp.h"

std::unique_ptr< c_esp > esp = std::make_unique< c_esp >( );
void c_esp::scope_lines( )
{
	static int height, width;
	if ( height == 0 || width == 0 )
		engine->get_screen_size( width, height );

	vec_t punchAngle = local_player->get_aimpunch_angle( );

	float x = width / 2;
	float y = height / 2;
	int dy = height / 90;
	int dx = width / 90;
	x -= ( dx*( punchAngle.y ) );
	y += ( dy*( punchAngle.x ) );

	vec_t screenPunch = { x, y };

	renderer->line( 0, screenPunch.y, width, screenPunch.y, color( 0, 0, 0, 255 ) );
	renderer->line( screenPunch.x, 0, screenPunch.x, height, color( 0, 0, 0, 255 ) );
}
void c_esp::draw_angles( vec_t angle, color color, const char *input )
{
	vec_t src3D, dst3D, forward, src, dst;
	trace_t tr;
	ray_t ray;
	c_trace_filter filter;

	filter.skip = local_player;

	forward = math->angle_vectors( angle );
	src3D = local_player->get_origin( );
	dst3D = src3D + ( forward * 45.f );

	ray.init( src3D, dst3D );
	engine_trace->trace_ray( ray, 0, &filter, &tr );

	if ( !math->world_to_screen( src3D, src ) || !math->world_to_screen( tr.end_pos, dst ) )
		return;

	renderer->line( src.x, src.y, dst.x, dst.y, color );
	renderer->string( true, renderer->fonts.esp, dst.x, dst.y, color, font_center, input );
}
void c_esp::render( void ) {

	if ( !c_config::get( ).ragebot.enable )
		return;

	auto local_index = local_player->get_index( );

	if ( local_player  && local_player->get_is_scoped( ) )
		scope_lines( );

	if ( local_player && local_player->is_alive( ) )
	{
		draw_angles( vec_t( 0.f, thirdperson->thirdperson_angle.y, 0.f ), color( 255, 0, 0, 255 ), "real" );
		draw_angles( vec_t( 0.f, resolver->data [ local_index ].fl_goalfeetyaw, 0.f ), color( 255, 255, 0, 255 ), "goal feet yaw" );
		draw_angles( vec_t( 0.f, resolver->data [ local_index ].fl_goalfeetyaw_calc, 0.f ), color( 255, 255, 255, 255 ), "self goal feet yaw" );
		draw_angles( vec_t( 0.f, resolver->data [ local_index ].lby, 0.f ), color( 255, 55, 100, 255 ), "lby" );
	}

	if ( !local_player ) {
		for ( int i = 1; i <= globals->max_clients; i++ ) {
			auto entity = c_baseentity::get_client_entity( i );

			if ( !entity )
				continue;

			auto& esp_box = esp_records.at( entity->get_index( ) );
			esp_box.time = globals->curtime - 6.0f;
			esp_box.dormant = true;
		}

		return;
	}

	for ( int i = 1; i <= globals->max_clients; i++ )
	{
		auto entity = c_baseentity::get_client_entity( i );

		if ( entity->is_valid_player( ) && entity->get_active_weapon( ) )
		{
			auto& esp_box = esp_records.at( entity->get_index( ) );

			player_info_t info;
			engine->get_player_info( entity->get_index( ), &info );

			strcpy_s( esp_box.name, info.name );
			esp_box.origin = entity->get_origin( );
			esp_box.min = entity->get_collision( )->vec_mins( );
			esp_box.max = entity->get_collision( )->vec_maxs( );
			esp_box.health = entity->get_health( );
			esp_box.time = globals->curtime;
			if ( entity->get_active_weapon( ) )
				esp_box.weapon_item = ( item_definition_index ) entity->get_active_weapon( )->get_item_definition_index( );
			esp_box.weapon = entity->get_active_weapon( );
			esp_box.dormant = false;

			if ( esp_box.health > 100 )
				esp_box.health = 100;

			vec_t flb, brt, blb, frt, frb, brb, blt, flt;
			float left, top, right, bottom;

			vec_t min = esp_box.min + esp_box.origin;
			vec_t max = esp_box.max + esp_box.origin;

			vec_t points [ ] = { vec_t( min.x, min.y, min.z ),
				vec_t( min.x, max.y, min.z ),
				vec_t( max.x, max.y, min.z ),
				vec_t( max.x, min.y, min.z ),
				vec_t( max.x, max.y, max.z ),
				vec_t( min.x, max.y, max.z ),
				vec_t( min.x, min.y, max.z ),
				vec_t( max.x, min.y, max.z ) };

			if ( !math->world_to_screen( points [ 3 ], flb )
				|| !math->world_to_screen( points [ 5 ], brt )
				|| !math->world_to_screen( points [ 0 ], blb )
				|| !math->world_to_screen( points [ 4 ], frt )
				|| !math->world_to_screen( points [ 2 ], frb )
				|| !math->world_to_screen( points [ 1 ], brb )
				|| !math->world_to_screen( points [ 6 ], blt )
				|| !math->world_to_screen( points [ 7 ], flt ) ) {
				esp_box.time = globals->curtime - 6.0f;
				esp_box.dormant = true;
				continue;
			}

			vec_t arr [ ] = { flb, brt, blb, frt, frb, brb, blt, flt };

			left = flb.x;
			top = flb.y;
			right = flb.x;
			bottom = flb.y;

			for ( int i = 1; i < 8; i++ ) {
				if ( left > arr [ i ].x )
					left = arr [ i ].x;

				if ( bottom < arr [ i ].y )
					bottom = arr [ i ].y;

				if ( right < arr [ i ].x )
					right = arr [ i ].x;

				if ( top > arr [ i ].y )
					top = arr [ i ].y;
			}

			esp_box.top = top;
			esp_box.left = left;
			esp_box.right = right;
			esp_box.bottom = bottom;

			draw_esp_box( entity, esp_box );
		}
	}

}

void c_esp::draw_esp_box( c_baseentity* entity, esp_record_t esp_box ) {
	if ( !local_player )
		return;

	if ( esp_box.dormant && globals->curtime - esp_box.time > 6.0f )
		return;

	auto alpha = 100;
	auto alpha1 = 100;

	if ( esp_box.dormant && globals->curtime - esp_box.time > 5.0f ) {
		alpha = static_cast< int >( ( 1.0f - ( globals->curtime - esp_box.time ) ) * 100.0f + 0.5f );
		alpha1 = static_cast< int >( ( 1.0f - ( globals->curtime - esp_box.time ) ) * 100.0f + 0.5f );
	}

	auto clr = esp_box.dormant?color( 79, 80, 81, 150 ):color( 255, 80, 81, 250 );
	auto w = static_cast< float >( esp_box.right - esp_box.left );
	auto w_addative = w * 0.1f;

	auto left = esp_box.left;
	auto right = esp_box.right;

	left += w_addative;
	right -= w_addative;

	auto w1 = static_cast< float >( right - left );

	if ( c_config::get( ).esp.box ) {
		renderer->bordered_box( left - 1, esp_box.top - 1, ( right - left ) + 2, esp_box.bottom - esp_box.top + 2, color( 0, 0, 0, alpha1 ), 1 );
		renderer->bordered_box( left, esp_box.top, right - left, esp_box.bottom - esp_box.top, clr, 1 );
		renderer->bordered_box( left + 1, esp_box.top + 1, ( right - left ) - 2, esp_box.bottom - esp_box.top - 2, color( 0, 0, 0, alpha1 ), 1 );
	}

	static auto get_health_clr = [ & ] ( int health ) {
		return color( static_cast< int >( 255 - ( health * 2.55f ) ), static_cast< int >( health * 2.55f ), 0, 100 );
	};

	static auto get_health_clr_1 = [ ] ( int health ) {
		auto clr = get_health_clr( health );

		clr.r -= 75;
		clr.g -= 75;
		clr.b -= 75;

		if ( clr.r < 0 )
			clr.r = 0;

		if ( clr.g < 0 )
			clr.g = 0;

		if ( clr.b < 0 )
			clr.b = 0;

		if ( clr.a < 0 )
			clr.a = 0;

		return clr;
	};

	if ( esp_box.health > 100 )
		esp_box.health = 100;


	if ( c_config::get( ).esp.health )
	{
		renderer->filled_box( left - 6, esp_box.bottom, 4, ( esp_box.top - esp_box.bottom ) * ( ( float ) esp_box.health / 100.0f ) - 0.5f, get_health_clr( esp_box.health ) );
		renderer->bordered_box( left - 6, esp_box.bottom, 4, ( esp_box.top - esp_box.bottom ), color( 0, 0, 0, 240 ) );
	}

	if ( c_config::get( ).esp.name )
		renderer->string( true, renderer->fonts.esp, left + ( right - left ) / 2.0f, esp_box.top - 6, clr, font_center, esp_box.name );

	if ( !esp_box.weapon || !esp_box.weapon->get_cs_weapon_data( ) ) return;

	int clip = esp_box.weapon->get_clip_1( );
	int clip_max = esp_box.weapon->get_cs_weapon_data( )->max_clip;

	if ( c_config::get( ).esp.weapon )
	{
		std::string weapon_name = esp_box.weapon->get_cs_weapon_data( )->hud_name;
		weapon_name.erase( 0, 13 );
		std::string final( weapon_name + std::string( " [ " ) + std::to_string( clip ) + std::string( " / " ) + std::to_string( clip_max ) + std::string( " ]" ) );
		std::transform( weapon_name.begin( ), weapon_name.end( ), weapon_name.begin( ), ::toupper );
		renderer->string( true, renderer->fonts.esp, left + ( right - left ) / 2.0f, esp_box.bottom + 13, clr, font_center, final.c_str( ) );
	}
}
void c_esp::capsule_overlay( c_baseentity *entity, float duration, matrix3x4_t pBoneToWorldOut [ 128 ] )
{
	if ( !entity )
		return;

	studiohdr_t* studio_model = model_info->get_studio_model( ( model_t* ) entity->get_model( ) );
	if ( !studio_model )
		return;

	mstudiohitboxset_t* h_set = studio_model->hitbox_set( 0 );
	if ( !h_set )
		return;

	for ( int i = 0; i < h_set->num_hitboxes; i++ )
	{
		mstudiobbox_t* hitbox = h_set->hitbox( i );
		if ( !hitbox )
			continue;

		auto min = math->vector_transform( hitbox->bbmin, pBoneToWorldOut [ hitbox->bone ] );
		auto max = math->vector_transform( hitbox->bbmin, pBoneToWorldOut [ hitbox->bone ] );

		if ( hitbox->radius > -1 )
			debug_overlay->capsule_overlay( min, max, hitbox->radius, 255, 0, 0, 255, duration );

	}
}
void c_esp::run( void* ecx, void* edx, void* context, void* state, const c_model_render_info& info, matrix3x4_t* bone_to_world, void* o_drawmodelexecute ) {
	if ( !info.model )
		return;

	std::string model_name = model_info->get_model_name( info.model );

	if ( model_name.find( "models/player/" ) == std::string::npos )
		return;

	c_baseentity* entity = reinterpret_cast< c_baseentity* >( entity_list->get_client_entity( info.entity_index ) );

	if ( !entity->is_valid_player( ) )
		return;

	if ( c_config::get( ).esp.chams )
	{
		if ( ragebot->best_tick_global [ entity->get_index( ) ].simtime > 0.f )
		{
			renderview->set_blend( 0.5f );
			modelrender->forced_material_override( mat_xqz_flat );
			( ( void( __fastcall* )( void*, void*, void*, void*, const c_model_render_info&, matrix3x4_t* ) ) o_drawmodelexecute )( ecx, edx, context, state, info, ragebot->best_tick_global [ entity->get_index( ) ].cached_bones );
		}
	}
}