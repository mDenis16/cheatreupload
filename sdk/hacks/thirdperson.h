#pragma once
#include "../sdk/sdk.h"

class c_thirdperson {
public:
	vec_t thirdperson_angle = vec_t( );
	vec_t fake_angle = vec_t( );
	vec_t last_real = vec_t( );
	void pre_framestagenotify( client_frame_stage_t stage );
	void framestagenotify( client_frame_stage_t stage );
};

extern std::unique_ptr< c_thirdperson > thirdperson;